#include "Headers/Framework/SampleShapeRepositoryDataModel.h"

#include "Headers/Adapters/IFrontEndAdapter.h"

SampleShapeRepositoryDataModel::SampleShapeRepositoryDataModel()
{
    m_SampleShapeRoleNames[Name] = "Name";
}

void SampleShapeRepositoryDataModel::SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter)
{
    m_adapter = adapter;
}

int SampleShapeRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_SampleShapeList.size());
}

QVariant SampleShapeRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(m_SampleShapeList.empty())
    {
        return QVariant();
    }

    int row = index.row();
    if(row < 0 || row >= m_SampleShapeList.size())
    {
        return QVariant();
    }

    switch(role) {
        case Name:
            auto sampleShapeName = m_SampleShapeList.begin();
            for(auto i = 0; i < row; i++)
            {
                sampleShapeName++;
            }
            return QString(sampleShapeName->c_str());
    }

    return QVariant();
}

void SampleShapeRepositoryDataModel::SetSampleShapeList(std::set<std::string>&& sampleShapeList)
{
    m_SampleShapeList = std::move(sampleShapeList);
    emit endResetModel();
}

void SampleShapeRepositoryDataModel::AddNewShape(const QString &shapeName)
{
    if(shapeName != "")
    {
        m_SampleShapeList.insert(shapeName.toStdString());
        m_adapter->InsertOrUpdateSampleShape(shapeName.toStdString());
        emit endResetModel();
    }
}

void SampleShapeRepositoryDataModel::Delete(const QString &shapeName)
{
    if(shapeName != "")
    {
        auto findValue = m_SampleShapeList.find(shapeName.toStdString());
        if(findValue == m_SampleShapeList.end())
        {
            return;
        }

        m_SampleShapeList.erase(findValue);
        m_adapter->DeleteSampleShape(shapeName.toStdString());
        emit endResetModel();
    }
}

QHash<int, QByteArray> SampleShapeRepositoryDataModel::roleNames() const
{
    return m_SampleShapeRoleNames;
}
