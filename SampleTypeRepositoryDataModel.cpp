#include "Headers/Framework/SampleTypeRepositoryDataModel.h"
#include "Headers/Adapters/IFrontEndAdapter.h"

SampleTypeRepositoryDataModel::SampleTypeRepositoryDataModel()
{
    m_SampleTypeRoleNames[Name] = "Name";
}

void SampleTypeRepositoryDataModel::SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter)
{
    m_adapter = adapter;
}

int SampleTypeRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_SampleTypeList.size());
}

QVariant SampleTypeRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(m_SampleTypeList.empty())
    {
        return QVariant();
    }

    int row = index.row();
    if(row < 0 || row >= m_SampleTypeList.size())
    {
        return QVariant();
    }

    switch(role) {
        case Name:
            auto sampleTypeName = m_SampleTypeList.begin();
            for(auto i = 0; i < row; i++)
            {
                sampleTypeName++;
            }
            return QString(sampleTypeName->c_str());
    }

    return QVariant();
}

void SampleTypeRepositoryDataModel::SetSampleTypeList(std::set<std::string>&& sampleTypeList)
{
    m_SampleTypeList = std::move(sampleTypeList);
    emit endResetModel();
}

void SampleTypeRepositoryDataModel::AddNewType(const QString &typeName)
{
    if(typeName != "")
    {
        m_SampleTypeList.insert(typeName.toStdString());
        m_adapter->InsertOrUpdateSampleType(typeName.toStdString());
        emit endResetModel();
    }
}

void SampleTypeRepositoryDataModel::Delete(const QString &typeName)
{
    if(typeName != "")
    {
        auto findValue = m_SampleTypeList.find(typeName.toStdString());
        if(findValue == m_SampleTypeList.end())
        {
            return;
        }

        m_SampleTypeList.erase(findValue);
        m_adapter->DeleteSampleType(typeName.toStdString());
        emit endResetModel();
    }
}

QHash<int, QByteArray> SampleTypeRepositoryDataModel::roleNames() const
{
    return m_SampleTypeRoleNames;
}
