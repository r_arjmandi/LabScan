#include "Headers/Framework/SqliteDatabaseConnection.h"
#include "Headers/Framework/SqliteDatabaseException.h"
#include "Headers/Adapters/IDatabaseCommand.h"

SqliteDatabaseConnection::SqliteDatabaseConnection(
    const std::string & filePath,
    std::shared_ptr<ISqliteQueryResultFactory> queryResultFactory,
    std::shared_ptr<IDateTimeOperation> dateTimeOps)
    : m_DatabaseFileName(filePath),
    m_Database(nullptr),
    m_QueryResultFactory(queryResultFactory),
    m_DateTimeOps(dateTimeOps)
{
}

SqliteDatabaseConnection::~SqliteDatabaseConnection()
{
    sqlite3_close(m_Database);
}

void SqliteDatabaseConnection::Open()
{
    if (!IsOpen())
    {
        auto resultCode = sqlite3_open(m_DatabaseFileName.c_str(), &m_Database);
        if (resultCode != SQLITE_OK)
            throw SqliteDatabaseException("Error opening database", resultCode);
        auto stmt = PrepareStatement("PRAGMA foreign_keys = ON");
        resultCode = sqlite3_step(stmt);
        if (resultCode != SQLITE_DONE)
            throw SqliteDatabaseException("Error opening database", resultCode);
        resultCode = sqlite3_finalize(stmt);
        if (resultCode != SQLITE_OK)
            throw SqliteDatabaseException("Error opening database", resultCode);
    }
}

bool SqliteDatabaseConnection::IsOpen() const
{
    return m_Database != nullptr;
}

void SqliteDatabaseConnection::Close()
{
    if (IsOpen())
    {
        auto resultCode = sqlite3_close(m_Database);
        if (resultCode != SQLITE_OK)
            throw SqliteDatabaseException("Error closing database", resultCode);
        m_Database = nullptr;
    }
}

class SqliteStmtBindingVisitor : public boost::static_visitor<>
{
public:
    SqliteStmtBindingVisitor(sqlite3_stmt* stmt, int paramIndex, std::shared_ptr<IDateTimeOperation> dateTimeOps)
        : m_Stmt(stmt),
        m_Index(paramIndex),
        m_DateTimeOps(dateTimeOps)
    { }

    void operator()(std::shared_ptr<ByteArray> binaryData) const
    {
        if (!binaryData)
        {
            auto resultCode = sqlite3_bind_null(m_Stmt, m_Index);
            FinalizaAndThrowOnError("Error sqlite3 binding NULL", resultCode);
        }
        else
        {
            auto resultCode = sqlite3_bind_blob64(m_Stmt, m_Index, binaryData->data(), binaryData->size(), SQLITE_STATIC);
            FinalizaAndThrowOnError("Error sqlite3 binding binary data", resultCode);
        }
    }

    void operator()(bool boolValue) const
    {
        auto resultCode = sqlite3_bind_int(m_Stmt, m_Index, boolValue ? 1 : 0);
        FinalizaAndThrowOnError("Error sqlite3 binding boolean as int64", resultCode);
    }

    void operator()(long long int int64Value) const
    {
        auto resultCode = sqlite3_bind_int64(m_Stmt, m_Index, int64Value);
        FinalizaAndThrowOnError("Error sqlite3 binding int64", resultCode);
    }

    void operator()(double realValue) const
    {
        auto resultCode = sqlite3_bind_double(m_Stmt, m_Index, realValue);
        FinalizaAndThrowOnError("Error sqlite3 binding double", resultCode);
    }

    void operator()(const std::string& stringValue) const
    {
        auto resultCode = sqlite3_bind_text(m_Stmt, m_Index, stringValue.c_str(), -1, SQLITE_TRANSIENT);
        FinalizaAndThrowOnError("Error sqlite3 binding text", resultCode);
    }

    void operator()(const DateTime& dateTimeValue) const
    {
        auto encodedValue = m_DateTimeOps->EncodeInt64(dateTimeValue);
        auto resultCode = sqlite3_bind_int64(m_Stmt, m_Index, encodedValue);
        FinalizaAndThrowOnError("Error sqlite3 binding DateTime as int64", resultCode);
    }

private:
    void FinalizaAndThrowOnError(const std::string& errMsg, int resultCode) const
    {
        if (resultCode == SQLITE_OK)
            return;

        sqlite3_finalize(m_Stmt);
        throw SqliteDatabaseException(errMsg.c_str(), resultCode);
    }

    sqlite3_stmt* m_Stmt;
    int m_Index;
    std::shared_ptr<IDateTimeOperation> m_DateTimeOps;
};

sqlite3_stmt* SqliteDatabaseConnection::PrepareStatement(const std::string& queryStr)
{
    sqlite3_stmt *stmt = nullptr;
    auto resultCode = sqlite3_prepare_v2(m_Database, queryStr.c_str(), -1, &stmt, NULL);
    if (resultCode != SQLITE_OK)
        throw SqliteDatabaseException("Error preparing statement", resultCode);
    return stmt;
}

std::shared_ptr<IQueryResult> SqliteDatabaseConnection::Exec(std::shared_ptr<const IDatabaseCommand> dbCommand)
{
    auto stmt = PrepareStatement(dbCommand->GetCommandString());

    auto bindings = dbCommand->GetBindings();
    for (const auto &binding : bindings)
    {
        auto paramName = binding.second.LowLevelParameterName;
        auto paramValue = binding.second.Value;
        auto paramIndex = sqlite3_bind_parameter_index(stmt, paramName.c_str());
        boost::apply_visitor(SqliteStmtBindingVisitor(stmt, paramIndex, m_DateTimeOps), paramValue);
    }

    auto queryResult = m_QueryResultFactory->CreateQueryResult(stmt);
    return queryResult;
}

void SqliteDatabaseConnection::SetFilePath(const std::string& path)
{
    if(IsOpen())
        throw SqliteDatabaseException("Unable to change the database file path. Connection open.");
    m_DatabaseFileName = path;
}
