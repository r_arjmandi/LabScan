#include "Headers/Framework/SqliteMediaQueryBuilder.h"

#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Framework/IDatabaseCommandFactory.h"

SqliteMediaQueryBuilder::SqliteMediaQueryBuilder(
        std::shared_ptr<IDatabaseCommandFactory> commandFactory):
    m_CommandFactory(commandFactory)
{
}

std::shared_ptr<IDatabaseCommand> SqliteMediaQueryBuilder::GetCreateTableIfNotExistsQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "CREATE TABLE IF NOT EXISTS Table_MediaData "
        "(Media TEXT PRIMARY KEY NOT NULL)");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteMediaQueryBuilder::GetSelectAllQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("SELECT Media FROM Table_MediaData");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteMediaQueryBuilder::GetInsertQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "INSERT INTO "
        "Table_MediaData (Media) "
        "VALUES(:Media)");
    dbCommand->AddParameter("Media", ":Media", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteMediaQueryBuilder::GetRowCountQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "SELECT COUNT(Table_MediaData.Media) "
        "FROM Table_MediaData "
        "WHERE :Media = Media");
    dbCommand->AddParameter("Media", ":Media", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteMediaQueryBuilder::GetUpdateQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("UPDATE Table_MediaData SET "
        "Media = :Media "
        "WHERE Media = :Media ");
    dbCommand->AddParameter("Media", ":Media", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteMediaQueryBuilder::GetDeleteQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("DELETE FROM Table_MediaData WHERE Media = :Media");
    dbCommand->AddParameter("Media", ":Media", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}
