#include "Headers/Framework/SqliteSampleTypeQueryBuilder.h"

#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Framework/IDatabaseCommandFactory.h"

SqliteSampleTypeQueryBuilder::SqliteSampleTypeQueryBuilder(
        std::shared_ptr<IDatabaseCommandFactory> commandFactory):
    m_CommandFactory(commandFactory)
{
}

std::shared_ptr<IDatabaseCommand> SqliteSampleTypeQueryBuilder::GetCreateTableIfNotExistsQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "CREATE TABLE IF NOT EXISTS Table_SampleTypeData "
        "(TypeName TEXT PRIMARY KEY NOT NULL)");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleTypeQueryBuilder::GetInsertQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "INSERT INTO "
        "Table_SampleTypeData (TypeName) "
        "VALUES(:TypeName)");
    dbCommand->AddParameter("TypeName", ":TypeName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleTypeQueryBuilder::GetRowCountQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "SELECT COUNT(Table_SampleTypeData.TypeName) "
        "FROM Table_SampleTypeData "
        "WHERE :TypeName = TypeName");
    dbCommand->AddParameter("TypeName", ":TypeName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleTypeQueryBuilder::GetUpdateQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("UPDATE Table_SampleTypeData SET "
        "TypeName = :TypeName "
        "WHERE TypeName = :TypeName ");
    dbCommand->AddParameter("TypeName", ":TypeName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleTypeQueryBuilder::GetSelectAllQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("SELECT TypeName FROM Table_SampleTypeData");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleTypeQueryBuilder::GetDeleteQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("DELETE FROM Table_SampleTypeData WHERE TypeName = :TypeName");
    dbCommand->AddParameter("TypeName", ":TypeName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}
