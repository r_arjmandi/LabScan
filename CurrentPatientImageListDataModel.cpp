#include "Headers/Framework/CurrentPatientImageListDataModel.h"

CurrentPatientImageListDataModel::CurrentPatientImageListDataModel()
{
    m_ImageRoleNames[FileAddress] = "FileAddress";
}

int CurrentPatientImageListDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_ImageFiles.size());
}

QVariant CurrentPatientImageListDataModel::data(const QModelIndex &index, int role) const
{
    if(m_ImageFiles.empty())
    {
        return QVariant();
    }

    int row = index.row();
    if(row < 0 || row >= m_ImageFiles.size())
    {
        return QVariant();
    }

    switch(role) {
        case FileAddress:
    {
        auto lockedImage = m_ImageFiles.at(row).lock();
        if(!lockedImage)
        {
            return QVariant();
        }
        return QUrl::fromLocalFile(QString(lockedImage->string().c_str()));

    }
    }

    return QVariant();
}

void CurrentPatientImageListDataModel::SetImageList(std::vector<std::weak_ptr<std::experimental::filesystem::path>>&& imageList)
{
    m_ImageFiles = imageList;
    emit endResetModel();
}

void CurrentPatientImageListDataModel::ClearImages()
{
    m_ImageFiles.clear();
    emit endResetModel();
}

QHash<int, QByteArray> CurrentPatientImageListDataModel::roleNames() const
{
    return m_ImageRoleNames;
}
