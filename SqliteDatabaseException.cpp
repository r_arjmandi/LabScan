#include "Headers/Framework/SqliteDatabaseException.h"

SqliteDatabaseException::SqliteDatabaseException(const char* message)
	: std::exception(message)
{
}

SqliteDatabaseException::SqliteDatabaseException(const char* message, int resultCode)
	: m_ResultCode(resultCode), 
	std::exception((std::string(message) + "\n" + sqlite3_errstr(resultCode)).c_str())
{
}

int SqliteDatabaseException::ResultCode() const
{
	return m_ResultCode;
}
