#include "Headers/Framework/PatientLogger.h"
#include "Headers/BusinessEntities/IPatientData.h"
#include "Headers/BusinessEntities/CircularTarget.h"

PatientLogger::PatientLogger(const std::string& fileName):
    m_FileName(fileName)
{
}

void PatientLogger::Dump(
        std::weak_ptr<IPatientData> patientData,
        std::weak_ptr<std::vector<CircularTarget>> circularTargets)
{
    auto circluarTargetsLocked = circularTargets.lock();
    auto patientDataLocked = patientData.lock();
    if(!circluarTargetsLocked || !patientDataLocked ||
            circluarTargetsLocked->size() == 0)
    {
        return;
    }
    auto outStream = std::ofstream(m_FileName, std::ios::out | std::ios::trunc);
    if(!outStream.is_open())
    {
        return;
    }
    outStream << "ID : " << patientDataLocked->GetId() << std::endl;
    outStream << "Name : " << patientDataLocked->GetName() << std::endl;
    outStream << "FamilyName : " << patientDataLocked->GetFamilyName() << std::endl;
    outStream << "Age : " << patientDataLocked->GetAge() << std::endl;
    auto gender = patientDataLocked->GetGender() == GenderType::Male ? "Male" : "Female";
    outStream << "Gender : " <<  gender << std::endl;

    int count = 0;
    for(const auto& circleTarget : *circluarTargetsLocked)
    {
        if(circleTarget.Report)
        {
            count++;
        }
    }
    outStream << "Count : " <<  count << std::endl;
    int counter = 1;
    for(const auto& circleTarget : *circluarTargetsLocked)
    {
        if(!circleTarget.Report)
        {
            continue;
        }

        std::string status;

        switch (circleTarget.SelectedAntibiotic.Status)
        {
        case(AntibuticStatus::S):
            status ="Susceptible";
            break;
        case(AntibuticStatus::I):
            status = "Intermediate";
            break;
        case(AntibuticStatus::R):
            status = "Resistance";
            break;
        }
        outStream << counter++ << " - " << circleTarget.SelectedAntibiotic.Name <<
                     "\t" << status << std::endl;
    }
}
