#ifndef IOPERATORDATAACCESS_H
#define IOPERATORDATAACCESS_H

class IOperatorDataAccess{
public:
    virtual ~IOperatorDataAccess() = default;
    virtual void InsertOrUpdate(const std::string& operatorName) = 0;
    virtual void Delete(const std::string& operatorName) = 0;
    virtual std::set<std::string> GetAllOperators() = 0;
};
#endif // IOPERATORDATAACCESS_H
