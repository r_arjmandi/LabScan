#ifndef ICOMMENTDATAACCESS_H
#define ICOMMENTDATAACCESS_H

struct CommentData;

class ICommentDataAccess{
public:
    virtual ~ICommentDataAccess() = default;
    virtual void InsertOrUpdate(std::unique_ptr<CommentData> comment) = 0;
    virtual void Delete(const std::string& subject) = 0;
    virtual std::map<std::string, std::string> GetAllComments() = 0;

};

#endif // ICOMMENTDATAACCESS_H
