#ifndef REGIONGROWING_H
#define REGIONGROWING_H

#include "IRegionGrowing.h"

class RegionGrowing : public IRegionGrowing
{
public:
    std::vector<CircularTarget> TryDetectRegions(
            const std::experimental::filesystem::path &imageAddress,
            std::vector<Circle> &circles) const override;
};

#endif // REGIONGROWING_H
