#ifndef HEADERSBUSINESSRULESUSINESSRULES_H
#define HEADERSBUSINESSRULESUSINESSRULES_H

#include "IBusinessRules.h"

class IBackEndAdapter;
class ICircleDetector;
class ICircularSorter;
class IImageListGui;
class ICircleRepository;
class IPatientShortInformationRepository;
class IOperatorRepository;
class ISampleTypeRepository;
class ISampleShapeRepository;
class IGeneralCommentRepository;
class IMediaRepository;
class IPatientDataAccess;
class IImageDataAccess;
class IOperatorDataAccess;
class ISampleTypeDataAccess;
class ISampleShapeDataAccess;
class ICommentDataAccess;
class IMediaDataAccess;
class IPatientListSearchEngine;
class IDateTimeOperation;
class ICalibratorDataAccess;
class ICalibratorRepository;
class IPatientLogger;
class IRegionGrowing;
class IExcelFilePrinter;

class BusinessRules : public IBusinessRules
{
public:
    BusinessRules(
            std::shared_ptr<IBackEndAdapter> backEndAdapter,
            std::shared_ptr<ICircleDetector> circleDetector,
            std::shared_ptr<ICircularSorter> circularSorter,
            std::shared_ptr<IPatientDataAccess> patientDataAccess,
            std::shared_ptr<IImageDataAccess> imageDataAccess,
            std::shared_ptr<IOperatorDataAccess> operatorDataAccess,
            std::shared_ptr<ISampleTypeDataAccess> sampleTypeDataAccess,
            std::shared_ptr<ISampleShapeDataAccess> sampleShapeDataAccess,
            std::shared_ptr<ICommentDataAccess> commentDataAccess,
            std::shared_ptr<IMediaDataAccess> mediaDataAccess,
            std::shared_ptr<IPatientShortInformationRepository> patientInfoList,
            std::shared_ptr<ICircleRepository> circleRepo,
            std::shared_ptr<IOperatorRepository> operatorRepo,
            std::shared_ptr<ISampleTypeRepository> sampleTypeRepo,
            std::shared_ptr<ISampleShapeRepository> sampleShapeRepo,
            std::shared_ptr<IGeneralCommentRepository> commentRepo,
            std::shared_ptr<IMediaRepository> mediaRepo,
            std::shared_ptr<IPatientListSearchEngine> patientListSearchEngine,
            std::shared_ptr<IDateTimeOperation> dateTimeOperation,
            std::shared_ptr<ICalibratorDataAccess> calibratorDataAccess,
            std::shared_ptr<ICalibratorRepository> calibratorRepo,
            std::shared_ptr<IPatientLogger> patientLogger,
            std::shared_ptr<IRegionGrowing> regionGrowing,
            std::shared_ptr<IExcelFilePrinter> excelFilePrinter);

    void InsertOrUpdateCurrentPatient() override;
    void DetectImageCircles(double diameter, int count) override;
    void InsertOrUpdateOperator(const std::string &operatorName) override;
    void DeleteOperator(const std::string &operatorName) override;
    void InsertOrUpdateSampleType(const std::string &sampleType) override;
    void DeleteSampleType(const std::string &sampleType) override;
    void InsertOrUpdateSampleShape(const std::string &sampleShape) override;
    void DeleteSampleShape(const std::string &sampleShape) override;
    void InsertOrUpdateComment(std::unique_ptr<CommentData> comment) override;
    void DeleteComment(const std::string &subject) override;
    void InsertOrUpdateMedia(const std::string &media) override;
    void DeleteMedia(const std::string &media) override;
    void ShuffleImageCircles() override;
    void SortCurrentImageCirclesCircular(const CircularSortType &sortType) override;
    void LoadPatient(const std::string &patientUid) override;
    void FilterPatientList(std::unique_ptr<PatientListFilter> patientListFilter) const override;
    std::weak_ptr<IPatientData> GetCurrentPatientData() const override;
    void InsertOrUpdateCalibrator(const std::pair<std::string, double> &&calibrator) override;
    void DeleteCalibrator(const std::string &calibratorName) override;
    void PrintCurrentPatient() override;

private:
    void LoadAllPatientsInfo();
    void LoadAllOperators();
    void LoadAllSampleTypes();
    void LoadAllSampleShapes();
    void LoadAllComments();
    void LoadAllMedia();
    void LoadAllCalibrator();

    std::shared_ptr<IBackEndAdapter> m_BackEndAdapter;
    std::shared_ptr<ICircleDetector> m_CircleDetector;

    std::shared_ptr<ICircularSorter> m_CircularSorter;

    std::shared_ptr<IPatientDataAccess> m_PatientDataAccess;
    std::shared_ptr<IImageDataAccess> m_ImageDataAccess;
    std::shared_ptr<IOperatorDataAccess> m_OperatorDataAccess;
    std::shared_ptr<ISampleTypeDataAccess> m_SampleTypeDataAccess;
    std::shared_ptr<ISampleShapeDataAccess> m_SampleShapeDataAccess;
    std::shared_ptr<ICommentDataAccess> m_CommentDataAccess;
    std::shared_ptr<IMediaDataAccess> m_MediaDataAccess;
    std::shared_ptr<ICalibratorDataAccess> m_CalibratorDataAccess;

    std::shared_ptr<ICircleRepository> m_CircleRepo;
    std::shared_ptr<IPatientShortInformationRepository> m_PatientInfoList;
    std::shared_ptr<IOperatorRepository> m_OperatorRepo;
    std::shared_ptr<ISampleTypeRepository> m_SampleTypeRepo;
    std::shared_ptr<ISampleShapeRepository> m_SampleShapeRepo;
    std::shared_ptr<IGeneralCommentRepository> m_CommentRepo;
    std::shared_ptr<IMediaRepository> m_MediaRepo;
    std::shared_ptr<ICalibratorRepository> m_CalibratorRepo;

    std::shared_ptr<IPatientListSearchEngine> m_PatientListSearchEngine;

    std::shared_ptr<IPatientData> m_CurrentPatientData;
    std::shared_ptr<IDateTimeOperation> m_DateTimeOperation;

    std::shared_ptr<IPatientLogger> m_PatientLogger;
    std::shared_ptr<IRegionGrowing> m_RegionGrowing;
    std::shared_ptr<IExcelFilePrinter> m_ExcelFilePrinter;

};

#endif // HEADERSBUSINESSRULESUSINESSRULES_H
