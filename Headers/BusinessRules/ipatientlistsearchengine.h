#ifndef IPATIENTLISTSEARCHENGINE_H
#define IPATIENTLISTSEARCHENGINE_H

struct PatientListFilter;
struct PatientShortInformation;

class IPatientListSearchEngine
{
public:
    virtual std::vector<std::weak_ptr<PatientShortInformation>> Search(
            std::vector<std::weak_ptr<PatientShortInformation>>&& patientInfoList,
            std::unique_ptr<PatientListFilter> patientListFilter) const = 0;
};

#endif // IPATIENTLISTSEARCHENGINE_H
