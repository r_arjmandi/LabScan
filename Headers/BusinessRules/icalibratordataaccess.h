#ifndef ICALIBRATORDATAACCESS_H
#define ICALIBRATORDATAACCESS_H

class ICalibratorDataAccess
{
    public:
        ~ICalibratorDataAccess() = default;
        virtual void InsertOrUpdate(const std::pair<std::string, double> &&calibrator) = 0;
        virtual void Delete(const std::string& calibrator) = 0;
        virtual std::vector<std::pair<std::string, double>> GetAllCalibrator() = 0;
};

#endif // ICALIBRATORDATAACCESS_H
