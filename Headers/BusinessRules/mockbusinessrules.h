#ifndef MOCKBUSINESSRULES_H
#define MOCKBUSINESSRULES_H

#include "IBusinessRules.h"

MOCK_BASE_CLASS(MockBusinessRules, IBusinessRules)
{
    MOCK_NON_CONST_METHOD(AddNewPatient, 1, void(std::shared_ptr<PatientData> patientData))
    MOCK_NON_CONST_METHOD(LoadAllPatients, 0, void())
    MOCK_NON_CONST_METHOD(DetectImageCircles, 2, void(double minRadius, double maxRadius))
};

#endif // MOCKBUSINESSRULES_H
