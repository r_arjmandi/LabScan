#ifndef PATIENTLISTSEARCHENGINE_H
#define PATIENTLISTSEARCHENGINE_H

#include "IPatientListSearchEngine.h"

class IDateTimeOperation;

class PatientListSearchEngine : public IPatientListSearchEngine
{
public:
    PatientListSearchEngine(std::shared_ptr<IDateTimeOperation> dateTimeOperation);
    std::vector<std::weak_ptr<PatientShortInformation> > Search(
            std::vector<std::weak_ptr<PatientShortInformation>>&& patientInfoList,
            std::unique_ptr<PatientListFilter> patientListFilter) const override;

private:
    std::shared_ptr<IDateTimeOperation> m_DateTimeOperation;

};

#endif // PATIENTLISTSEARCHENGINE_H
