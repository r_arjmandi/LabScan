#ifndef IBACKENDADAPTER_H
#define IBACKENDADAPTER_H

struct PatientShortInformation;
class IPatientData;

class IBackEndAdapter{
public:
    virtual ~IBackEndAdapter() = default;
    virtual void UpdatePatientListRepresentation(std::vector<std::weak_ptr<PatientShortInformation>>&&
                                                 patientInfoList) = 0;
    virtual void UpdateCurrentPatientRepresentation(std::weak_ptr<IPatientData> currentPatientData) = 0;
};

#endif // IBACKENDADAPTER_H
