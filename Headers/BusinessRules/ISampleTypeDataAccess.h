#ifndef ISAMPLETYPEDATAACCESS_H
#define ISAMPLETYPEDATAACCESS_H

class ISampleTypeDataAccess{
public:
    virtual ~ISampleTypeDataAccess() = default;
    virtual void InsertOrUpdate(const std::string& typeName) = 0;
    virtual void Delete(const std::string& typeName) = 0;
    virtual std::set<std::string> GetAllTypes() = 0;
};

#endif // ISAMPLETYPEDATAACCESS_H
