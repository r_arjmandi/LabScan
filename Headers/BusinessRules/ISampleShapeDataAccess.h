#ifndef ISAMPLESHAPEDATAACCESS_H
#define ISAMPLESHAPEDATAACCESS_H

class ISampleShapeDataAccess{

public:
    virtual ~ISampleShapeDataAccess() = default;
    virtual void InsertOrUpdate(const std::string& shapeName) = 0;
    virtual void Delete(const std::string& shapeName) = 0;
    virtual std::set<std::string> GetAllShapes() = 0;

};

#endif // ISAMPLESHAPEDATAACCESS_H
