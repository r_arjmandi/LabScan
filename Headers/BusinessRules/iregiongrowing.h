#ifndef IREGIONGROWING_H
#define IREGIONGROWING_H

struct Circle;
struct CircularTarget;

class IRegionGrowing
{
public:
    virtual std::vector<CircularTarget> TryDetectRegions(
            const std::experimental::filesystem::path& imageAddress,
            std::vector<Circle>& circles) const = 0;
};

#endif // IREGIONGROWING_H
