#ifndef IIMAGEDATAACCESS_H
#define IIMAGEDATAACCESS_H

class IImageDataAccess
{
public:
    virtual void InsertOrUpdate(
            const std::string& patientId,
            std::vector<std::weak_ptr<std::experimental::filesystem::path>>&& images) = 0;
    virtual std::vector<std::unique_ptr<std::experimental::filesystem::path>> GetAll(
            const std::string& patientId) = 0;
};

#endif // IIMAGEDATAACCESS_H
