#ifndef IEXCELFILEPRINTER_H
#define IEXCELFILEPRINTER_H

class IExcelFilePrinter
{
public:
    virtual void Print() = 0;
};

#endif // IEXCELFILEPRINTER_H
