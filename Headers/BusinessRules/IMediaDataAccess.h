#ifndef IMEDIADATAACCESS_H
#define IMEDIADATAACCESS_H

class IMediaDataAccess
{
public:
    ~IMediaDataAccess() = default;
    virtual void InsertOrUpdate(const std::string& media) = 0;
    virtual void Delete(const std::string& media) = 0;
    virtual std::set<std::string> GetAllMedia() = 0;
};

#endif // IMEDIADATAACCESS_H
