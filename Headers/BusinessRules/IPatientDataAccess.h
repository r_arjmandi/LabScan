#ifndef IPATIENTDATAACCESS_H
#define IPATIENTDATAACCESS_H

class IPatientData;
struct PatientShortInformation;

class IPatientDataAccess{
public:
    virtual ~IPatientDataAccess() = default;
    virtual void InsertOrUpdate(std::weak_ptr<IPatientData> patientData) = 0;
    virtual std::vector<std::unique_ptr<PatientShortInformation>> GetAll() = 0;
    virtual std::unique_ptr<IPatientData> Get(const std::string& patientUid) = 0;
};

#endif // IPATIENTDATAACCESS_H
