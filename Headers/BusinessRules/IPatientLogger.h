#ifndef IPATIENTLOGGER_H
#define IPATIENTLOGGER_H

class IPatientData;
struct CircularTarget;

class IPatientLogger
{
public:
    virtual void Dump(
            std::weak_ptr<IPatientData> patientData,
            std::weak_ptr<std::vector<CircularTarget>> circularTargets) = 0;
};

#endif // IPATIENTLOGGER_H
