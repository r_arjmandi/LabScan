#ifndef SQLITECALIBRATORQUERYBUILDER_H
#define SQLITECALIBRATORQUERYBUILDER_H

#include "Headers/Adapters/ICalibratorQueryBuilder.h"

class IDatabaseCommandFactory;

class SqliteCalibratorQueryBuilder : public ICalibratorQueryBuilder
{
public:
    SqliteCalibratorQueryBuilder(std::shared_ptr<IDatabaseCommandFactory> commandFactory);

    std::shared_ptr<IDatabaseCommand> GetCreateTableIfNotExistsQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetSelectAllQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetInsertQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetRowCountQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetUpdateQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetDeleteQuery() const override;

private:
        std::shared_ptr<IDatabaseCommandFactory> m_CommandFactory;
};

#endif // SQLITECALIBRATORQUERYBUILDER_H
