#ifndef HEADERSFRAMEWORKCALIBRATORREPOSITORYDATAMODEL_H
#define HEADERSFRAMEWORKCALIBRATORREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/ICalibratorRepository.h"

class IFrontEndAdapter;

class CalibratorRepositoryDataModel : public QAbstractListModel, public ICalibratorRepository
{
    Q_OBJECT

public:
    enum CalibratorRoleNames{
        Name = Qt::UserRole,
        Coefficient = Qt::UserRole + 2
    };

    CalibratorRepositoryDataModel();
    void SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter);
    Q_INVOKABLE void AddCalibrator(const QString& name, double coefficient);
    Q_INVOKABLE void SetCurrentCalibrator(int index);
    Q_INVOKABLE void DeleteCurrentCalibrator();
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    double GetCurrentCoefficient() const override;
    void SetCalibratorList(std::vector<std::pair<std::string, double> > &&calibratorList) override;

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::shared_ptr<IFrontEndAdapter> m_adapter;
    QHash<int, QByteArray> m_CalibratorRoleNames;
    std::vector<std::pair<std::string, double>> m_Calibrators;
    int m_CurrentCalibratorIndex = -1;

};

#endif // HEADERSFRAMEWORKCALIBRATORREPOSITORYDATAMODEL_H
