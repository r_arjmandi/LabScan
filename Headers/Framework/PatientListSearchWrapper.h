#ifndef PATIENTLISTSEARCHWRAPPER_H
#define PATIENTLISTSEARCHWRAPPER_H

class IFrontEndAdapter;
class IDateTimeOperation;

class PatientListSearchWrapper : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString familyName READ familyName WRITE setFamilyName NOTIFY familyNameChanged)
    Q_PROPERTY(QString fromDate READ fromDate WRITE setFromDate NOTIFY fromDateChanged)
    Q_PROPERTY(QString toDate READ toDate WRITE setToDate NOTIFY toDateChanged)

public:

    PatientListSearchWrapper(
            std::shared_ptr<IFrontEndAdapter> frontEndAdapter,
            std::shared_ptr<IDateTimeOperation> dateTimeOperation);

    //getters:
    QString id() const;
    QString name() const;
    QString familyName() const;
    QString fromDate() const;
    QString toDate() const;

    //setters:
    void setId(const QString& id);
    void setName(const QString& name);
    void setFamilyName(const QString& familyName);
    void setFromDate(const QString& fromDate);
    void setToDate(const QString& toDate);

signals:
    void idChanged();
    void nameChanged();
    void familyNameChanged();
    void fromDateChanged();
    void toDateChanged();

private:
    void FilterPatientList() const;

    std::shared_ptr<IFrontEndAdapter> m_FrontEndAdapter;
    std::shared_ptr<IDateTimeOperation> m_DateTimeOperation;

    QString m_Id;
    QString m_Name;
    QString m_FamilyName;
    QString m_FromDate;
    QString m_ToDate;
};

#endif // PATIENTLISTSEARCHWRAPPER_H
