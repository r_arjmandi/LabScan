#ifndef SAMPLESHAPEREPOSITORYDATAMODEL_H
#define SAMPLESHAPEREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/ISampleShapeRepository.h"

class IFrontEndAdapter;

class SampleShapeRepositoryDataModel : public QAbstractListModel, public ISampleShapeRepository
{
    Q_OBJECT

public:
    enum SampleShapeRoleNames{
        Name = Qt::UserRole
    };

    SampleShapeRepositoryDataModel();
    void SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter);
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void SetSampleShapeList(std::set<std::string>&& sampleShapeList) override;
    Q_INVOKABLE void AddNewShape(const QString& shapeName);
    Q_INVOKABLE void Delete(const QString& shapeName);

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::shared_ptr<IFrontEndAdapter> m_adapter;
    std::set<std::string> m_SampleShapeList;
    QHash<int, QByteArray> m_SampleShapeRoleNames;

};

#endif // SAMPLESHAPEREPOSITORYDATAMODEL_H
