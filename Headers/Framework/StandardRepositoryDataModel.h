#ifndef HEADERSBUSINESSENTITIESSTANDARDREPOSITORY_H
#define HEADERSBUSINESSENTITIESSTANDARDREPOSITORY_H

#include "Headers/BusinessEntities/IStandardRepository.h"

class IBacteriaRepository;
class IAntibuticRepository;

class StandardRepositoryDataModel : public QAbstractListModel, public IStandardRepository
{
    Q_OBJECT

public:
    enum StandardRoleNames{
        FileName = Qt::UserRole,
    };

    StandardRepositoryDataModel(
            const std::experimental::filesystem::path& standarDirectoryAddress,
            std::shared_ptr<IBacteriaRepository> bacteriaRepo,
            std::shared_ptr<IAntibuticRepository> antibuticRepo);

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;

    Q_INVOKABLE void SetCurrentBacteria(const QString& bacteria);
    Q_INVOKABLE void SetCurrentStandard(int stdIndex);

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    void UpdateBacteriaRepo();

    std::shared_ptr<IBacteriaRepository> m_BacteriaRepo;
    std::shared_ptr<IAntibuticRepository> m_AntibuticRepo;
    std::experimental::filesystem::path m_StandardDirectoryAddress;
    std::vector<std::string> m_StandardFiles;
    QHash<int, QByteArray> m_StandardRoleNames;
    std::string m_CurrentStandardFile;

};

#endif // HEADERSBUSINESSENTITIESSTANDARDREPOSITORY_H
