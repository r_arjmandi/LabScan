#ifndef ANTIBUTICREPOSITORYDATAMODEL_H
#define ANTIBUTICREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/IAntibuticRepository.h"

class AntibuticRepositoryDataModel : public QAbstractListModel, public IAntibuticRepository
{
    Q_OBJECT

public:
    enum AntibuticRoleNames{
        Name = Qt::UserRole
    };

    AntibuticRepositoryDataModel();
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void SetAntibuticList(const std::vector<Antibiotic>&& antibioticList) override;
    Q_INVOKABLE QString GetData(int index);
    Antibiotic GetAntibioticByIndex(int index) const override;

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::vector<Antibiotic> m_AntibuticList;
    QHash<int, QByteArray> m_AntibuticRoleNames;

};

#endif // ANTIBUTICREPOSITORYDATAMODEL_H
