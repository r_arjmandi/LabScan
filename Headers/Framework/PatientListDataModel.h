#ifndef HEADERSFRAMEWORKPATIENTREPOSITORYDATAMODEL_H
#define HEADERSFRAMEWORKPATIENTREPOSITORYDATAMODEL_H

#include "Headers/Adapters/IPatientListGui.h"

class IDateTimeOperation;

class PatientListDataModel : public QAbstractListModel, public IPatientListGui
{
    Q_OBJECT

public:
    PatientListDataModel(std::shared_ptr<IDateTimeOperation> dateTimeOperation);

    enum PatientRoleNames{
        ID = Qt::UserRole,
        Name = Qt::UserRole + 2,
        FamilyName = Qt::UserRole + 3,
        ModifyDateTime = Qt::UserRole + 4
    };

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual void SetPatientDataList(std::vector<std::weak_ptr<PatientShortInformation>>&&
                                    patientInfoList) override;

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:

    std::shared_ptr<IDateTimeOperation> m_DateTimeOperation;

    std::vector<std::weak_ptr<PatientShortInformation>> m_PatientInfoList;
    QHash<int, QByteArray> m_PatientRoleNames;
};

#endif // HEADERSFRAMEWORKPATIENTREPOSITORYDATAMODEL_H
