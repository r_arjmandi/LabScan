#ifndef OPERATORREPOSITORYDATAMODEL_H
#define OPERATORREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/IOperatorRepository.h"

class IFrontEndAdapter;

class OperatorRepositoryDataModel : public QAbstractListModel, public IOperatorRepository
{
    Q_OBJECT

public:
    enum OperatorRoleNames{
        Name = Qt::UserRole
    };

    OperatorRepositoryDataModel();
    void SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter);
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void SetOperatorList(std::set<std::string>&& operatorList) override;
    Q_INVOKABLE void AddNewOperator(const QString& operatorName);
    Q_INVOKABLE void Delete(const QString& operatorName);

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::shared_ptr<IFrontEndAdapter> m_adapter;
    std::set<std::string> m_OperatorList;
    QHash<int, QByteArray> m_OperatorRoleNames;
};

#endif // OPERATORREPOSITORYDATAMODEL_H
