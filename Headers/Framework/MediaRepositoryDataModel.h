#ifndef MEDIAREPOSITORYDATAMODEL_H
#define MEDIAREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/IMediaRepository.h"

class IFrontEndAdapter;

class MediaRepositoryDataModel : public QAbstractListModel, public IMediaRepository
{
    Q_OBJECT

public:
    enum MediaRoleNames{
        Name = Qt::UserRole
    };

    MediaRepositoryDataModel();
    void SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter);
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void SetMediaList(std::set<std::string>&& mediaList) override;
    Q_INVOKABLE void AddNewMedia(const QString& media);
    Q_INVOKABLE void Delete(const QString& media);

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::shared_ptr<IFrontEndAdapter> m_adapter;
    std::set<std::string> m_MediaList;
    QHash<int, QByteArray> m_MediaRoleNames;
};

#endif // MEDIAREPOSITORYDATAMODEL_H
