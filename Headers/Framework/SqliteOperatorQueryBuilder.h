#ifndef SQLITEOPERATORQUERYBUILDER_H
#define SQLITEOPERATORQUERYBUILDER_H

#include "Headers/Adapters/IOperatorQueryBuilder.h"

class IDatabaseCommandFactory;

class SqliteOperatorQueryBuilder : public IOperatorQueryBuilder
{
public:
    SqliteOperatorQueryBuilder(std::shared_ptr<IDatabaseCommandFactory> commandFactory);

    std::shared_ptr<IDatabaseCommand> GetCreateTableIfNotExistsQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetInsertQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetRowCountQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetUpdateQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetSelectAllQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetDeleteQuery() const override;

private:
        std::shared_ptr<IDatabaseCommandFactory> m_CommandFactory;

};

#endif // SQLITEOPERATORQUERYBUILDER_H
