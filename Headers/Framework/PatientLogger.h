#ifndef PATIENTLOGGER_H
#define PATIENTLOGGER_H

#include "Headers/BusinessRules/IPatientLogger.h"

class PatientLogger : public IPatientLogger
{
public:
    PatientLogger(const std::string& fileName);
    void Dump(
            std::weak_ptr<IPatientData> patientData,
            std::weak_ptr<std::vector<CircularTarget>> circularTargets) override;

private:
    const std::string m_FileName;

};

#endif // PATIENTLOGGER_H
