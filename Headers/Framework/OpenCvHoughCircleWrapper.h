#ifndef OPENCVHOUGHCIRCLEWRAPPER_H
#define OPENCVHOUGHCIRCLEWRAPPER_H

#include "Headers/BusinessEntities/ICircleDetector.h"

class OpenCvHoughCircleWrapper : public ICircleDetector
{
public:
    std::vector<Circle> DetectCircles(
            const std::experimental::filesystem::path& imageAddress,
            double minRadius, double maxRadius, int count) override;
};

#endif // OPENCVHOUGHCIRCLEWRAPPER_H
