#pragma once

#include "Headers/Adapters/IDatabaseCommand.h"

class IDatabaseCommandFactory
{
public:
        virtual std::shared_ptr<IDatabaseCommand> CreateCommand() = 0;
        virtual ~IDatabaseCommandFactory() { }
};


