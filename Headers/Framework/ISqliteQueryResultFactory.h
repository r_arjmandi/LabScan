#pragma once

#include "../Adapters/IQueryResult.h"

class ISqliteQueryResultFactory
{
public:
        virtual std::shared_ptr<IQueryResult> CreateQueryResult(sqlite3_stmt *stmt) = 0;

        virtual ~ISqliteQueryResultFactory() { }
};
