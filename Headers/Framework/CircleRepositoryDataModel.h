#ifndef CIRCLEREPOSITORYDATAMODEL_H
#define CIRCLEREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/ICircleRepository.h"

class IAntibuticRepository;
class ICalibratorRepository;

class CircleRepositoryDataModel : public QAbstractListModel, public ICircleRepository
{
    Q_OBJECT

public:

    enum CircleRoleNames{
        Radius = Qt::UserRole,
        MinMax = Qt::UserRole + 2,
        SIR = Qt::UserRole + 3,
        Report = Qt::UserRole + 4
    };

    CircleRepositoryDataModel(
            std::shared_ptr<IAntibuticRepository> antibuticRepo,
            std::shared_ptr<ICalibratorRepository> calibratorRepo);

    void AddCircularTargets(const std::experimental::filesystem::path& imageAddress,
                    std::vector<CircularTarget>&& circles) override;
    std::weak_ptr<std::vector<CircularTarget>> GetCircularTargets(
            const std::experimental::filesystem::path &imageAddress) override;
    Q_INVOKABLE int Size(const QUrl& url) const ;
    Q_INVOKABLE double GetData(int count, int circleType, int role) const ;
    Q_INVOKABLE void SetRegionRadius(int count, double radius);
    Q_INVOKABLE void TranslateCircularTarget(int count, int x, int y);
    Q_INVOKABLE void SetCurrentImage(const QUrl& url);
    Q_INVOKABLE void SetAntibutic(int circleIndex, int antibioticIndex);
    Q_INVOKABLE void SetReport(int circleIndex, bool report);
    Q_INVOKABLE QString GetAntibutic(int circleIndex);
    Q_INVOKABLE void ChangeCalibrator();

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void Update() override;

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

signals:
    void updateRepository();

private:
    enum CircleType{
        Disk,
        Region
    };

    std::map<std::experimental::filesystem::path,
        std::shared_ptr<std::vector<CircularTarget>>> m_CirclesMap;
    std::shared_ptr<IAntibuticRepository> m_AntibuticRepository;
    std::shared_ptr<ICalibratorRepository> m_CalibratorRepo;
    QHash<int, QByteArray> m_CircleRoleNames;
    std::experimental::filesystem::path m_CurrentImage;

};

#endif // CIRCLEREPOSITORYDATAMODEL_H
