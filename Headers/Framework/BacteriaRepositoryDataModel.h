#ifndef HEADERSFRAMEWORKSTANDARDREPOSITORYDATAMODEL_H
#define HEADERSFRAMEWORKSTANDARDREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/IBacteriaRepository.h"

class BacteriaRepositoryDataModel : public QAbstractListModel, public IBacteriaRepository
{
    Q_OBJECT

public:
    BacteriaRepositoryDataModel();
    enum BacteriaRoleNames{
        Name = Qt::UserRole,
    };
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void SetBacteriaList(std::shared_ptr<std::vector<std::string> > bacteriaList) override;
    std::shared_ptr<std::vector<std::string>> GetBacteriaList() const override;

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::shared_ptr<std::vector<std::string>> m_BacteriaList;
    QHash<int, QByteArray> m_BacteriaRoleNames;

};

#endif // HEADERSFRAMEWORKSTANDARDREPOSITORYDATAMODEL_H
