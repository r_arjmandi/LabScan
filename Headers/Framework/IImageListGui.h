#ifndef IIMAGEREPOSITORY_H
#define IIMAGEREPOSITORY_H

class IImageListGui{
public:
    virtual void SetImageList(std::vector<std::weak_ptr<std::experimental::filesystem::path>>&& imageList) = 0;
    virtual void ClearImages() = 0;
};

#endif // IIMAGEREPOSITORY_H
