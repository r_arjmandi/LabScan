#ifndef GUIPATIENTTABWRAPPER_H
#define GUIPATIENTTABWRAPPER_H

class IFrontEndAdapter;
class IBusinessRules;
class ICurrentPatientGui;
class IImageListGui;

class BackEndWrapper : public QObject
{
    Q_OBJECT

public:
    BackEndWrapper(
            std::shared_ptr<IFrontEndAdapter> frontEndAdapter,
            std::shared_ptr<ICurrentPatientGui> currentPatient);
    Q_INVOKABLE void AnalyseImage(double diameter, int count);
    Q_INVOKABLE void ShuffleImageCircles();
    Q_INVOKABLE void SortCurrentImageCirclesClockwise();
    Q_INVOKABLE void SortCurrentImageCirclesCenterClockwise();
    Q_INVOKABLE void SortCurrentImageCirclesCounterClockwise();
    Q_INVOKABLE void SortCurrentImageCirclesCenterCounterClockwise();
    Q_INVOKABLE void SaveCurrentPatient();
    Q_INVOKABLE void LoadPatient(const QString& patientUid);
    Q_INVOKABLE void PrintCurrentPatient();

private:

    std::shared_ptr<IFrontEndAdapter> m_Adapter;
    std::shared_ptr<ICurrentPatientGui> m_CurrentPatient;
};

#endif // GUIPATIENTTABWRAPPER_H
