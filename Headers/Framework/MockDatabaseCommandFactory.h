#include "IDatabaseCommandFactory.h"

MOCK_BASE_CLASS(MockDatabaseCommandFactory, IDatabaseCommandFactory)
{
        MOCK_NON_CONST_METHOD(CreateCommand, 0, std::shared_ptr<IDatabaseCommand>());
};
