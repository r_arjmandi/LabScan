#ifndef SAMPLETYPEREPOSITORYDATAMODEL_H
#define SAMPLETYPEREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/ISampleTypeRepository.h"

class IFrontEndAdapter;

class SampleTypeRepositoryDataModel : public QAbstractListModel, public ISampleTypeRepository
{
    Q_OBJECT

public:
    enum SampleTypeRoleNames{
        Name = Qt::UserRole
    };

    SampleTypeRepositoryDataModel();
    void SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter);
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void SetSampleTypeList(std::set<std::string>&& sampleTypeList) override;
    Q_INVOKABLE void AddNewType(const QString& typeName);
    Q_INVOKABLE void Delete(const QString& typeName);

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::shared_ptr<IFrontEndAdapter> m_adapter;
    std::set<std::string> m_SampleTypeList;
    QHash<int, QByteArray> m_SampleTypeRoleNames;
};

#endif // SAMPLETYPEREPOSITORYDATAMODEL_H
