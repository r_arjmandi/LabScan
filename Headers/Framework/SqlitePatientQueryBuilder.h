#include "Headers/Adapters/IPatientQueryBuilder.h"

class IDatabaseCommandFactory;

class SqlitePatientQueryBuilder : public IPatientQueryBuilder
{
public:
        SqlitePatientQueryBuilder(std::shared_ptr<IDatabaseCommandFactory> commandFactory);

        std::shared_ptr<IDatabaseCommand> GetCreateTableIfNotExistsQuery() const override;
        std::shared_ptr<IDatabaseCommand> GetInsertQuery() const override;
        std::shared_ptr<IDatabaseCommand> GetRowCountQuery() const override;
        std::shared_ptr<IDatabaseCommand> GetUpdateQuery() const override;
        std::shared_ptr<IDatabaseCommand> GetSelectAllQuery() const override;
        std::shared_ptr<IDatabaseCommand> GetSelectQuery() const override;

private:
        std::shared_ptr<IDatabaseCommandFactory> m_CommandFactory;

};
