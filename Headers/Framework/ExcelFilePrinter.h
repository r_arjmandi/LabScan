#ifndef EXCELFILEPRINTER_H
#define EXCELFILEPRINTER_H

#include "Headers/BusinessRules/IExcelFilePrinter.h"

class ExcelFilePrinter : public IExcelFilePrinter
{
public:
    ExcelFilePrinter(const std::string& excelFilePath);

    void Print() override;

private:
    QString m_PrinterProgramPath = "ExPrint.exe";
    QString m_ExcelFilePath;
    QProcess m_Process;
};

#endif // EXCELFILEPRINTER_H
