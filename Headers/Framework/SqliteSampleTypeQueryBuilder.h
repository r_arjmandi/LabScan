#ifndef SQLITESAMPLETYPEQUERYBUILDER_H
#define SQLITESAMPLETYPEQUERYBUILDER_H

#include "Headers/Adapters/ISampleTypeQueryBuilder.h"

class IDatabaseCommandFactory;

class SqliteSampleTypeQueryBuilder : public ISampleTypeQueryBuilder
{
public:
    SqliteSampleTypeQueryBuilder(std::shared_ptr<IDatabaseCommandFactory> commandFactory);

    std::shared_ptr<IDatabaseCommand> GetCreateTableIfNotExistsQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetInsertQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetRowCountQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetUpdateQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetSelectAllQuery() const override;
    std::shared_ptr<IDatabaseCommand> GetDeleteQuery() const override;

private:
        std::shared_ptr<IDatabaseCommandFactory> m_CommandFactory;

};

#endif // SQLITESAMPLETYPEQUERYBUILDER_H
