#ifndef GENERALCOMMENTREPOSITORYDATAMODEL_H
#define GENERALCOMMENTREPOSITORYDATAMODEL_H

#include "Headers/BusinessEntities/IGeneralCommentRepository.h"

class IFrontEndAdapter;
class ICurrentPatientGui;

class GeneralCommentRepositoryDataModel : public QAbstractListModel, public IGeneralCommentRepository
{
    Q_OBJECT

public:
    enum GeneralCommentRoleNames{
        Subject = Qt::UserRole
    };

    GeneralCommentRepositoryDataModel(
            std::shared_ptr<ICurrentPatientGui> currentPatient);
    void SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter);
    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    Q_INVOKABLE QString GetData(int index) const;
    Q_INVOKABLE int GetSize() const;
    Q_INVOKABLE void AddNewComment(const QString& subject, const QString& description);
    Q_INVOKABLE void Delete(const QString& subject);
    Q_INVOKABLE QString GetDescription(const QString& subject);
    void SetGeneralCommentList(std::map<std::string, std::string>&& commentList) override;

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    std::shared_ptr<IFrontEndAdapter> m_Adapter;
    std::map<std::string, std::string> m_CommentList;
    std::shared_ptr<ICurrentPatientGui> m_CurrentPatient;
    QHash<int, QByteArray> m_CommentRoleNames;
};

#endif // GENERALCOMMENTREPOSITORYDATAMODEL_H
