#include "../Adapters/IDatabaseConnection.h"
#include "../BusinessEntities/IDateTimeOperation.h"
#include "ISqliteQueryResultFactory.h"

class IDatabaseCommand;

class SqliteDatabaseConnection : public IDatabaseConnection
{
public:
    SqliteDatabaseConnection(
        const std::string & filePath,
        std::shared_ptr<ISqliteQueryResultFactory> queryResultFactory,
        std::shared_ptr<IDateTimeOperation> dateTimeOps);
    ~SqliteDatabaseConnection();

    void Open() override;
    bool IsOpen() const override;
    void Close() override;
    std::shared_ptr<IQueryResult> Exec(std::shared_ptr<const IDatabaseCommand> dbCommand) override;
    void SetFilePath(const std::string& path) override;

private:
    sqlite3_stmt* PrepareStatement(const std::string& queryStr);


    sqlite3 * m_Database;
    std::string m_DatabaseFileName;
    std::shared_ptr<ISqliteQueryResultFactory> m_QueryResultFactory;
    std::shared_ptr<IDateTimeOperation> m_DateTimeOps;
};
