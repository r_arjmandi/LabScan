#include "IDatabaseCommandFactory.h"

class DatabaseCommandFactory : public IDatabaseCommandFactory
{
public:
        DatabaseCommandFactory();
        std::shared_ptr<IDatabaseCommand> CreateCommand() override;
};
