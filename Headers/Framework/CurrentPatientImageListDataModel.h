#ifndef CURRENTPATIENTIMAGEREPOSITORYDATAMODEL_H
#define CURRENTPATIENTIMAGEREPOSITORYDATAMODEL_H

#include "IImageListGui.h"

class CurrentPatientImageListDataModel : public QAbstractListModel, public IImageListGui
{
    Q_OBJECT

public:
    enum ImagesRoleNames{
        FileAddress = Qt::UserRole,
    };

    CurrentPatientImageListDataModel();

    virtual int rowCount(const QModelIndex &parent) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    void SetImageList(std::vector<std::weak_ptr<std::experimental::filesystem::path>>&& imageList) override;
    void ClearImages() override;

protected:
    virtual QHash<int, QByteArray> roleNames() const override;

private:
    QHash<int, QByteArray> m_ImageRoleNames;
    std::vector<std::weak_ptr<std::experimental::filesystem::path>> m_ImageFiles;

};

#endif // HEADERSFRAMEWORKCURRENTPATIENTIMAGEREPOSITORYDATAMODEL_H
