#ifndef SCANNERDEVICE_H
#define SCANNERDEVICE_H

class ScannerDevice : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE QUrl StartScan();

private:
    QString m_ScannerProgramPath = "TWAIN_APP_CMD32.exe";
    QProcess m_Process;

};

#endif // SCANNERDEVICE_H
