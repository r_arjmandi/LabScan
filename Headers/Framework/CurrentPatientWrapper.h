#ifndef HEADERSFRAMEWORKCURRENTPATIENTWRAPPER_H
#define HEADERSFRAMEWORKCURRENTPATIENTWRAPPER_H

#include "Headers/Adapters/ICurrentPatientGui.h"

class IImageListGui;
class IPatientData;

class CurrentPatientWrapper : public QObject, public ICurrentPatientGui
{
    Q_OBJECT

    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString family READ family WRITE setFamily NOTIFY familyChanged)
    Q_PROPERTY(QString age READ age WRITE setAge NOTIFY ageChanged)
    Q_PROPERTY(int gender READ gender WRITE setGender NOTIFY genderChanged)
    Q_PROPERTY(int labOperator READ labOperator WRITE setLabOperator NOTIFY labOperatorChanged)
    Q_PROPERTY(int sampleType READ sampleType WRITE setSampleType NOTIFY sampleTypeChanged)
    Q_PROPERTY(int sampleStandard READ sampleStandard WRITE setSampleStandard NOTIFY sampleStandardChanged)
    Q_PROPERTY(int sampleBacteria READ sampleBacteria WRITE setSampleBacteria NOTIFY sampleBacteriaChanged)
    Q_PROPERTY(int sampleShape READ sampleShape WRITE setSampleShape NOTIFY sampleShapeChanged)
    Q_PROPERTY(int sampleGram READ sampleGram WRITE setSampleGram NOTIFY sampleGramChanged)
    Q_PROPERTY(int sampleIsolated READ sampleIsolated WRITE setSampleIsolated NOTIFY sampleIsolatedChanged)
    Q_PROPERTY(QString sampleCount READ sampleCount WRITE setSampleCount NOTIFY sampleCountChanged)
    Q_PROPERTY(int commentSubjectIndex READ commentSubjectIndex WRITE SetCommentSubjectIndex NOTIFY commentSubjectIndexChanged)
    Q_PROPERTY(QString commentDescription READ commentDescription WRITE setCommentDescription NOTIFY commentDescriptionChanged)
    Q_PROPERTY(int plateMediaIndex READ plateMediaIndex WRITE setPlateMediaIndex NOTIFY plateMediaIndexChanged)
    Q_PROPERTY(QString plateTemplate READ plateTemplate WRITE setPlateTemplate NOTIFY plateTemplateChanged)
    Q_PROPERTY(int plateDiskNo READ plateDiskNo WRITE setPlateDiskNo NOTIFY plateDiskNoChanged)
    Q_PROPERTY(double plateDiskDiameter READ plateDiskDiameter WRITE setPlateDiskDiameter NOTIFY plateDiskDiameterChanged)
    Q_PROPERTY(int plateDiskType READ plateDiskType WRITE setPlateDiskType NOTIFY plateDiskTypeChanged)
    Q_PROPERTY(int plateDiskArrange READ plateDiskArrange WRITE setPlateDiskArrange NOTIFY plateDiskArrangeChanged)
    Q_PROPERTY(int currentImageIndex READ currentImageIndex WRITE setCurrentImageIndex NOTIFY currentImageIndexChanged)

public:
    CurrentPatientWrapper(std::shared_ptr<IImageListGui> imageListGui);

    void SetCurrenaPatientData(std::weak_ptr<IPatientData> currenaPatientData) override;

    std::string GetId() const override;
    Q_INVOKABLE void AddNewImage(const QUrl& newImagePath);
    Q_INVOKABLE void SetCurrentImage(int imageIndex);
    Q_INVOKABLE void Clear();

    //getters:
    QString id() const;
    QString name() const;
    QString family() const;
    QString age() const;
    int gender() const;
    int labOperator() const;
    int sampleType() const;
    int sampleStandard() const;
    int sampleBacteria() const;
    int sampleShape() const;
    int sampleGram() const;
    int sampleIsolated() const;
    QString sampleCount() const;
    int commentSubjectIndex() const;
    QString commentDescription() const;
    int plateMediaIndex() const;
    QString plateTemplate() const;
    int plateDiskNo() const;
    double plateDiskDiameter() const;
    int plateDiskType() const;
    int plateDiskArrange() const;
    int currentImageIndex() const;

    //setters:
    void setId(const QString& id);
    void setName(const QString& name);
    void setFamily(const QString& family);
    void setAge(const QString& age);
    void setGender(int gender);
    void setLabOperator(int labOperator);
    void setSampleType(int sampleType);
    void setSampleStandard(int sampleStandard);
    void setSampleBacteria(int sampleBacteria);
    void setSampleShape(int sampleShape);
    void setSampleGram(int sampleGram);
    void setSampleIsolated(int sampleIsolated);
    void setSampleCount(const QString& sampleCount);
    void setCommentDescription(const QString& commentDescription);
    void setPlateMediaIndex(int plateMediaIndex);
    void setPlateTemplate(const QString& plateTemplate);
    void setPlateDiskNo(int plateDiskNo);
    void setPlateDiskDiameter(double plateDiskDiameter);
    void setPlateDiskType(int plateDiskType);
    void setPlateDiskArrange(int plateDiskArrange);
    void SetCommentSubjectIndex(int index) override;
    void Update() override;
    void setCurrentImageIndex(int index);

signals:
    void nameChanged();
    void familyChanged();
    void ageChanged();
    void genderChanged();
    void labOperatorChanged();
    void idChanged();
    void sampleTypeChanged();
    void sampleStandardChanged();
    void sampleBacteriaChanged();
    void sampleGramChanged();
    void sampleShapeChanged();
    void sampleIsolatedChanged();
    void sampleCountChanged();
    void commentSubjectIndexChanged();
    void commentDescriptionChanged();
    void plateMediaIndexChanged();
    void plateTemplateChanged();
    void plateDiskNoChanged();
    void plateDiskDiameterChanged();
    void plateDiskTypeChanged();
    void plateDiskArrangeChanged();
    void currentImageIndexChanged();

private:
    std::weak_ptr<IPatientData> m_CurrenaPatientData;
    std::shared_ptr<IImageListGui> m_ImageListGui;
    int m_CurrentImageIndex = -1;
};

#endif // HEADERSFRAMEWORKCURRENTPATIENTWRAPPER_H
