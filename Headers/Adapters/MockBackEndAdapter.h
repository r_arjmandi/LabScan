#ifndef MOCKBACKENDADAPTER_H
#define MOCKBACKENDADAPTER_H

#include "IBackEndAdapter.h"

MOCK_BASE_CLASS(MockBackEndAdapter, IBackEndAdapter)
{
    MOCK_NON_CONST_METHOD(SavePatientToDatabase, 1, void(std::shared_ptr<PatientData> patientData))
    MOCK_NON_CONST_METHOD(LoadAllPatients, 0, void())
};

#endif // MOCKBACKENDADAPTER_H
