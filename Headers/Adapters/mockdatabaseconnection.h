#ifndef MOCKDATABASECONNECTION_H
#define MOCKDATABASECONNECTION_H

#include "IDatabaseConnection.h"

MOCK_BASE_CLASS(MockDatabaseConnection, IDatabaseConnection)
{
    MOCK_NON_CONST_METHOD(Open, 0, void());
    MOCK_CONST_METHOD(IsOpen, 0, bool());
    MOCK_NON_CONST_METHOD(SetFilePath, 1, void(const std::string& path));
    MOCK_NON_CONST_METHOD(Close, 0, void());
    MOCK_NON_CONST_METHOD(Exec, 1, std::shared_ptr<IQueryResult> (std::shared_ptr<const IDatabaseCommand> dbCommand));
};

#endif // MOCKDATABASECONNECTION_H
