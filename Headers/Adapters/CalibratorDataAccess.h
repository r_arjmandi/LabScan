#ifndef HEADERSADAPTERSCALIBRATORDATAACCESS_H
#define HEADERSADAPTERSCALIBRATORDATAACCESS_H

#include "Headers/BusinessRules/ICalibratorDataAccess.h"

class IDatabaseConnection;
class ICalibratorQueryBuilder;

class CalibratorDataAccess : public ICalibratorDataAccess
{
public:
    CalibratorDataAccess(std::shared_ptr<IDatabaseConnection> dbConnection,
                         std::shared_ptr<ICalibratorQueryBuilder> calibratorQueryBuilder);

    void InsertOrUpdate(const std::pair<std::string, double> &&calibrator) override;
    void Delete(const std::string &calibrator) override;
    std::vector<std::pair<std::string, double>> GetAllCalibrator() override;

private:
    std::shared_ptr<IDatabaseConnection> m_DatabaceConnection;
    std::shared_ptr<ICalibratorQueryBuilder> m_CalibratorQueryBuilder;

};

#endif // HEADERSADAPTERSCALIBRATORDATAACCESS_H
