#ifndef HEADERSADAPTERSPATIENTDATAACCESS_H
#define HEADERSADAPTERSPATIENTDATAACCESS_H

#include "Headers/BusinessRules/IPatientDataAccess.h"

class IDatabaseConnection;
class IPatientQueryBuilder;
class IDatabaseCommand;

class PatientDataAccess : public IPatientDataAccess
{
public:
    PatientDataAccess(std::shared_ptr<IDatabaseConnection> dbConnection,
                      std::shared_ptr<IPatientQueryBuilder> patientQueryBuilder);
    void InsertOrUpdate(std::weak_ptr<IPatientData> patientData) override;
    std::vector<std::unique_ptr<PatientShortInformation>> GetAll();
    std::unique_ptr<IPatientData> Get(const std::string &patientUid) override;

private:
    void PatientDataAccess::BindCommandParameter(
            std::shared_ptr<IDatabaseCommand> dbCommand,
            std::weak_ptr<IPatientData> inPatientData);

    std::shared_ptr<IDatabaseConnection> m_DatabaceConnection;
    std::shared_ptr<IPatientQueryBuilder> m_PatientQueryBuilder;

};

#endif // HEADERSADAPTERSPATIENTDATAACCESS_H
