#ifndef MOCKPATIENTQUERYBUILDER_H
#define MOCKPATIENTQUERYBUILDER_H

#include "IPatientQueryBuilder.h"

MOCK_BASE_CLASS(MockPatientQueryBuilder, IPatientQueryBuilder)
{
    MOCK_CONST_METHOD(GetSelectAllQuery, 0, std::shared_ptr<IDatabaseCommand>())
    MOCK_CONST_METHOD(GetRowCountQuery, 0, std::shared_ptr<IDatabaseCommand>())
    MOCK_CONST_METHOD(GetInsertQuery, 0, std::shared_ptr<IDatabaseCommand>())
    MOCK_CONST_METHOD(GetUpdateQuery, 0, std::shared_ptr<IDatabaseCommand>())
    MOCK_CONST_METHOD(GetCreateTableIfNotExistsQuery, 0, std::shared_ptr<IDatabaseCommand>())
};

#endif // MOCKPATIENTQUERYBUILDER_H
