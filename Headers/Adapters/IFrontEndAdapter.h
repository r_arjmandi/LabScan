#ifndef IADAPTER_H
#define IADAPTER_H

class IPatientData;
struct CommentData;
struct PatientListFilter;

class IFrontEndAdapter
{
public:
    virtual ~IFrontEndAdapter() = default;
    virtual void InsertOrUpdateCurrentPatient() = 0;
    virtual void InsertOrUpdateOperator(const std::string& operatorName) = 0;
    virtual void DeleteOperator(const std::string& operatorName) = 0;
    virtual void InsertOrUpdateSampleType(const std::string& sampleType) = 0;
    virtual void DeleteSampleType(const std::string& sampleType) = 0;
    virtual void InsertOrUpdateSampleShape(const std::string& sampleShape) = 0;
    virtual void DeleteSampleShape(const std::string& sampleShape) = 0;
    virtual void InsertOrUpdateComment(std::unique_ptr<CommentData> comment) = 0;
    virtual void DeleteComment(const std::string& subject) = 0;
    virtual void InsertOrUpdateMedia(const std::string& media) = 0;
    virtual void DeleteMedia(const std::string& media) = 0;
    virtual void DetectImageCircles(double diameter, int count) = 0;
    virtual void ShuffleImageCircles() = 0;
    virtual void SortCurrentImageCirclesClockwise() = 0;
    virtual void SortCurrentImageCirclesCenterClockwise() = 0;
    virtual void SortCurrentImageCirclesCounterClockwise() = 0;
    virtual void SortCurrentImageCirclesCenterCounterClockwise() = 0;
    virtual void LoadPatient(const std::string& patientUid) = 0;
    virtual void FilterPatientList(std::unique_ptr<PatientListFilter> patientListFilter) const = 0;
    virtual void InsertOrUpdateCalibrator(const std::pair<std::string, double>&& calibrator) = 0;
    virtual void DeleteCalibrator(const std::string& calibratorName) = 0;
    virtual void PrintCurrentPatient() = 0;
};

#endif // IADAPTER_H
