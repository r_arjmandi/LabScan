#ifndef MEDIADATAACCESS_H
#define MEDIADATAACCESS_H

#include "Headers/BusinessRules/IMediaDataAccess.h"

class IDatabaseConnection;
class IMediaQueryBuilder;

class MediaDataAccess : public IMediaDataAccess
{
public:
    MediaDataAccess(std::shared_ptr<IDatabaseConnection> dbConnection,
                    std::shared_ptr<IMediaQueryBuilder> mediaQueryBuilder);
    void InsertOrUpdate(const std::string &media) override;
    void Delete(const std::string &media) override;
    std::set<std::string> GetAllMedia() override;

private:
    std::shared_ptr<IDatabaseConnection> m_DatabaceConnection;
    std::shared_ptr<IMediaQueryBuilder> m_MediaQueryBuilder;
};

#endif // MEDIADATAACCESS_H
