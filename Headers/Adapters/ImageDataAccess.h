#ifndef IMAGEDATAACCESS_H
#define IMAGEDATAACCESS_H

#include "Headers/BusinessRules/IImageDataAccess.h"

class IFileNameGenerator;

class ImageDataAccess : public IImageDataAccess
{
public:
    ImageDataAccess(const std::experimental::filesystem::path& imageListDirectoryPath,
                    std::shared_ptr<IFileNameGenerator> fileNameGenerator);

    void InsertOrUpdate(
            const std::string &patientId,
            std::vector<std::weak_ptr<std::experimental::filesystem::path>>&& images) override;
    std::vector<std::unique_ptr<std::experimental::filesystem::path>> GetAll(
            const std::string &patientId) override;

private:
    std::experimental::filesystem::path m_ImageListDirectoryPath;
    std::shared_ptr<IFileNameGenerator> m_FileNameGenerator;

};

#endif // IMAGEDATAACCESS_H
