#ifndef SAMPLETYPEDATAACCESS_H
#define SAMPLETYPEDATAACCESS_H

#include "Headers/BusinessRules/ISampleTypeDataAccess.h"

class IDatabaseConnection;
class ISampleTypeQueryBuilder;

class SampleTypeDataAccess : public ISampleTypeDataAccess
{
public:
    SampleTypeDataAccess(std::shared_ptr<IDatabaseConnection> dbConnection,
                         std::shared_ptr<ISampleTypeQueryBuilder> sampleTypeQueryBuilder);

    void InsertOrUpdate(const std::string &typeName) override;
    void Delete(const std::string &typeName) override;
    std::set<std::string> GetAllTypes() override;

private:
    std::shared_ptr<IDatabaseConnection> m_DatabaceConnection;
    std::shared_ptr<ISampleTypeQueryBuilder> m_SampleTypeQueryBuilder;

};

#endif // SAMPLETYPEDATAACCESS_H
