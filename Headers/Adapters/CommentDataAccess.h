#ifndef COMMENTDATAACCESS_H
#define COMMENTDATAACCESS_H

#include "Headers/BusinessRules/ICommentDataAccess.h"

class IDatabaseConnection;
class ICommentQueryBuilder;
struct CommentData;

class CommentDataAccess : public ICommentDataAccess
{
public:
    CommentDataAccess(std::shared_ptr<IDatabaseConnection> dbConnection,
                      std::shared_ptr<ICommentQueryBuilder> commentQueryBuilder);

    void InsertOrUpdate(std::unique_ptr<CommentData> comment) override;
    void Delete(const std::string &subject) override;
    std::map<std::string, std::string> GetAllComments() override;

private:
    std::shared_ptr<IDatabaseConnection> m_DatabaceConnection;
    std::shared_ptr<ICommentQueryBuilder> m_CommentQueryBuilder;

};

#endif // COMMENTDATAACCESS_H
