#ifndef HEADERSADAPTERSBACKENDADAPTER_H
#define HEADERSADAPTERSBACKENDADAPTER_H

#include "Headers/BusinessRules/IBackEndAdapter.h"

class IPatientListGui;
class ICurrentPatientGui;

class BackEndAdapter : public IBackEndAdapter
{
public:
    BackEndAdapter(
            std::shared_ptr<IPatientListGui> patientListGui,
            std::shared_ptr<ICurrentPatientGui> currentPatientGui);
    void UpdatePatientListRepresentation(std::vector<std::weak_ptr<PatientShortInformation>>&&
                                         patientInfoList) override;
    void UpdateCurrentPatientRepresentation(std::weak_ptr<IPatientData> currentPatientData) override;

private:
    std::shared_ptr<IPatientListGui> m_PatientListGui;
    std::shared_ptr<ICurrentPatientGui> m_CurrentPatientGui;

};

#endif // HEADERSADAPTERSBACKENDADAPTER_H
