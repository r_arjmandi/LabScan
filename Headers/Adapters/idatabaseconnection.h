#ifndef IDATABASECONNECTION_H
#define IDATABASECONNECTION_H

class IQueryResult;
class IDatabaseCommand;

class IDatabaseConnection{
public:
    virtual ~IDatabaseConnection() = default;
    virtual void Open() = 0;
    virtual bool IsOpen() const = 0;
    virtual void Close() = 0;
    virtual void SetFilePath(const std::string& path) = 0;
    virtual std::shared_ptr<IQueryResult> Exec(std::shared_ptr<const IDatabaseCommand> dbCommand) = 0;
};

#endif // IDATABASECONNECTION_H
