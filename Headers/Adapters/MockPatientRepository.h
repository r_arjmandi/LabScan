#ifndef MOCKGUI_H
#define MOCKGUI_H

#include "Headers/BusinessEntities/IPatientRepository.h"

MOCK_BASE_CLASS(MockPatientRepository, IPatientRepository)
{
    MOCK_NON_CONST_METHOD(SetPatientDataList, 1, void (std::vector<std::shared_ptr<PatientData>> patientDataList));
};

#endif // MOCKGUI_H
