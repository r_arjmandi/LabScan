#ifndef ISAMPLETYPEQUERYBUILDER_H
#define ISAMPLETYPEQUERYBUILDER_H

class IDatabaseCommand;

class ISampleTypeQueryBuilder{
public:
    virtual ~ISampleTypeQueryBuilder() = default;

    virtual std::shared_ptr<IDatabaseCommand> GetCreateTableIfNotExistsQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetSelectAllQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetInsertQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetRowCountQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetUpdateQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetDeleteQuery() const = 0;
};

#endif // ISAMPLETYPEQUERYBUILDER_H
