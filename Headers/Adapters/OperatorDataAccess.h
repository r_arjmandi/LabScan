#ifndef HEADERSADAPTERSOPERATORDATAACCESS_H
#define HEADERSADAPTERSOPERATORDATAACCESS_H

#include "Headers/BusinessRules/IOperatorDataAccess.h"

class IDatabaseConnection;
class IOperatorQueryBuilder;

class OperatorDataAccess : public IOperatorDataAccess
{
public:
    OperatorDataAccess(std::shared_ptr<IDatabaseConnection> dbConnection,
                       std::shared_ptr<IOperatorQueryBuilder> operatorQueryBuilder);
    void InsertOrUpdate(const std::string &operatorName) override;
    void Delete(const std::string &operatorName) override;
    std::set<std::string> GetAllOperators() override;

private:
    std::shared_ptr<IDatabaseConnection> m_DatabaceConnection;
    std::shared_ptr<IOperatorQueryBuilder> m_OperatorQueryBuilder;

};

#endif // HEADERSADAPTERSOPERATORDATAACCESS_H
