#ifndef IOPERATORQUERYBUILDER_H
#define IOPERATORQUERYBUILDER_H

class IDatabaseCommand;

class IOperatorQueryBuilder
{
public:
    virtual ~IOperatorQueryBuilder() = default;

    virtual std::shared_ptr<IDatabaseCommand> GetCreateTableIfNotExistsQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetSelectAllQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetInsertQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetRowCountQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetUpdateQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetDeleteQuery() const = 0;
};
#endif // IOPERATORQUERYBUILDER_H
