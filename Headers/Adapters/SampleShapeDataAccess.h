#ifndef SAMPLESHAPEDATAACCESS_H
#define SAMPLESHAPEDATAACCESS_H

#include "Headers/BusinessRules/ISampleShapeDataAccess.h"

class IDatabaseConnection;
class ISampleShapeQueryBuilder;

class SampleShapeDataAccess : public ISampleShapeDataAccess
{

public:
    SampleShapeDataAccess(std::shared_ptr<IDatabaseConnection> dbConnection,
                          std::shared_ptr<ISampleShapeQueryBuilder> sampleTypeQueryBuilder);

    void InsertOrUpdate(const std::string &shapeName) override;
    void Delete(const std::string &shapeName) override;
    std::set<std::string> GetAllShapes() override;

private:
    std::shared_ptr<IDatabaseConnection> m_DatabaceConnection;
    std::shared_ptr<ISampleShapeQueryBuilder> m_SampleShapeQueryBuilder;

};

#endif // SAMPLESHAPEDATAACCESS_H
