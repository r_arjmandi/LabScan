#ifndef HEADERSADAPTERSADAPTER_H
#define HEADERSADAPTERSADAPTER_H

#include "IFrontEndAdapter.h"

class IBusinessRules;

class FrontEndAdapter : public IFrontEndAdapter
{
public:
    FrontEndAdapter(std::shared_ptr<IBusinessRules> businessRules);
    void InsertOrUpdateCurrentPatient() override;
    void DetectImageCircles(double diameter, int count) override;
    void InsertOrUpdateOperator(const std::string &operatorName) override;
    void DeleteOperator(const std::string &operatorName) override;
    void InsertOrUpdateSampleType(const std::string &sampleType) override;
    void DeleteSampleType(const std::string &sampleType) override;
    void InsertOrUpdateSampleShape(const std::string &sampleShape) override;
    void DeleteSampleShape(const std::string &sampleShape) override;
    void InsertOrUpdateComment(std::unique_ptr<CommentData> comment) override;
    void DeleteComment(const std::string &subject) override;
    void InsertOrUpdateMedia(const std::string &media) override;
    void DeleteMedia(const std::string &media) override;
    void ShuffleImageCircles() override;
    void SortCurrentImageCirclesClockwise() override;
    void SortCurrentImageCirclesCenterClockwise() override;
    void SortCurrentImageCirclesCounterClockwise() override;
    void SortCurrentImageCirclesCenterCounterClockwise() override;
    void LoadPatient(const std::string &patientUid) override;
    void FilterPatientList(std::unique_ptr<PatientListFilter> patientListFilter) const override;
    void InsertOrUpdateCalibrator(const std::pair<std::string, double> &&calibrator) override;
    void DeleteCalibrator(const std::string &calibratorName) override;
    void PrintCurrentPatient() override;

private:
    std::shared_ptr<IBusinessRules> m_BusinessRules;

};

#endif // HEADERSADAPTERSADAPTER_H
