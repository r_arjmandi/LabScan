#ifndef IMEDIAQUERYBUILDER_H
#define IMEDIAQUERYBUILDER_H

class IDatabaseCommand;

class IMediaQueryBuilder
{
public:
    ~IMediaQueryBuilder() = default;

    virtual std::shared_ptr<IDatabaseCommand> GetCreateTableIfNotExistsQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetSelectAllQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetInsertQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetRowCountQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetUpdateQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetDeleteQuery() const = 0;
};

#endif // IMEDIAQUERYBUILDER_H
