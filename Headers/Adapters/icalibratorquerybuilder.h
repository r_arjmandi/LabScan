#ifndef ICALIBRATORQUERYBUILDER_H
#define ICALIBRATORQUERYBUILDER_H

class IDatabaseCommand;

class ICalibratorQueryBuilder
{
public:
    ~ICalibratorQueryBuilder() = default;

    virtual std::shared_ptr<IDatabaseCommand> GetCreateTableIfNotExistsQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetSelectAllQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetInsertQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetRowCountQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetUpdateQuery() const = 0;
    virtual std::shared_ptr<IDatabaseCommand> GetDeleteQuery() const = 0;
};

#endif // ICALIBRATORQUERYBUILDER_H
