#ifndef IPATIENTLISTGUI_H
#define IPATIENTLISTGUI_H

struct PatientShortInformation;

class IPatientListGui
{
public:
    virtual ~IPatientListGui() = default;
    virtual void SetPatientDataList(std::vector<std::weak_ptr<PatientShortInformation>>&&
                                    patientInfoList) = 0;
};

#endif // IPATIENTLISTGUI_H
