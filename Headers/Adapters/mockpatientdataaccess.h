#ifndef MOCKPATIENTDATAACCESS_H
#define MOCKPATIENTDATAACCESS_H

#include "IPatientDataAccess.h"

MOCK_BASE_CLASS(MockPatientDataAccess, IPatientDataAccess)
{
    MOCK_NON_CONST_METHOD(InsertOrUpdate, 1, void(std::shared_ptr<PatientData> patientData))
    MOCK_NON_CONST_METHOD(GetAllPatient, 0, std::vector<std::shared_ptr<PatientData>>())
};

#endif // MOCKPATIENTDATAACCESS_H
