#ifndef ICURRENTPATIENTWRAPPER_H
#define ICURRENTPATIENTWRAPPER_H

class IPatientData;

class ICurrentPatientGui
{
public:
    virtual void SetCurrenaPatientData(std::weak_ptr<IPatientData> currenaPatientData) = 0;
    virtual std::string GetId() const = 0;
    virtual void SetCommentSubjectIndex(int index) = 0;
    virtual void Update() =0;
};

#endif // ICURRENTPATIENTWRAPPER_H
