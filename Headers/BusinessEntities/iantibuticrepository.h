#ifndef IANTIBUTICREPOSITORY_H
#define IANTIBUTICREPOSITORY_H

struct Range;
struct Antibiotic;

class IAntibuticRepository{
public:
    virtual ~IAntibuticRepository() = default;
    virtual void SetAntibuticList(const std::vector<Antibiotic>&& antibioticList) = 0;
    virtual Antibiotic GetAntibioticByIndex(int index) const = 0;
};

#endif // IANTIBUTICREPOSITORY_H
