#ifndef IFILENAMEGENERATOR_H
#define IFILENAMEGENERATOR_H

class IFileNameGenerator
{
public:
    virtual std::string Generate() const = 0;
};

#endif // IFILENAMEGENERATOR_H
