#ifndef MOCKSTANDARDREPOSITORY_H
#define MOCKSTANDARDREPOSITORY_H

#include "IStandardRepository.h"

MOCK_BASE_CLASS(MockStandardRepository, IStandardRepository)
{
    MOCK_NON_CONST_METHOD(LoadAllBacterias, 0, std::vector<std::string>())
};

#endif // MOCKSTANDARDREPOSITORY_H
