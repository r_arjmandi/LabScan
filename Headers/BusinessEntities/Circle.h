#ifndef CIRCLE_H
#define CIRCLE_H

#include "Point.h"

struct Circle{
    Point Center;
    double Radius = 0.0;
};

#endif // CIRCLE_H
