#ifndef ISTANDARDREPOSITORY_H
#define ISTANDARDREPOSITORY_H

class IStandardRepository{
public:
    virtual ~IStandardRepository() = default;
};

#endif // ISTANDARDREPOSITORY_H
