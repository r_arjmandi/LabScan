#ifndef IOPERATORREPOSITORY_H
#define IOPERATORREPOSITORY_H

class IOperatorRepository{
public:
    ~IOperatorRepository() = default;
    virtual void SetOperatorList(std::set<std::string>&& operatorList) = 0;
};

#endif // IOPERATORREPOSITORY_H
