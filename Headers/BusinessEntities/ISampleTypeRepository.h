#ifndef ISAMPLETYPEREPOSITORY_H
#define ISAMPLETYPEREPOSITORY_H

class ISampleTypeRepository{
public:
    ~ISampleTypeRepository() = default;
    virtual void SetSampleTypeList(std::set<std::string>&& sampleTypeList) = 0;
};

#endif // ISAMPLETYPEREPOSITORY_H
