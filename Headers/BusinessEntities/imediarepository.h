#ifndef IMEDIAREPOSITORY_H
#define IMEDIAREPOSITORY_H

class IMediaRepository{
public:
    ~IMediaRepository() = default;
    virtual void SetMediaList(std::set<std::string>&& mediaList) = 0;
};

#endif // IMEDIAREPOSITORY_H
