#ifndef ICIRCLEDETECTOR_H
#define ICIRCLEDETECTOR_H

struct Circle;

class ICircleDetector{
public:
    virtual std::vector<Circle> DetectCircles(
            const std::experimental::filesystem::path& imageAddress,
            double minRadius, double maxRadius, int count) = 0;
};

#endif // ICIRCLEDETECTOR_H
