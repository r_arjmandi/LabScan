#ifndef PATIENTLISTFILTER_H
#define PATIENTLISTFILTER_H

#include "DateTime.h"

struct PatientListFilter
{
    std::string Id;
    std::string Name;
    std::string FamilyName;
    DateTime FromDateTime;
    DateTime ToDateTime;
};

#endif // PATIENTLISTFILTER_H
