#ifndef CIRCLETARGET_H
#define CIRCLETARGET_H

#include "Range.h"
#include "Circle.h"
#include "Antibiotic.h"

struct CircularTarget{
    Circle Disk;
    Circle Region;
    Antibiotic SelectedAntibiotic;
    bool Report;
};

#endif // CIRCLETARGET_H
