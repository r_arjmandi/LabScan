#ifndef PATIENTSHORTINFORMATION_H
#define PATIENTSHORTINFORMATION_H

#include "Headers/BusinessEntities/DateTime.h"

struct PatientShortInformation
{
    std::string Id;
    std::string Name;
    std::string FamilyName;
    DateTime ModifyDateTime;
};

#endif // PATIENTSHORTINFORMATION_H
