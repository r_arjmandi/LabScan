#ifndef FILENAMEGENERATOR_H
#define FILENAMEGENERATOR_H

#include "IFileNameGenerator.h"

class FileNameGenerator : public IFileNameGenerator
{
public:
    std::string Generate() const override;
};

#endif // FILENAMEGENERATOR_H
