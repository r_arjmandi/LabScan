#ifndef ICIRCLEREPOSITORY_H
#define ICIRCLEREPOSITORY_H

struct CircularTarget;

class ICircleRepository{
public:
    virtual void AddCircularTargets(
            const std::experimental::filesystem::path& imageAddress,
            std::vector<CircularTarget>&& circles) = 0;
    virtual std::weak_ptr<std::vector<CircularTarget>> GetCircularTargets(
            const std::experimental::filesystem::path& imageAddress) = 0;
    virtual void Update() = 0;
};

#endif // ICIRCLEREPOSITORY_H
