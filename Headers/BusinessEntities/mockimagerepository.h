#ifndef MOCKIMAGEREPOSITORY_H
#define MOCKIMAGEREPOSITORY_H

#include "IImageRepository.h"

MOCK_BASE_CLASS(MockImageRepository, IImageRepository)
{
    MOCK_CONST_METHOD(GetCurrentImageAddress, 0, std::string());
};

#endif // MOCKIMAGEREPOSITORY_H
