#ifndef MOCKCIRCLEDETECTOR_H
#define MOCKCIRCLEDETECTOR_H

#include "ICircleDetector.h"

MOCK_BASE_CLASS(MockCircleDetector, ICircleDetector)
{
    MOCK_NON_CONST_METHOD(DetectCircles, 3, std::shared_ptr<std::vector<Circle>>(const std::string& imageAddress, double minRadius, double maxRadius))
};

#endif // MOCKCIRCLEDETECTOR_H
