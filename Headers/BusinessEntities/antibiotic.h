#ifndef ANTIBIOTIC_H
#define ANTIBIOTIC_H

#include "Headers/BusinessEntities/Range.h"

enum AntibuticStatus{
    S = 1,
    I = 2,
    R = 3
};

struct Antibiotic{
    std::string Name;
    std::string Abbreviation;
    Range StdRange;
    AntibuticStatus Status;
};

#endif // ANTIBIOTIC_H
