#ifndef ISAMPLESHAPEREPOSITORY_H
#define ISAMPLESHAPEREPOSITORY_H

class ISampleShapeRepository{
public:
    ~ISampleShapeRepository() = default;
    virtual void SetSampleShapeList(std::set<std::string>&& sampleShapeList) = 0;
};

#endif // ISAMPLESHAPEREPOSITORY_H
