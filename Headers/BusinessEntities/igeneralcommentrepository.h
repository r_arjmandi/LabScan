#ifndef IGENERALCOMMENTREPOSITORY_H
#define IGENERALCOMMENTREPOSITORY_H

class IGeneralCommentRepository{
public:
    ~IGeneralCommentRepository() = default;
    virtual void SetGeneralCommentList(std::map<std::string, std::string>&& commentList) = 0;
};

#endif // IGENERALCOMMENTREPOSITORY_H
