#ifndef IPATIENTREPOSITORY_H
#define IPATIENTREPOSITORY_H

struct PatientShortInformation;

class IPatientShortInformationRepository
{
public:
    virtual void SetList(std::vector<std::unique_ptr<PatientShortInformation>>&& patientInfoList) = 0;
    virtual void InsertOrUpdate(std::unique_ptr<PatientShortInformation> patientInfo) = 0;
    virtual std::weak_ptr<PatientShortInformation> Get(const std::string& patientUid) const = 0;
    virtual std::vector<std::weak_ptr<PatientShortInformation>> GetList() const = 0;
};

#endif // IPATIENTREPOSITORY_H
