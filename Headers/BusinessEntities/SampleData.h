#ifndef SAMPLEDATA_H
#define SAMPLEDATA_H

enum class GramType
{
    Positive = 1,
    Negative = -1
};

enum class IsolatedType
{
    Yes = 1,
    No = -1
};

struct SampleData
{
    int Type = -1;
    int Standard = 0;
    int Bacteria = -1;
    int Shape = -1;
    GramType Gram;
    IsolatedType Isolated;
    std::string Count;
};

#endif // SAMPLEDATA_H
