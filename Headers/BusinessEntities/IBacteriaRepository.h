#ifndef IBACTERIAREPOSITORY_H
#define IBACTERIAREPOSITORY_H

class IBacteriaRepository{
public:
    virtual ~IBacteriaRepository() = default;
    virtual void SetBacteriaList(
            std::shared_ptr<std::vector<std::string>> bacteriaList) = 0;
    virtual std::shared_ptr<std::vector<std::string>> GetBacteriaList() const = 0;
};

#endif // IBACTERIAREPOSITORY_H
