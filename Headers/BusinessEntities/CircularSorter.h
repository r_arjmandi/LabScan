#ifndef CIRCULARSORTER_H
#define CIRCULARSORTER_H

#include "ICircularSorter.h"

struct Point;

class CircularSorter : public ICircularSorter
{
public:
    void SortClockwise(std::weak_ptr<std::vector<CircularTarget>> circles) override;
    void SortCenterClockwise(std::weak_ptr<std::vector<CircularTarget> > circles) override;
    void SortCounterClockwise(std::weak_ptr<std::vector<CircularTarget> > circles) override;
    void SortCenterCounterClockwise(std::weak_ptr<std::vector<CircularTarget> > circles) override;

private:
    Point CalculateCenterPoint(std::weak_ptr<std::vector<CircularTarget>> circles);
    void MoveCentralCirlceToFirst(std::weak_ptr<std::vector<CircularTarget>> circles, const Point& centerPoint);

};

#endif // CIRCULARSORTER_H
