#ifndef PATIENTDATA_H
#define PATIENTDATA_H

#include "IPatientData.h"
#include "DateTime.h"

#include "SampleData.h"
#include "CommentData.h"

class PatientData : public IPatientData
{
public:

    std::vector<std::weak_ptr<std::experimental::filesystem::path>>
        GetImages() const override;
    std::string GetId() const override;
    std::string GetName() const override;
    std::string GetFamilyName() const override;
    int GetAge() const override;
    GenderType GetGender() const override;
    int GetLabOperator() const override;
    SampleData GetSample() const override;
    CommentData GetComment() const override;
    int GetPlateMediaIndex() const override;
    std::weak_ptr<std::experimental::filesystem::path> GetCurrentImage() const override;
    DateTime GetModifyDateTime() const override;

    void SetId(const std::string &id) override;
    void SetName(const std::string &name) override;
    void SetFamilyName(const std::string &familyName) override;
    void SetAge(int age) override;
    void SetGender(const GenderType &genderType) override;
    void SetLabOperator(int labOperator) override;
    void SetSample(const SampleData &sampleData) override;
    void SetComment(const CommentData &commentData) override;
    void SetPlateMediaIndex(int plateMediaIndex) override;
    void SetImages(
            std::vector<std::unique_ptr<std::experimental::filesystem::path>>&& images) override;
    void SetCurrentImage(int imageIndex) override;
    void AddImage(std::unique_ptr<std::experimental::filesystem::path> image) override;
    void SetModifyDateTime(DateTime dateTime) override;

    void ClearImages() override;

private:
    std::vector<std::shared_ptr<std::experimental::filesystem::path>> m_Images;

    DateTime m_ModifyDateTime;

    std::string m_Id;
    std::string m_Name;
    std::string m_FamilyName;
    int m_Age;
    GenderType m_Gender;
    int m_LabOperator = -1;
    SampleData m_Sample;
    CommentData m_Comment;
    int m_PlateMediaIndex;
    int m_CurrentImageIndex = -1;

};

#endif // PATIENTDATA_H
