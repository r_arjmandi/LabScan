#ifndef ICLOCKWISESORTER_H
#define ICLOCKWISESORTER_H

struct CircularTarget;

class ICircularSorter{
public:
    virtual ~ICircularSorter() = default;
    virtual void SortClockwise(std::weak_ptr<std::vector<CircularTarget>> circles) = 0;
    virtual void SortCenterClockwise(std::weak_ptr<std::vector<CircularTarget>> circles) = 0;
    virtual void SortCounterClockwise(std::weak_ptr<std::vector<CircularTarget>> circles) = 0;
    virtual void SortCenterCounterClockwise(std::weak_ptr<std::vector<CircularTarget>> circles) = 0;
};

#endif // ICLOCKWISESORTER_H
