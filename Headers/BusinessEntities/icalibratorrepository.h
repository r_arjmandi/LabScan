#ifndef ICALIBRATORREPOSITORY_H
#define ICALIBRATORREPOSITORY_H

class ICalibratorRepository{
public:
    virtual double GetCurrentCoefficient() const = 0;
    virtual void SetCalibratorList(std::vector<std::pair<std::string, double>>&& calibratorList) = 0;
};

#endif // ICALIBRATORREPOSITORY_H
