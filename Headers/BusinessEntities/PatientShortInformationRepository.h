#ifndef PATIENTREPOSITORY_H
#define PATIENTREPOSITORY_H

#include "IPatientShortInformationRepository.h"

class PatientShortInformationRepository : public IPatientShortInformationRepository
{
public:
    void SetList(std::vector<std::unique_ptr<PatientShortInformation>>&& patientInfoList) override;
    void InsertOrUpdate(std::unique_ptr<PatientShortInformation> patientInfo) override;
    std::vector<std::weak_ptr<PatientShortInformation>> GetList() const override;
    std::weak_ptr<PatientShortInformation> Get(const std::string &patientUid) const override;

private:

    std::vector<std::shared_ptr<PatientShortInformation>> m_PatientInfoList;
};

#endif // PATIENTREPOSITORY_H
