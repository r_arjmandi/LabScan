#ifndef IPATIENTDATA_H
#define IPATIENTDATA_H

class DateTime;

struct SampleData;
struct CommentData;

enum class GenderType{
    Male = 1,
    Female = 2
};

class IPatientData
{
public:
    virtual ~IPatientData() = default;

    virtual std::string GetId() const = 0;
    virtual std::string GetName() const = 0;
    virtual std::string GetFamilyName() const = 0;
    virtual int GetAge() const = 0 ;
    virtual GenderType GetGender() const = 0;
    virtual int GetLabOperator() const = 0;
    virtual SampleData GetSample() const = 0;
    virtual CommentData GetComment() const = 0;
    virtual int GetPlateMediaIndex() const = 0;
    virtual std::vector<std::weak_ptr<std::experimental::filesystem::path>>
        GetImages() const = 0;
    virtual std::weak_ptr<std::experimental::filesystem::path> GetCurrentImage() const = 0;
    virtual DateTime GetModifyDateTime() const = 0;

    virtual void SetId(const std::string& id) = 0;
    virtual void SetName(const std::string& name) = 0;
    virtual void SetFamilyName(const std::string& familyName) = 0;
    virtual void SetAge(int age) = 0 ;
    virtual void SetGender(const GenderType& genderType) = 0;
    virtual void SetLabOperator(int labOperator) = 0;
    virtual void SetSample(const SampleData& sampleData) = 0;
    virtual void SetComment(const CommentData& commentData) = 0;
    virtual void SetPlateMediaIndex(int plateMediaIndex) = 0;
    virtual void SetImages(
            std::vector<std::unique_ptr<std::experimental::filesystem::path>>&& images) = 0;
    virtual void AddImage(std::unique_ptr<std::experimental::filesystem::path> image) = 0;
    virtual void SetCurrentImage(int imageIndex) = 0;
    virtual void SetModifyDateTime(DateTime dateTime) = 0;

    virtual void ClearImages() = 0;
};

#endif // IPATIENTDATA_H
