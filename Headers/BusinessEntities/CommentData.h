#ifndef COMMENTDATA_H
#define COMMENTDATA_H

struct CommentData
{
    int SubjectIndex = -1;
    std::string Subject;
    std::string Description;
};

#endif // COMMENTDATA_H
