#ifndef MOCKCIRCLEREPOSITORY_H
#define MOCKCIRCLEREPOSITORY_H

#include "ICircleRepository.h"

MOCK_BASE_CLASS(MockCircleRepository, ICircleRepository)
{
    MOCK_NON_CONST_METHOD(AddCircles, 2, void(const std::string& imageAddress, std::shared_ptr<std::vector<Circle>> circles))
};

#endif // MOCKCIRCLEREPOSITORY_H
