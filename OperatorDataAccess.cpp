#include "Headers/Adapters/OperatorDataAccess.h"

#include "Headers/Adapters/ConnectionGuard.h"
#include "Headers/Adapters/IOperatorQueryBuilder.h"
#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Adapters/IDatabaseConnection.h"
#include "Headers/Adapters/IQueryResult.h"

OperatorDataAccess::OperatorDataAccess(
        std::shared_ptr<IDatabaseConnection> dbConnection,
        std::shared_ptr<IOperatorQueryBuilder> operatorQueryBuilder):
    m_DatabaceConnection(dbConnection),
    m_OperatorQueryBuilder(operatorQueryBuilder)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto createTableCommand = m_OperatorQueryBuilder->GetCreateTableIfNotExistsQuery();
    m_DatabaceConnection->Exec(createTableCommand);
}

void OperatorDataAccess::InsertOrUpdate(const std::string &operatorName)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto rowCountCommand = m_OperatorQueryBuilder->GetRowCountQuery();
    rowCountCommand->BindParameter("OperatorName", operatorName);
    auto queryResult = m_DatabaceConnection->Exec(rowCountCommand);

    if (queryResult->GetInt(0) == 0)
    {
        auto insertCommand = m_OperatorQueryBuilder->GetInsertQuery();
        insertCommand->BindParameter("OperatorName", operatorName);
        m_DatabaceConnection->Exec(insertCommand);
    }
    else
    {
        auto updateCommand = m_OperatorQueryBuilder->GetUpdateQuery();
        updateCommand->BindParameter("OperatorName", operatorName);
        m_DatabaceConnection->Exec(updateCommand);
    }
}

void OperatorDataAccess::Delete(const std::string &operatorName)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto deleteCommand = m_OperatorQueryBuilder->GetDeleteQuery();
    deleteCommand->BindParameter("OperatorName", operatorName);
    m_DatabaceConnection->Exec(deleteCommand);
}

std::set<std::string> OperatorDataAccess::GetAllOperators()
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto selectAllCommand = m_OperatorQueryBuilder->GetSelectAllQuery();
    auto queryResult = m_DatabaceConnection->Exec(selectAllCommand);

    auto operatorList = std::set<std::string>();

    while(queryResult->HasData())
    {
        auto operatorName = queryResult->GetString(0);
        operatorList.insert(operatorName);
        queryResult->Next();
    }
    return std::move(operatorList);
}
