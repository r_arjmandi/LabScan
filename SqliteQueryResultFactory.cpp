#include "Headers/Framework/SqliteQueryResultFactory.h"
#include "Headers/Framework/SqliteQueryResult.h"

SqliteQueryResultFactory::SqliteQueryResultFactory(const std::shared_ptr<IDateTimeOperation> dateTimeOpes)
	:m_DateTimeOperation(dateTimeOpes){ }


std::shared_ptr<IQueryResult> SqliteQueryResultFactory::CreateQueryResult(sqlite3_stmt * stmt)
{
	auto queryResult = std::make_shared<SqliteQueryResult>(stmt, m_DateTimeOperation);
	return queryResult;
}

