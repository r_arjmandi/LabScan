#include "Headers/Framework/CircleRepositoryDataModel.h"
#include "Headers/BusinessEntities/CircularTarget.h"
#include "Headers/BusinessEntities/IAntibuticrepository.h"
#include "Headers/BusinessEntities/ICalibratorRepository.h"

CircleRepositoryDataModel::CircleRepositoryDataModel(
        std::shared_ptr<IAntibuticRepository> antibuticRepo,
        std::shared_ptr<ICalibratorRepository> calibratorRepo):
    m_AntibuticRepository(antibuticRepo),
    m_CalibratorRepo(calibratorRepo)
{
    m_CircleRoleNames[Radius] = "Radius";
    m_CircleRoleNames[MinMax] = "MinMax";
    m_CircleRoleNames[SIR] = "SIR";
    m_CircleRoleNames[Report] = "Report";
}

void CircleRepositoryDataModel::AddCircularTargets(
        const std::experimental::filesystem::path& imageAddress,
        std::vector<CircularTarget>&& circles)
{
    m_CirclesMap[imageAddress] = std::make_shared<std::vector<CircularTarget>>(std::move(circles));
    emit updateRepository();
    emit endResetModel();
}

std::weak_ptr<std::vector<CircularTarget>> CircleRepositoryDataModel::GetCircularTargets(const std::experimental::filesystem::path &imageAddress)
{
    return m_CirclesMap[imageAddress];
}

int CircleRepositoryDataModel::Size(const QUrl& url) const
{
    auto findValue = m_CirclesMap.find(std::experimental::filesystem::path(url.toLocalFile().toStdString()));
    if(findValue == m_CirclesMap.end())
    {
        return 0;
    }
    if(!findValue->second)
    {
        return 0;
    }
    return static_cast<int>(findValue->second->size());
}

void CircleRepositoryDataModel::SetCurrentImage(const QUrl &url)
{
    m_CurrentImage = std::experimental::filesystem::path(url.toLocalFile().toStdString());
    emit endResetModel();
}

void CircleRepositoryDataModel::SetAntibutic(int circleIndex, int antibioticIndex)
{
    auto findValue = m_CirclesMap.find(m_CurrentImage);
    if(findValue == m_CirclesMap.end())
    {
        return;
    }

    if(circleIndex == -1)
    {
        return;
    }

    auto antibiotic = m_AntibuticRepository->GetAntibioticByIndex(antibioticIndex);
    findValue->second->at(circleIndex).SelectedAntibiotic = antibiotic;

    auto regionRadius = findValue->second->at(circleIndex).Region.Radius * m_CalibratorRepo->GetCurrentCoefficient();

    if(regionRadius < findValue->second->at(circleIndex).SelectedAntibiotic.StdRange.Min)
    {
        findValue->second->at(circleIndex).SelectedAntibiotic.Status = AntibuticStatus::R;
    }
    else if(regionRadius > findValue->second->at(circleIndex).SelectedAntibiotic.StdRange.Max)
    {
        findValue->second->at(circleIndex).SelectedAntibiotic.Status = AntibuticStatus::S;
    }
    else
    {
        findValue->second->at(circleIndex).SelectedAntibiotic.Status = AntibuticStatus::I;
    }

    auto minMaxUpdated = createIndex(circleIndex, 3);
    emit dataChanged(minMaxUpdated, minMaxUpdated);

    auto sirUpdated = createIndex(circleIndex, 3);
    emit dataChanged(sirUpdated, sirUpdated);

    emit updateRepository();
}

void CircleRepositoryDataModel::SetReport(int circleIndex, bool report)
{
    auto findValue = m_CirclesMap.find(m_CurrentImage);
    if(findValue == m_CirclesMap.end())
    {
        return;
    }

    if(circleIndex == -1)
    {
        return;
    }

    findValue->second->at(circleIndex).Report = report;
}

QString CircleRepositoryDataModel::GetAntibutic(int circleIndex)
{
    auto findValue = m_CirclesMap.find(m_CurrentImage);
    if(findValue == m_CirclesMap.end())
        return {};

    if(circleIndex == -1)
        return {};

    return QString(findValue->second->at(circleIndex).SelectedAntibiotic.Name.c_str());
}

void CircleRepositoryDataModel::ChangeCalibrator()
{
    emit endResetModel();
}

double CircleRepositoryDataModel::GetData(int count, int circleType, int role) const
{
    auto findValue = m_CirclesMap.find(m_CurrentImage);
    if(findValue == m_CirclesMap.end())
        return -1;

    switch(role) {
    case 0:
        if(circleType == Disk)
        {
            return findValue->second->at(count).Disk.Center.X;
        }
        else if(circleType == Region)
        {
            return findValue->second->at(count).Region.Center.X;
        }
    case 1:
        if(circleType == Disk)
        {
            return findValue->second->at(count).Disk.Center.Y;
        }
        else if(circleType == Region)
        {
            return findValue->second->at(count).Region.Center.Y;
        }
    case 2:
        if(circleType == Disk)
        {
            return findValue->second->at(count).Disk.Radius;
        }
        else if(circleType == Region)
        {
            return findValue->second->at(count).Region.Radius;
        }
    case 3:
        return findValue->second->at(count).SelectedAntibiotic.Status;
    default:
        return -1.0;
    }
}

void CircleRepositoryDataModel::SetRegionRadius(int count, double radius)
{
    auto findValue = m_CirclesMap.find(m_CurrentImage);
    if(findValue == m_CirclesMap.end())
    {
        return;
    }
    findValue->second->at(count).Region.Radius = radius;
    Update();
}

void CircleRepositoryDataModel::TranslateCircularTarget(int count, int x, int y)
{
    auto findValue = m_CirclesMap.find(m_CurrentImage);
    if(findValue == m_CirclesMap.end())
    {
        return;
    }
    findValue->second->at(count).Disk.Center.X = x;
    findValue->second->at(count).Disk.Center.Y = y;
    findValue->second->at(count).Region.Center.X = x;
    findValue->second->at(count).Region.Center.Y = y;
    Update();
}

int CircleRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    auto findValue = m_CirclesMap.find(m_CurrentImage);
    if(findValue == m_CirclesMap.end())
        return 0;

    if(!findValue->second)
        return 0;

    return static_cast<int>(findValue->second->size());
}

QVariant CircleRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    auto findValue = m_CirclesMap.find(m_CurrentImage);

    if(findValue == m_CirclesMap.end())
        QVariant();

    if(!findValue->second)
        return QVariant();

    if(findValue->second->empty())
        return QVariant();

    int row = index.row();
    if(row < 0 || row >= findValue->second->size())
        return QVariant();

    switch(role) {
        case Radius:
        {
            std::stringstream strStream;
            auto calibCoefficient = m_CalibratorRepo->GetCurrentCoefficient();
            strStream << std::fixed << std::setprecision(2) << findValue->second->at(row).Region.Radius * calibCoefficient;
            return QString(strStream.str().c_str());
        }
        case MinMax:
        {
            auto range = findValue->second->at(row).SelectedAntibiotic.StdRange;
            std::stringstream sstream;
            if(range.Min != -1 && range.Max != -1)
            {
                sstream << range.Min << "-" <<range.Max;
            }
            else
            {
                sstream << "";
            }
            return QString(sstream.str().c_str());
        }
        case SIR:
        {
            auto antibuticStatus = findValue->second->at(row).SelectedAntibiotic.Status;
            switch (antibuticStatus) {
            case AntibuticStatus::S:
                return QString("S");
                break;
            case AntibuticStatus::I:
                return QString("I");
                break;
            case AntibuticStatus::R:
                return QString("R");
                break;
            default:
                return QString("");
                break;
            }
        }
    case(Report):
    {
        return findValue->second->at(row).Report;
    }
    }

    return QVariant();
}

void CircleRepositoryDataModel::Update()
{
    emit endResetModel();
    updateRepository();
}

QHash<int, QByteArray> CircleRepositoryDataModel::roleNames() const
{
    return m_CircleRoleNames;
}
