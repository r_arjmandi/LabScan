#include "Headers/Framework/ExcelFilePrinter.h"

ExcelFilePrinter::ExcelFilePrinter(const std::string& excelFilePath)
{
    m_ExcelFilePath = QString(excelFilePath.c_str());
}

void ExcelFilePrinter::Print()
{
    QStringList argument;
    argument << m_ExcelFilePath;
    m_Process.start(m_PrinterProgramPath, argument);
    m_Process.setProcessChannelMode(QProcess::MergedChannels);
    m_Process.waitForStarted();
    m_Process.waitForFinished();
}
