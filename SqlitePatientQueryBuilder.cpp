#include "Headers/Framework/SqlitePatientQueryBuilder.h"

#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Framework/IDatabaseCommandFactory.h"

SqlitePatientQueryBuilder::SqlitePatientQueryBuilder(std::shared_ptr<IDatabaseCommandFactory> commandFactory)
    :m_CommandFactory(commandFactory)
{
}

std::shared_ptr<IDatabaseCommand> SqlitePatientQueryBuilder::GetCreateTableIfNotExistsQuery() const
{
	auto dbCommand = m_CommandFactory->CreateCommand();
	dbCommand->SetCommandString(
		"CREATE TABLE IF NOT EXISTS Table_PatientData "
        "(ID TEXT PRIMARY KEY NOT NULL, "
        "ModifyDateTime INTEGER, "
        "Name TEXT, "
        "FamilyName TEXT, "
        "Age INTEGER, "
        "Gender INTEGER, "
        "LabOperator INTEGER, "
        "SampleBacteria INTEGER, "
        "SampleCount TEXT, "
        "SampleGram INTEGER, "
        "SampleIsolated INTEGER, "
        "SampleShape INTEGER, "
        "SampleStandard INTEGER, "
        "SampleType INTEGER, "
        "CommentDescription TEXT, "
        "CommentSubjectIndex INTEGER, "
        "PlateMediaIndex INTEGER)");
	return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqlitePatientQueryBuilder::GetInsertQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();

    dbCommand->SetCommandString(
        "INSERT INTO "
        "Table_PatientData (ID, ModifyDateTime, Name, FamilyName, Age, Gender, LabOperator, SampleBacteria, SampleCount, SampleGram, "
        "SampleIsolated, SampleShape, SampleStandard, SampleType, CommentDescription, CommentSubjectIndex, PlateMediaIndex) "
        "VALUES(:ID, :ModifyDateTime, :Name, :FamilyName, :Age, :Gender, :LabOperator, :SampleBacteria, :SampleCount, :SampleGram, "
        ":SampleIsolated, :SampleShape, :SampleStandard, :SampleType, :CommentDescription, :CommentSubjectIndex, :PlateMediaIndex)");

    dbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("ModifyDateTime", ":ModifyDateTime", IDatabaseCommand::ParameterType::DateTime);
    dbCommand->AddParameter("Name", ":Name", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("FamilyName", ":FamilyName", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("Age", ":Age", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("Gender", ":Gender", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("LabOperator", ":LabOperator", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleBacteria", ":SampleBacteria", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleCount", ":SampleCount", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("SampleGram", ":SampleGram", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleIsolated", ":SampleIsolated", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleShape", ":SampleShape", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleStandard", ":SampleStandard", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleType", ":SampleType", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("CommentDescription", ":CommentDescription", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("CommentSubjectIndex", ":CommentSubjectIndex", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("PlateMediaIndex", ":PlateMediaIndex", IDatabaseCommand::ParameterType::Integer);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqlitePatientQueryBuilder::GetRowCountQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "SELECT COUNT(Table_PatientData.ID) "
        "FROM Table_PatientData "
        "WHERE :ID = ID");
    dbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqlitePatientQueryBuilder::GetUpdateQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("UPDATE Table_PatientData SET "
        "ID = :ID,"
        "ModifyDateTime = :ModifyDateTime, "
        "Name = :Name, "
        "FamilyName = :FamilyName, "
        "Age = :Age, "
        "Gender = :Gender,"
        "LabOperator = :LabOperator,"
        "SampleBacteria = :SampleBacteria, "
        "SampleCount = :SampleCount, "
        "SampleGram = :SampleGram, "
        "SampleIsolated = :SampleIsolated, "
        "SampleShape = :SampleShape, "
        "SampleStandard = :SampleStandard, "
        "SampleType = :SampleType, "
        "CommentDescription = :CommentDescription, "
        "CommentSubjectIndex = :CommentSubjectIndex, "
        "PlateMediaIndex = :PlateMediaIndex "
        "WHERE ID = :ID ");
    dbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("ModifyDateTime", ":ModifyDateTime", IDatabaseCommand::ParameterType::DateTime);
    dbCommand->AddParameter("Name", ":Name", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("FamilyName", ":FamilyName", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("Age", ":Age", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("Gender", ":Gender", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("LabOperator", ":LabOperator", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleBacteria", ":SampleBacteria", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleCount", ":SampleCount", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("SampleGram", ":SampleGram", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleIsolated", ":SampleIsolated", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleShape", ":SampleShape", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleStandard", ":SampleStandard", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("SampleType", ":SampleType", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("CommentDescription", ":CommentDescription", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("CommentSubjectIndex", ":CommentSubjectIndex", IDatabaseCommand::ParameterType::Integer);
    dbCommand->AddParameter("PlateMediaIndex", ":PlateMediaIndex", IDatabaseCommand::ParameterType::Integer);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqlitePatientQueryBuilder::GetSelectAllQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("SELECT "
        "ID, ModifyDateTime, Name, FamilyName "
        "FROM Table_PatientData");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqlitePatientQueryBuilder::GetSelectQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("SELECT "
        "ID, ModifyDateTime, Name, FamilyName, Age, Gender, LabOperator, SampleBacteria, "
        "SampleCount, SampleGram, SampleIsolated, SampleShape, "
        "SampleStandard, SampleType, CommentDescription, CommentSubjectIndex, PlateMediaIndex "
        "FROM Table_PatientData "
        "WHERE ID = :ID ");
    dbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}
