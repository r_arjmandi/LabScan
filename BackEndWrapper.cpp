#include "Headers/Framework/BackEndWrapper.h"

#include "Headers/BusinessEntities/PatientData.h"
#include "Headers/Adapters/IFrontEndAdapter.h"
#include "Headers/Adapters/ICurrentPatientGui.h"

BackEndWrapper::BackEndWrapper(
        std::shared_ptr<IFrontEndAdapter> frontEndAdapter,
        std::shared_ptr<ICurrentPatientGui> currentPatient) :
    QObject(nullptr),
    m_Adapter(frontEndAdapter),
    m_CurrentPatient(currentPatient)
{
}

void BackEndWrapper::AnalyseImage(double diameter, int count)
{
    m_Adapter->DetectImageCircles(diameter, count);
}

void BackEndWrapper::ShuffleImageCircles()
{
    m_Adapter->ShuffleImageCircles();
}

void BackEndWrapper::SortCurrentImageCirclesClockwise()
{
    m_Adapter->SortCurrentImageCirclesClockwise();
}

void BackEndWrapper::SortCurrentImageCirclesCenterClockwise()
{
    m_Adapter->SortCurrentImageCirclesCenterClockwise();
}

void BackEndWrapper::SortCurrentImageCirclesCounterClockwise()
{
    m_Adapter->SortCurrentImageCirclesCounterClockwise();
}

void BackEndWrapper::SortCurrentImageCirclesCenterCounterClockwise()
{
    m_Adapter->SortCurrentImageCirclesCenterCounterClockwise();
}

void BackEndWrapper::SaveCurrentPatient()
{
    m_Adapter->InsertOrUpdateCurrentPatient();
}

void BackEndWrapper::LoadPatient(const QString &patientUid)
{
    m_Adapter->LoadPatient(patientUid.toStdString());
}

void BackEndWrapper::PrintCurrentPatient()
{
    m_Adapter->PrintCurrentPatient();
}
