#include "Headers/Framework/AntibuticRepositoryDataModel.h"
#include "Headers/BusinessEntities/Range.h"
#include "Headers/BusinessEntities/Antibiotic.h"

AntibuticRepositoryDataModel::AntibuticRepositoryDataModel()
{
    m_AntibuticRoleNames[Name] = "Name";
}

int AntibuticRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_AntibuticList.size());
}

QVariant AntibuticRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(m_AntibuticList.empty())
    {
        return QVariant();
    }

    int row = index.row();
    if(row < 0 || row >= m_AntibuticList.size())
    {
        return QVariant();
    }

    switch(role) {
        case Name:
            return QString("%1(%2)").arg(m_AntibuticList[row].Name.c_str(), m_AntibuticList[row].Abbreviation.c_str());
    }

    return QVariant();
}

void AntibuticRepositoryDataModel::SetAntibuticList(const std::vector<Antibiotic>&& antibioticList)
{
    m_AntibuticList = std::move(antibioticList);
    emit endResetModel();
}

QString AntibuticRepositoryDataModel::GetData(int index) {
    if(m_AntibuticList.empty())
    {
        return "";
    }

    if(index < 0 || index >= m_AntibuticList.size())
    {
        return "";
    }

    return QString("%1(%2)").arg(m_AntibuticList[index].Name.c_str(), m_AntibuticList[index].Abbreviation.c_str());
}

Antibiotic AntibuticRepositoryDataModel::GetAntibioticByIndex(int index) const
{
    return m_AntibuticList.at(index);
}

QHash<int, QByteArray> AntibuticRepositoryDataModel::roleNames() const
{
    return m_AntibuticRoleNames;
}
