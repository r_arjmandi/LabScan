#include "Headers/Adapters/CommentDataAccess.h"

#include "Headers/Adapters/ConnectionGuard.h"
#include "Headers/Adapters/ICommentQueryBuilder.h"
#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Adapters/IDatabaseConnection.h"
#include "Headers/Adapters/IQueryResult.h"
#include "Headers/BusinessEntities/CommentData.h"

CommentDataAccess::CommentDataAccess(
        std::shared_ptr<IDatabaseConnection> dbConnection,
        std::shared_ptr<ICommentQueryBuilder> commentQueryBuilder):
    m_DatabaceConnection(dbConnection),
    m_CommentQueryBuilder(commentQueryBuilder)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto createTableCommand = m_CommentQueryBuilder->GetCreateTableIfNotExistsQuery();
    m_DatabaceConnection->Exec(createTableCommand);
}

void CommentDataAccess::InsertOrUpdate(std::unique_ptr<CommentData> comment)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto rowCountCommand = m_CommentQueryBuilder->GetRowCountQuery();
    rowCountCommand->BindParameter("Subject", comment->Subject);
    auto queryResult = m_DatabaceConnection->Exec(rowCountCommand);

    if (queryResult->GetInt(0) == 0)
    {
        auto insertCommand = m_CommentQueryBuilder->GetInsertQuery();
        insertCommand->BindParameter("Subject", comment->Subject);
        insertCommand->BindParameter("Description", comment->Description);
        m_DatabaceConnection->Exec(insertCommand);
    }
    else
    {
        auto updateCommand = m_CommentQueryBuilder->GetUpdateQuery();
        updateCommand->BindParameter("Subject", comment->Subject);
        updateCommand->BindParameter("Description", comment->Description);
        m_DatabaceConnection->Exec(updateCommand);
    }
}

void CommentDataAccess::Delete(const std::string &subject)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto deleteCommand = m_CommentQueryBuilder->GetDeleteQuery();
    deleteCommand->BindParameter("Subject", subject);
    m_DatabaceConnection->Exec(deleteCommand);
}

std::map<std::string, std::string> CommentDataAccess::GetAllComments()
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto selectAllCommand = m_CommentQueryBuilder->GetSelectAllQuery();
    auto queryResult = m_DatabaceConnection->Exec(selectAllCommand);

    auto commentList = std::map<std::string, std::string>();

    while(queryResult->HasData())
    {
        auto subject = queryResult->GetString(0);
        auto description = queryResult->GetString(1);
        commentList.insert_or_assign(subject, description);
        queryResult->Next();
    }
    return std::move(commentList);
}
