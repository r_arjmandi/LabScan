#include "Headers/Framework/DatabaseCommandFactory.h"
#include "Headers/Adapters/DatabaseCommand.h"

DatabaseCommandFactory::DatabaseCommandFactory()
{

}

std::shared_ptr<IDatabaseCommand> DatabaseCommandFactory::CreateCommand()
{
    auto dbCommand = std::make_shared<DatabaseCommand>();
	return dbCommand;
}
