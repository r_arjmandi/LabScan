#include "Headers/Framework/OpenCvHoughCircleWrapper.h"
#include "Headers/BusinessEntities/Circle.h"

std::vector<Circle> OpenCvHoughCircleWrapper::DetectCircles(
        const std::experimental::filesystem::path& imageAddress,
        double minRadius, double maxRadius, int count)
{
    auto imageMat = cv::imread(imageAddress.string(), 0);
    if(imageMat.empty())
    {
        return {};
    }
    auto ratio = 400.0 / imageMat.cols;
    cv::resize(imageMat, imageMat, cv::Size(400, imageMat.rows * ratio));

    std::ifstream configFile("config.txt");
    int kernel, dp, minDist, canny, accThresh;
    configFile >> kernel >> dp >> minDist >> canny >> accThresh;

    //cv::medianBlur(imageMat, imageMat, kernel);
    std::vector<cv::Vec3f> circles;

    //3 20 1 5 12 18 7
    cv::HoughCircles(imageMat, circles, cv::HOUGH_GRADIENT, dp, minDist, canny, accThresh, minRadius * ratio, maxRadius * ratio);
    auto results = std::vector<Circle>();
    int counter = 0;
    //auto imageOrginal = cv::imread(imageAddress.string());
    for(const auto& circle : circles)
    {
        if(counter == count)
            break;
        Circle newCircle;
        newCircle.Center.X = circle[0] / ratio;
        newCircle.Center.Y = circle[1] / ratio;
        newCircle.Radius = circle[2] / ratio;
        results.push_back(newCircle);
        auto center = cv::Point(newCircle.Center.X, newCircle.Center.Y);
        //cv::circle(imageOrginal, center, newCircle.Radius, cv::Scalar(0, 0, 255), 3, 8, 0);
        counter++;
    }
    //cv::imwrite("output.jpg", imageOrginal);
    return std::move(results);
}
