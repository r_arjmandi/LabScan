#include "Headers/Framework/MediaRepositoryDataModel.h"
#include "Headers/Adapters/IFrontEndAdapter.h"

MediaRepositoryDataModel::MediaRepositoryDataModel()
{
    m_MediaRoleNames[Name] = "Name";
}

void MediaRepositoryDataModel::SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter)
{
    m_adapter = adapter;
}

int MediaRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_MediaList.size());
}

QVariant MediaRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(m_MediaList.empty())
    {
        return QVariant();
    }
    int row = index.row();
    if(row < 0 || row >= m_MediaList.size())
    {
        return QVariant();
    }
    switch(role) {
        case Name:
        {
            auto media = m_MediaList.begin();
            for(auto i = 0; i < row; i++)
            {
                media++;
            }
            return QString(media->c_str());
        }
    }
    return QVariant();
}

void MediaRepositoryDataModel::SetMediaList(std::set<std::string>&& mediaList)
{
    m_MediaList = std::move(mediaList);
    emit endResetModel();
}

void MediaRepositoryDataModel::AddNewMedia(const QString &media)
{
    if(media != "")
    {
        m_MediaList.insert(media.toStdString());
        m_adapter->InsertOrUpdateMedia(media.toStdString());
        emit endResetModel();
    }
}

void MediaRepositoryDataModel::Delete(const QString &media)
{
    if(media != "")
    {
        auto findValue = m_MediaList.find(media.toStdString());
        if(findValue == m_MediaList.end())
        {
            return;
        }
        m_MediaList.erase(findValue);
        m_adapter->DeleteMedia(media.toStdString());
        emit endResetModel();
    }
}

QHash<int, QByteArray> MediaRepositoryDataModel::roleNames() const
{
    return m_MediaRoleNames;
}
