#include "Headers/Adapters/BackEndAdapter.h"

#include "Headers/Adapters/IPatientListGui.h"
#include "Headers/BusinessEntities/IPatientData.h"
#include "Headers/Adapters/ICurrentPatientGui.h"

BackEndAdapter::BackEndAdapter(
        std::shared_ptr<IPatientListGui> patientListGui,
        std::shared_ptr<ICurrentPatientGui> currentPatientGui):
    m_PatientListGui(patientListGui),
    m_CurrentPatientGui(currentPatientGui)
{
}

void BackEndAdapter::UpdatePatientListRepresentation(
        std::vector<std::weak_ptr<PatientShortInformation>>&& patientInfoList)
{
    m_PatientListGui->SetPatientDataList(std::move(patientInfoList));
}

void BackEndAdapter::UpdateCurrentPatientRepresentation(std::weak_ptr<IPatientData> currentPatientData)
{
    m_CurrentPatientGui->SetCurrenaPatientData(currentPatientData);
    m_CurrentPatientGui->Update();
}
