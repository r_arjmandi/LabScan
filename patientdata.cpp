#include "Headers/BusinessEntities/PatientData.h"

std::vector<std::weak_ptr<std::experimental::filesystem::path> > PatientData::GetImages() const
{
    auto result = std::vector<std::weak_ptr<std::experimental::filesystem::path>>();
    for(auto& image : m_Images)
    {
        result.push_back(std::weak_ptr<std::experimental::filesystem::path>(image));
    }
    return result;
}

std::string PatientData::GetId() const
{
    return m_Id;
}

std::string PatientData::GetName() const
{
    return m_Name;
}

std::string PatientData::GetFamilyName() const
{
    return m_FamilyName;
}

int PatientData::GetAge() const
{
    return m_Age;
}

GenderType PatientData::GetGender() const
{
    return m_Gender;
}

int PatientData::GetLabOperator() const
{
    return m_LabOperator;
}

SampleData PatientData::GetSample() const
{
    return m_Sample;
}

CommentData PatientData::GetComment() const
{
    return m_Comment;
}

int PatientData::GetPlateMediaIndex() const
{
    return m_PlateMediaIndex;
}

std::weak_ptr<std::experimental::filesystem::path> PatientData::GetCurrentImage() const
{
    if(m_CurrentImageIndex < 0 || m_CurrentImageIndex > m_Images.size())
    {
        return{};
    }

    return m_Images.at(m_CurrentImageIndex);
}

DateTime PatientData::GetModifyDateTime() const
{
    return m_ModifyDateTime;
}

void PatientData::SetId(const std::string &id)
{
    m_Id = id;
}

void PatientData::SetName(const std::string &name)
{
    m_Name = name;
}

void PatientData::SetFamilyName(const std::string &familyName)
{
    m_FamilyName = familyName;
}

void PatientData::SetAge(int age)
{
    m_Age = age;
}

void PatientData::SetGender(const GenderType &genderType)
{
    m_Gender = genderType;
}

void PatientData::SetLabOperator(int labOperator)
{
    m_LabOperator = labOperator;
}

void PatientData::SetSample(const SampleData &sampleData)
{
    m_Sample = sampleData;
}

void PatientData::SetComment(const CommentData &commentData)
{
    m_Comment = commentData;
}

void PatientData::SetPlateMediaIndex(int plateMediaIndex)
{
    m_PlateMediaIndex = plateMediaIndex;
}

void PatientData::SetImages(
        std::vector<std::unique_ptr<std::experimental::filesystem::path>>&& images)
{
    for(auto& image : images)
    {
        m_Images.push_back(std::shared_ptr<std::experimental::filesystem::path>(std::move(image)));
    }
}

void PatientData::SetCurrentImage(int imageIndex)
{
    m_CurrentImageIndex = imageIndex;
}

void PatientData::AddImage(std::unique_ptr<std::experimental::filesystem::path> image)
{
    auto sharedImage = std::shared_ptr<std::experimental::filesystem::path>(std::move(image));
    m_Images.push_back(sharedImage);
}

void PatientData::SetModifyDateTime(DateTime dateTime)
{
    m_ModifyDateTime = dateTime;
}

void PatientData::ClearImages()
{
    m_Images.clear();
}
