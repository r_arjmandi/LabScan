#include "Headers/Framework/PatientListDataModel.h"
#include "Headers/BusinessEntities/PatientShortInformation.h"
#include "Headers/BusinessEntities/IDateTimeOperation.h"

PatientListDataModel::PatientListDataModel(std::shared_ptr<IDateTimeOperation> dateTimeOperation):
    m_DateTimeOperation(dateTimeOperation)
{
    m_PatientRoleNames[ID] = "ID";
    m_PatientRoleNames[Name] = "Name";
    m_PatientRoleNames[FamilyName] = "FamilyName";
    m_PatientRoleNames[ModifyDateTime] = "ModifyDateTime";
}

int PatientListDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_PatientInfoList.size());
}

QVariant PatientListDataModel::data(const QModelIndex &index, int role) const
{
    if(m_PatientInfoList.empty())
    {
        return QVariant();
    }

    int row = index.row();
    if(row < 0 || row >= m_PatientInfoList.size())
    {
        return QVariant();
    }

    const auto& patientInfo = m_PatientInfoList.at(row).lock();

    if(!patientInfo)
    {
        return QVariant();
    }

    switch(role) {
    case ID:
        return QString(patientInfo->Id.c_str());
    case Name:
        return QString(patientInfo->Name.c_str());
    case FamilyName:
        return QString(patientInfo->FamilyName.c_str());
    case ModifyDateTime:
        auto dateTimeStr = m_DateTimeOperation->ToString(patientInfo->ModifyDateTime);
        auto dateStr = dateTimeStr.substr(0, 10);
        return QString(dateStr.c_str());
    }
    return QVariant();
}

QHash<int, QByteArray> PatientListDataModel::roleNames() const
{
    return m_PatientRoleNames;
}

void PatientListDataModel::SetPatientDataList(std::vector<std::weak_ptr<PatientShortInformation>>&&
                                              patientInfoList)
{
    m_PatientInfoList = std::move(patientInfoList);
    emit endResetModel();
}
