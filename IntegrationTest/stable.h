#if defined __cplusplus

#include <memory>
#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <cctype>
#include <sstream>
#include <iomanip>

#include <boost/variant.hpp>
#include <boost/filesystem.hpp>

#endif

#include "sqlite3.h"
