TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    SqliteDataBaseConnectionTest.cpp \
    ..\SqliteDatabaseConnection.cpp \
    ..\SqliteDatabaseException.cpp \
    ..\SqlitePatientQueryBuilder.cpp \
    ..\DateTime.cpp \
    PatientQueryBuilderTest.cpp \
    SqliteQueryResultTest.cpp \
    ..\SqliteQueryResult.cpp \
    DateTimeOperationInegrationTest.cpp \
    ..\DateTimeOperation.cpp

win32:CONFIG(release, debug|release): LIBS += -L"C:\Program Files\Boost\lib" -llibboost_unit_test_framework-vc140-mt-1_62
else:win32:CONFIG(debug, debug|release): LIBS += -L"C:\Program Files\Boost\lib" -llibboost_unit_test_framework-vc140-mt-gd-1_62

win32:CONFIG(release, debug|release): LIBS += -L"C:\Program Files\Sqlite3\lib.Debug" -lSqlite3
else:win32:CONFIG(debug, debug|release): LIBS += -L"C:\Program Files\Sqlite3\lib.Release" -lSqlite

INCLUDEPATH += "C:\Program Files\Boost\include\boost-1_62" "C:\Program Files\Boost\include" \
    ".." "C:\Program Files\Sqlite3\include"

PRECOMPILED_HEADER = "stable.h"

HEADERS += ../Headers/Adapters/MockQueryResult.h \
            ../Framework/SqliteDataBaseConnection.h \
    DateTimeMatcher.h

