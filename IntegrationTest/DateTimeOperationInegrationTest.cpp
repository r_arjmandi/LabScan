#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "DateTimeMatcher.h"
#include "../Headers/BusinessEntities/DateTimeOperation.h"

struct DateTimeOperationIntegrationFixture
{
	DateTimeOperationIntegrationFixture()
	{
		m_DateTimeOperation = std::make_shared<DateTimeOperation>();
	}

	void BoostCheckEqualDateTimeWithFromEpochReturned(const DateTime & dateTimeValue, std::time_t EpochValue)
	{
		auto resultDateTime = m_DateTimeOperation->FromEpoch(EpochValue);
		BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, dateTimeValue));
	}

	void BoostCheckEqualEpochWithToEpochReturned(std::time_t EpochValue, const DateTime & dateTimeValue)
	{
		auto resultEpoch = m_DateTimeOperation->ToEpoch(dateTimeValue);
		BOOST_CHECK_EQUAL(resultEpoch, EpochValue); //Epoch value get from internet
	}

	void CheckDateTimeOperationEncodeAndDecode(const DateTime& originalDateTime)
	{
		DateTimeOperation codec1, codec2;
		auto encoded = codec1.EncodeInt64(originalDateTime);
		auto decodedDateTime = codec2.DecodeInt64(encoded);
		BOOST_CHECK(DateTimeMatcher::IsEqual(decodedDateTime, originalDateTime));
	}

	std::shared_ptr<DateTimeOperation> m_DateTimeOperation;
};

BOOST_FIXTURE_TEST_SUITE(DateTimeOperationTestSuite, DateTimeOperationIntegrationFixture)

BOOST_AUTO_TEST_CASE(WhenDecodeInt64FunctionIsCalledWithALongIntValue1_ThenMustRetrievedCorrectResult)
{
	auto resultDateTime = m_DateTimeOperation->DecodeInt64(8663221798946);
	BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, DateTime(2017, 1, 1, 1, 1, 1)));
}

BOOST_AUTO_TEST_CASE(WhenDecodeInt64FunctionIsCalledWithALongIntValue2_ThenMustRetrievedCorrectResult)
{
	auto resultDateTime = m_DateTimeOperation->DecodeInt64(0);
	BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, DateTime(0, 0, 0, 0, 0, 0)));
}

BOOST_AUTO_TEST_CASE(WhenDecodeInt64FunctionIsCalledWithALongIntValue3_ThenMustRetrievedCorrectResult)
{
	auto resultDateTime = m_DateTimeOperation->DecodeInt64(503066456234);
	BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, DateTime(117, 2, 4, 12, 30, 5)));
}

BOOST_AUTO_TEST_CASE(WhenDecodeInt64FunctionIsCalledWithALongIntValue4_ThenMustRetrievedCorrectResult)
{
	auto resultDateTime = m_DateTimeOperation->DecodeInt64(503066456251);
	BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, DateTime(117, 2, 4, 12, 30, 5.55)));
}

BOOST_AUTO_TEST_CASE(GivenAnEncodedDateTimeValue_WhenTheValueIsDecodedUsingDateTimeOps_TheOriginalDateTimeValueMustBeRetrieved)
{
	CheckDateTimeOperationEncodeAndDecode(DateTime(63, 1, 3, 7, 15, 31));
}

BOOST_AUTO_TEST_CASE(GivenAnEncodedDateTimeValue2_WhenTheValueIsDecodedUsingDateTimeOps_TheOriginalDateTimeValueMustBeRetrieved)
{
	CheckDateTimeOperationEncodeAndDecode(DateTime(0, 0, 0, 0, 0, 0));
}

BOOST_AUTO_TEST_CASE(GivenAnEncodedDateTimeValue3_WhenTheValueIsDecodedUsingDateTimeOps_TheOriginalDateTimeValueMustBeRetrieved)
{
	CheckDateTimeOperationEncodeAndDecode(DateTime(117, 2, 4, 12, 30, 5.55));
}

BOOST_AUTO_TEST_CASE(GivenAnEncodedDateTimeValue4_WhenTheValueIsDecodedUsingDateTimeOps_TheOriginalDateTimeValueMustBeRetrieved)
{
	CheckDateTimeOperationEncodeAndDecode(DateTime(11, 12, 31, 23, 59, 59.59));
}

BOOST_AUTO_TEST_CASE(GivenAnEncodedDateTimeValue5_WhenTheValueIsDecodedUsingDateTimeOps_TheOriginalDateTimeValueMustBeRetrieved)
{
	CheckDateTimeOperationEncodeAndDecode(DateTime(1969, 12, 31, 23, 59, 59.59));
}

BOOST_AUTO_TEST_CASE(GivenAnEncodedDateTimeValue6_WhenTheValueIsDecodedUsingDateTimeOps_TheOriginalDateTimeValueMustBeRetrieved)
{
	CheckDateTimeOperationEncodeAndDecode(DateTime(1970, 1, 1, 0, 0, 0.59));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue1_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 1, 1, 1, 1, 1), 1483219861);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime1_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1483219861, DateTime(2017, 1, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue2_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 2, 1, 1, 1, 1), 1485898261);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime2_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1485898261, DateTime(2017, 2, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue3_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 3, 1, 1, 1, 1), 1488317461);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime3_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1488317461, DateTime(2017, 3, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue4_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 4, 1, 1, 1, 1), 1490992261);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime4_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1490992261, DateTime(2017, 4, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue5_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 5, 1, 1, 1, 1), 1493584261);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime5_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1493584261, DateTime(2017, 5, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue6_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 6, 1, 1, 1, 1), 1496262661);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime6_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1496262661, DateTime(2017, 6, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue7_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 7, 1, 1, 1, 1), 1498854661);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime7_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1498854661, DateTime(2017, 7, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue8_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 8, 1, 1, 1, 1), 1501533061);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime8_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1501533061, DateTime(2017, 8, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue9_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 9, 1, 1, 1, 1), 1504211461);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime9_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1504211461, DateTime(2017, 9, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue10_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 10, 1, 1, 1, 1), 1506807061);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime10_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1506807061, DateTime(2017, 10, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue11_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 11, 1, 1, 1, 1), 1509485461);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime11_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1509485461, DateTime(2017, 11, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenFromEpochFunctionIsCalledWithAEpochvalue12_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualDateTimeWithFromEpochReturned(DateTime(2017, 12, 1, 1, 1, 1), 1512077461);
}

BOOST_AUTO_TEST_CASE(WhenToEpochFunctionIsCalledWithADateTime12_ThenMustRetrievedCorrectResult)
{
	BoostCheckEqualEpochWithToEpochReturned(1512077461, DateTime(2017, 12, 1, 1, 1, 1));
}

BOOST_AUTO_TEST_CASE(WhenToStringFunctionIsCalledWithADateTime_ThenMustRetrievedCorrectResult)
{
	DateTime dateTimeValue(2017, 12, 1, 1, 1, 1);
	auto resultStr = m_DateTimeOperation->ToString(dateTimeValue);
	BOOST_CHECK_EQUAL(resultStr, std::string("2017-12-01 01:01:01"));
}

BOOST_AUTO_TEST_CASE(WhenToStringFunctionIsCalledWithAVeryOldDateTime_ThenMustRetrievedCorrectResult)
{
	DateTime dateTimeValue(117, 2, 4, 7, 8, 9);
	auto resultStr = m_DateTimeOperation->ToString(dateTimeValue);
	BOOST_CHECK_EQUAL(resultStr, std::string("0117-02-04 07:08:09"));
}

BOOST_AUTO_TEST_CASE(WhenFromStringFunctionIsCalledWithADateTimeString_ThenMustRetrievedCorrectResult)
{
	DateTime dateTimeValue(2017, 12, 1, 1, 1, 1);
	auto resultDateTime = m_DateTimeOperation->FromString("2017-12-01 01:01:01");
	BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, dateTimeValue));
}

BOOST_AUTO_TEST_CASE(WhenFromStringFunctionIsCalledWithADateTimeString_ThenMustRetrievedCorrectResult2)
{
	DateTime dateTimeValue(2017, 12, 1, 13, 10, 20);
	auto resultDateTime = m_DateTimeOperation->FromString("2017-12-01 13:10:20");
	BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, dateTimeValue)); 
}

BOOST_AUTO_TEST_CASE(WhenFromStringFunctionIsCalledWithADateTimeStringYear117_ThenMustRetrievedCorrectResult2)
{
	DateTime dateTimeValue(117, 12, 1, 13, 10, 20);
	auto resultDateTime = m_DateTimeOperation->FromString("0117-12-01 13:10:20");
	BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, dateTimeValue)); 
}

BOOST_AUTO_TEST_CASE(WhenFromStringFunctionIsCalledWithADateTimeStringYear0000_ThenMustRetrievedCorrectResult2)
{
	DateTime dateTimeValue(0, 12, 1, 13, 10, 20);
	auto resultDateTime = m_DateTimeOperation->FromString("0000-12-01 13:10:20");
	BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, dateTimeValue)); 
}

BOOST_AUTO_TEST_CASE(WhenToStringFunctionIsCalledWithAVeryOldDateTimeAllZero_ThenMustRetrievedCorrectResult)
{
	DateTime dateTimeValue(0, 0, 0, 0, 0, 0);
	auto resultStr = m_DateTimeOperation->ToString(dateTimeValue);
	BOOST_CHECK_EQUAL(resultStr, std::string("0000-00-00 00:00:00")); 
}

BOOST_AUTO_TEST_CASE(WhenAStringDateTimeOfADifferentFormatIsPassedToFromString_CorrectDateTimeMustBeReturned)
{
	auto convertedDateTime = m_DateTimeOperation->FromString("Mon 1785/2/3, 12:10:30");
	BOOST_CHECK(DateTimeMatcher::IsEqual(convertedDateTime, DateTime(1785, 2, 3, 12, 10, 30)));
}


BOOST_AUTO_TEST_SUITE_END()
