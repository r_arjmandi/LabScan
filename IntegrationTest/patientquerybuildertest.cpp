#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "Headers/Framework/SqlitePatientQueryBuilder.h"
#include "Headers/Framework/DatabaseCommandFactory.h"
#include "Headers/Adapters/MockDatabaseCommand.h"
#include "Headers/Framework/MockDatabaseCommandFactory.h"
#include "Headers/BusinessEntities/PatientData.h"

template <typename T>
class ParameterCapturer
{
public:
    ParameterCapturer(T & capturedParameter) : m_CapturedParameter(capturedParameter) { }
    void operator()(const T & receivedParameter) { m_CapturedParameter = receivedParameter; }

private:
    T & m_CapturedParameter;
};

struct PatientQueryBuilderFixture
{
    PatientQueryBuilderFixture()
    {
        m_TestPatientData1.ID = "testPatientID";
        m_TestPatientData1.Name = "testName";
        m_TestPatientData1.FamilyName = "testFamilyName";
        m_TestPatientData1.Age = 25;
        m_TestPatientData1.Gender = PatientData::GenderType::Male;

        m_TestPatientData1.Sample.Bacteria = "testBacteria";
        m_TestPatientData1.Sample.Count = 10;
        m_TestPatientData1.Sample.Gram = "testGram";
        m_TestPatientData1.Sample.Isolated = "testIsolated";
        m_TestPatientData1.Sample.Shape = "testShape";
        m_TestPatientData1.Sample.Standard = "testStandard";
        m_TestPatientData1.Sample.Type = "testType";

        m_TestPatientData1.Comment.Description = "testCommentDescription";
        m_TestPatientData1.Comment.Subject = "testCommentSubject";

        m_TestPatientData2.ID = "testPatientID2";
        m_TestPatientData2.Name = "testName2";
        m_TestPatientData2.FamilyName = "testFamilyName2";
        m_TestPatientData2.Age = 21;
        m_TestPatientData2.Gender = PatientData::GenderType::Female;

        m_TestPatientData2.Sample.Bacteria = "testBacteria2";
        m_TestPatientData2.Sample.Count = 12;
        m_TestPatientData2.Sample.Gram = "testGram2";
        m_TestPatientData2.Sample.Isolated = "testIsolated2";
        m_TestPatientData2.Sample.Shape = "testShape2";
        m_TestPatientData2.Sample.Standard = "testStandard2";
        m_TestPatientData2.Sample.Type = "testType2";

        m_TestPatientData2.Comment.Description = "testCommentDescription2";
        m_TestPatientData2.Comment.Subject = "testCommentSubject2";

        m_UpdatedPatientData.ID = "testPatientID";
        m_UpdatedPatientData.Name = "changedName";
        m_UpdatedPatientData.FamilyName = "changedFamilyName";
        m_UpdatedPatientData.Age = 29;
        m_UpdatedPatientData.Gender = PatientData::GenderType::Female;

        m_UpdatedPatientData.Sample.Bacteria = "changedBacteria";
        m_UpdatedPatientData.Sample.Count = 20;
        m_UpdatedPatientData.Sample.Gram = "changedGram";
        m_UpdatedPatientData.Sample.Isolated = "changedIsolated";
        m_UpdatedPatientData.Sample.Shape = "changedShape";
        m_UpdatedPatientData.Sample.Standard = "changedStandard";
        m_UpdatedPatientData.Sample.Type = "changedType";

        m_UpdatedPatientData.Comment.Description = "changedCommentDescription";
        m_UpdatedPatientData.Comment.Subject = "changedCommentSubject";
    }

    void RemoveFile(const std::string & filePath)
    {
        if (boost::filesystem::exists(filePath))
            boost::filesystem::remove(filePath);
    }

    ~PatientQueryBuilderFixture()
    {
        RemoveFile(m_DbFilePath);
    }

    void SqliteInsertPatientData(sqlite3 * dbHandle, const PatientData & data)
    {
        sqlite3_stmt *insertStmt = NULL;
        sqlite3_prepare_v2(dbHandle, "INSERT INTO Table_PatientData "
            "(ID, Name, FamilyName,	Age, Gender, SampleBacteria, SampleCount, SampleGram, "
            "SampleIsolated, SampleShape, SampleStandard, SampleType, CommentDescription, "
            "CommentSubject) "
            "VALUES(:ID, :Name, :FamilyName, :Age, :Gender, :SampleBacteria, :SampleCount, "
            ":SampleGram, :SampleIsolated, :SampleShape, :SampleStandard, :SampleType, "
            ":CommentDescription, :CommentSubject)"
            , -1, &insertStmt, NULL);
        sqlite3_bind_text(insertStmt, 1, data.ID.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 2, data.Name.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 3, data.FamilyName.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(insertStmt, 4, data.Age);
        sqlite3_bind_int64(insertStmt, 5, static_cast<long long int>(data.Gender));
        sqlite3_bind_text(insertStmt, 6, data.Sample.Bacteria.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(insertStmt, 7, data.Sample.Count);
        sqlite3_bind_text(insertStmt, 8, data.Sample.Gram.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 9, data.Sample.Isolated.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 10, data.Sample.Shape.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 11, data.Sample.Standard.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 12, data.Sample.Type.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 13, data.Comment.Description.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 14, data.Comment.Subject.c_str(), -1, SQLITE_TRANSIENT);
        sqlite3_step(insertStmt);
        sqlite3_finalize(insertStmt);
    }


    static void SqlExec(sqlite3 * dbHandle, const std::string & cmd)
    {
        sqlite3_stmt *stmt = nullptr;
        BOOST_REQUIRE_EQUAL(sqlite3_prepare_v2(dbHandle, cmd.c_str(), -1, &stmt, NULL), SQLITE_OK);
        BOOST_REQUIRE_EQUAL(sqlite3_step(stmt), SQLITE_DONE);
        BOOST_REQUIRE_EQUAL(sqlite3_finalize(stmt), SQLITE_OK);
    }

    void SqliteCreatePatientDataTable(sqlite3 * dbHandle)
    {
        SqlExec(dbHandle,
            "CREATE TABLE Table_PatientData "
            "(ID TEXT, "
            "Name TEXT,  "
            "FamilyName TEXT, "
            "Age INTEGER, "
            "Gender INTEGER, "
            "SampleBacteria TEXT, "
            "SampleCount INTEGER, "
            "SampleGram TEXT, "
            "SampleIsolated TEXT, "
            "SampleShape TEXT, "
            "SampleStandard TEXT, "
            "SampleType TEXT, "
            "CommentDescription TEXT, "
            "CommentSubject TEXT)");
    }

    static void BindSqliteStmtParamText(sqlite3_stmt* stmt, const std::string& paramName, const std::string& value)
    {
        auto index = sqlite3_bind_parameter_index(stmt, paramName.c_str());
        BOOST_REQUIRE_EQUAL(sqlite3_bind_text(stmt, index, value.c_str(), -1, SQLITE_TRANSIENT), SQLITE_OK);
    }

    static void BoostCheckCurrentStatementColumnTextEqualToString(sqlite3_stmt* stmt, int colIdx, const std::string& testStr)
    {
        auto data = reinterpret_cast<const char*>(sqlite3_column_text(stmt, colIdx));
        BOOST_REQUIRE(data);
        BOOST_CHECK_EQUAL(std::string(data), testStr);
    }

    static void BoostCheckCurrentStatementColumnValueEqualToInt64(sqlite3_stmt* stmt, int colIdx, long long int testInt)
    {
        auto data = static_cast<long long int>(sqlite3_column_int64(stmt, colIdx));
        BOOST_CHECK_EQUAL(data, testInt);
    }

    static void BindSqliteStmtParamInt64(sqlite3_stmt* stmt, const std::string& paramName, long long int value)
    {
        auto index = sqlite3_bind_parameter_index(stmt, paramName.c_str());
        BOOST_REQUIRE_EQUAL(sqlite3_bind_int64(stmt, index, value), SQLITE_OK);
    }

    void BoostCheckSqliteStatementHasOneRowEqualToTestPatientData(sqlite3_stmt *stmt)
    {
        BOOST_REQUIRE(sqlite3_step(stmt) == SQLITE_ROW);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 0, m_TestPatientData1.ID);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 1, m_TestPatientData1.Name);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 2, m_TestPatientData1.FamilyName);
        BoostCheckCurrentStatementColumnValueEqualToInt64(stmt, 3, m_TestPatientData1.Age);
        BoostCheckCurrentStatementColumnValueEqualToInt64(stmt, 4, static_cast<long long int>(m_TestPatientData1.Gender));
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 5, m_TestPatientData1.Sample.Bacteria);
        BoostCheckCurrentStatementColumnValueEqualToInt64(stmt, 6, m_TestPatientData1.Sample.Count);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 7, m_TestPatientData1.Sample.Gram);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 8, m_TestPatientData1.Sample.Isolated);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 9, m_TestPatientData1.Sample.Shape);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 10, m_TestPatientData1.Sample.Standard);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 11, m_TestPatientData1.Sample.Type);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 12, m_TestPatientData1.Comment.Description);
        BoostCheckCurrentStatementColumnTextEqualToString(stmt, 13, m_TestPatientData1.Comment.Subject);
        BOOST_CHECK(sqlite3_step(stmt) != SQLITE_ROW);
        sqlite3_finalize(stmt);
    }

    std::shared_ptr<MockDatabaseCommandFactory> m_CommandFactory = std::make_shared<MockDatabaseCommandFactory>();
    std::shared_ptr<MockDatabaseCommand> m_DatabaseCommand = std::make_shared<MockDatabaseCommand>();

    typedef ParameterCapturer<std::string> StringCapturer;

    PatientData m_TestPatientData1;
    PatientData m_TestPatientData2;
    PatientData m_UpdatedPatientData;
    std::string m_DbFilePath = "DatabaseIntegrationFixtureTest.db";
};

BOOST_FIXTURE_TEST_SUITE(PatientQueryBuilderTestSuite, PatientQueryBuilderFixture)

BOOST_AUTO_TEST_CASE(GivenTheDatabaseWithoutPatientDataTable_WhenExecutingTheCommandFromGetCreateTableIfNotExists_TheTableMustBeCreatedWithNoRows)
{
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);

    SqlitePatientQueryBuilder queryBuilder(m_CommandFactory);

    std::string createTableIfNotExistsQueryStr;
    MOCK_EXPECT(m_CommandFactory->CreateCommand).once().returns(m_DatabaseCommand);
    MOCK_EXPECT(m_DatabaseCommand->SetCommandString).once().calls(StringCapturer(createTableIfNotExistsQueryStr));

    auto createTableIfNotExistsCommand = queryBuilder.GetCreateTableIfNotExistsQuery();

    BOOST_CHECK_EQUAL(createTableIfNotExistsCommand, m_DatabaseCommand);
    sqlite3_stmt *createIfNotExistsStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, createTableIfNotExistsQueryStr.c_str(), -1, &createIfNotExistsStmt, nullptr);

    BOOST_CHECK_EQUAL(sqlite3_step(createIfNotExistsStmt), SQLITE_DONE);
    sqlite3_finalize(createIfNotExistsStmt);

    sqlite3_stmt *selectStmt = NULL;
    sqlite3_prepare_v2(dbHandle,
        "SELECT ID, Name, FamilyName, Age, Gender, SampleBacteria, SampleCount, SampleGram, SampleIsolated, SampleShape, SampleStandard, SampleType, CommentDescription, CommentSubject FROM Table_PatientData ",
        -1, &selectStmt, NULL);
    BOOST_CHECK_EQUAL(sqlite3_step(selectStmt), SQLITE_DONE);
    sqlite3_finalize(selectStmt);

    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenTheDatabaseWithAPopulatedPatientDataTable_WhenExecutingTheCommandFromGetCreateTableIfNotExists_TheDataWithinTheTableMustBeUnchanged)
{
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqliteCreatePatientDataTable(dbHandle);
    SqliteInsertPatientData(dbHandle, m_TestPatientData1);

    SqlitePatientQueryBuilder queryBuilder(m_CommandFactory);

    std::string createTableIfNotExistsQueryStr;
    MOCK_EXPECT(m_CommandFactory->CreateCommand).once().returns(m_DatabaseCommand);
    MOCK_EXPECT(m_DatabaseCommand->SetCommandString).once().calls(StringCapturer(createTableIfNotExistsQueryStr));

    auto createTableIfNotExistsCommand = queryBuilder.GetCreateTableIfNotExistsQuery();

    BOOST_CHECK_EQUAL(createTableIfNotExistsCommand, m_DatabaseCommand);

    sqlite3_stmt *createIfNotExistsStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, createTableIfNotExistsQueryStr.c_str(), -1, &createIfNotExistsStmt, nullptr);

    BOOST_CHECK_EQUAL(sqlite3_step(createIfNotExistsStmt), SQLITE_DONE);
    sqlite3_finalize(createIfNotExistsStmt);

    sqlite3_stmt *selectStmt = NULL;
    sqlite3_prepare_v2(dbHandle,
        "SELECT ID, Name, FamilyName, Age, Gender, SampleBacteria, SampleCount, SampleGram, SampleIsolated, SampleShape, SampleStandard, SampleType, CommentDescription, CommentSubject FROM Table_PatientData ",
        -1, &selectStmt, NULL);
    BoostCheckSqliteStatementHasOneRowEqualToTestPatientData(selectStmt);

    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(WhenRunningGetInsertQueryFromPatientQueryBuilder_ThenADataBaseCommandMustBeCreatedWithTheCorrectParametersAddedAndCommandStringSet_AndPassingTheCommandStringToTheSqliteAndBindingParametersMustEffectAnInsertOperation)
{
    std::string insertStr;

    MOCK_EXPECT(m_CommandFactory->CreateCommand).once().returns(m_DatabaseCommand);
    MOCK_EXPECT(m_DatabaseCommand->SetCommandString).once().calls(StringCapturer(insertStr));
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("ID", ":ID", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("Name", ":Name", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("FamilyName", ":FamilyName", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("Age", ":Age", IDatabaseCommand::ParameterType::Integer);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("Gender", ":Gender", IDatabaseCommand::ParameterType::Integer);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleBacteria", ":SampleBacteria", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleCount", ":SampleCount", IDatabaseCommand::ParameterType::Integer);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleGram", ":SampleGram", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleIsolated", ":SampleIsolated", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleShape", ":SampleShape", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleStandard", ":SampleStandard", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleType", ":SampleType", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("CommentDescription", ":CommentDescription", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("CommentSubject", ":CommentSubject", IDatabaseCommand::ParameterType::String);

    SqlitePatientQueryBuilder queryBuilder(m_CommandFactory);

    auto insertCommand = queryBuilder.GetInsertQuery();

    BOOST_CHECK_EQUAL(m_DatabaseCommand, insertCommand);

    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqliteCreatePatientDataTable(dbHandle);

    sqlite3_stmt *insertStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, insertStr.c_str(), -1, &insertStmt, nullptr);
    BindSqliteStmtParamText(insertStmt, ":ID", m_TestPatientData1.ID);
    BindSqliteStmtParamText(insertStmt, ":Name", m_TestPatientData1.Name);
    BindSqliteStmtParamText(insertStmt, ":FamilyName", m_TestPatientData1.FamilyName);
    BindSqliteStmtParamInt64(insertStmt, ":Age", m_TestPatientData1.Age);
    BindSqliteStmtParamInt64(insertStmt, ":Gender", static_cast<long long int>(m_TestPatientData1.Gender));
    BindSqliteStmtParamText(insertStmt, ":SampleBacteria", m_TestPatientData1.Sample.Bacteria);
    BindSqliteStmtParamInt64(insertStmt, ":SampleCount", m_TestPatientData1.Sample.Count);
    BindSqliteStmtParamText(insertStmt, ":SampleGram", m_TestPatientData1.Sample.Gram);
    BindSqliteStmtParamText(insertStmt, ":SampleIsolated", m_TestPatientData1.Sample.Isolated);
    BindSqliteStmtParamText(insertStmt, ":SampleShape", m_TestPatientData1.Sample.Shape);
    BindSqliteStmtParamText(insertStmt, ":SampleStandard", m_TestPatientData1.Sample.Standard);
    BindSqliteStmtParamText(insertStmt, ":SampleType", m_TestPatientData1.Sample.Type);
    BindSqliteStmtParamText(insertStmt, ":CommentDescription", m_TestPatientData1.Comment.Description);
    BindSqliteStmtParamText(insertStmt, ":CommentSubject", m_TestPatientData1.Comment.Subject);
    sqlite3_step(insertStmt);
    sqlite3_finalize(insertStmt);

    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM Table_PatientData", -1, &selectStmt, nullptr);
    BoostCheckSqliteStatementHasOneRowEqualToTestPatientData(selectStmt);

    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(WhenTheGetCountRowQueryFromThePatientQueryBuilderIsRun_ThenExpectCmdFactoryCreateFunctionIsCalledAndReturnDbCommand_ThenExpectDbCommandSetCommandStringFucntionIsCalled_ThenExpectDbCommandAddParameterFunctionsIsCalledCorrectly_ThenFinalyTheTableMustBeRetrievedWithNew1ColumnThatContainZeroNumber)
{
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqliteCreatePatientDataTable(dbHandle);
    std::string getCountQueryStr;

    MOCK_EXPECT(m_CommandFactory->CreateCommand).once().returns(m_DatabaseCommand);
    MOCK_EXPECT(m_DatabaseCommand->SetCommandString).once().calls(StringCapturer(getCountQueryStr));
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("ID", ":ID", IDatabaseCommand::ParameterType::String);

    SqlitePatientQueryBuilder queryBuilder(m_CommandFactory);
    auto countRowCommand = queryBuilder.GetRowCountQuery();

    BOOST_CHECK_EQUAL(m_DatabaseCommand, countRowCommand);

    sqlite3_stmt *countRowStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, getCountQueryStr.c_str(), -1, &countRowStmt, nullptr);
    BindSqliteStmtParamText(countRowStmt, ":ID", m_TestPatientData1.ID);
    BOOST_REQUIRE(sqlite3_step(countRowStmt) == SQLITE_ROW);
    BoostCheckCurrentStatementColumnValueEqualToInt64(countRowStmt, 0, 0);
    BOOST_CHECK(sqlite3_step(countRowStmt) != SQLITE_ROW);
    sqlite3_step(countRowStmt);
    sqlite3_finalize(countRowStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenARowInThePatientDataTable_WhenGetUpdateQueryIsRun_ThenMustDbCommandCreatedThenDbCommandAddParametersIsCalled)
{
    // GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqliteCreatePatientDataTable(dbHandle);
    SqliteInsertPatientData(dbHandle, m_TestPatientData1);;

    // WHEN
    std::string updateQueryStr;
    MOCK_EXPECT(m_CommandFactory->CreateCommand).once().returns(m_DatabaseCommand);
    MOCK_EXPECT(m_DatabaseCommand->SetCommandString).once().calls(StringCapturer(updateQueryStr));
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("ID", ":ID", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("Name", ":Name", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("FamilyName", ":FamilyName", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("Age", ":Age", IDatabaseCommand::ParameterType::Integer);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("Gender", ":Gender", IDatabaseCommand::ParameterType::Integer);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleBacteria", ":SampleBacteria", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleCount", ":SampleCount", IDatabaseCommand::ParameterType::Integer);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleGram", ":SampleGram", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleIsolated", ":SampleIsolated", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleShape", ":SampleShape", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleStandard", ":SampleStandard", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("SampleType", ":SampleType", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("CommentDescription", ":CommentDescription", IDatabaseCommand::ParameterType::String);
    MOCK_EXPECT(m_DatabaseCommand->AddParameter).once().with("CommentSubject", ":CommentSubject", IDatabaseCommand::ParameterType::String);

    SqlitePatientQueryBuilder queryBuilder(m_CommandFactory);
    auto updateCommand = queryBuilder.GetUpdateQuery();

    BOOST_CHECK_EQUAL(m_DatabaseCommand, updateCommand);

    sqlite3_stmt *updateStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, updateQueryStr.c_str(), -1, &updateStmt, nullptr);
    BindSqliteStmtParamText(updateStmt, ":ID", m_UpdatedPatientData.ID);
    BindSqliteStmtParamText(updateStmt, ":Name", m_UpdatedPatientData.Name);
    BindSqliteStmtParamText(updateStmt, ":FamilyName", m_UpdatedPatientData.FamilyName);
    BindSqliteStmtParamInt64(updateStmt, ":Age", m_UpdatedPatientData.Age);
    BindSqliteStmtParamInt64(updateStmt, ":Gender", static_cast<long long int>(m_UpdatedPatientData.Gender));
    BindSqliteStmtParamText(updateStmt, ":SampleBacteria", m_UpdatedPatientData.Sample.Bacteria);
    BindSqliteStmtParamInt64(updateStmt, ":SampleCount", m_UpdatedPatientData.Sample.Count);
    BindSqliteStmtParamText(updateStmt, ":SampleGram", m_UpdatedPatientData.Sample.Gram);
    BindSqliteStmtParamText(updateStmt, ":SampleIsolated", m_UpdatedPatientData.Sample.Isolated);
    BindSqliteStmtParamText(updateStmt, ":SampleShape", m_UpdatedPatientData.Sample.Shape);
    BindSqliteStmtParamText(updateStmt, ":SampleStandard", m_UpdatedPatientData.Sample.Standard);
    BindSqliteStmtParamText(updateStmt, ":SampleType", m_UpdatedPatientData.Sample.Type);
    BindSqliteStmtParamText(updateStmt, ":CommentDescription", m_UpdatedPatientData.Comment.Description);
    BindSqliteStmtParamText(updateStmt, ":CommentSubject", m_UpdatedPatientData.Comment.Subject);
    sqlite3_step(updateStmt);
    sqlite3_finalize(updateStmt);

    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM Table_PatientData", -1, &selectStmt, nullptr);
    BOOST_REQUIRE(sqlite3_step(selectStmt) == SQLITE_ROW);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 0, m_UpdatedPatientData.ID);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 1, m_UpdatedPatientData.Name);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 2, m_UpdatedPatientData.FamilyName);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectStmt, 3, m_UpdatedPatientData.Age);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectStmt, 4, static_cast<long long int>(m_UpdatedPatientData.Gender));
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 5, m_UpdatedPatientData.Sample.Bacteria);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectStmt, 6, m_UpdatedPatientData.Sample.Count);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 7, m_UpdatedPatientData.Sample.Gram);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 8, m_UpdatedPatientData.Sample.Isolated);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 9, m_UpdatedPatientData.Sample.Shape);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 10, m_UpdatedPatientData.Sample.Standard);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 11, m_UpdatedPatientData.Sample.Type);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 12, m_UpdatedPatientData.Comment.Description);
    BoostCheckCurrentStatementColumnTextEqualToString(selectStmt, 13, m_UpdatedPatientData.Comment.Subject);
    BOOST_CHECK(sqlite3_step(selectStmt) != SQLITE_ROW);
    sqlite3_finalize(selectStmt);

    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(Given2RowInThePatientDataTable_WhenGetSelectAllQueryIsRun_ThenMustDbCommandCreatedThenDbCommandAddParametersIsCalled)
{
    // GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqliteCreatePatientDataTable(dbHandle);
    SqliteInsertPatientData(dbHandle, m_TestPatientData1);
    SqliteInsertPatientData(dbHandle, m_TestPatientData2);

    // WHEN
    std::string selectAllQueryStr;
    MOCK_EXPECT(m_CommandFactory->CreateCommand).once().returns(m_DatabaseCommand);
    MOCK_EXPECT(m_DatabaseCommand->SetCommandString).once().calls(StringCapturer(selectAllQueryStr));

    SqlitePatientQueryBuilder queryBuilder(m_CommandFactory);
    auto selectAllCommand = queryBuilder.GetSelectAllQuery();

    BOOST_CHECK_EQUAL(m_DatabaseCommand, selectAllCommand);

    sqlite3_stmt *selectAllStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, selectAllQueryStr.c_str(), -1, &selectAllStmt, nullptr);
    BOOST_REQUIRE(sqlite3_step(selectAllStmt) == SQLITE_ROW);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 0, m_TestPatientData1.ID);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 1, m_TestPatientData1.Name);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 2, m_TestPatientData1.FamilyName);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectAllStmt, 3, m_TestPatientData1.Age);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectAllStmt, 4, static_cast<long long int>(m_TestPatientData1.Gender));
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 5, m_TestPatientData1.Sample.Bacteria);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectAllStmt, 6, m_TestPatientData1.Sample.Count);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 7, m_TestPatientData1.Sample.Gram);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 8, m_TestPatientData1.Sample.Isolated);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 9, m_TestPatientData1.Sample.Shape);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 10, m_TestPatientData1.Sample.Standard);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 11, m_TestPatientData1.Sample.Type);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 12, m_TestPatientData1.Comment.Description);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 13, m_TestPatientData1.Comment.Subject);
    BOOST_CHECK(sqlite3_step(selectAllStmt) == SQLITE_ROW);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 0, m_TestPatientData2.ID);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 1, m_TestPatientData2.Name);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 2, m_TestPatientData2.FamilyName);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectAllStmt, 3, m_TestPatientData2.Age);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectAllStmt, 4, static_cast<long long int>(m_TestPatientData2.Gender));
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 5, m_TestPatientData2.Sample.Bacteria);
    BoostCheckCurrentStatementColumnValueEqualToInt64(selectAllStmt, 6, m_TestPatientData2.Sample.Count);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 7, m_TestPatientData2.Sample.Gram);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 8, m_TestPatientData2.Sample.Isolated);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 9, m_TestPatientData2.Sample.Shape);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 10, m_TestPatientData2.Sample.Standard);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 11, m_TestPatientData2.Sample.Type);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 12, m_TestPatientData2.Comment.Description);
    BoostCheckCurrentStatementColumnTextEqualToString(selectAllStmt, 13, m_TestPatientData2.Comment.Subject);
    BOOST_CHECK(sqlite3_step(selectAllStmt) != SQLITE_ROW);
    sqlite3_finalize(selectAllStmt);

    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_SUITE_END()
