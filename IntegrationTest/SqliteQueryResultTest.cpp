#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "DateTimeMatcher.h"
#include "../Headers/Adapters/SqliteDatabaseException.h"
#include "../Headers/Framework/SqliteQueryResult.h"
#include "../Headers/BusinessEntities/MockDateTimeOperation.h"

struct SqliteQueryResultTestFixture
{
    struct SampleData
    {
        bool Bool1, Bool2;
        long long int Int1, Int2;
        double Double1, Double2;
        std::string Str1, Str2;
        std::shared_ptr<ByteArray> Bin1, Bin2;
        DateTime DateTime1, DateTime2;
        long long int DateTime1Encoded, DateTime2Encoded;
    };

    SqliteQueryResultTestFixture()
    {
        m_Sample1.Bin1 = std::make_shared<ByteArray>(24, 43);
        m_Sample1.Bin2 = std::make_shared<ByteArray>(24, 14);
        m_Sample1.Bool1 = true;
        m_Sample1.Bool2 = false;
        m_Sample1.DateTime1 = DateTime(1893, 8, 24, 2, 35, 32.33);
        m_Sample1.DateTime2 = DateTime(1993, 2, 28, 16, 12, 15.4);
        m_Sample1.Double1 = 6.25;
        m_Sample1.Double2 = 3.25;
        m_Sample1.Int1 = 873817;
        m_Sample1.Int2 = 198779;
        m_Sample1.Str1 = "ashdgkh2 293928 fuy989&*967&y IUH AIUiufia 78992 ";
        m_Sample1.Str2 = " ger\3\t9 898u9 w89r8ytw9 yas 4345  u0ew8r8u9 3434\tq49 384938";
    }

    ~SqliteQueryResultTestFixture()
    {
        RemoveFile(m_DbFilePath);
    }

    void RemoveFile(const std::string & filePath)
    {
        if (boost::filesystem::exists(filePath))
            boost::filesystem::remove(filePath);
    }

    static void BindSqliteStmtParamInt64(sqlite3_stmt* stmt, const std::string& paramName, long long int value)
    {
        auto index = sqlite3_bind_parameter_index(stmt, paramName.c_str());
        BOOST_REQUIRE_EQUAL(sqlite3_bind_int64(stmt, index, value), SQLITE_OK);
    }

    static void BindSqliteStmtParamDouble(sqlite3_stmt* stmt, const std::string& paramName, double value)
    {
        auto index = sqlite3_bind_parameter_index(stmt, paramName.c_str());
        BOOST_REQUIRE_EQUAL(sqlite3_bind_double(stmt, index, value), SQLITE_OK);
    }

    void InsertSampleData(sqlite3* dbHandle, const SampleData& sampleData)
    {
        sqlite3_stmt *insertStmt = nullptr;
        BOOST_REQUIRE_EQUAL(
            sqlite3_prepare_v2(dbHandle,
                "INSERT INTO SampleData "
                "(Bool1, Bool2, Int1, Int2, Double1, Double2, Str1, Str2, Bin1, Bin2, DateTime1, DateTime2) "
                "VALUES(:Bool1, :Bool2, :Int1, :Int2, :Double1, :Double2, :Str1, :Str2, :Bin1, :Bin2, :DateTime1, :DateTime2)"
                , -1, &insertStmt, nullptr), SQLITE_OK);
        BindUpdateSampleDataParameters(insertStmt, sampleData);
        BOOST_REQUIRE_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);
        BOOST_REQUIRE_EQUAL(sqlite3_finalize(insertStmt), SQLITE_OK);
    }

    void BindUpdateSampleDataParameters(sqlite3_stmt* stmt, const SampleData& sampleData)
    {
        BindSqliteStmtParamInt64(stmt, ":Bool1", sampleData.Bool1);
        BindSqliteStmtParamInt64(stmt, ":Bool2", sampleData.Bool2);
        BindSqliteStmtParamInt64(stmt, ":Int1", sampleData.Int1);
        BindSqliteStmtParamInt64(stmt, ":Int2", sampleData.Int2);
        BindSqliteStmtParamDouble(stmt, ":Double1", sampleData.Double1);
        BindSqliteStmtParamDouble(stmt, ":Double2", sampleData.Double2);
        BindSqliteStmtParamText(stmt, ":Str1", sampleData.Str1);
        BindSqliteStmtParamText(stmt, ":Str2", sampleData.Str2);
        BindSqliteStmtParamBinary(stmt, ":Bin1", *sampleData.Bin1);
        BindSqliteStmtParamBinary(stmt, ":Bin2", *sampleData.Bin2);
        BindSqliteStmtParamInt64(stmt, ":DateTime1", sampleData.DateTime1Encoded);
        BindSqliteStmtParamInt64(stmt, ":DateTime2", sampleData.DateTime2Encoded);
    }

    static void BindSqliteStmtParamBinary(sqlite3_stmt* stmt, const std::string& paramName, const ByteArray& value)
    {
        auto index = sqlite3_bind_parameter_index(stmt, paramName.c_str());
        BOOST_REQUIRE_EQUAL(
            sqlite3_bind_blob(stmt, index, value.data(), static_cast<int>(value.size()), SQLITE_TRANSIENT), SQLITE_OK);
    }

    static void BindSqliteStmtParamText(sqlite3_stmt* stmt, const std::string& paramName, const std::string& value)
    {
        auto index = sqlite3_bind_parameter_index(stmt, paramName.c_str());
        BOOST_REQUIRE_EQUAL(sqlite3_bind_text(stmt, index, value.c_str(), -1, SQLITE_TRANSIENT), SQLITE_OK);
    }

    void CreateSampleTable(sqlite3* dbHandle)
    {
        SqlExec(dbHandle,
            "CREATE TABLE SampleData "
            "(Bool1 INTEGER, "
            "Bool2 INTEGER, "
            "Int1 INTEGER, "
            "Int2 INTEGER, "
            "Double1 REAL, "
            "Double2 REAL, "
            "Str1 TEXT, "
            "Str2 TEXT, "
            "Bin1 BLOB, "
            "Bin2 BLOB, "
            "DateTime1 INTEGER, "
            "DateTime2 INTEGER)");
    }

    static void SqlExec(sqlite3 * dbHandle, const std::string & cmd)
    {
        sqlite3_stmt *stmt = nullptr;
        BOOST_REQUIRE_EQUAL(sqlite3_prepare_v2(dbHandle, cmd.c_str(), -1, &stmt, NULL), SQLITE_OK);
        BOOST_REQUIRE_EQUAL(sqlite3_step(stmt), SQLITE_DONE);
        BOOST_REQUIRE_EQUAL(sqlite3_finalize(stmt), SQLITE_OK);
    }

    std::shared_ptr<MockDateTimeOperation> m_DateTimeOperation = std::make_shared<MockDateTimeOperation>();
    std::string m_DbFilePath = "DatabaseIntegrationFixtureTest.db";
    SampleData m_Sample1;
};

BOOST_FIXTURE_TEST_SUITE(SqliteQueryResultTest, SqliteQueryResultTestFixture)

BOOST_AUTO_TEST_CASE(GivenASatementThatContain1RowWith1ColumnThatHasInteger_WhenTheGetIntTheQueryResultIsRunForAGivenStatement_ThenMustRetrivedSameValue)
{
    // GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField INTEGER)"));
    SqlExec(dbHandle, std::string("INSERT INTO TestTable (TestField) values(12345)"));
    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM TestTable", -1, &selectStmt, nullptr);
    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        auto resultInt = queryResult.GetInt(0);
        BOOST_CHECK_EQUAL(resultInt, 12345);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenASatementThatContain1RowWith1ColumnThatHasDouble_WhenTheGetDoubleTheQueryResultIsRunForAGivenStatement_ThenMustRetrivedSameValue)
{
    // GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField DOUBLE)"));
    SqlExec(dbHandle, std::string("INSERT INTO TestTable (TestField) values(12.345)"));
    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM TestTable", -1, &selectStmt, nullptr);
    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_EQUAL(queryResult.GetDouble(0), 12.345);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenASatementThatContain1RowWith1ColumnThatHasBooleanValue_WhenTheGetBoolTheQueryResultIsRunForAGivenStatement_ThenMustRetrivedSameValue)
{
    // GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField INTEGER)"));
    SqlExec(dbHandle, std::string("INSERT INTO TestTable (TestField) values(1)"));
    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM TestTable", -1, &selectStmt, nullptr);
    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_EQUAL(queryResult.GetBool(0), true);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenASatementThatContain1RowWith1ColumnThatHasDateTimeValue_WhenTheGetDateTimeTheQueryResultIsRunForAGivenStatement_ThenMustRetrivedSameValue)
{
    // GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField INTEGER)"));
    SqlExec(dbHandle, std::string("INSERT INTO TestTable (TestField) values(1234)"));
    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM TestTable", -1, &selectStmt, nullptr);
    long long int captureValue;
    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        DateTime returnDateTime(217, 2, 21, 3, 57, 0);
        MOCK_EXPECT(m_DateTimeOperation->DecodeInt64).once().calls(
            [&captureValue, &returnDateTime](long long int passedValue) {
            captureValue = passedValue;
            return returnDateTime; });
        auto resultDateTime = queryResult.GetDateTime(0);
        BOOST_CHECK(DateTimeMatcher::IsEqual(resultDateTime, returnDateTime));
    }
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM TestTable", -1, &selectStmt, nullptr);
    BOOST_CHECK_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    BOOST_CHECK_EQUAL(sqlite3_column_int64(selectStmt, 0), captureValue);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenASatementThatContainsOneRowWithOneBinaryColumn_WhenTheGetBinaryOfQueryResultIsRun_ThenTheSameValueMustBeRetrived)
{
     //GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField BLOB)"));

    ByteArray data(100, 42);
    sqlite3_stmt *insertStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "INSERT INTO TestTable (TestField) values(:binVal)", -1, &insertStmt, nullptr);
    sqlite3_bind_blob(insertStmt, 1, data.data(), static_cast<int>(data.size()), SQLITE_STATIC);
    sqlite3_step(insertStmt);
    sqlite3_finalize(insertStmt);

    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT TestField FROM TestTable", -1, &selectStmt, nullptr);

    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        auto resultVect = queryResult.GetBinary(0);
        BOOST_CHECK_EQUAL_COLLECTIONS(resultVect->cbegin(), resultVect->cend(), data.cbegin(), data.cend());
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenAnEmptyTableWithABinaryColumn_WhenTheGetBinaryOfQueryResultIsRun_ThenTheNullValueMustBeRetrived)
{
    //GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField BLOB)"));

    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT TestField FROM TestTable", -1, &selectStmt, nullptr);

    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_THROW(queryResult.GetBinary(0), SqliteDatabaseException);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenASatementThatContain1RowWith1ColumnThatHasText_WhenCommandStringFromTheQueryResultIsRunForAGivenStatement_ThenTheRetrivedValueMustBeTheSame)
{
    // GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField TEXT)"));
    SqlExec(dbHandle, std::string("INSERT INTO TestTable (TestField) values('testString')"));
    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM TestTable", -1, &selectStmt, nullptr);
    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_EQUAL(queryResult.GetString(0), std::string("testString"));
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(WhenTheQueryResultIsConstructedWithASelectStatementThatReturns1Row_HasNextAfterOneCallToNextOnTheQueryResultMustReturnFalse)
{
    // WHEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    CreateSampleTable(dbHandle);
    InsertSampleData(dbHandle, m_Sample1);
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM SampleData", -1, &selectStmt, nullptr);
    {
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        // THEN
        BOOST_REQUIRE(queryResult.HasData());
        queryResult.Next();
        BOOST_CHECK_EQUAL(queryResult.HasData(), false);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(WhenTheQueryResultIsConstructedWithASelectStatementThatReturns1Row_HasNextOnTheQueryResultMustReturnTrue)
{
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    CreateSampleTable(dbHandle);
    InsertSampleData(dbHandle, m_Sample1);
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM SampleData", -1, &selectStmt, nullptr);
    {
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_EQUAL(queryResult.HasData(), true);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(WhenTheQueryResultIsConstructedWithASelectStatementThatReturnsNoRows_HasNextOnTheQueryResultMustReturnFalse)
{
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    CreateSampleTable(dbHandle);
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT * FROM SampleData", -1, &selectStmt, nullptr);
    {
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_EQUAL(queryResult.HasData(), false);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenASatementThatContainsOneRowWithOneNullBinaryColumn_WhenTheGetBinaryOfQueryResultIsRun_ThenAndEmptyPointerMustBeRetrived)
{
    //GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField BLOB)"));

    sqlite3_stmt *insertStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "INSERT INTO TestTable (TestField) values(:binVal)", -1, &insertStmt, nullptr);
    sqlite3_bind_null(insertStmt, 1);
    sqlite3_step(insertStmt);
    sqlite3_finalize(insertStmt);

    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT TestField FROM TestTable", -1, &selectStmt, nullptr);

    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        auto resultVect = queryResult.GetBinary(0);
        BOOST_CHECK_EQUAL(resultVect, std::shared_ptr<ByteArray>());
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenAnEmptyTableWithAStringColumn_WhenTheGetStringOfQueryResultIsRun_ThenAnExceptionMustBeThrown)
{
    //GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField TEXT)"));

    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT TestField FROM TestTable", -1, &selectStmt, nullptr);

    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_THROW(queryResult.GetString(0), SqliteDatabaseException);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenAnEmptyTableWithAIntegerColumn_WhenTheGetIntOfQueryResultIsRun_ThenAnExceptionMustBeThrown)
{
    //GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField INTEGER)"));

    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT TestField FROM TestTable", -1, &selectStmt, nullptr);

    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_THROW(queryResult.GetInt(0), SqliteDatabaseException);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenAnEmptyTableWithADoubleColumn_WhenTheGetRealOfQueryResultIsRun_ThenAnExceptionMustBeThrown)
{
    //GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField Real)"));

    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT TestField FROM TestTable", -1, &selectStmt, nullptr);

    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_THROW(queryResult.GetDouble(0), SqliteDatabaseException);
    }
    sqlite3_close(dbHandle);
}


BOOST_AUTO_TEST_CASE(GivenAnEmptyTableWithADateTimeColumn_WhenTheGetDateTimeOfQueryResultIsRun_ThenAnExceptionMustBeThrown)
{
    //GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField INTEGER)"));

    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT TestField FROM TestTable", -1, &selectStmt, nullptr);

    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_THROW(queryResult.GetDateTime(0), SqliteDatabaseException);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenAnEmptyTableWithABooleanColumn_WhenTheGetBoolOfQueryResultIsRun_ThenAnExceptionMustBeThrown)
{
    //GIVEN
    sqlite3* dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, std::string("CREATE TABLE TestTable (TestField INTEGER)"));

    // WHEN
    sqlite3_stmt *selectStmt = nullptr;
    sqlite3_prepare_v2(dbHandle, "SELECT TestField FROM TestTable", -1, &selectStmt, nullptr);

    {
        // THEN
        SqliteQueryResult queryResult(selectStmt, m_DateTimeOperation);
        BOOST_CHECK_THROW(queryResult.GetBool(0), SqliteDatabaseException);
    }
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(WhenStepReturnsConstraint_NextMustThrowAnException)
{
    sqlite3* dbHandle;
    BOOST_REQUIRE_EQUAL(sqlite3_open("", &dbHandle), SQLITE_OK);

    SqlExec(dbHandle, "PRAGMA foreign_keys = ON");

    SqlExec(dbHandle,
        "CREATE TABLE ParentTable "
        "(Bool1 INTEGER, "
        "Uid TEXT PRIMARY KEY, "
        "Str1 TEXT, "
        "DateTime1 INTEGER)");

    SqlExec(dbHandle,
        "CREATE TABLE ChildTable "
        "(Bool2 INTEGER, "
        "ParentUid TEXT, "
        "Str2 TEXT, "
        "DateTime2 INTEGER, "
        "FOREIGN KEY(ParentUid) REFERENCES ParentTable(Uid))");

    sqlite3_stmt *insertStmt = nullptr;
    BOOST_REQUIRE_EQUAL(sqlite3_prepare_v2(
        dbHandle, "INSERT INTO ChildTable (Bool2, ParentUid) values(554, 'usiof')", -1, &insertStmt, nullptr), SQLITE_OK);
    {
        BOOST_CHECK_THROW(SqliteQueryResult queryResult(insertStmt, m_DateTimeOperation), SqliteDatabaseException);
    }
    BOOST_CHECK_EQUAL(sqlite3_close(dbHandle), SQLITE_OK);
}

BOOST_AUTO_TEST_SUITE_END()
