#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "Headers/Framework/SqliteDataBaseConnection.h"
#include "Headers/Framework/SqliteDatabaseException.h"
#include "Headers/Framework/MockSqliteQueryResultFactory.h"
#include "Headers/Adapters/MockDatabaseCommand.h"
#include "Headers/Adapters/MockQueryResult.h"
#include "Headers/BusinessEntities/MockDateTimeOperation.h"
#include "DateTimeMatcher.h"

struct DataBaseConnectionFixture
{
	DataBaseConnectionFixture()
	{
        RemoveFile(m_DbFilePath);

        m_DataBaseConnection = std::make_shared<SqliteDatabaseConnection>(
            m_DbFilePath, m_MockQueryResultFactory, m_DateTimeOperation);
        auto data = { 15, 50, 100, 150, 120, 33, 25, 74, 75, 33, 25, 88, 56 };
        m_ByteArray = std::make_shared<std::vector<uint8_t>>(data.begin(), data.end());
	}

    static void SqlExec(sqlite3 * dbHandle, const std::string & cmd)
    {
        sqlite3_stmt *stmt = nullptr;
        BOOST_REQUIRE_EQUAL(sqlite3_prepare_v2(dbHandle, cmd.c_str(), -1, &stmt, NULL), SQLITE_OK);
        BOOST_REQUIRE_EQUAL(sqlite3_step(stmt), SQLITE_DONE);
        BOOST_REQUIRE_EQUAL(sqlite3_finalize(stmt), SQLITE_OK);
    }

    void CreateSampleTable(sqlite3* dbHandle)
    {
        SqlExec(dbHandle,
            "CREATE TABLE SampleData "
            "(Bool1 INTEGER, "
            "Bool2 INTEGER, "
            "Int1 INTEGER, "
            "Int2 INTEGER, "
            "Double1 REAL, "
            "Double2 REAL, "
            "Str1 TEXT, "
            "Str2 TEXT, "
            "Bin1 BLOB, "
            "Bin2 BLOB, "
            "DateTime1 INTEGER, "
            "DateTime2 INTEGER)");
    }

    void SqliteDbOpenThenCreateSampleDataTableAndThenCloseIt()
    {
        sqlite3* dbHandle;
        sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
        CreateSampleTable(dbHandle);
        sqlite3_close(dbHandle);
    }

    template<typename T>
    void BoostCheckThrowForDbCommandBindingWithInvalidParameter(
        const std::string& commandString, const std::string& parameterName,
        const std::string& parameterLowLevelName, IDatabaseCommand::ParameterType type,
        const T& bindValue)
    {
        SqliteDbOpenThenCreateSampleDataTableAndThenCloseIt();
        m_DataBaseConnection->Open();
        MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns(
            commandString);
        std::map<std::string, IDatabaseCommand::Binding> bindings;
        bindings["SolutionUid"].LowLevelParameterName = parameterLowLevelName;
        bindings["SolutionUid"].Name = parameterName;
        bindings["SolutionUid"].Type = type;
        bindings["SolutionUid"].Value = bindValue;
        MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
        BOOST_CHECK_THROW(m_DataBaseConnection->Exec(m_DatabaseCommand), SqliteDatabaseException);
        m_DataBaseConnection->Close();
    }

    void RemoveFile(const std::string & filePath)
    {
        if (boost::filesystem::exists(filePath))
            boost::filesystem::remove(filePath);
    }

    std::shared_ptr<MockQueryResult> m_MockQueryResult = std::make_shared<MockQueryResult>();
    std::shared_ptr<SqliteDatabaseConnection> m_DataBaseConnection;
    std::shared_ptr<MockSqliteQueryResultFactory> m_MockQueryResultFactory = std::make_shared<MockSqliteQueryResultFactory>();
    std::shared_ptr<std::vector<uint8_t>> m_ByteArray;
    std::shared_ptr<MockDatabaseCommand> m_DatabaseCommand = std::make_shared<MockDatabaseCommand>();
    std::shared_ptr<MockDateTimeOperation> m_DateTimeOperation = std::make_shared<MockDateTimeOperation>();
    std::string m_DbFilePath = "DatabaseIntegrationFixtureTest.db";
};

BOOST_FIXTURE_TEST_SUITE(DataBaseConnectionIntegrationTestSuite, DataBaseConnectionFixture)

BOOST_AUTO_TEST_CASE(WhenACreateTableQueryWithoutBindingsIsPassedToExec_ThenATableMustHaveBeenCreatedWithinTheDatabase)
{
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("CREATE TABLE test (col1 TEXT)");

    std::map<std::string, IDatabaseCommand::Binding> bindings;

    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);

    sqlite3_stmt *createTableStmt = nullptr;

    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        createTableStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(createTableStmt), SQLITE_DONE);

    sqlite3_finalize(createTableStmt);
    m_DataBaseConnection->Close();

    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_CHECK_EQUAL(resultPrepare, SQLITE_OK);
    BOOST_CHECK(sqlite3_step(selectStmt) != SQLITE_ROW);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithOneBlobColumn_WhenAnInsertQueryWithOneBinaryParameterIsPassedToExec_\
ThenARowMustBeInsertedWithTheBoundValue)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 BLOB)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();

    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) VALUES(:param1)");
    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = ":param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::Binary;
    bindings["param1"].Value = m_ByteArray;
    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);
    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    auto storedDataSize = sqlite3_column_bytes(selectStmt, 0);
    auto storedData = reinterpret_cast<const uint8_t*>(sqlite3_column_blob(selectStmt, 0));
    BOOST_REQUIRE_EQUAL(storedDataSize, m_ByteArray->size());
    BOOST_CHECK_EQUAL_COLLECTIONS(storedData, storedData + storedDataSize, m_ByteArray->cbegin(), m_ByteArray->cend());
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithOneBlobColumn_WhenAnInsertQueryWithAnUnboundBinaryParameterIsPassedToExec_\
ThenARowMustBeInsertedWithTheNullValue)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 BLOB)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();

    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) VALUES(:param1)");
    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = ":param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::Binary;
    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);
    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    auto storedDataSize = sqlite3_column_bytes(selectStmt, 0);
    auto storedData = reinterpret_cast<const uint8_t*>(sqlite3_column_blob(selectStmt, 0));
    BOOST_REQUIRE_EQUAL(storedDataSize, 0);
    BOOST_CHECK_EQUAL(storedData, reinterpret_cast<const uint8_t*>(nullptr));
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithOneStringColumn_WhenAnInsertQueryIsWithOneStringParameterIsPassedToExec_ThenARowMustHaveBeenInsertedWithTheBoundValue)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 TEXT)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) VALUES(:param1)");

    std::string testString = "Test String";
    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = ":param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::String;
    bindings["param1"].Value = testString;

    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);

    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    std::string storedString = reinterpret_cast<const char*>(sqlite3_column_text(selectStmt, 0));
    BOOST_CHECK_EQUAL(storedString, testString);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithOneDateTimeColumn_WhenAnInsertQueryIsWithOneDateTimeParameterIsPassedToExec_ThenARowMustHaveBeenInsertedWithTheBoundValue)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 INTEGER)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) VALUES(:param1)");

    std::string testString = "Test String";
    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = ":param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::DateTime;
    bindings["param1"].Value = DateTime(2018, 3, 9, 4, 9, 1);
    long long int testEncodedTime = 123456789;
    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    DateTime t(2018, 3, 9, 4, 9, 1);
    MOCK_EXPECT(m_DateTimeOperation->EncodeInt64).with(DateTimeMatcher(t)).once().returns(testEncodedTime);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);

    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    long long int storedInt64 = sqlite3_column_int64(selectStmt, 0);
    BOOST_CHECK_EQUAL(storedInt64, testEncodedTime);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithOneDateTimeColumn_WhenAnInsertQueryIsWithOneDateTimeParameterIsPassedToExec_ThenARowMustHaveBeenInsertedWithTheBoundValue2)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 INTEGER)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) VALUES(:param1)");

    std::string testString = "Test String";
    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = ":param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::DateTime;
    bindings["param1"].Value = DateTime(0, 0, 0, 0, 0, 0);
    long long int testEncodedTime = 123456789;
    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    DateTime t(0, 0, 0, 0, 0, 0);
    MOCK_EXPECT(m_DateTimeOperation->EncodeInt64).with(DateTimeMatcher(t)).once().returns(testEncodedTime);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);

    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    long long int storedInt64 = sqlite3_column_int64(selectStmt, 0);
    BOOST_CHECK_EQUAL(storedInt64, testEncodedTime);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithOneIntegerColumn_WhenAnInsertQueryIsWithOneIntegerParameterIsPassedToExec_ThenARowMustHaveBeenInsertedWithTheBoundValue)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 INTEGER)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) VALUES(:param1)");

    std::string testString = "Test String";
    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = ":param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::Integer;
    bindings["param1"].Value = static_cast<long long int>(1234);

    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);

    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    long long int storedInt = sqlite3_column_int64(selectStmt, 0);
    BOOST_CHECK_EQUAL(storedInt, 1234);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithOneBooleanColumn_WhenAnInsertQueryIsWithOneBooleanParameterIsPassedToExec_ThenARowMustHaveBeenInsertedWithTheBoundValue)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 INTEGER)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) VALUES(:param1)");

    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = ":param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::Boolean;
    bindings["param1"].Value = true;

    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);

    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    long long int storedInt = sqlite3_column_int64(selectStmt, 0);
    BOOST_CHECK_EQUAL((storedInt == 1 ? true : false), true);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithOneRealColumn_WhenAnInsertQueryIsWithOneRealParameterIsPassedToExec_ThenARowMustHaveBeenInsertedWithTheBoundValue)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 REAL)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) VALUES(:param1)");

    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = ":param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::Real;
    bindings["param1"].Value = 12.34;

    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);

    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    double storedReal = sqlite3_column_double(selectStmt, 0);
    BOOST_CHECK_EQUAL(storedReal, 12.34);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(GivenATableWithStringColumn_WhenAnInsertCommandWithAnAtSignLowLevelParamIsExecuted_ThenTheInsertedRowMustBeRetrievableAfterwards)
{
    //Given
    sqlite3 * dbHandle;
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    SqlExec(dbHandle, "CREATE TABLE test (col1 TEXT)");
    sqlite3_close(dbHandle);

    //When
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO test (col1) values(@param1)");

    std::string testString = "Test String";
    std::map<std::string, IDatabaseCommand::Binding> bindings;
    bindings["param1"].LowLevelParameterName = "@param1";
    bindings["param1"].Name = "param1";
    bindings["param1"].Type = IDatabaseCommand::ParameterType::String;
    bindings["param1"].Value = testString;

    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    sqlite3_stmt * insertStmt = nullptr;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().
        calls([&](sqlite3_stmt* stmt) {
        insertStmt = stmt;
        return m_MockQueryResult;
    });

    auto queryResult = m_DataBaseConnection->Exec(m_DatabaseCommand);

    BOOST_CHECK_EQUAL(queryResult, m_MockQueryResult);
    BOOST_CHECK_EQUAL(sqlite3_step(insertStmt), SQLITE_DONE);

    sqlite3_finalize(insertStmt);
    m_DataBaseConnection->Close();

    //Then
    sqlite3_open(m_DbFilePath.c_str(), &dbHandle);
    sqlite3_stmt * selectStmt;
    auto resultPrepare = sqlite3_prepare_v2(dbHandle, "SELECT col1 from test", -1, &selectStmt, NULL);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_ROW);
    std::string storedString = reinterpret_cast<const char *>(sqlite3_column_text(selectStmt, 0));
    BOOST_CHECK_EQUAL(storedString, testString);
    sqlite3_finalize(selectStmt);
    sqlite3_close(dbHandle);
}

BOOST_AUTO_TEST_CASE(WhenConnectionOpened_ThenTheFileMustHaveBeenCreated)
{
    m_DataBaseConnection->Open();
    BOOST_CHECK(boost::filesystem::exists(m_DbFilePath));
    m_DataBaseConnection->Close();
}

BOOST_AUTO_TEST_CASE(WhenConnectionOpened_ThenIsOpenMustReturnTrue)
{
    m_DataBaseConnection->Open();
    BOOST_CHECK(m_DataBaseConnection->IsOpen());
    m_DataBaseConnection->Close();
}

BOOST_AUTO_TEST_CASE(WhenConnectionNotOpened_ThenIsOpenMustReturnFalse)
{
    BOOST_CHECK(!m_DataBaseConnection->IsOpen());
}

BOOST_AUTO_TEST_CASE(WhenConnectionIsOpenedAndThenClosed_ThenIsOpenMustReturnFalse)
{
    m_DataBaseConnection->Open();
    m_DataBaseConnection->Close();
    BOOST_CHECK(!m_DataBaseConnection->IsOpen());
}

BOOST_AUTO_TEST_CASE(WhenClosingDatabaseWithANonFinalizeStatement_ThenDatabaseConnectionMustThrowSqliteDatabaseConnectionException)
{
    SqliteDbOpenThenCreateSampleDataTableAndThenCloseIt();
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("SELECT * FROM SampleData");
    std::map<std::string, IDatabaseCommand::Binding> bindings;
    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(bindings);
    sqlite3_stmt * captureStmt;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().calls(
        [&captureStmt, this](sqlite3_stmt * stmt)
        {
            captureStmt = stmt;
            return m_MockQueryResult;
        });
    {
        auto result = m_DataBaseConnection->Exec(m_DatabaseCommand);
        BOOST_CHECK_EQUAL(m_MockQueryResult, result);
        BOOST_CHECK_THROW(m_DataBaseConnection->Close(), SqliteDatabaseException);
    }
    sqlite3_finalize(captureStmt);
    m_DataBaseConnection->Close();
}

BOOST_AUTO_TEST_CASE(WhenDatabaseConnectionExecFunctionIsCalledWithInvalidDatabaseCommand_ThenDatabaseConnectionMustThrowSqliteDatabaseConnectionException)
{
    SqliteDbOpenThenCreateSampleDataTableAndThenCloseIt();
    m_DataBaseConnection->Open();
    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("TestInvalidQuery");
    BOOST_CHECK_THROW(m_DataBaseConnection->Exec(m_DatabaseCommand), SqliteDatabaseException);
    m_DataBaseConnection->Close();
}

BOOST_AUTO_TEST_CASE(WhenDatabaseConnectionExecFunctionIsCalledWithInvalidDatabaseCommandThatBindingWithBadStringParameter_\
ThenDatabaseConnectionMustThrowSqliteDatabaseConnectionException)
{
    BoostCheckThrowForDbCommandBindingWithInvalidParameter("select * from SampleData where :Str1 = Str1",
        "Str1", ":BadParameter", IDatabaseCommand::ParameterType::String, std::string("test")
    );
}


BOOST_AUTO_TEST_CASE(WhenDatabaseConnectionExecFunctionIsCalledWithInvalidDatabaseCommandThatBindingWithBadFloatParameter_ThenDatabaseConnectionMustThrowSqliteDatabaseConnectionException)
{
    BoostCheckThrowForDbCommandBindingWithInvalidParameter(
        "select * from SampleData where :Str1 = Str1",
        "SolutionUid", ":BadParameter", IDatabaseCommand::ParameterType::Real, 0.0);
}

BOOST_AUTO_TEST_CASE(WhenDatabaseConnectionExecFunctionIsCalledWithInvalidDatabaseCommandThatBindingWithBadIntParameter_ThenDatabaseConnectionMustThrowSqliteDatabaseConnectionException)
{
    BoostCheckThrowForDbCommandBindingWithInvalidParameter(
        "select * from SampleData where :Int1 = Int1",
        "Int1",
        ":BadParameter",
        IDatabaseCommand::ParameterType::Integer,
        static_cast<long long int>(10)
    );
}

BOOST_AUTO_TEST_CASE(WhenDatabaseConnectionExecFunctionIsCalledWithInvalidDatabaseCommandThatBindingWithBadBooleanParameter_ThenDatabaseConnectionMustThrowSqliteDatabaseConnectionException)
{
    BoostCheckThrowForDbCommandBindingWithInvalidParameter(
        "select * from SampleData where :Bool1 = Bool1",
        "Bool1",
        ":BadParameter",
        IDatabaseCommand::ParameterType::Boolean,
        true
    );
}

BOOST_AUTO_TEST_CASE(WhenConnectionNotOpened_ThenDestructorClosesConnectionWithoutError)
{
    SqliteDatabaseConnection connection("dbfile.db", m_MockQueryResultFactory, m_DateTimeOperation);
}

BOOST_AUTO_TEST_CASE(WhenOpenDatabaseWithInvalidFileAddress_ThenMustDatabaseConnectionThrowSqliteDatabaseConnectionException)
{
    auto dbConnection = SqliteDatabaseConnection("http://www.google.com", m_MockQueryResultFactory, m_DateTimeOperation);
    BOOST_CHECK_THROW(dbConnection.Open(), SqliteDatabaseException);
}

BOOST_AUTO_TEST_CASE(WhenConnectionExecInsertCommandForARowWithAForeginKeyThatDoesntExistInDb_AnExceptionMustBeThrown)
{
    sqlite3* dbHandle;
    BOOST_REQUIRE_EQUAL(sqlite3_open(m_DbFilePath.c_str(), &dbHandle), SQLITE_OK);
    SqlExec(dbHandle,
        "CREATE TABLE ParentTable "
        "(Bool1 INTEGER, "
        "Uid TEXT PRIMARY KEY, "
        "Str1 TEXT, "
        "DateTime1 INTEGER)");

    SqlExec(dbHandle,
        "CREATE TABLE ChildTable "
        "(Bool2 INTEGER, "
        "ParentUid TEXT, "
        "Str2 TEXT, "
        "DateTime2 INTEGER, "
        "FOREIGN KEY(ParentUid) REFERENCES ParentTable(Uid))");
    BOOST_REQUIRE_EQUAL(sqlite3_close(dbHandle), SQLITE_OK);

    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns("INSERT INTO ChildTable (Bool2, ParentUid) values(554, 'usiof')");
    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(IDatabaseCommand::BindingCollection{});

    sqlite3_stmt * captureStmt;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().calls(
        [&captureStmt, this](sqlite3_stmt * stmt)
    {
        captureStmt = stmt;
        return m_MockQueryResult;
    });

    m_DataBaseConnection->Open();
    m_DataBaseConnection->Exec(m_DatabaseCommand);
    BOOST_CHECK_EQUAL(sqlite3_step(captureStmt), SQLITE_CONSTRAINT);
    BOOST_CHECK_EQUAL(sqlite3_finalize(captureStmt), SQLITE_CONSTRAINT);
    m_DataBaseConnection->Close();
}

BOOST_AUTO_TEST_CASE(WhenSetFilePathIsCalledAndConnectionIsNotClosed_AnExceptionMustBeThrown)
{
    m_DataBaseConnection->Open();
    BOOST_CHECK_THROW(m_DataBaseConnection->SetFilePath("123"), SqliteDatabaseException);
}

BOOST_AUTO_TEST_CASE(AfterANewFilePathIsSetToDbConnection_CreatingATableInNewDatabaseCallingExecMethodCouldBeDoneCorrectly)
{
    std::string newDatabaseFilePath = "NewTemporaryDatabase.mdd";
    RemoveFile(newDatabaseFilePath);
    m_DataBaseConnection->Close();
    m_DataBaseConnection->SetFilePath(newDatabaseFilePath);

    MOCK_EXPECT(m_DatabaseCommand->GetCommandString).once().returns(
        "CREATE TABLE NewTable "
        "(Bool1 INTEGER, "
        "Uid TEXT PRIMARY KEY, "
        "Str1 TEXT, "
        "DateTime1 INTEGER)");
    MOCK_EXPECT(m_DatabaseCommand->GetBindings).once().returns(IDatabaseCommand::BindingCollection{});

    sqlite3_stmt * captureStmt;
    MOCK_EXPECT(m_MockQueryResultFactory->CreateQueryResult).once().calls(
        [&captureStmt, this](sqlite3_stmt * stmt)
    {
        captureStmt = stmt;
        return m_MockQueryResult;
    });

    m_DataBaseConnection->Open();
    m_DataBaseConnection->Exec(m_DatabaseCommand);
    BOOST_CHECK_EQUAL(sqlite3_step(captureStmt), SQLITE_DONE);
    BOOST_CHECK_EQUAL(sqlite3_finalize(captureStmt), SQLITE_OK);
    m_DataBaseConnection->Close();

    //- Check if the new database is created and the table is created in the new database
    sqlite3 * dbHandle;
    BOOST_REQUIRE_EQUAL(sqlite3_open(newDatabaseFilePath.c_str(), &dbHandle), SQLITE_OK);
    sqlite3_stmt * selectStmt;
    BOOST_REQUIRE_EQUAL(sqlite3_prepare_v2(dbHandle, "SELECT * from NewTable", -1, &selectStmt, NULL), SQLITE_OK);
    BOOST_REQUIRE_EQUAL(sqlite3_step(selectStmt), SQLITE_DONE);
    BOOST_REQUIRE_EQUAL(sqlite3_finalize(selectStmt), SQLITE_OK);
    BOOST_REQUIRE_EQUAL(sqlite3_close(dbHandle), SQLITE_OK);
    RemoveFile(newDatabaseFilePath);
}

BOOST_AUTO_TEST_SUITE_END()

