#include "Headers/Framework/SqliteSampleShapeQueryBuilder.h"

#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Framework/IDatabaseCommandFactory.h"

SqliteSampleShapeQueryBuilder::SqliteSampleShapeQueryBuilder(
        std::shared_ptr<IDatabaseCommandFactory> commandFactory):
        m_CommandFactory(commandFactory)
{
}

std::shared_ptr<IDatabaseCommand> SqliteSampleShapeQueryBuilder::GetCreateTableIfNotExistsQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "CREATE TABLE IF NOT EXISTS Table_SampleShapeData "
        "(ShapeName TEXT PRIMARY KEY NOT NULL)");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleShapeQueryBuilder::GetSelectAllQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("SELECT ShapeName FROM Table_SampleShapeData");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleShapeQueryBuilder::GetInsertQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "INSERT INTO "
        "Table_SampleShapeData (ShapeName) "
        "VALUES(:ShapeName)");
    dbCommand->AddParameter("ShapeName", ":ShapeName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleShapeQueryBuilder::GetRowCountQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "SELECT COUNT(Table_SampleShapeData.ShapeName) "
        "FROM Table_SampleShapeData "
        "WHERE :ShapeName = ShapeName");
    dbCommand->AddParameter("ShapeName", ":ShapeName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleShapeQueryBuilder::GetUpdateQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("UPDATE Table_SampleShapeData SET "
        "ShapeName = :ShapeName "
        "WHERE ShapeName = :ShapeName ");
    dbCommand->AddParameter("ShapeName", ":ShapeName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteSampleShapeQueryBuilder::GetDeleteQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("DELETE FROM Table_SampleShapeData WHERE ShapeName = :ShapeName");
    dbCommand->AddParameter("ShapeName", ":ShapeName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}
