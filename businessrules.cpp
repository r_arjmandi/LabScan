#include "Headers/BusinessRules/BusinessRules.h"

#include "Headers/BusinessEntities/PatientData.h"
#include "Headers/BusinessEntities/ICircleDetector.h"
#include "Headers/BusinessEntities/ICircularSorter.h"
#include "Headers/BusinessEntities/CircularTarget.h"
#include "Headers/BusinessEntities/PatientShortInformation.h"
#include "Headers/BusinessEntities/IDateTimeOperation.h"
#include "Headers/BusinessEntities/ICalibratorRepository.h"

#include "Headers/BusinessEntities/IPatientShortInformationRepository.h"
#include "Headers/BusinessEntities/ICircleRepository.h"
#include "Headers/BusinessEntities/IOperatorRepository.h"
#include "Headers/BusinessEntities/ISampleTypeRepository.h"
#include "Headers/BusinessEntities/ISampleShapeRepository.h"
#include "Headers/BusinessEntities/IGeneralCommentRepository.h"
#include "Headers/BusinessEntities/IMediaRepository.h"
#include "Headers/BusinessEntities/PatientListFilter.h"

#include "Headers/BusinessRules/IPatientDataAccess.h"
#include "Headers/BusinessRules/IImageDataAccess.h"
#include "Headers/BusinessRules/IOperatorDataAccess.h"
#include "Headers/BusinessRules/ISampleTypeDataAccess.h"
#include "Headers/BusinessRules/ISampleShapeDataAccess.h"
#include "Headers/BusinessRules/ICommentDataAccess.h"
#include "Headers/BusinessRules/IMediaDataAccess.h"
#include "Headers/BusinessRules/IBackEndAdapter.h"
#include "Headers/BusinessRules/IPatientListSearchEngine.h"
#include "Headers/BusinessRules/ICalibratorDataAccess.h"
#include "Headers/BusinessRules/IPatientLogger.h"
#include "Headers/BusinessRules/IRegionGrowing.h"
#include "Headers/BusinessRules/IExcelFilePrinter.h"

BusinessRules::BusinessRules(
        std::shared_ptr<IBackEndAdapter> backEndAdapter,
        std::shared_ptr<ICircleDetector> circleDetector,
        std::shared_ptr<ICircularSorter> circularSorter,
        std::shared_ptr<IPatientDataAccess> patientDataAccess,
        std::shared_ptr<IImageDataAccess> imageDataAccess,
        std::shared_ptr<IOperatorDataAccess> operatorDataAccess,
        std::shared_ptr<ISampleTypeDataAccess> sampleTypeDataAccess,
        std::shared_ptr<ISampleShapeDataAccess> sampleShapeDataAccess,
        std::shared_ptr<ICommentDataAccess> commentDataAccess,
        std::shared_ptr<IMediaDataAccess> mediaDataAccess,
        std::shared_ptr<IPatientShortInformationRepository> patientInfoList,
        std::shared_ptr<ICircleRepository> circleRepo,
        std::shared_ptr<IOperatorRepository> operatorRepo,
        std::shared_ptr<ISampleTypeRepository> sampleTypeRepo,
        std::shared_ptr<ISampleShapeRepository> sampleShapeRepo,
        std::shared_ptr<IGeneralCommentRepository> commentRepo,
        std::shared_ptr<IMediaRepository> mediaRepo,
        std::shared_ptr<IPatientListSearchEngine> patientListSearchEngine,
        std::shared_ptr<IDateTimeOperation> dateTimeOperation,
        std::shared_ptr<ICalibratorDataAccess> calibratorDataAccess,
        std::shared_ptr<ICalibratorRepository> calibratorRepo,
        std::shared_ptr<IPatientLogger> patientLogger,
        std::shared_ptr<IRegionGrowing> regionGrowing,
        std::shared_ptr<IExcelFilePrinter> excelFilePrinter):
    m_BackEndAdapter(backEndAdapter),
    m_CircleDetector(circleDetector),
    m_CircularSorter(circularSorter),
    m_PatientDataAccess(patientDataAccess),
    m_ImageDataAccess(imageDataAccess),
    m_OperatorDataAccess(operatorDataAccess),
    m_SampleTypeDataAccess(sampleTypeDataAccess),
    m_SampleShapeDataAccess(sampleShapeDataAccess),
    m_CommentDataAccess(commentDataAccess),
    m_MediaDataAccess(mediaDataAccess),
    m_CircleRepo(circleRepo),
    m_OperatorRepo(operatorRepo),
    m_SampleTypeRepo(sampleTypeRepo),
    m_SampleShapeRepo(sampleShapeRepo),
    m_CommentRepo(commentRepo),
    m_MediaRepo(mediaRepo),
    m_PatientListSearchEngine(patientListSearchEngine),
    m_CurrentPatientData(std::make_shared<PatientData>()),
    m_PatientInfoList(patientInfoList),
    m_DateTimeOperation(dateTimeOperation),
    m_CalibratorDataAccess(calibratorDataAccess),
    m_CalibratorRepo(calibratorRepo),
    m_PatientLogger(patientLogger),
    m_RegionGrowing(regionGrowing),
    m_ExcelFilePrinter(excelFilePrinter)
{
    LoadAllPatientsInfo();
    LoadAllOperators();

    m_SampleTypeDataAccess->InsertOrUpdate("Urine");
    m_SampleTypeDataAccess->InsertOrUpdate("Blood");
    m_SampleTypeDataAccess->InsertOrUpdate("Mucous");
    m_SampleTypeDataAccess->InsertOrUpdate("Saliva");
    LoadAllSampleTypes();

    m_SampleShapeDataAccess->InsertOrUpdate("Cocci");
    m_SampleShapeDataAccess->InsertOrUpdate("Bacilli");
    m_SampleShapeDataAccess->InsertOrUpdate("Spirilla");
    LoadAllSampleShapes();

    LoadAllComments();
    LoadAllMedia();
    LoadAllCalibrator();
}

void BusinessRules::InsertOrUpdateCurrentPatient()
{
    auto nowDateTimeEpoch = std::time(0);
    auto nowDataTime = m_DateTimeOperation->FromEpoch(nowDateTimeEpoch);
    m_CurrentPatientData->SetModifyDateTime(nowDataTime);

    m_ImageDataAccess->InsertOrUpdate(m_CurrentPatientData->GetId(), m_CurrentPatientData->GetImages());
    m_PatientDataAccess->InsertOrUpdate(m_CurrentPatientData);

    auto patientInfo = std::make_unique<PatientShortInformation>();
    patientInfo->Id = m_CurrentPatientData->GetId();
    patientInfo->Name = m_CurrentPatientData->GetName();
    patientInfo->FamilyName = m_CurrentPatientData->GetFamilyName();
    patientInfo->ModifyDateTime = m_CurrentPatientData->GetModifyDateTime();

    m_PatientInfoList->InsertOrUpdate(std::move(patientInfo));
    m_BackEndAdapter->UpdatePatientListRepresentation(std::move(m_PatientInfoList->GetList()));
}

void BusinessRules::DetectImageCircles(double diameter, int count)
{
    auto lockedCurrentImageAddress = m_CurrentPatientData->GetCurrentImage().lock();
    if(!lockedCurrentImageAddress)
    {
        return;
    }
    auto calibratorCoef = m_CalibratorRepo->GetCurrentCoefficient();
    auto radius = (diameter / 2.0) / calibratorCoef;
    qDebug() << "radius" << radius;
    auto detectedCircles = std::move(m_CircleDetector->DetectCircles(
                                         *lockedCurrentImageAddress, radius - 20, radius + 20, count));
    auto circularTargets = m_RegionGrowing->TryDetectRegions(*lockedCurrentImageAddress, detectedCircles);
    m_CircleRepo->AddCircularTargets(*lockedCurrentImageAddress, std::move(circularTargets));
}

void BusinessRules::InsertOrUpdateOperator(const std::string& operatorName)
{
    m_OperatorDataAccess->InsertOrUpdate(operatorName);
}

void BusinessRules::DeleteOperator(const std::string& operatorName)
{
    m_OperatorDataAccess->Delete(operatorName);
}

void BusinessRules::InsertOrUpdateSampleType(const std::string& sampleType)
{
    m_SampleTypeDataAccess->InsertOrUpdate(sampleType);
}

void BusinessRules::DeleteSampleType(const std::string& sampleType)
{
    m_SampleTypeDataAccess->Delete(sampleType);
}

void BusinessRules::InsertOrUpdateSampleShape(const std::string& sampleShape)
{
    m_SampleShapeDataAccess->InsertOrUpdate(sampleShape);
}

void BusinessRules::DeleteSampleShape(const std::string& sampleShape)
{
    m_SampleShapeDataAccess->Delete(sampleShape);
}

void BusinessRules::InsertOrUpdateComment(std::unique_ptr<CommentData> comment)
{
    m_CommentDataAccess->InsertOrUpdate(std::move(comment));
}

void BusinessRules::DeleteComment(const std::string& subject)
{
    m_CommentDataAccess->Delete(subject);
}

void BusinessRules::InsertOrUpdateMedia(const std::string& media)
{
    m_MediaDataAccess->InsertOrUpdate(media);
}

void BusinessRules::DeleteMedia(const std::string& media)
{
    m_MediaDataAccess->Delete(media);
}

void BusinessRules::ShuffleImageCircles()
{
    auto lockedCurrentImage = m_CurrentPatientData->GetCurrentImage().lock();
    if(!lockedCurrentImage)
    {
        return;
    }

    auto circles = m_CircleRepo->GetCircularTargets(*lockedCurrentImage);
    auto lockedCircles = circles.lock();
    if(!lockedCircles)
    {
        return;
    }
    std::random_device randomBitGeneratorDevice;
    std::shuffle(lockedCircles->begin(), lockedCircles->end(), randomBitGeneratorDevice);
    m_CircleRepo->Update();
}

void BusinessRules::SortCurrentImageCirclesCircular(const CircularSortType& sortType)
{
    auto lockedCurrentImage = m_CurrentPatientData->GetCurrentImage().lock();
    if(!lockedCurrentImage)
    {
        return;
    }

    auto circles = m_CircleRepo->GetCircularTargets(*lockedCurrentImage);
    switch (sortType) {
    case CircularSortType::Clockwise:
    {
        m_CircularSorter->SortClockwise(circles);
        break;
    }
    case CircularSortType::CenterClockwise:
    {
        m_CircularSorter->SortCenterClockwise(circles);
        break;
    }
    case CircularSortType::CounterClockwise:
    {
        m_CircularSorter->SortCounterClockwise(circles);
        break;
    }
    case CircularSortType::CenterCounterClockwise:
    {
        m_CircularSorter->SortCenterCounterClockwise(circles);
        break;
    }
    }
    m_CircleRepo->Update();
}

void BusinessRules::LoadPatient(const std::string &patientUid)
{
    auto patientData = std::move(m_PatientDataAccess->Get(patientUid));
    patientData->SetImages(std::move(m_ImageDataAccess->GetAll(patientData->GetId())));
    m_CurrentPatientData = std::shared_ptr<IPatientData>(std::move(patientData));
    m_BackEndAdapter->UpdateCurrentPatientRepresentation(m_CurrentPatientData);
}

void BusinessRules::FilterPatientList(std::unique_ptr<PatientListFilter> patientListFilter) const
{
    auto patientInfoList = m_PatientInfoList->GetList();
    auto newPatientInfoList = m_PatientListSearchEngine->Search(std::move(patientInfoList), std::move(patientListFilter));
    m_BackEndAdapter->UpdatePatientListRepresentation(std::move(newPatientInfoList));
}

std::weak_ptr<IPatientData> BusinessRules::GetCurrentPatientData() const
{
    return m_CurrentPatientData;
}

void BusinessRules::InsertOrUpdateCalibrator(const std::pair<std::string, double> &&calibrator)
{
    m_CalibratorDataAccess->InsertOrUpdate(std::move(calibrator));
}

void BusinessRules::DeleteCalibrator(const std::string &calibratorName)
{
    m_CalibratorDataAccess->Delete(calibratorName);
}

void BusinessRules::PrintCurrentPatient()
{
    auto currentImage = m_CurrentPatientData->GetCurrentImage().lock();
    if(!currentImage)
    {
        return;
    }

    auto circularTargets = m_CircleRepo->GetCircularTargets(*currentImage);
    m_PatientLogger->Dump(m_CurrentPatientData, circularTargets);
    m_ExcelFilePrinter->Print();
}

void BusinessRules::LoadAllPatientsInfo()
{
    m_PatientInfoList->SetList(std::move(m_PatientDataAccess->GetAll()));
    m_BackEndAdapter->UpdatePatientListRepresentation(std::move(m_PatientInfoList->GetList()));
}

void BusinessRules::LoadAllOperators()
{
    auto operatorList = std::move(m_OperatorDataAccess->GetAllOperators());
    m_OperatorRepo->SetOperatorList(std::move(operatorList));
}

void BusinessRules::LoadAllSampleTypes()
{
    auto typeList = std::move(m_SampleTypeDataAccess->GetAllTypes());
    m_SampleTypeRepo->SetSampleTypeList(std::move(typeList));
}

void BusinessRules::LoadAllSampleShapes()
{
    auto shapeList = std::move(m_SampleShapeDataAccess->GetAllShapes());
    m_SampleShapeRepo->SetSampleShapeList(std::move(shapeList));
}

void BusinessRules::LoadAllComments()
{
    auto commentList = std::move(m_CommentDataAccess->GetAllComments());
    m_CommentRepo->SetGeneralCommentList(std::move(commentList));
}

void BusinessRules::LoadAllMedia()
{
    auto mediaList = std::move(m_MediaDataAccess->GetAllMedia());
    m_MediaRepo->SetMediaList(std::move(mediaList));
}

void BusinessRules::LoadAllCalibrator()
{
    auto calibratorList = std::move(m_CalibratorDataAccess->GetAllCalibrator());
    m_CalibratorRepo->SetCalibratorList(std::move(calibratorList));
}
