#include "Headers/Adapters/PatientDataAccess.h"

#include "Headers/Adapters/ConnectionGuard.h"
#include "Headers/Adapters/IPatientQueryBuilder.h"
#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Adapters/IDatabaseConnection.h"
#include "Headers/Adapters/IQueryResult.h"

#include "Headers/BusinessEntities/PatientData.h"
#include "Headers/BusinessEntities/PatientShortInformation.h"

PatientDataAccess::PatientDataAccess(
        std::shared_ptr<IDatabaseConnection> dbConnection,
        std::shared_ptr<IPatientQueryBuilder> patientQueryBuilder):
    m_DatabaceConnection(dbConnection),
    m_PatientQueryBuilder(patientQueryBuilder)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto createTableCommand = m_PatientQueryBuilder->GetCreateTableIfNotExistsQuery();
    m_DatabaceConnection->Exec(createTableCommand);
}

void PatientDataAccess::InsertOrUpdate(std::weak_ptr<IPatientData> patientData)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto rowCountCommand = m_PatientQueryBuilder->GetRowCountQuery();
    auto lockedPatientData = patientData.lock();

    if(!lockedPatientData)
    {
        return;
    }

    rowCountCommand->BindParameter("ID", lockedPatientData->GetId());
    auto queryResult = m_DatabaceConnection->Exec(rowCountCommand);

    if (queryResult->GetInt(0) == 0)
    {
        auto insertCommand = m_PatientQueryBuilder->GetInsertQuery();
        BindCommandParameter(insertCommand, lockedPatientData);
        m_DatabaceConnection->Exec(insertCommand);
    }
    else
    {
        auto updateCommand = m_PatientQueryBuilder->GetUpdateQuery();
        BindCommandParameter(updateCommand, lockedPatientData);
        m_DatabaceConnection->Exec(updateCommand);
    }
}

std::unique_ptr<IPatientData> PatientDataAccess::Get(const std::string &patientUid)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto selectCommand = m_PatientQueryBuilder->GetSelectQuery();
    selectCommand->BindParameter("ID", patientUid);

    auto queryResult = m_DatabaceConnection->Exec(selectCommand);

    auto patientData = std::make_unique<PatientData>();
    patientData->SetId(queryResult->GetString(0));
    patientData->SetModifyDateTime(queryResult->GetDateTime(1));
    patientData->SetName(queryResult->GetString(2));
    patientData->SetFamilyName(queryResult->GetString(3));
    patientData->SetAge(queryResult->GetInt(4));
    patientData->SetGender(queryResult->GetInt(5) == 1 ? GenderType::Male : GenderType::Female);
    patientData->SetLabOperator(queryResult->GetInt(6));
    SampleData sampleData;
    sampleData.Bacteria = queryResult->GetInt(7);
    sampleData.Count = queryResult->GetString(8);
    sampleData.Gram = queryResult->GetInt(9) == 1 ? GramType::Positive : GramType::Negative;
    sampleData.Isolated = queryResult->GetInt(10) == 1 ? IsolatedType::Yes : IsolatedType::No;
    sampleData.Shape = queryResult->GetInt(11);
    sampleData.Standard = queryResult->GetInt(12);
    sampleData.Type = queryResult->GetInt(13);
    patientData->SetSample(sampleData);
    CommentData commentData;
    commentData.Description = queryResult->GetString(14);
    commentData.SubjectIndex = queryResult->GetInt(15);
    patientData->SetComment(commentData);
    patientData->SetPlateMediaIndex(queryResult->GetInt(16));

    return std::move(patientData);
}

std::vector<std::unique_ptr<PatientShortInformation>> PatientDataAccess::GetAll()
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto selectAllCommand = m_PatientQueryBuilder->GetSelectAllQuery();
    auto queryResult = m_DatabaceConnection->Exec(selectAllCommand);

    auto patientInfoList = std::vector<std::unique_ptr<PatientShortInformation>>();

    while(queryResult->HasData())
    {
        auto patientData = std::make_unique<PatientShortInformation>();
        patientData->Id = queryResult->GetString(0);
        patientData->ModifyDateTime = queryResult->GetDateTime(1);
        patientData->Name = queryResult->GetString(2);
        patientData->FamilyName = queryResult->GetString(3);
        patientInfoList.push_back(std::move(patientData));
        queryResult->Next();
    }
    return std::move(patientInfoList);
}

void PatientDataAccess::BindCommandParameter(
        std::shared_ptr<IDatabaseCommand> dbCommand,
        std::weak_ptr<IPatientData> inPatientData)
{
    auto lockedPatientData = inPatientData.lock();
    dbCommand->BindParameter("ID", lockedPatientData->GetId());
    dbCommand->BindParameter("ModifyDateTime", lockedPatientData->GetModifyDateTime());
    dbCommand->BindParameter("Name", lockedPatientData->GetName());
    dbCommand->BindParameter("FamilyName", lockedPatientData->GetFamilyName());
    dbCommand->BindParameter("Age", lockedPatientData->GetAge());
    dbCommand->BindParameter("Gender",
                             lockedPatientData->GetGender() == GenderType::Male ? 1 : 2);
    dbCommand->BindParameter("LabOperator", lockedPatientData->GetLabOperator());
    dbCommand->BindParameter("SampleType", lockedPatientData->GetSample().Type);
    dbCommand->BindParameter("SampleStandard", lockedPatientData->GetSample().Standard);
    dbCommand->BindParameter("SampleBacteria", lockedPatientData->GetSample().Bacteria);
    dbCommand->BindParameter("SampleGram",
                             lockedPatientData->GetSample().Gram ==
                             GramType::Positive ? 1 : -1);
    dbCommand->BindParameter("SampleShape", lockedPatientData->GetSample().Shape);
    dbCommand->BindParameter("SampleIsolated",
                             lockedPatientData->GetSample().Isolated ==
                             IsolatedType::Yes ? 1 : -1);
    dbCommand->BindParameter("SampleCount", lockedPatientData->GetSample().Count);
    dbCommand->BindParameter("CommentSubjectIndex",
                             lockedPatientData->GetComment().SubjectIndex);
    dbCommand->BindParameter("CommentDescription",
                             lockedPatientData->GetComment().Description);
    dbCommand->BindParameter("PlateMediaIndex", lockedPatientData->GetPlateMediaIndex());
}
