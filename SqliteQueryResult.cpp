#include "Headers/Framework/SqliteQueryResult.h"
#include "Headers/Framework/SqliteDatabaseException.h"

SqliteQueryResult::SqliteQueryResult
(sqlite3_stmt* sqliteStmt, const std::shared_ptr<IDateTimeOperation> dateTimeOps)
	: m_SqliteStmt(sqliteStmt),
	m_DateTimeOps(dateTimeOps)
{
	m_SqliteStatus = sqlite3_step(m_SqliteStmt);

	switch (m_SqliteStatus)
	{
	case SQLITE_CONSTRAINT:
		sqlite3_finalize(m_SqliteStmt);
		throw(SqliteDatabaseException("Constraints not met!", SQLITE_CONSTRAINT));
		break;
	case SQLITE_ROW:
	case SQLITE_DONE:
		break;
	default:
		sqlite3_finalize(m_SqliteStmt);
		throw(SqliteDatabaseException("Error executing SQL statement: ", m_SqliteStatus));
	}
}

SqliteQueryResult::~SqliteQueryResult()
{
	sqlite3_finalize(m_SqliteStmt);
}

void SqliteQueryResult::Next()
{
	m_SqliteStatus = sqlite3_step(m_SqliteStmt);
}

bool SqliteQueryResult::HasData()
{
	return m_SqliteStatus == SQLITE_ROW;
}

std::string SqliteQueryResult::GetString(int colIndex)
{
	ThrowExceptionIfTableEmpty();
	auto textGet = reinterpret_cast<char const *>(sqlite3_column_text(m_SqliteStmt, colIndex));
	auto result = std::string(textGet);
	return result;
}

long long int SqliteQueryResult::GetInt(int colIndex)
{
	ThrowExceptionIfTableEmpty(); 
	auto result = static_cast<long long int>(sqlite3_column_int64(m_SqliteStmt, colIndex));
	return result;
}

double SqliteQueryResult::GetDouble(int colIndex)
{
	ThrowExceptionIfTableEmpty();
	auto result = static_cast<double>(sqlite3_column_double(m_SqliteStmt, colIndex));
	return result;
}

bool SqliteQueryResult::GetBool(int colIndex)
{
	ThrowExceptionIfTableEmpty();
	auto result = static_cast<bool>(sqlite3_column_int64(m_SqliteStmt, colIndex) ? true : false);
	return result;
}

DateTime SqliteQueryResult::GetDateTime(int colIndex)
{
	ThrowExceptionIfTableEmpty();
	auto timeEncode = static_cast<long long int>(sqlite3_column_int64(m_SqliteStmt, colIndex));
	auto resultTime = m_DateTimeOps->DecodeInt64(timeEncode);
	return resultTime;
}

void SqliteQueryResult::CheckStatus()
{
	if (m_SqliteStatus == SQLITE_CONSTRAINT)
		throw(SqliteDatabaseException("A constraint is not met!"));
}

std::shared_ptr<ByteArray> SqliteQueryResult::GetBinary(int colIndex)
{
	ThrowExceptionIfTableEmpty();
	auto sizeData = sqlite3_column_bytes(m_SqliteStmt, colIndex);
	auto resultData = reinterpret_cast<const uint8_t*>(sqlite3_column_blob(m_SqliteStmt, colIndex));
	auto result = resultData ? std::make_shared<ByteArray>(resultData, resultData + sizeData) : nullptr;
	return result;
}

void SqliteQueryResult::ThrowExceptionIfTableEmpty()
{
	if (!HasData())
		throw(SqliteDatabaseException("the table is empty!"));
}
