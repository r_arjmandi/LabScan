#include "Headers/Framework/SqliteOperatorQueryBuilder.h"

#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Framework/IDatabaseCommandFactory.h"

SqliteOperatorQueryBuilder::SqliteOperatorQueryBuilder(
        std::shared_ptr<IDatabaseCommandFactory> commandFactory):
    m_CommandFactory(commandFactory)
{
}

std::shared_ptr<IDatabaseCommand> SqliteOperatorQueryBuilder::GetCreateTableIfNotExistsQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "CREATE TABLE IF NOT EXISTS Table_OperatorData "
        "(OperatorName TEXT PRIMARY KEY NOT NULL)");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteOperatorQueryBuilder::GetInsertQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();

    dbCommand->SetCommandString(
        "INSERT INTO "
        "Table_OperatorData (OperatorName) "
        "VALUES(:OperatorName)");

    dbCommand->AddParameter("OperatorName", ":OperatorName", IDatabaseCommand::ParameterType::String);

    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteOperatorQueryBuilder::GetRowCountQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "SELECT COUNT(Table_OperatorData.OperatorName) "
        "FROM Table_OperatorData "
        "WHERE :OperatorName = OperatorName");
    dbCommand->AddParameter("OperatorName", ":OperatorName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteOperatorQueryBuilder::GetUpdateQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("UPDATE Table_OperatorData SET "
        "OperatorName = :OperatorName "
        "WHERE OperatorName = :OperatorName ");
    dbCommand->AddParameter("OperatorName", ":OperatorName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteOperatorQueryBuilder::GetSelectAllQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("SELECT OperatorName FROM Table_OperatorData");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteOperatorQueryBuilder::GetDeleteQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("DELETE FROM Table_OperatorData WHERE OperatorName = :OperatorName");
    dbCommand->AddParameter("OperatorName", ":OperatorName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}


