#include "Headers\Framework\BacteriaRepositoryDataModel.h"

BacteriaRepositoryDataModel::BacteriaRepositoryDataModel()
{
    m_BacteriaRoleNames[Name] = "Name";
}

int BacteriaRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    if(!m_BacteriaList)
        return 0;

    return static_cast<int>(m_BacteriaList->size());
}

QVariant BacteriaRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(!m_BacteriaList)
        return QVariant();

    if(m_BacteriaList->empty())
        return QVariant();

    int row = index.row();
    if(row < 0 || row >= m_BacteriaList->size())
        return QVariant();

    switch(role) {
        case Name:
            return QString(m_BacteriaList->at(row).c_str());
    }

    return QVariant();
}

void BacteriaRepositoryDataModel::SetBacteriaList(std::shared_ptr<std::vector<std::string> > bacteriaList)
{
    m_BacteriaList = bacteriaList;
    emit endResetModel();
}

std::shared_ptr<std::vector<std::string>> BacteriaRepositoryDataModel::GetBacteriaList() const
{
    return m_BacteriaList;
}

QHash<int, QByteArray> BacteriaRepositoryDataModel::roleNames() const
{
    return m_BacteriaRoleNames;
}
