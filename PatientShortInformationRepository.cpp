#include "Headers/BusinessEntities/PatientShortInformationRepository.h"
#include "Headers/BusinessEntities/SampleData.h"
#include "Headers/BusinessEntities/CommentData.h"
#include "Headers/BusinessEntities/PatientShortInformation.h"

class PatientDataMatcher
{
public:
    PatientDataMatcher(const std::string& patientUid):
        m_PatientUid(patientUid)
    {
    }

    bool operator()(const std::shared_ptr<PatientShortInformation> patientInfo)
    {
        return m_PatientUid == patientInfo->Id;
    }

private:
    std::string m_PatientUid;

};

void PatientShortInformationRepository::SetList(std::vector<std::unique_ptr<PatientShortInformation>>&& patientInfoList)
{
    for(auto& patient : patientInfoList)
    {
        m_PatientInfoList.push_back(std::shared_ptr<PatientShortInformation>(std::move(patient)));
    }
}

void PatientShortInformationRepository::InsertOrUpdate(std::unique_ptr<PatientShortInformation> patientInfo)
{
    auto patientDataMatcher = PatientDataMatcher(patientInfo->Id);
    auto findPatint = std::find_if(m_PatientInfoList.begin(), m_PatientInfoList.end(),
                                   patientDataMatcher);

    if(findPatint == m_PatientInfoList.cend())
    {
        m_PatientInfoList.push_back(std::move(patientInfo));
    }
    else
    {
        (*findPatint)->Name = patientInfo->Name;
        (*findPatint)->FamilyName = patientInfo->FamilyName;
    }
}

std::vector<std::weak_ptr<PatientShortInformation> > PatientShortInformationRepository::GetList() const
{
    auto result = std::vector<std::weak_ptr<PatientShortInformation>>();
    for(auto& patient :m_PatientInfoList)
    {
        result.push_back(std::weak_ptr<PatientShortInformation>(patient));
    }
    return result;
}

std::weak_ptr<PatientShortInformation> PatientShortInformationRepository::Get(const std::string &patientUid) const
{
    auto patientDataMatcher = PatientDataMatcher(patientUid);
    auto findPatint = std::find_if(m_PatientInfoList.begin(), m_PatientInfoList.end(),
                                   patientDataMatcher);

    if(findPatint == m_PatientInfoList.cend())
    {
        return {};
    }
    return *findPatint;
}
