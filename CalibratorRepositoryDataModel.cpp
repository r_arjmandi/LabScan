#include "Headers/Framework/CalibratorRepositoryDataModel.h"

#include "Headers/Adapters/IFrontEndAdapter.h"

CalibratorRepositoryDataModel::CalibratorRepositoryDataModel()
{
    m_CalibratorRoleNames[Name] = "Name";
    m_CalibratorRoleNames[Coefficient] = "Coefficient";
}

void CalibratorRepositoryDataModel::SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter)
{
    m_adapter = adapter;
}

void CalibratorRepositoryDataModel::AddCalibrator(const QString &name, double coefficient)
{
    auto calibrator = std::make_pair(name.toStdString(), coefficient);
    m_Calibrators.push_back(calibrator);
    m_adapter->InsertOrUpdateCalibrator(std::move(calibrator));
    emit endResetModel();
}

void CalibratorRepositoryDataModel::SetCurrentCalibrator(int index)
{
    m_CurrentCalibratorIndex = index;
}

void CalibratorRepositoryDataModel::DeleteCurrentCalibrator()
{
    if(m_CurrentCalibratorIndex >= 0)
    {
        auto currentCalibrator = m_Calibrators.begin() + m_CurrentCalibratorIndex;
        m_adapter->DeleteCalibrator(currentCalibrator->first);
        m_Calibrators.erase(currentCalibrator);
        emit endResetModel();
    }
}

int CalibratorRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return static_cast<int>(m_Calibrators.size());
}

QVariant CalibratorRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(m_Calibrators.empty())
        return QVariant();

    int row = index.row();
    if(row < 0 || row >= m_Calibrators.size())
        return QVariant();

    switch(role) {
        case Name:
            return QString(m_Calibrators.at(row).first.c_str());
    }

    return QVariant();
}

double CalibratorRepositoryDataModel::GetCurrentCoefficient() const
{
    if(m_CurrentCalibratorIndex >= 0)
    {
        auto currentCalib = m_Calibrators.begin() + m_CurrentCalibratorIndex;
        return currentCalib->second;
    }

    return 1.0;
}

void CalibratorRepositoryDataModel::SetCalibratorList(std::vector<std::pair<std::string, double> > &&calibratorList)
{
    m_Calibrators = std::move(calibratorList);
}

QHash<int, QByteArray> CalibratorRepositoryDataModel::roleNames() const
{
    return m_CalibratorRoleNames;
}
