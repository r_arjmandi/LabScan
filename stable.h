    #if defined __cplusplus

#include <memory>
#include <string>
#include <iostream>
#include <set>
#include <ctime>
#include <sstream>
#include <algorithm>
#include <cmath>
#include <functional>
#include <filesystem>
#include <cctype>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <map>
#include <sstream>
#include <utility>
#include <regex>
#include <random>
#include <unordered_map>

#include <QtCore>
#include <QtGui>
#include <QObject>
#include <QString>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QApplication>

#include <boost/variant.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#endif

#include "sqlite3.h"
