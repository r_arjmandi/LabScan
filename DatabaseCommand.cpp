#include "Headers/Adapters/DatabaseCommand.h"

void DatabaseCommand::SetCommandString(const std::string & command)
{
	m_CommandStr = command;
}

std::string DatabaseCommand::GetCommandString() const
{
	return m_CommandStr;
}

void DatabaseCommand::AddParameter(
	const std::string & paramName, const std::string & lowLevelParameterName, ParameterType type)
{
	auto find = m_Bindings.find(paramName);
	if (find != m_Bindings.end())
		if (find->second.Type != type || find->second.LowLevelParameterName != lowLevelParameterName)
			throw DataAccessException("Attempt to add duplicate parameter name with differing types or low level name.");

	Binding binding;
	binding.Name = paramName;
	binding.Type = type;
	binding.LowLevelParameterName = lowLevelParameterName;
	m_Bindings[paramName] = binding;
}

void DatabaseCommand::BindParameter(const std::string & paramName, const std::string & paramValue)
{
    BindParameterImpl<const std::string>(paramName, paramValue, IDatabaseCommand::ParameterType::String);
}

void DatabaseCommand::BindParameter(const std::string & paramName, double paramValue)
{
    BindParameterImpl<double>(paramName, paramValue, IDatabaseCommand::ParameterType::Real);
}

void DatabaseCommand::BindParameter(const std::string & paramName, long long int paramValue)
{
    BindParameterImpl<long long int>(paramName, paramValue, IDatabaseCommand::ParameterType::Integer);
}

void DatabaseCommand::BindParameter(const std::string & paramName, bool paramValue)
{
    BindParameterImpl<bool>(paramName, paramValue, IDatabaseCommand::ParameterType::Boolean);
}

void DatabaseCommand::BindParameter(const std::string& paramName, const DateTime& paramValue)
{
    BindParameterImpl<DateTime>(paramName, paramValue, IDatabaseCommand::ParameterType::DateTime);
}

void DatabaseCommand::BindParameter(const std::string & paramName, std::shared_ptr<ByteArray> paramValue)
{
    BindParameterImpl<std::shared_ptr<ByteArray>>(paramName, paramValue, IDatabaseCommand::ParameterType::Binary);
}

IDatabaseCommand::BindingCollection DatabaseCommand::GetBindings() const
{
	return m_Bindings;
}
