#include "Headers/Adapters/ImageDataAccess.h"

#include "Headers/BusinessEntities/IFileNameGenerator.h"

ImageDataAccess::ImageDataAccess(
        const std::experimental::filesystem::path& imageListDirectoryPath,
        std::shared_ptr<IFileNameGenerator> fileNameGenerator):
    m_ImageListDirectoryPath(imageListDirectoryPath),
    m_FileNameGenerator(fileNameGenerator)
{
    if(!std::experimental::filesystem::exists(m_ImageListDirectoryPath))
    {
        std::experimental::filesystem::create_directory(m_ImageListDirectoryPath);
    }
}

void ImageDataAccess::InsertOrUpdate(const std::string &patientId,
     std::vector<std::weak_ptr<std::experimental::filesystem::path>>&& images)
{
    auto currentPatientImageDirectoryPath =
            std::experimental::filesystem::path(m_ImageListDirectoryPath) /
            std::experimental::filesystem::path(patientId);

    if(!std::experimental::filesystem::exists(currentPatientImageDirectoryPath))
    {
        std::experimental::filesystem::create_directory(currentPatientImageDirectoryPath);
    }

    for(const auto& image : images)
    {
        auto lockedImage = image.lock();
        if(!lockedImage)
        {
            continue;
        }
        auto fileName = lockedImage->filename().string();
        auto destinationPath = currentPatientImageDirectoryPath / fileName;

        if(!std::experimental::filesystem::exists(destinationPath))
        {
            std::experimental::filesystem::copy_file(*lockedImage, destinationPath);
        }
    }
}

std::vector<std::unique_ptr<std::experimental::filesystem::path>>
ImageDataAccess::GetAll(const std::string &patientId)
{
    auto imageDirectoryIter = std::experimental::filesystem::directory_iterator(
            std::experimental::filesystem::path(m_ImageListDirectoryPath) /
            std::experimental::filesystem::path(patientId));
    auto result = std::vector<std::unique_ptr<std::experimental::filesystem::path>>();
    for(const auto& fileEntry : imageDirectoryIter)
    {
        auto file = std::make_unique<std::experimental::filesystem::path>(fileEntry);
        result.push_back(std::move(file));
    }
    return std::move(result);
}
