#include "Headers/Adapters/FrontEndAdapter.h"

#include "Headers/BusinessEntities/PatientData.h"
#include "Headers/BusinessRules/IBusinessRules.h"
#include "Headers/BusinessEntities/PatientListFilter.h"

FrontEndAdapter::FrontEndAdapter(std::shared_ptr<IBusinessRules> businessRules):
    m_BusinessRules(businessRules)
{
}

void FrontEndAdapter::InsertOrUpdateCurrentPatient()
{
    m_BusinessRules->InsertOrUpdateCurrentPatient();
}

void FrontEndAdapter::DetectImageCircles(double diameter, int count)
{
    m_BusinessRules->DetectImageCircles(diameter, count);
}

void FrontEndAdapter::InsertOrUpdateOperator(const std::string &operatorName)
{
    m_BusinessRules->InsertOrUpdateOperator(operatorName);
}

void FrontEndAdapter::DeleteOperator(const std::string &operatorName)
{
    m_BusinessRules->DeleteOperator(operatorName);
}

void FrontEndAdapter::InsertOrUpdateSampleType(const std::string &sampleType)
{
    m_BusinessRules->InsertOrUpdateSampleType(sampleType);
}

void FrontEndAdapter::DeleteSampleType(const std::string &sampleType)
{
    m_BusinessRules->DeleteSampleType(sampleType);
}

void FrontEndAdapter::InsertOrUpdateSampleShape(const std::string &sampleShape)
{
    m_BusinessRules->InsertOrUpdateSampleShape(sampleShape);
}

void FrontEndAdapter::DeleteSampleShape(const std::string &sampleShape)
{
    m_BusinessRules->DeleteSampleShape(sampleShape);
}

void FrontEndAdapter::InsertOrUpdateComment(std::unique_ptr<CommentData> comment)
{
    m_BusinessRules->InsertOrUpdateComment(std::move(comment));
}

void FrontEndAdapter::DeleteComment(const std::string &subject)
{
    m_BusinessRules->DeleteComment(subject);
}

void FrontEndAdapter::InsertOrUpdateMedia(const std::string &media)
{
    m_BusinessRules->InsertOrUpdateMedia(media);
}

void FrontEndAdapter::DeleteMedia(const std::string &media)
{
    m_BusinessRules->DeleteMedia(media);
}

void FrontEndAdapter::ShuffleImageCircles()
{
    m_BusinessRules->ShuffleImageCircles();
}

void FrontEndAdapter::SortCurrentImageCirclesClockwise()
{
    m_BusinessRules->SortCurrentImageCirclesCircular(CircularSortType::Clockwise);
}

void FrontEndAdapter::SortCurrentImageCirclesCenterClockwise()
{
    m_BusinessRules->SortCurrentImageCirclesCircular(CircularSortType::CenterClockwise);
}

void FrontEndAdapter::SortCurrentImageCirclesCounterClockwise()
{
    m_BusinessRules->SortCurrentImageCirclesCircular(CircularSortType::CounterClockwise);
}

void FrontEndAdapter::SortCurrentImageCirclesCenterCounterClockwise()
{
    m_BusinessRules->SortCurrentImageCirclesCircular(CircularSortType::CenterCounterClockwise);
}

void FrontEndAdapter::LoadPatient(const std::string &patientUid)
{
    m_BusinessRules->LoadPatient(patientUid);
}

void FrontEndAdapter::FilterPatientList(std::unique_ptr<PatientListFilter> patientListFilter) const
{
    m_BusinessRules->FilterPatientList(std::move(patientListFilter));
}

void FrontEndAdapter::InsertOrUpdateCalibrator(const std::pair<std::string, double> &&calibrator)
{
    m_BusinessRules->InsertOrUpdateCalibrator(std::move(calibrator));
}

void FrontEndAdapter::DeleteCalibrator(const std::string &calibratorName)
{
    m_BusinessRules->DeleteCalibrator(calibratorName);
}

void FrontEndAdapter::PrintCurrentPatient()
{
    m_BusinessRules->PrintCurrentPatient();
}
