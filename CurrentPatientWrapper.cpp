#include "Headers/Framework/CurrentPatientWrapper.h"
#include "Headers/BusinessEntities/IPatientData.h"
#include "Headers/Framework/IImageListGui.h"
#include "Headers/BusinessEntities/SampleData.h"
#include "Headers/BusinessEntities/CommentData.h"

CurrentPatientWrapper::CurrentPatientWrapper(
        std::shared_ptr<IImageListGui> imageListGui):
    m_ImageListGui(imageListGui)
{
}

void CurrentPatientWrapper::SetCurrenaPatientData(std::weak_ptr<IPatientData> currenaPatientData)
{
    m_CurrenaPatientData = currenaPatientData;
}

std::string CurrentPatientWrapper::GetId() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetId();
}

QString CurrentPatientWrapper::id() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return QString(currentPatientData->GetId().c_str());
}

QString CurrentPatientWrapper::name() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return QString(currentPatientData->GetName().c_str());
}

QString CurrentPatientWrapper::family() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return QString(currentPatientData->GetFamilyName().c_str());
}

QString CurrentPatientWrapper::age() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    if(currentPatientData->GetAge() > 0)
    {
        return QString(std::to_string(currentPatientData->GetAge()).c_str());
    }

    return {};
}

int CurrentPatientWrapper::gender() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetGender() == GenderType::Male ? 1 : 2;
}

int CurrentPatientWrapper::labOperator() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetLabOperator();
}

int CurrentPatientWrapper::sampleType() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetSample().Type;
}

int CurrentPatientWrapper::sampleStandard() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetSample().Standard;
}

int CurrentPatientWrapper::sampleBacteria() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetSample().Bacteria;
}

int CurrentPatientWrapper::sampleGram() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetSample().Gram == GramType::Positive ? 1 : -1;
}

int CurrentPatientWrapper::sampleShape() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetSample().Shape;
}

int CurrentPatientWrapper::sampleIsolated() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetSample().Isolated == IsolatedType::Yes ? 1 : -1;
}

QString CurrentPatientWrapper::sampleCount() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return QString(currentPatientData->GetSample().Count.c_str());
}

int CurrentPatientWrapper::commentSubjectIndex() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetComment().SubjectIndex;
}

QString CurrentPatientWrapper::commentDescription() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return QString(currentPatientData->GetComment().Description.c_str());
}

int CurrentPatientWrapper::plateMediaIndex() const
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return {};
    }

    return currentPatientData->GetPlateMediaIndex();
}

QString CurrentPatientWrapper::plateTemplate() const
{
    return {};
}

int CurrentPatientWrapper::plateDiskNo() const
{
    return {};
}

double CurrentPatientWrapper::plateDiskDiameter() const
{
    return {};
}

int CurrentPatientWrapper::plateDiskType() const
{
    return {};
}

int CurrentPatientWrapper::plateDiskArrange() const
{
    return {};
}

int CurrentPatientWrapper::currentImageIndex() const
{
    return m_CurrentImageIndex;
}

void CurrentPatientWrapper::setId(const QString &id)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || id == QString(currentPatientData->GetId().c_str()))
    {
        return;
    }

    currentPatientData->SetId(id.toStdString());
    emit idChanged();
}

void CurrentPatientWrapper::setName(const QString &name)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || name == QString(currentPatientData->GetName().c_str()))
    {
        return;
    }

    currentPatientData->SetName(name.toStdString());
    emit nameChanged();
}

void CurrentPatientWrapper::setFamily(const QString &family)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || family == QString(currentPatientData->GetFamilyName().c_str()))
    {
        return;
    }

    currentPatientData->SetFamilyName(family.toStdString());
    emit familyChanged();
}

void CurrentPatientWrapper::setAge(const QString &age)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || age == QString(std::to_string(currentPatientData->GetAge()).c_str()))
    {
        return;
    }

    currentPatientData->SetAge(std::atoi(age.toStdString().c_str()));
    emit ageChanged();
}

void CurrentPatientWrapper::setGender(int gender)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    auto convertedGender = gender == 1 ? GenderType::Male : GenderType::Female;
    if(!currentPatientData || convertedGender == currentPatientData->GetGender())
    {
        return;
    }

    currentPatientData->SetGender(convertedGender);
    emit genderChanged();
}

void CurrentPatientWrapper::setLabOperator(int labOperator)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || labOperator == currentPatientData->GetLabOperator())
    {
        return;
    }

    currentPatientData->SetLabOperator(labOperator);
    emit labOperatorChanged();
}

void CurrentPatientWrapper::setSampleType(int sampleType)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || sampleType == currentPatientData->GetSample().Type)
    {
        return;
    }

    auto patientSample = currentPatientData->GetSample();
    patientSample.Type = sampleType;
    currentPatientData->SetSample(patientSample);
    emit sampleTypeChanged();
}

void CurrentPatientWrapper::setSampleStandard(int sampleStandard)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || sampleStandard == currentPatientData->GetSample().Standard)
    {
        return;
    }

    auto patientSample = currentPatientData->GetSample();
    patientSample.Standard = sampleStandard;
    currentPatientData->SetSample(patientSample);
    emit sampleStandardChanged();
}

void CurrentPatientWrapper::setSampleBacteria(int sampleBacteria)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || sampleBacteria == currentPatientData->GetSample().Bacteria)
    {
        return;
    }

    auto patientSample = currentPatientData->GetSample();
    patientSample.Bacteria = sampleBacteria;
    currentPatientData->SetSample(patientSample);
    emit sampleBacteriaChanged();
}

void CurrentPatientWrapper::setSampleGram(int sampleGram)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return;
    }

    auto gramNumber = currentPatientData->GetSample().Gram == GramType::Negative ? -1 : 1;
    if(sampleGram == gramNumber)
    {
        return;
    }

    auto patientSample = currentPatientData->GetSample();
    patientSample.Gram = sampleGram == -1 ? GramType::Negative : GramType::Positive;
    currentPatientData->SetSample(patientSample);
    emit sampleGramChanged();
}

void CurrentPatientWrapper::setSampleShape(int sampleShape)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || sampleShape == currentPatientData->GetSample().Shape)
    {
        return;
    }

    auto patientSample = currentPatientData->GetSample();
    patientSample.Shape = sampleShape;
    currentPatientData->SetSample(patientSample);
    emit sampleShapeChanged();
}

void CurrentPatientWrapper::setSampleIsolated(int sampleIsolated)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return;
    }

    auto isolatedNumber = currentPatientData->GetSample().Isolated == IsolatedType::Yes ? 1 : -1;
    if(sampleIsolated == isolatedNumber)
    {
        return;
    }
    auto patientSample = currentPatientData->GetSample();
    patientSample.Isolated = sampleIsolated == 1 ? IsolatedType::Yes : IsolatedType::No;
    currentPatientData->SetSample(patientSample);
    emit sampleIsolatedChanged();
}

void CurrentPatientWrapper::setSampleCount(const QString& sampleCount)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || sampleCount == QString(currentPatientData->GetSample().Count.c_str()))
    {
        return;
    }
    auto patientSample = currentPatientData->GetSample();
    patientSample.Count = sampleCount.toStdString();
    currentPatientData->SetSample(patientSample);
    emit sampleCountChanged();
}

void CurrentPatientWrapper::SetCommentSubjectIndex(int index)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || index == currentPatientData->GetComment().SubjectIndex)
    {
        return;
    }

    auto patientComment = currentPatientData->GetComment();
    patientComment.SubjectIndex = index;
    currentPatientData->SetComment(patientComment);
    emit commentSubjectIndexChanged();
}

void CurrentPatientWrapper::Update()
{
    emit idChanged();
    emit nameChanged();
    emit familyChanged();
    emit ageChanged();
    emit genderChanged();
    emit labOperatorChanged();
    emit sampleTypeChanged();
    emit sampleStandardChanged();
    emit sampleBacteriaChanged();
    emit sampleGramChanged();
    emit sampleShapeChanged();
    emit sampleIsolatedChanged();
    emit sampleCountChanged();
    emit commentSubjectIndexChanged();
    emit commentDescriptionChanged();
    emit plateMediaIndexChanged();
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return;
    }
    m_ImageListGui->SetImageList(std::move(currentPatientData->GetImages()));
}

void CurrentPatientWrapper::setCurrentImageIndex(int index)
{
    if(m_CurrentImageIndex == index)
    {
        return;
    }

    m_CurrentImageIndex = index;
    emit currentImageIndexChanged();
}

void CurrentPatientWrapper::setCommentDescription(const QString &commentDescription)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData ||
            commentDescription == QString(currentPatientData->GetComment().Description.c_str()))
    {
        return;
    }

    auto patientComment = currentPatientData->GetComment();
    patientComment.Description = commentDescription.toStdString();
    currentPatientData->SetComment(patientComment);
    emit commentDescriptionChanged();
}

void CurrentPatientWrapper::setPlateMediaIndex(int plateMediaIndex)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData || plateMediaIndex == currentPatientData->GetPlateMediaIndex())
    {
        return;
    }

    currentPatientData->SetPlateMediaIndex(plateMediaIndex);
    emit plateMediaIndexChanged();
}

void CurrentPatientWrapper::setPlateTemplate(const QString &plateTemplate)
{
    plateTemplate;
}

void CurrentPatientWrapper::setPlateDiskNo(int plateDiskNo)
{
    plateDiskNo;
}

void CurrentPatientWrapper::setPlateDiskDiameter(double plateDiskDiameter)
{
    plateDiskDiameter;
}

void CurrentPatientWrapper::setPlateDiskType(int plateDiskType)
{
    plateDiskType;
}

void CurrentPatientWrapper::setPlateDiskArrange(int plateDiskArrange)
{
    plateDiskArrange;
}

void CurrentPatientWrapper::AddNewImage(const QUrl &newImagePath)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return;
    }

    auto path = newImagePath.toLocalFile().toStdString();
    auto sourcePath = std::make_unique<std::experimental::filesystem::path>(path);
    currentPatientData->AddImage(std::move(sourcePath));
    m_ImageListGui->SetImageList(std::move(currentPatientData->GetImages()));
    currentPatientData->SetCurrentImage(m_CurrentImageIndex + 1);
    setCurrentImageIndex(m_CurrentImageIndex + 1);
}

void CurrentPatientWrapper::SetCurrentImage(int imageIndex)
{
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return;
    }
    currentPatientData->SetCurrentImage(imageIndex);
    setCurrentImageIndex(imageIndex);
}

void CurrentPatientWrapper::Clear()
{
    setId("");
    setName("");
    setFamily("");
    setAge("");
    setGender(1);
    setLabOperator(-1);
    setSampleType(-1);
    setSampleStandard(-1);
    setSampleBacteria(-1);
    setSampleShape(-1);
    setSampleGram(1);
    setSampleIsolated(1);
    setSampleCount("");
    SetCommentSubjectIndex(-1);
    setCommentDescription("");
    auto currentPatientData = m_CurrenaPatientData.lock();
    if(!currentPatientData)
    {
        return;
    }
    currentPatientData->ClearImages();
    m_ImageListGui->ClearImages();
}
