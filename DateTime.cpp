#include "Headers/BusinessEntities/DateTime.h"

DateTime::DateTime()
	: m_Year(1970),
	m_Month(1),
	m_Day(1),
	m_Hour(0),
	m_Minute(0),
	m_Second(0)
{
}

DateTime::DateTime(int year, unsigned char month, unsigned char day)
	: m_Year(year),
	m_Month(month),
	m_Day(day),
	m_Hour(0),
	m_Minute(0),
	m_Second(0)
{
}

DateTime::DateTime(
	int year, unsigned char month, unsigned char day, unsigned char hour, unsigned char minute, double second)
	: m_Year(year),
	m_Month(month),
	m_Day(day),
	m_Hour(hour),
	m_Minute(minute),
	m_Second(second)
{
}

int DateTime::GetYear() const
{
	return m_Year;
}

int DateTime::GetMonth() const
{
	return m_Month;
}

int DateTime::GetDay() const
{
	return m_Day;
}

int DateTime::GetHour() const
{
	return m_Hour;
}

int DateTime::GetMinute() const
{
	return m_Minute;
}

double DateTime::GetSecond() const
{
	return m_Second;
}
