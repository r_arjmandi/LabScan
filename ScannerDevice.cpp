#include "Headers\Framework\ScannerDevice.h"

QUrl ScannerDevice::StartScan()
{
    m_Process.start(m_ScannerProgramPath);
    m_Process.setProcessChannelMode(QProcess::MergedChannels);
    m_Process.waitForStarted();
    m_Process.write("cdsm\r\n");
    m_Process.write("cds1\r\n");
    m_Process.write("scan\r\n");
    m_Process.write("q\r\n");
    m_Process.waitForFinished();
    QString output(m_Process.readAll());
    auto pattern = std::regex("File \"(.*)\" saved");
    std::smatch matches;
    std::string tokenStr = output.toStdString();
    std::regex_search(tokenStr, matches, pattern);
    return QUrl::fromLocalFile(QString(matches[1].str().c_str()));
}
