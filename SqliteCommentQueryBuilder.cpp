#include "Headers/Framework/SqliteCommentQueryBuilder.h"

#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Framework/IDatabaseCommandFactory.h"

SqliteCommentQueryBuilder::SqliteCommentQueryBuilder(
        std::shared_ptr<IDatabaseCommandFactory> commandFactory):
    m_CommandFactory(commandFactory)
{
}

std::shared_ptr<IDatabaseCommand> SqliteCommentQueryBuilder::GetCreateTableIfNotExistsQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "CREATE TABLE IF NOT EXISTS Table_CommentData "
        "(Subject TEXT PRIMARY KEY NOT NULL, Description TEXT)");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCommentQueryBuilder::GetSelectAllQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("SELECT Subject, Description FROM Table_CommentData");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCommentQueryBuilder::GetInsertQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "INSERT INTO "
        "Table_CommentData (Subject, Description) "
        "VALUES(:Subject, :Description)");
    dbCommand->AddParameter("Subject", ":Subject", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("Description", ":Description", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCommentQueryBuilder::GetRowCountQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "SELECT COUNT(Table_CommentData.Subject) "
        "FROM Table_CommentData "
        "WHERE :Subject = Subject");
    dbCommand->AddParameter("Subject", ":Subject", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCommentQueryBuilder::GetUpdateQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("UPDATE Table_CommentData SET "
        "Description = :Description "
        "WHERE Subject = :Subject ");
    dbCommand->AddParameter("Description", ":Description", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("Subject", ":Subject", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCommentQueryBuilder::GetDeleteQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("DELETE FROM Table_CommentData WHERE Subject = :Subject");
    dbCommand->AddParameter("Subject", ":Subject", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}
