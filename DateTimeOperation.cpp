#include "Headers/BusinessEntities/DateTimeOperation.h"

DateTimeOperation::DateTimeOperation()
{
}

DateTime DateTimeOperation::FromString(const std::string& dateTimeStr) const
{
	std::string copyStr = dateTimeStr;
	for (auto &ch : copyStr)
		if (!std::isdigit(ch))
			ch = ' ';
	int year, month, day, hour, minute;
	double second;
	std::istringstream dateTimeStream(copyStr);
	dateTimeStream >> year >> month >> day >> hour >> minute >> second;
	return DateTime(year, month, day, hour, minute, second);
}

std::string DateTimeOperation::ToString(const DateTime& dateTime) const
{
	std::ostringstream dateTimeStream;
	dateTimeStream << std::setfill('0') << std::setw(4) << dateTime.GetYear() << "-" << std::setw(2) << dateTime.GetMonth() << 
		"-" << std::setw(2) << dateTime.GetDay() << " " << std::setw(2) << dateTime.GetHour() << ":" << std::setw(2) << 
		dateTime.GetMinute() << ":" << std::setw(2) << dateTime.GetSecond();
	return dateTimeStream.str();
}

std::time_t DateTimeOperation::ToEpoch(const DateTime& dateTime) const
{
	std::tm timetOfInsert;
	timetOfInsert.tm_year = dateTime.GetYear() - 1900;
	timetOfInsert.tm_mon = dateTime.GetMonth() - 1;
	timetOfInsert.tm_mday = dateTime.GetDay();
	timetOfInsert.tm_hour = dateTime.GetHour();
	timetOfInsert.tm_min = dateTime.GetMinute();
	timetOfInsert.tm_sec = static_cast<int>(dateTime.GetSecond());
	timetOfInsert.tm_isdst = -1;
	auto result = std::mktime(&timetOfInsert);
	return result;
}

DateTime DateTimeOperation::DecodeInt64(long long int encodedTime) const
{
	DateTimeCodec codec;
	codec.Encoded = encodedTime;
	auto resultDateTime = DateTime(
		codec.m_DateTime.m_Year,
		codec.m_DateTime.m_Month,
		codec.m_DateTime.m_Day,
		codec.m_DateTime.m_Hour,
		codec.m_DateTime.m_Minute,
		codec.m_DateTime.m_Second / 34.0);
	return resultDateTime;
}

long long int DateTimeOperation::EncodeInt64(const DateTime & dateTime) const
{
	DateTimeCodec codec;
	codec.m_DateTime.m_Year = dateTime.GetYear();
	codec.m_DateTime.m_Month = dateTime.GetMonth();
	codec.m_DateTime.m_Day = dateTime.GetDay();
	codec.m_DateTime.m_Hour = dateTime.GetHour();
	codec.m_DateTime.m_Minute = dateTime.GetMinute();
	codec.m_DateTime.m_Second = static_cast<int>(dateTime.GetSecond() * 34);
	return codec.Encoded;
}

DateTime DateTimeOperation::FromEpoch(std::time_t timeSinceEpoch) const
{
	tm getDateTime;
	localtime_s(&getDateTime, &timeSinceEpoch);
	return DateTime(getDateTime.tm_year + 1900, getDateTime.tm_mon + 1, getDateTime.tm_mday,
		getDateTime.tm_hour, getDateTime.tm_min, getDateTime.tm_sec);
}

