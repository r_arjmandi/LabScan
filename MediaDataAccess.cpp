#include "Headers/Adapters/MediaDataAccess.h"

#include "Headers/Adapters/ConnectionGuard.h"
#include "Headers/Adapters/IMediaQueryBuilder.h"
#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Adapters/IDatabaseConnection.h"
#include "Headers/Adapters/IQueryResult.h"

MediaDataAccess::MediaDataAccess(
        std::shared_ptr<IDatabaseConnection> dbConnection,
        std::shared_ptr<IMediaQueryBuilder> mediaQueryBuilder):
    m_DatabaceConnection(dbConnection),
    m_MediaQueryBuilder(mediaQueryBuilder)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto createTableCommand = m_MediaQueryBuilder->GetCreateTableIfNotExistsQuery();
    m_DatabaceConnection->Exec(createTableCommand);
}

void MediaDataAccess::InsertOrUpdate(const std::string &media)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto rowCountCommand = m_MediaQueryBuilder->GetRowCountQuery();
    rowCountCommand->BindParameter("Media", media);
    auto queryResult = m_DatabaceConnection->Exec(rowCountCommand);

    if (queryResult->GetInt(0) == 0)
    {
        auto insertCommand = m_MediaQueryBuilder->GetInsertQuery();
        insertCommand->BindParameter("Media", media);
        m_DatabaceConnection->Exec(insertCommand);
    }
    else
    {
        auto updateCommand = m_MediaQueryBuilder->GetUpdateQuery();
        updateCommand->BindParameter("Media", media);
        m_DatabaceConnection->Exec(updateCommand);
    }
}

void MediaDataAccess::Delete(const std::string &media)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto deleteCommand = m_MediaQueryBuilder->GetDeleteQuery();
    deleteCommand->BindParameter("Media", media);
    m_DatabaceConnection->Exec(deleteCommand);
}

std::set<std::string> MediaDataAccess::GetAllMedia()
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto selectAllCommand = m_MediaQueryBuilder->GetSelectAllQuery();
    auto queryResult = m_DatabaceConnection->Exec(selectAllCommand);

    auto mediaList = std::set<std::string>();

    while(queryResult->HasData())
    {
        auto media = queryResult->GetString(0);
        mediaList.insert(media);
        queryResult->Next();
    }
    return std::move(mediaList);
}
