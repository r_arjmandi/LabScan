#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "Headers/Adapters/FrontEndAdapter.h"
#include "Headers/BusinessEntities/patientdata.h"
#include "Headers/BusinessRules/MockBusinessRules.h"

struct FrontEndAdapterTestFixture
{
    std::shared_ptr<FrontEndAdapter> m_Adapter;
    std::shared_ptr<MockBusinessRules> m_BusinessRules = std::make_shared<MockBusinessRules>();

    FrontEndAdapterTestFixture()
    {
        m_Adapter = std::make_shared<FrontEndAdapter>(m_BusinessRules);
    }
};

BOOST_FIXTURE_TEST_SUITE(FrontEndAdapterTestSuite, FrontEndAdapterTestFixture)

BOOST_AUTO_TEST_CASE(construction)
{

}

BOOST_AUTO_TEST_CASE(WhenAddNewPatientIsCalledWithAPatientData_ThenPatientDataMustBePassedDownToBusinessRulesLayer)
{
    auto testPatientData = std::make_shared<PatientData>();
    MOCK_EXPECT(m_BusinessRules->AddNewPatient).once().with(testPatientData);
    m_Adapter->AddNewPatient(testPatientData);
}

BOOST_AUTO_TEST_CASE(WhenLoadAllPatientIsCalled_ThenBusinessRuleLoadAllPatientMustBeCalled)
{
    MOCK_EXPECT(m_BusinessRules->LoadAllPatients).once();
    m_Adapter->LoadAllPatients();
}

BOOST_AUTO_TEST_CASE(WhenDetectImageCirclesIsCalled_ThenBusinessRolesDetectImageCirclesMustBeCalled)
{
    MOCK_EXPECT(m_BusinessRules->DetectImageCircles).once();
    m_Adapter->DetectImageCircles(100, 200);
}

BOOST_AUTO_TEST_SUITE_END()
