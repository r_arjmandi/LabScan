#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "../Headers/Adapters/DatabaseCommand.h"
#include "DateTimeMatcher.h"

struct DatabaseCommandFixture
{
    DatabaseCommandFixture()
	{
        m_SqliteDbCommand = std::make_shared<DatabaseCommand>();
		auto data = { 10, 50, 24, 64, 47, 243, 42, 123, 53, 25, 77, 45 };
		m_BinDataPtr = std::make_shared<ByteArray>(data.begin(), data.end());
	}
	
    template<typename ValueType>
    void CheckOneBindingNameAndType(
        const IDatabaseCommand::BindingCollection& bindings, const std::string & paramName,
        const ValueType& value, IDatabaseCommand::ParameterType paramType, const std::string& lowLevelName)
    {
        BOOST_REQUIRE_EQUAL(bindings.count(paramName), 1);
        auto binding = bindings.at(paramName);
        BOOST_CHECK_EQUAL(binding.Name, paramName);
        BOOST_CHECK(binding.Type == paramType);
        BOOST_CHECK_EQUAL(binding.LowLevelParameterName, lowLevelName);
        BOOST_CHECK_EQUAL(boost::get<ValueType>(binding.Value), value);
    }

    std::shared_ptr<DatabaseCommand> m_SqliteDbCommand;
	std::shared_ptr<ByteArray> m_BinDataPtr;
};

BOOST_FIXTURE_TEST_SUITE(DatabaseCommandTestSuite, DatabaseCommandFixture)

BOOST_AUTO_TEST_CASE(ConstructDatabaseCommand)
{
}

BOOST_AUTO_TEST_CASE(WhenACommandStringIsSet_GetCommandStringMustReturnTheSameValue)
{
    auto testStr = "INSERT INTO test (col1, col2, col3, Col4) values(@col1, @col2, @col3, @col4)";
    m_SqliteDbCommand->SetCommandString(testStr);
    BOOST_CHECK_EQUAL(m_SqliteDbCommand->GetCommandString(), testStr);
}

BOOST_AUTO_TEST_CASE(WhenNoParemetersAreBound_ThenGetBindingsMustReturnEmpty)
{
    BOOST_CHECK(m_SqliteDbCommand->GetBindings().empty());
}

BOOST_AUTO_TEST_CASE(WhenOneStringParemeterIsBound_ThenGetBindingsMustReturnWithOneBindingWithCorrectType_Name_AndValue)
{
    std::string paramName("col1"), value("Test");
    m_SqliteDbCommand->AddParameter(paramName, ":col1", IDatabaseCommand::ParameterType::String);
    m_SqliteDbCommand->BindParameter(paramName, value);
    auto bindings = m_SqliteDbCommand->GetBindings();
    CheckOneBindingNameAndType<std::string>
        (bindings, paramName, value, IDatabaseCommand::ParameterType::String, ":col1");
}

BOOST_AUTO_TEST_CASE(WhenOneIntegerParemeterIsBound_ThenGetBindingsMustReturnWithOneBindingWithCorrectType_Name_AndValue)
{
    std::string paramName("col1");
    long long int value(1234);
    m_SqliteDbCommand->AddParameter(paramName, ":col1", IDatabaseCommand::ParameterType::Integer);
    m_SqliteDbCommand->BindParameter(paramName, value);
    auto bindings = m_SqliteDbCommand->GetBindings();
    CheckOneBindingNameAndType<long long int>
        (bindings, paramName, value, IDatabaseCommand::ParameterType::Integer, ":col1");
}

BOOST_AUTO_TEST_CASE(WhenOneBoolParemeterIsBound_ThenGetBindingsMustReturnWithOneBindingWithCorrectType_Name_AndValue)
{
    std::string paramName("col1");
    bool value(true);
    m_SqliteDbCommand->AddParameter(paramName, ":col1", IDatabaseCommand::ParameterType::Boolean);
    m_SqliteDbCommand->BindParameter(paramName, value);
    auto bindings = m_SqliteDbCommand->GetBindings();
    CheckOneBindingNameAndType<bool>
        (bindings, paramName, value, IDatabaseCommand::ParameterType::Boolean, ":col1");
}

BOOST_AUTO_TEST_CASE(WhenOneDoubleParemeterIsBound_ThenGetBindingsMustReturnWithOneBindingWithCorrectType_Name_AndValue)
{
    std::string paramName("col1");
    double value(12.5756);
    m_SqliteDbCommand->AddParameter(paramName, ":col1", IDatabaseCommand::ParameterType::Real);
    m_SqliteDbCommand->BindParameter(paramName, value);
    auto bindings = m_SqliteDbCommand->GetBindings();
    CheckOneBindingNameAndType<double>
        (bindings, paramName, value, IDatabaseCommand::ParameterType::Real, ":col1");
}

BOOST_AUTO_TEST_CASE(WhenOneDateTimeParemeterIsBound_ThenGetBindingsMustReturnWithOneBindingWithCorrectType_Name_AndValue)
{
    std::string paramName("col1");
    DateTime value(2017, 7, 8, 2, 37, 0);
    m_SqliteDbCommand->AddParameter(paramName, ":col1", IDatabaseCommand::ParameterType::DateTime);
    m_SqliteDbCommand->BindParameter(paramName, value);
    auto bindings = m_SqliteDbCommand->GetBindings();
    BOOST_REQUIRE_EQUAL(bindings.count(paramName), 1);
    auto binding = bindings.at(paramName);
    BOOST_CHECK_EQUAL(binding.Name, paramName);
    BOOST_CHECK(binding.Type == IDatabaseCommand::ParameterType::DateTime);
    BOOST_CHECK_EQUAL(binding.LowLevelParameterName, ":col1");
    BOOST_CHECK(DateTimeMatcher::IsEqual(boost::get<DateTime>(binding.Value), value));
}

BOOST_AUTO_TEST_CASE(WhenOneBinaryParemeterIsBound_ThenGetBindingsMustReturnWithOneBindingWithCorrectType_Name_AndValue)
{
    std::string paramName("col1");
    m_SqliteDbCommand->AddParameter(paramName, ":col1", IDatabaseCommand::ParameterType::Binary);
    m_SqliteDbCommand->BindParameter(paramName, m_BinDataPtr);
    auto bindings = m_SqliteDbCommand->GetBindings();
    BOOST_REQUIRE_EQUAL(bindings.count(paramName), 1);
    auto binding = bindings.at(paramName);
    BOOST_CHECK_EQUAL(binding.Name, paramName);
    BOOST_CHECK(binding.Type == IDatabaseCommand::ParameterType::Binary);
    auto bindingValue = boost::get<std::shared_ptr<ByteArray>>(binding.Value);
    BOOST_CHECK_EQUAL(bindingValue, m_BinDataPtr);
    BOOST_CHECK_EQUAL(binding.LowLevelParameterName, ":col1");
    BOOST_CHECK_EQUAL_COLLECTIONS(bindingValue->cbegin(), bindingValue->cend(), m_BinDataPtr->cbegin(), m_BinDataPtr->cend());
}

BOOST_AUTO_TEST_CASE(WhenFourDifferentParemeterIsBound_ThenGetBindingMustReturnWithOneBindingWithCorrectType_Name_AndValue)
{
    m_SqliteDbCommand->AddParameter("col1", ":col1", IDatabaseCommand::ParameterType::String);
    m_SqliteDbCommand->AddParameter("col2", ":col2", IDatabaseCommand::ParameterType::Integer);
    m_SqliteDbCommand->AddParameter("col3", ":col3", IDatabaseCommand::ParameterType::Boolean);
    m_SqliteDbCommand->AddParameter("col4", ":col4", IDatabaseCommand::ParameterType::Real);
    m_SqliteDbCommand->AddParameter("col5", ":col5", IDatabaseCommand::ParameterType::DateTime);
    m_SqliteDbCommand->AddParameter("col6", ":col6", IDatabaseCommand::ParameterType::Binary);

    m_SqliteDbCommand->BindParameter("col1", std::string("test"));
    m_SqliteDbCommand->BindParameter("col2", static_cast<long long int>(1234));
    m_SqliteDbCommand->BindParameter("col3", false);
    m_SqliteDbCommand->BindParameter("col4", 12.544);
    m_SqliteDbCommand->BindParameter("col5", DateTime(2017, 7, 8, 2, 37, 0));
    m_SqliteDbCommand->BindParameter("col6", m_BinDataPtr);

    auto bindings = m_SqliteDbCommand->GetBindings();

    BOOST_REQUIRE_EQUAL(bindings.size(), 6);

    CheckOneBindingNameAndType<std::string>(bindings, "col1", "test", IDatabaseCommand::ParameterType::String, ":col1");
    CheckOneBindingNameAndType<long long int>(bindings, "col2", 1234, IDatabaseCommand::ParameterType::Integer, ":col2");
    CheckOneBindingNameAndType<bool>(bindings, "col3", false, IDatabaseCommand::ParameterType::Boolean, ":col3");
    CheckOneBindingNameAndType<double>(bindings, "col4", 12.544, IDatabaseCommand::ParameterType::Real, ":col4");
    BOOST_REQUIRE_EQUAL(bindings.count("col5"), 1);
    auto binding = bindings.at("col5");
    BOOST_CHECK_EQUAL(binding.LowLevelParameterName, ":col5");
    BOOST_CHECK_EQUAL(binding.Name, "col5");
    BOOST_CHECK(binding.Type == IDatabaseCommand::ParameterType::DateTime);
    DateTimeMatcher matcher(boost::get<DateTime>(binding.Value));
    BOOST_CHECK_EQUAL(matcher(DateTime(2017, 7, 8, 2, 37, 0)), true);

    binding = bindings.at("col6");
    BOOST_CHECK(binding.Type == IDatabaseCommand::ParameterType::Binary);
    auto bindingValue = boost::get<std::shared_ptr<ByteArray>>(binding.Value);
    BOOST_CHECK_EQUAL(bindingValue, m_BinDataPtr);
    BOOST_CHECK_EQUAL(binding.LowLevelParameterName, ":col6");
    BOOST_CHECK_EQUAL_COLLECTIONS(bindingValue->cbegin(), bindingValue->cend(), m_BinDataPtr->cbegin(), m_BinDataPtr->cend());
}

BOOST_AUTO_TEST_CASE(WhenDbCommandStringBindParameterFunctionIsCalledWithInvalidParameterName_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::String);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("badpPramName", std::string("paramValue")), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandStringBindParameterFunctionIsCalledWithInvalidType_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Boolean);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("paramName", std::string("paramValue")), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandIntegerBindParameterFunctionIsCalledWithInvalidParameterName_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Integer);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("badpPramName", 1234), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandIntegerBindParameterFunctionIsCalledWithInvalidParameterType_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Boolean);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("paramName", 1234), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandBooleanBindParameterFunctionIsCalledWithInvalidParameterName_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Boolean);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("badpPramName", true), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandBooleanBindParameterFunctionIsCalledWithInvalidParameterType_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Integer);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("pramName", true), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandDoubleBindParameterFunctionIsCalledWithInvalidParameterName_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Real);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("badpPramName", 12.3), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandDoubleBindParameterFunctionIsCalledWithInvalidParameterType_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Integer);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("pramName", 12.3), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandDateTimeBindParameterFunctionIsCalledWithInvalidParameterName_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::DateTime);
    DateTime testValueTime = DateTime(1, 2, 3, 4, 5, 6);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("badpPramName", testValueTime), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandDateTimeBindParameterFunctionIsCalledWithInvalidParameterType_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Integer);
    DateTime testValueTime = DateTime(1, 2, 3, 4, 5, 6);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("pramName", testValueTime), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandBinaryBindParameterFunctionIsCalledWithInvalidParameterName_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Binary);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("badpPramName", m_BinDataPtr), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenDbCommandBinaryBindParameterFunctionIsCalledWithInvalidParameterType_ThenExpectThrowADatabaseCommandException)
{
    m_SqliteDbCommand->AddParameter("paramName", ":paramName", DatabaseCommand::ParameterType::Integer);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("pramName", m_BinDataPtr), DataAccessException);
}

BOOST_AUTO_TEST_CASE(GivenADatabaseCommandWithASingleParameter_WhenAnEmptyStringIsPassedToBindParameter_AnExceptionMustBeThrown)
{
    m_SqliteDbCommand->SetCommandString("select * from PatientData where :ID = ID");
    m_SqliteDbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::Boolean);
    BOOST_CHECK_THROW(m_SqliteDbCommand->BindParameter("", true), DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenAddParameterIsCalledWithDuplicateParameterNameWithDifferentTypes_ThenMustThrowAnException)
{
    m_SqliteDbCommand->SetCommandString("select * from PatientData where :ID = ID");
    m_SqliteDbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::Boolean);
    BOOST_CHECK_THROW(m_SqliteDbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::Integer),
        DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenAddParameterIsCalledWithDuplicateParameterNameWithDifferentLowLevelNames_ThenMustThrowAnException)
{
    m_SqliteDbCommand->SetCommandString("select * from PatientData where :ID = ID");
    m_SqliteDbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::Boolean);
    BOOST_CHECK_THROW(m_SqliteDbCommand->AddParameter("ID", ":Different", IDatabaseCommand::ParameterType::Boolean),
        DataAccessException);
}

BOOST_AUTO_TEST_CASE(WhenAddParameterIsCalledWithDuplicateParameterNameWithAgreeingTypesAndLowLevelNames_ThenMustReturnWithNoEffect)
{
    m_SqliteDbCommand->SetCommandString("select * from PatientData where :ID = ID");
    m_SqliteDbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::String);
    auto bindingsBefore = m_SqliteDbCommand->GetBindings();
    BOOST_REQUIRE_EQUAL(bindingsBefore.size(), 1);
    BOOST_CHECK_NO_THROW(m_SqliteDbCommand->AddParameter("ID", ":ID", IDatabaseCommand::ParameterType::String));
    auto bindingsAfter = m_SqliteDbCommand->GetBindings();
    BOOST_REQUIRE_EQUAL(bindingsAfter.size(), 1);
    BOOST_CHECK_EQUAL(bindingsAfter.begin()->first, bindingsBefore.begin()->first);
    BOOST_CHECK_EQUAL(bindingsAfter.begin()->second.LowLevelParameterName, bindingsBefore.begin()->second.LowLevelParameterName);
    BOOST_CHECK(bindingsAfter.begin()->second.Type == bindingsBefore.begin()->second.Type);
}

BOOST_AUTO_TEST_SUITE_END()
