#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "Headers/BusinessEntities/patientdata.h"
#include "Headers/Adapters/BackEndAdapter.h"
#include "Headers/Adapters/MockPatientDataAccess.h"
#include "Headers/Adapters/MockPatientRepository.h"

struct BackEndAdapterTestFixture
{
    BackEndAdapterTestFixture()
    {
        m_BackEndAdapter = std::make_shared<BackEndAdapter>(m_PatientRepository, m_PatientDataAccess);
    }

    std::shared_ptr<BackEndAdapter> m_BackEndAdapter;
    std::shared_ptr<MockPatientDataAccess> m_PatientDataAccess = std::make_shared<MockPatientDataAccess>();
    std::shared_ptr<MockPatientRepository> m_PatientRepository = std::make_shared<MockPatientRepository>();
};

BOOST_FIXTURE_TEST_SUITE(BackEndAdapterTestSuite, BackEndAdapterTestFixture)

BOOST_AUTO_TEST_CASE(construction)
{

}

BOOST_AUTO_TEST_CASE(WhenSavePatientToDatabaseIsCalledWithAPatientData_ThenPatientDataMustBePassedToInsertOrUpdateOfPatientDataAccess)
{
    auto testPatientData = std::make_shared<PatientData>();
    MOCK_EXPECT(m_PatientDataAccess->InsertOrUpdate).once().with(testPatientData);
    m_BackEndAdapter->SavePatientToDatabase(testPatientData);
}

BOOST_AUTO_TEST_CASE(WhenLoadAllPatientIsCalled_ThenPatientDataAccessGetAllPatientMustBeCalledAndRetrivedPatientList_ThenPatientListMustBePassedUpToGui)
{
    std::vector<std::shared_ptr<PatientData>> patientList;
    MOCK_EXPECT(m_PatientDataAccess->GetAllPatient).once().returns(patientList);
    MOCK_EXPECT(m_PatientRepository->SetPatientDataList).once().with(patientList);
    m_BackEndAdapter->LoadAllPatients();
}

BOOST_AUTO_TEST_SUITE_END()
