#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "Headers/BusinessRules/BusinessRules.h"
#include "Headers/Adapters/MockBackEndAdapter.h"
#include "Headers/BusinessEntities/patientdata.h"
#include "Headers/BusinessEntities/MockCircleDetector.h"
#include "Headers/BusinessEntities/MockImageRepository.h"
#include "Headers/BusinessEntities/MockCircleRepository.h"
#include "Headers/BusinessEntities/Circle.h"

struct BusinessRulesTestFixture
{
    std::shared_ptr<BusinessRules> m_BusinessRules;
    std::shared_ptr<MockBackEndAdapter> m_BackEndAdapter = std::make_shared<MockBackEndAdapter>();
    std::shared_ptr<MockCircleDetector> m_CircleDetector = std::make_shared<MockCircleDetector>();
    std::shared_ptr<MockImageRepository> m_ImageRepository = std::make_shared<MockImageRepository>();
    std::shared_ptr<MockCircleRepository> m_CircleRepository = std::make_shared<MockCircleRepository>();

    BusinessRulesTestFixture()
    {
        m_BusinessRules = std::make_shared<BusinessRules>(m_BackEndAdapter, m_CircleDetector, m_ImageRepository, m_CircleRepository);
    }
};

BOOST_FIXTURE_TEST_SUITE(BusinessRulesTestSuite, BusinessRulesTestFixture)

BOOST_AUTO_TEST_CASE(construction)
{

}

BOOST_AUTO_TEST_CASE(WhenAddNewPatientIsCalledWithAPatientData_ThenPatientDataMustBePassedUpToSavePatientToDatabaseOfBackEndAdapter)
{
   auto testPatientData = std::make_shared<PatientData>();
   MOCK_EXPECT(m_BackEndAdapter->SavePatientToDatabase).once().with(testPatientData);
   m_BusinessRules->AddNewPatient(testPatientData);
}

BOOST_AUTO_TEST_CASE(WhenLoadAllPatientIsCalled_ThenAdapterLoadAllPatientMustBeCalled)
{
    MOCK_EXPECT(m_BackEndAdapter->LoadAllPatients).once();
    m_BusinessRules->LoadAllPatients();
}

BOOST_AUTO_TEST_CASE(WhenDetectImageCirclesIsCalled_ThenDetectCirclesOfCircleDetectorMustBeCalledAndRetrievedCircleAndPassedUpToAdapter)
{
    std::string imageAddress = "testAddress";
    double minRadius = 100.;
    double maxRadius = 200.;
    auto detectCircles = std::make_shared<std::vector<Circle>>();
    MOCK_EXPECT(m_ImageRepository->GetCurrentImageAddress).once().returns(imageAddress);
    MOCK_EXPECT(m_CircleDetector->DetectCircles).once().with(imageAddress, minRadius, maxRadius).returns(detectCircles);
    MOCK_EXPECT(m_CircleRepository->AddCircles).once().with(imageAddress, detectCircles);
    m_BusinessRules->DetectImageCircles(minRadius, maxRadius);
}

BOOST_AUTO_TEST_SUITE_END()
