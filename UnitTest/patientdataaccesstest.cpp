#define MOCK_MAX_SEQUENCES 16
#include <boost/test/unit_test.hpp>
#include <turtle/mock.hpp>

#include "Headers/BusinessEntities/patientdata.h"
#include "Headers/Adapters/PatientDataAccess.h"
#include "Headers/Adapters/MockDatabaseConnection.h"
#include "Headers/Adapters/MockPatientQueryBuilder.h"
#include "Headers/Adapters/MockDatabaseCommand.h"
#include "Headers/Adapters/MockQueryResult.h"

struct PatientDataAccessTestFixture
{

    PatientDataAccessTestFixture()
    {
        mock::sequence seq;
        MOCK_EXPECT(m_DatabaseConnection->IsOpen).once().in(seq).returns(false);
        MOCK_EXPECT(m_DatabaseConnection->Open).once().in(seq);
        MOCK_EXPECT(m_PatientQueryBuilder->GetCreateTableIfNotExistsQuery).once().in(seq).returns(m_DbCommand);
        MOCK_EXPECT(m_DatabaseConnection->Exec).once().with(m_DbCommand).in(seq).returns(m_QueryResult);
        MOCK_EXPECT(m_DatabaseConnection->IsOpen).once().in(seq).returns(true);
        MOCK_EXPECT(m_DatabaseConnection->Close).once().in(seq);

        m_PatientDataAccess = std::make_shared<PatientDataAccess>(m_DatabaseConnection, m_PatientQueryBuilder);

        MOCK_VERIFY(m_DatabaseConnection->IsOpen);
        MOCK_VERIFY(m_DatabaseConnection->Open);
        MOCK_VERIFY(m_PatientQueryBuilder->GetCreateTableIfNotExistsQuery);
        MOCK_VERIFY(m_DatabaseConnection->Exec);
        MOCK_VERIFY(m_DatabaseConnection->IsOpen);
        MOCK_VERIFY(m_DatabaseConnection->Close);
    }

    static bool IsPatientDataEqual(const PatientData& p1, const PatientData& p2)
    {
        bool result = true;
        result = result && (p1.ID == p2.ID);
        result = result && (p1.Name == p2.Name);
        result = result && (p1.FamilyName == p2.FamilyName);
        result = result && (p1.Age == p2.Age);
        result = result && (p1.Gender == p2.Gender);
        result = result && (p1.Gender == p2.Gender);
        result = result && (p1.Sample.Type == p2.Sample.Type);
        result = result && (p1.Sample.Standard == p2.Sample.Standard);
        result = result && (p1.Sample.Bacteria == p2.Sample.Bacteria);
        result = result && (p1.Sample.Gram == p2.Sample.Gram);
        result = result && (p1.Sample.Shape == p2.Sample.Shape);
        result = result && (p1.Sample.Isolated == p2.Sample.Isolated);
        result = result && (p1.Sample.Count == p2.Sample.Count);
        result = result && (p1.Comment.Subject == p2.Comment.Subject);
        result = result && (p1.Comment.Description == p2.Comment.Description);
        return result;
    }

    std::shared_ptr<PatientDataAccess> m_PatientDataAccess;
    std::shared_ptr<MockDatabaseConnection> m_DatabaseConnection = std::make_shared<MockDatabaseConnection>();
    std::shared_ptr<MockPatientQueryBuilder> m_PatientQueryBuilder = std::make_shared<MockPatientQueryBuilder>();
    std::shared_ptr<MockDatabaseCommand> m_DatabaseCommand = std::make_shared<MockDatabaseCommand>();
    std::shared_ptr<IDatabaseCommand> m_DbCommand = m_DatabaseCommand;
    std::shared_ptr<MockQueryResult> m_QueryResult = std::make_shared<MockQueryResult>();
};

BOOST_FIXTURE_TEST_SUITE(PatientDataAccessTestSuite, PatientDataAccessTestFixture)

BOOST_AUTO_TEST_CASE(construction)
{

}

BOOST_AUTO_TEST_CASE(WhenNewPatientDataIsUpdateOrInsertedThatBeforeNotExist_ThenDataBaseConnetionMustBeOpened_DbCommandForGetRowCountRetrievedFromQueryBuilder_DataBaseCommandParametersBoundCorrectly_CommandSentToDbConnectionForExecution_DbCommandForInsertRetrievedFromQueryBuilder_DataBaseCommandParametersBoundCorrectly_CommandSentToDbConnectionForExecution_AndDatabaseClosed)
{
    auto testPatientData = std::make_shared<PatientData>();
    mock::sequence sequences[15];
    MOCK_EXPECT(m_DatabaseConnection->IsOpen).once().in(sequences[0]).returns(false);
    MOCK_EXPECT(m_DatabaseConnection->Open).once().in(sequences[0]);
    MOCK_EXPECT(m_PatientQueryBuilder->GetRowCountQuery).once().returns(m_DbCommand);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("ID", testPatientData->ID).in(sequences[3]);
    MOCK_EXPECT(m_DatabaseConnection->Exec).once().with(m_DbCommand).in(sequences[0], sequences[1], sequences[2], sequences[3], sequences[4], sequences[5], sequences[6], sequences[7], sequences[8], sequences[9], sequences[10], sequences[11], sequences[12], sequences[13], sequences[14]).returns(m_QueryResult);
    MOCK_EXPECT(m_QueryResult->GetInt).once().with(0).in(sequences[0], sequences[1], sequences[2], sequences[3], sequences[4], sequences[5], sequences[6], sequences[7], sequences[8], sequences[9], sequences[10], sequences[11], sequences[12], sequences[13], sequences[14]).returns(0);
    MOCK_EXPECT(m_PatientQueryBuilder->GetInsertQuery).once().in(sequences[0], sequences[1], sequences[2], sequences[3], sequences[4], sequences[5], sequences[6], sequences[7], sequences[8], sequences[9], sequences[10], sequences[11], sequences[12], sequences[13], sequences[14]).returns(m_DatabaseCommand);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("ID", testPatientData->ID).in(sequences[1]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("Name", testPatientData->Name).in(sequences[2]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("FamilyName", testPatientData->FamilyName).in(sequences[3]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_int64).once().with("Age", testPatientData->Age).in(sequences[4]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_int64).once().with("Gender", testPatientData->Gender == PatientData::GenderType::Male ? 1 : 2).in(sequences[5]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("SampleType", testPatientData->Sample.Type).in(sequences[6]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("SampleStandard", testPatientData->Sample.Standard).in(sequences[7]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("SampleBacteria", testPatientData->Sample.Bacteria).in(sequences[8]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("SampleGram", testPatientData->Sample.Gram).in(sequences[9]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("SampleShape", testPatientData->Sample.Shape).in(sequences[10]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("SampleIsolated", testPatientData->Sample.Isolated).in(sequences[11]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_int64).once().with("SampleCount", testPatientData->Sample.Count).in(sequences[12]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("CommentSubject", testPatientData->Comment.Subject).in(sequences[13]);
    MOCK_EXPECT(m_DatabaseCommand->BindParameter_string).once().with("CommentDescription", testPatientData->Comment.Description).in(sequences[14]);
    MOCK_EXPECT(m_DatabaseConnection->Exec).once().with(m_DbCommand).in(sequences[0], sequences[1], sequences[2], sequences[3], sequences[4], sequences[5], sequences[6], sequences[7], sequences[8], sequences[9], sequences[10], sequences[11], sequences[12], sequences[13], sequences[14]).returns(m_QueryResult);
    MOCK_EXPECT(m_DatabaseConnection->IsOpen).once().in(sequences[0]).returns(true);
    MOCK_EXPECT(m_DatabaseConnection->Close).once().in(sequences[0]);

    m_PatientDataAccess->InsertOrUpdate(testPatientData);
}

BOOST_AUTO_TEST_CASE(WhenGetAllPatientIsCalled_ThenAllPatientMustBeRetrivedFromDbAndReturn)
{
    PatientData patient1;
    patient1.ID = "testID";
    patient1.Name = "testName";
    patient1.FamilyName = "testFamilyName";
    patient1.Age = 23;
    patient1.Gender = PatientData::GenderType::Male;
    patient1.Sample.Type = "testSampleType";
    patient1.Sample.Standard = "testSampleStandard";
    patient1.Sample.Bacteria = "testSampleBacteria";
    patient1.Sample.Gram = "testSampleGram";
    patient1.Sample.Shape = "testSampleShape";
    patient1.Sample.Isolated = "testSampleIsolated";
    patient1.Sample.Count = 123;
    patient1.Comment.Subject = "testCommentSubject";
    patient1.Comment.Description = "testCommentDescription";

    PatientData patient2;
    patient2.ID = "testID2";
    patient2.Name = "testName2";
    patient2.FamilyName = "testFamilyName2";
    patient2.Age = 26;
    patient2.Gender = PatientData::GenderType::Female;
    patient2.Sample.Type = "testSampleType2";
    patient2.Sample.Standard = "testSampleStandard2";
    patient2.Sample.Bacteria = "testSampleBacteria2";
    patient2.Sample.Gram = "testSampleGram2";
    patient2.Sample.Shape = "testSampleShape2";
    patient2.Sample.Isolated = "testSampleIsolated2";
    patient2.Sample.Count = 123;
    patient2.Comment.Subject = "testCommentSubject2";
    patient2.Comment.Description = "testCommentDescription2";

    std::vector<std::shared_ptr<PatientData>> patientDataList;
    patientDataList.push_back(std::make_shared<PatientData>(patient1));
    patientDataList.push_back(std::make_shared<PatientData>(patient2));

    mock::sequence sequences[15];
    MOCK_EXPECT(m_DatabaseConnection->IsOpen).once().in(sequences[0]).returns(false);
    MOCK_EXPECT(m_DatabaseConnection->Open).once().in(sequences[0]);
    MOCK_EXPECT(m_PatientQueryBuilder->GetSelectAllQuery).once().returns(m_DbCommand);
    MOCK_EXPECT(m_DatabaseConnection->Exec).once().with(m_DbCommand).in(sequences[0], sequences[1], sequences[2], sequences[3], sequences[4], sequences[5], sequences[6], sequences[7], sequences[8], sequences[9], sequences[10], sequences[11], sequences[12], sequences[13], sequences[14]).returns(m_QueryResult);

    MOCK_EXPECT(m_QueryResult->HasData).once().in(sequences[0], sequences[1], sequences[2], sequences[3], sequences[4], sequences[5], sequences[6], sequences[7], sequences[8], sequences[9], sequences[10], sequences[11], sequences[12], sequences[13], sequences[14]).returns(true);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[0]).with(0).returns(patient1.ID);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[1]).with(1).returns(patient1.Name);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[2]).with(2).returns(patient1.FamilyName);
    MOCK_EXPECT(m_QueryResult->GetInt).once().in(sequences[3]).with(3).returns(patient1.Age);
    MOCK_EXPECT(m_QueryResult->GetInt).once().in(sequences[4]).with(4).returns(patient1.Gender == PatientData::GenderType::Male ? 1 : 2);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[5]).with(5).returns(patient1.Sample.Type);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[6]).with(6).returns(patient1.Sample.Standard);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[7]).with(7).returns(patient1.Sample.Bacteria);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[8]).with(8).returns(patient1.Sample.Gram);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[9]).with(9).returns(patient1.Sample.Shape);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[10]).with(10).returns(patient1.Sample.Isolated);
    MOCK_EXPECT(m_QueryResult->GetInt).once().in(sequences[11]).with(11).returns(patient1.Sample.Count);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[12]).with(12).returns(patient1.Comment.Subject);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[13]).with(13).returns(patient1.Comment.Description);
    MOCK_EXPECT(m_QueryResult->Next).once().in(sequences[0], sequences[1], sequences[2], sequences[3], sequences[4], sequences[5], sequences[6], sequences[7], sequences[8], sequences[9], sequences[10], sequences[11], sequences[12], sequences[13], sequences[14]);
    MOCK_EXPECT(m_QueryResult->HasData).once().in(sequences[0]).returns(true);

    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[0]).with(0).returns(patient2.ID);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[1]).with(1).returns(patient2.Name);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[2]).with(2).returns(patient2.FamilyName);
    MOCK_EXPECT(m_QueryResult->GetInt).once().in(sequences[3]).with(3).returns(patient2.Age);
    MOCK_EXPECT(m_QueryResult->GetInt).once().in(sequences[4]).with(4).returns(patient2.Gender == PatientData::GenderType::Male ? 1 : 2);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[5]).with(5).returns(patient2.Sample.Type);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[6]).with(6).returns(patient2.Sample.Standard);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[7]).with(7).returns(patient2.Sample.Bacteria);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[8]).with(8).returns(patient2.Sample.Gram);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[9]).with(9).returns(patient2.Sample.Shape);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[10]).with(10).returns(patient2.Sample.Isolated);
    MOCK_EXPECT(m_QueryResult->GetInt).once().in(sequences[11]).with(11).returns(patient2.Sample.Count);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[12]).with(12).returns(patient2.Comment.Subject);
    MOCK_EXPECT(m_QueryResult->GetString).once().in(sequences[13]).with(13).returns(patient2.Comment.Description);
    MOCK_EXPECT(m_QueryResult->Next).once().in(sequences[0], sequences[1], sequences[2], sequences[3], sequences[4], sequences[5], sequences[6], sequences[7], sequences[8], sequences[9], sequences[10], sequences[11], sequences[12], sequences[13], sequences[14]);
    MOCK_EXPECT(m_QueryResult->HasData).once().in(sequences[0]).returns(false);

    MOCK_EXPECT(m_DatabaseConnection->IsOpen).once().in(sequences[0]).returns(true);
    MOCK_EXPECT(m_DatabaseConnection->Close).once().in(sequences[0]);

    auto getPatientDataList = m_PatientDataAccess->GetAllPatient();

    BOOST_CHECK_EQUAL(getPatientDataList.size(), patientDataList.size());
    IsPatientDataEqual(*getPatientDataList[0], *patientDataList[0]);
    IsPatientDataEqual(*getPatientDataList[1], *patientDataList[1]);
}

BOOST_AUTO_TEST_SUITE_END()
