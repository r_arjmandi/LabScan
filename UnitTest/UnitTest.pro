TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    FrontEndAdapterTest.cpp \
    BusinessRulesTest.cpp \
    BackEndAdapterTest.cpp \
    PatientDataAccessTest.cpp \
    ..\FrontEndAdapter.cpp \
    ..\BusinessRules.cpp \
    ..\BackEndAdapter.cpp \
    ..\PatientDataAccess.cpp \
    DatabaseCommandTest.cpp \
    ..\DatabaseCommand.cpp \
    ..\DataAccessException.cpp \
    ..\DateTime.cpp

win32:CONFIG(release, debug|release): LIBS += -L"C:\Program Files\Boost\lib" -llibboost_unit_test_framework-vc140-mt-1_62
else:win32:CONFIG(debug, debug|release): LIBS += -L"C:\Program Files\Boost\lib" -llibboost_unit_test_framework-vc140-mt-gd-1_62

INCLUDEPATH += "C:\Program Files\Boost\include\boost-1_62" "C:\Program Files\Boost\include" \
    ".."

PRECOMPILED_HEADER = "stable.h"

HEADERS += "../Headers/BusinessRules/MockBusinessRules.h" \
    ../Headers/Adapters/MockBackEndAdapter.h \
    ../Headers/Adapters/MockPatientDataAccess.h \
    ../Headers/Adapters/MockDatabaseConnection.h \
    ../Headers/Adapters/MockPatientQueryBuilder.h \
    ../Headers/Adapters/MockDatabaseCommand.h \
    ../Headers/Adapters/MockQueryResult.h \
    ../Headers/Adapters/MockPatientRepository.h \
    ../Headers/BusinessEntities/MockCircleDetector.h \
    ../Headers/BusinessEntities/MockImageRepository.h \
    ../Headers/BusinessEntities/MockCircleRepository.h
