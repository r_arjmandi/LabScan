#include "Headers/Framework/GeneralCommentRepositoryDataModel.h"

#include "Headers/Adapters/IFrontEndAdapter.h"
#include "Headers/BusinessEntities/CommentData.h"
#include "Headers/Adapters/ICurrentPatientGui.h"

GeneralCommentRepositoryDataModel::GeneralCommentRepositoryDataModel(
        std::shared_ptr<ICurrentPatientGui> currentPatient):
    m_CurrentPatient(currentPatient)
{
    m_CommentRoleNames[Subject] = "Subject";
}

void GeneralCommentRepositoryDataModel::SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter)
{
    m_Adapter = adapter;
}

int GeneralCommentRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_CommentList.size());
}

int GeneralCommentRepositoryDataModel::GetSize() const
{
    return static_cast<int>(m_CommentList.size());
}

QVariant GeneralCommentRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(m_CommentList.empty())
    {
        return QVariant();
    }

    int row = index.row();
    if(row < 0 || row >= m_CommentList.size())
    {
        return QVariant();
    }

    switch(role) {
        case Subject:
        {
            auto comment = m_CommentList.begin();
            for(auto i = 0; i < row; i++)
            {
                comment++;
            }
            return QString(comment->first.c_str());
        }
    }

    return QVariant();
}

QString GeneralCommentRepositoryDataModel::GetData(int index) const
{
    if(m_CommentList.empty())
    {
        return QString("");
    }

    if(index < 0 || index >= m_CommentList.size())
    {
        return QString();
    }

    auto comment = m_CommentList.begin();
    for(auto i = 0; i < index; i++)
    {
        comment++;
    }
    return QString(comment->second.c_str());
}

void GeneralCommentRepositoryDataModel::AddNewComment(const QString &subject, const QString &description)
{
    if(subject != "" && description != "")
    {
        m_CommentList.insert_or_assign(subject.toStdString(), description.toStdString());
        auto subjectCheckEqual = [&subject](const std::pair<std::string, std::string> itr){
            return itr.first == subject.toStdString();
        };
        auto findSubject = std::find_if(m_CommentList.cbegin(), m_CommentList.cend(), subjectCheckEqual);
        auto subjectIndex = std::distance(m_CommentList.cbegin(), findSubject);
        if(subjectIndex >= 0)
        {
            m_CurrentPatient->SetCommentSubjectIndex(subjectIndex);
        }
        auto comment = std::make_unique<CommentData>();
        comment->Subject = subject.toStdString();
        comment->Description = description.toStdString();
        m_Adapter->InsertOrUpdateComment(std::move(comment));
        emit endResetModel();
    }
}

void GeneralCommentRepositoryDataModel::Delete(const QString &subject)
{
    if(subject != "")
    {
        auto findValue = m_CommentList.find(subject.toStdString());
        if(findValue == m_CommentList.end())
            return;

        m_CommentList.erase(findValue);
        m_Adapter->DeleteComment(subject.toStdString());
        emit endResetModel();
    }
}

QString GeneralCommentRepositoryDataModel::GetDescription(const QString &subject)
{
    if(m_CommentList.empty())
    {
        return QString("");
    }

    auto findValue = m_CommentList.find(subject.toStdString());
    if(findValue == m_CommentList.end())
    {
        return QString("");
    }

    return QString(findValue->second.c_str());
}

void GeneralCommentRepositoryDataModel::SetGeneralCommentList(std::map<std::string, std::string>&& commentList)
{
    m_CommentList = std::move(commentList);
    emit endResetModel();
}

QHash<int, QByteArray> GeneralCommentRepositoryDataModel::roleNames() const
{
    return m_CommentRoleNames;
}
