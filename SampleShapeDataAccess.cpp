#include "Headers/Adapters/SampleShapeDataAccess.h"

#include "Headers/Adapters/ConnectionGuard.h"
#include "Headers/Adapters/ISampleShapeQueryBuilder.h"
#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Adapters/IDatabaseConnection.h"
#include "Headers/Adapters/IQueryResult.h"

SampleShapeDataAccess::SampleShapeDataAccess(
        std::shared_ptr<IDatabaseConnection> dbConnection,
        std::shared_ptr<ISampleShapeQueryBuilder> sampleTypeQueryBuilder):
    m_DatabaceConnection(dbConnection),
    m_SampleShapeQueryBuilder(sampleTypeQueryBuilder)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto createTableCommand = m_SampleShapeQueryBuilder->GetCreateTableIfNotExistsQuery();
    m_DatabaceConnection->Exec(createTableCommand);
}

void SampleShapeDataAccess::InsertOrUpdate(const std::string &shapeName)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto rowCountCommand = m_SampleShapeQueryBuilder->GetRowCountQuery();
    rowCountCommand->BindParameter("ShapeName", shapeName);
    auto queryResult = m_DatabaceConnection->Exec(rowCountCommand);

    if (queryResult->GetInt(0) == 0)
    {
        auto insertCommand = m_SampleShapeQueryBuilder->GetInsertQuery();
        insertCommand->BindParameter("ShapeName", shapeName);
        m_DatabaceConnection->Exec(insertCommand);
    }
    else
    {
        auto updateCommand = m_SampleShapeQueryBuilder->GetUpdateQuery();
        updateCommand->BindParameter("ShapeName", shapeName);
        m_DatabaceConnection->Exec(updateCommand);
    }
}

void SampleShapeDataAccess::Delete(const std::string &shapeName)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto deleteCommand = m_SampleShapeQueryBuilder->GetDeleteQuery();
    deleteCommand->BindParameter("ShapeName", shapeName);
    m_DatabaceConnection->Exec(deleteCommand);
}

std::set<std::string> SampleShapeDataAccess::GetAllShapes()
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto selectAllCommand = m_SampleShapeQueryBuilder->GetSelectAllQuery();
    auto queryResult = m_DatabaceConnection->Exec(selectAllCommand);

    auto shapeList = std::set<std::string>();

    while(queryResult->HasData())
    {
        auto shapeName = queryResult->GetString(0);
        shapeList.insert(shapeName);
        queryResult->Next();
    }
    return std::move(shapeList);
}
