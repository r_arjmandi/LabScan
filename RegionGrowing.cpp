#include "Headers/BusinessRules/RegionGrowing.h"

#include "Headers/BusinessEntities/CircularTarget.h"

std::vector<CircularTarget> RegionGrowing::TryDetectRegions(
        const std::experimental::filesystem::path &imageAddress,
        std::vector<Circle> &circles) const
{
    auto imageMat = cv::imread(imageAddress.string(), 0);

    if(imageMat.empty())
    {
        return{};
    }
    auto ratio = 400.0 / imageMat.cols;
    cv::resize(imageMat, imageMat, cv::Size(400, imageMat.rows * ratio));
    //std::ifstream configFile("region.txt");
    //int kernel;
    //configFile >> kernel;
    cv::medianBlur(imageMat, imageMat, 23);
    //cv::imwrite("output.jpg", imageMat);
    const double PI = std::acos(-1);

    //std::ofstream outFile("average.txt");
    std::vector<CircularTarget> result;
    for(auto &circle : circles)
    {
        auto circleCenter = circle.Center;
        auto circleRadius = circle.Radius * ratio;
        int counter = 0;
        double gradient = 0.0;
        //outFile << "New Slice: " << std::endl;
        double lastIntensityAverage = -1.0;
        double lastSlope = -1.0;
        CircularTarget circularTarget;
        circularTarget.Disk = circle;
        while(true)
        {

            if(circleRadius > (imageMat.cols*0.3) || circleRadius > (imageMat.rows*0.3))
                break;
            double intensityAccumulator = 0.0;
            counter++;
            for(auto teta = 0.0; teta < 360.0; teta++)
            {
                auto x = circleRadius * std::cos((teta * 2 * PI) / 360.0) + circleCenter.X * ratio;
                auto y = circleRadius * std::sin((teta * 2 * PI) / 360.0) + circleCenter.Y * ratio;
                auto point = cv::Point(x, y);
                if((point.x < imageMat.cols && point.x > 0) && (point.y < imageMat.rows && point.y > 0))
                    intensityAccumulator += imageMat.at<unsigned char>(point);
            }

            auto intensityAverage = intensityAccumulator / 360.0;
            //gradient += intensityAverage;
            //auto slope = gradient / counter;
            //outFile << intensityAverage << std::endl;
            if(lastIntensityAverage == -1.0)
            {
                lastIntensityAverage = intensityAverage;
                continue;
            }

            //double thresh;
            //configFile >> thresh;

            if(intensityAverage - lastIntensityAverage > 0.8)
            {
                Circle region;
                region.Center.X = circle.Center.X;
                region.Center.Y = circle.Center.Y;
                region.Radius = circleRadius / ratio;
                circularTarget.Region = region;
                break;
            }

            circleRadius++;
            lastIntensityAverage = intensityAverage;
        }
        result.push_back(circularTarget);
    }
    return result;
}
