#include "Headers/BusinessRules/PatientListSearchEngine.h"
#include "Headers/BusinessEntities/PatientListFilter.h"
#include "Headers/BusinessEntities/PatientShortInformation.h"
#include "Headers/BusinessEntities/IDateTimeOperation.h"

PatientListSearchEngine::PatientListSearchEngine(
        std::shared_ptr<IDateTimeOperation> dateTimeOperation):
    m_DateTimeOperation(dateTimeOperation)
{
}

std::vector<std::weak_ptr<PatientShortInformation>> PatientListSearchEngine::Search(
        std::vector<std::weak_ptr<PatientShortInformation>>&& patientInfoList,
        std::unique_ptr<PatientListFilter> patientListFilter) const
{
    auto filter = [&patientListFilter, this](const std::weak_ptr<PatientShortInformation> patientInfo)
    {
        auto lockedPatintData = patientInfo.lock();
        if(!lockedPatintData)
        {
            return false;
        }

        bool findId = true, findName = true, findFamily = true, findDate = true;

        if(patientListFilter->Id != "")
        {
            findId = lockedPatintData->Id.find(patientListFilter->Id) == std::string::npos ? false : true;
        }
        if(patientListFilter->Name != "")
        {
            findName = lockedPatintData->Name.find(patientListFilter->Name) == std::string::npos ? false : true;
        }
        if(patientListFilter->FamilyName != "")
        {
            findFamily = lockedPatintData->FamilyName.find(patientListFilter->FamilyName) == std::string::npos ? false : true;
        }

        auto dateTimeEpoch = m_DateTimeOperation->ToEpoch(lockedPatintData->ModifyDateTime);
        findDate = dateTimeEpoch > m_DateTimeOperation->ToEpoch(patientListFilter->FromDateTime) &&
                dateTimeEpoch < m_DateTimeOperation->ToEpoch(patientListFilter->ToDateTime);

        return findId && findName && findFamily && findDate;
    };

    std::vector<std::weak_ptr<PatientShortInformation>> result;
    auto find = std::find_if(patientInfoList.cbegin(), patientInfoList.cend(), filter);
    while(find != patientInfoList.cend())
    {
        result.push_back(*find);
        find = std::find_if(find + 1, patientInfoList.cend(), filter);
    }
    return result;
}
