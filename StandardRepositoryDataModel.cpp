#include "Headers/Framework/StandardRepositoryDataModel.h"

#include "Headers/BusinessEntities/IBacteriaRepository.h"
#include "Headers/BusinessEntities/IAntibuticRepository.h"
#include "Headers/BusinessEntities/Range.h"
#include "Headers/BusinessEntities/Antibiotic.h"

StandardRepositoryDataModel::StandardRepositoryDataModel(
        const std::experimental::filesystem::path& standarDirectoryAddress,
        std::shared_ptr<IBacteriaRepository> bacteriaRepo,
        std::shared_ptr<IAntibuticRepository> antibuticRepo):
    m_StandardDirectoryAddress(standarDirectoryAddress),
    m_BacteriaRepo(bacteriaRepo),
    m_AntibuticRepo(antibuticRepo)
{
    m_StandardRoleNames[FileName] = "FileName";

    std::experimental::filesystem::directory_iterator standardDirItr(m_StandardDirectoryAddress);
    for(const auto& file : standardDirItr)
    {
        m_StandardFiles.push_back(file.path().filename().string());
    }

    m_CurrentStandardFile = (m_StandardDirectoryAddress / std::experimental::filesystem::path(m_StandardFiles[0])).string();
    UpdateBacteriaRepo();
}

int StandardRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return static_cast<int>(m_StandardFiles.size());
}

QVariant StandardRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(m_StandardFiles.empty())
        return QVariant();

    int row = index.row();
    if(row < 0 || row >= m_StandardFiles.size())
        return QVariant();

    switch(role) {
        case FileName:
        {
            auto pattern = std::regex("antibiotics_list_(.*)_.*");
            std::smatch matches;
            std::regex_search(m_StandardFiles.at(row), matches, pattern);
            return QString(matches[1].str().c_str());
        }

    }

    return QVariant();
}

void StandardRepositoryDataModel::SetCurrentBacteria(const QString& bacteria)
{
    auto bacteriList = m_BacteriaRepo->GetBacteriaList();

    int column = 0;
    for(; column<bacteriList->size() ; column++)
    {
        if(bacteriList->at(column) == bacteria.toStdString())
        {
            break;
        }
    }

    column+=3;

    std::ifstream file(m_CurrentStandardFile);
    std::string lineStr;

    auto antibuticList = std::vector<Antibiotic>();

    std::getline(file, lineStr);
    while(std::getline(file, lineStr))
    {
        std::stringstream lineStream(lineStr);
        std::string tokenStr;
        int counter = 0;
        std::string antibuticName, abbreviation;
        while (std::getline(lineStream, tokenStr, ';'))
        {
            if(counter == 0)
            {
                abbreviation = tokenStr;
            }
            if(counter == 1)
            {
                antibuticName = tokenStr;
            }
            else if(counter == column && tokenStr != "/")
            {
                auto pattern = std::regex("(\\d+)---(\\d+)");
                std::smatch matches;
                std::regex_search(tokenStr, matches, pattern);
                int min, max;
                min = std::stoi(matches[1]);
                max = std::stoi(matches[2]);
                Range range;
                range.Min = min;
                range.Max = max;
                Antibiotic antibiotoic;
                antibiotoic.Name = antibuticName;
                antibiotoic.Abbreviation = abbreviation;
                antibiotoic.StdRange = range;
                antibuticList.push_back(antibiotoic);
                break;
            }
            counter++;
        }

    }
    m_AntibuticRepo->SetAntibuticList(std::move(antibuticList));
}

void StandardRepositoryDataModel::SetCurrentStandard(int stdIndex)
{
    if(stdIndex < 0 || stdIndex > m_StandardFiles.size())
    {
        return;
    }
    m_CurrentStandardFile  = (m_StandardDirectoryAddress / std::experimental::filesystem::path(m_StandardFiles.at(stdIndex))).string();
    UpdateBacteriaRepo();
}

QHash<int, QByteArray> StandardRepositoryDataModel::roleNames() const
{
    return m_StandardRoleNames;
}

void StandardRepositoryDataModel::UpdateBacteriaRepo()
{
    std::ifstream file(m_CurrentStandardFile);
    std::string line1Str;
    std::getline(file, line1Str);

    auto bacteriaList = std::make_shared<std::vector<std::string>>();

    std::stringstream line1Stream(line1Str);
    std::string tokenStr;
    while (std::getline(line1Stream, tokenStr, ';'))
    {
        if(tokenStr == "Default" || tokenStr.empty())
            continue;
        bacteriaList->push_back(tokenStr);
    }
    m_BacteriaRepo->SetBacteriaList(bacteriaList);
}
