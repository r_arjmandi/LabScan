#include "Headers/BusinessEntities/CircularSorter.h"

#include "Headers/BusinessEntities/Point.h"
#include "Headers/BusinessEntities/CircularTarget.h"

template<bool clockwise>
class CircularSorterComparator
{
public:
    CircularSorterComparator(Point centerPoint):
        m_CenterPoint(centerPoint)
    {
    }

    bool operator()(const CircularTarget& a, const CircularTarget& b)
    {
        if (a.Disk.Center.X - m_CenterPoint.X >= 0 && b.Disk.Center.X - m_CenterPoint.X < 0)
        {
            return clockwise;
        }
        if (a.Disk.Center.X - m_CenterPoint.X < 0 && b.Disk.Center.X - m_CenterPoint.X >= 0)
        {
            return !clockwise;
        }
        if (a.Disk.Center.X - m_CenterPoint.X == 0 && b.Disk.Center.X - m_CenterPoint.X == 0)
        {
            if (a.Disk.Center.Y - m_CenterPoint.Y >= 0 || b.Disk.Center.Y - m_CenterPoint.Y >= 0)
            {
                return comparator(a.Disk.Center.Y, b.Disk.Center.Y);
            }
            return comparator(b.Disk.Center.Y, a.Disk.Center.Y);
        }

        // compute the cross product of vectors (center -> a) x (center -> b)
        auto det = static_cast<int>((a.Disk.Center.X - m_CenterPoint.X) * (b.Disk.Center.Y - m_CenterPoint.Y) -
                                    (b.Disk.Center.X - m_CenterPoint.X) * (a.Disk.Center.Y - m_CenterPoint.Y));
        if (det < 0)
        {
            return !clockwise;
        }
        if (det > 0)
        {
            return clockwise;
        }

        // points a and b are on the same line from the center
        // check which point is closer to the center
        auto d1 = static_cast<int>(std::pow(a.Disk.Center.X - m_CenterPoint.X, 2) + std::pow(a.Disk.Center.Y - m_CenterPoint.Y, 2));
        auto d2 = static_cast<int>(std::pow(b.Disk.Center.X - m_CenterPoint.X, 2) + std::pow(b.Disk.Center.Y - m_CenterPoint.Y, 2));
        return comparator(d1, d2);
    }

private:
    static bool comparator(const double& a, const double& b)
    {
        auto greater = std::greater<double>();
        auto less = std::less<double>();
        return clockwise ? greater(a, b) : less(a, b);
    }

    Point m_CenterPoint;
};

void CircularSorter::SortClockwise(std::weak_ptr<std::vector<CircularTarget>> circles)
{
    auto getCircles = circles.lock();
    if(!getCircles || getCircles->empty())
    {
        return;
    }
    auto centerPoint = CalculateCenterPoint(getCircles);
    auto clockwiseSorterFunc = CircularSorterComparator<true>(centerPoint);
    std::sort(getCircles->begin(), getCircles->end(), clockwiseSorterFunc);
}

void CircularSorter::SortCenterClockwise(std::weak_ptr<std::vector<CircularTarget> > circles)
{
    auto getCircles = circles.lock();
    if(!getCircles || getCircles->empty())
    {
        return;
    }
    auto centerPoint = CalculateCenterPoint(getCircles);
    MoveCentralCirlceToFirst(getCircles, centerPoint);
    auto clockwiseSorterFunc = CircularSorterComparator<true>(centerPoint);
    std::sort(getCircles->begin() + 1, getCircles->end(), clockwiseSorterFunc);
}

void CircularSorter::SortCounterClockwise(std::weak_ptr<std::vector<CircularTarget> > circles)
{
    auto getCircles = circles.lock();
    if(!getCircles || getCircles->empty())
    {
        return;
    }
    auto centerPoint = CalculateCenterPoint(getCircles);
    auto counterClockwiseSorterFunc = CircularSorterComparator<false>(centerPoint);
    std::sort(getCircles->begin(), getCircles->end(), counterClockwiseSorterFunc);
}

void CircularSorter::SortCenterCounterClockwise(std::weak_ptr<std::vector<CircularTarget> > circles)
{
    auto getCircles = circles.lock();
    if(!getCircles || getCircles->empty())
    {
        return;
    }
    auto centerPoint = CalculateCenterPoint(getCircles);
    MoveCentralCirlceToFirst(getCircles, centerPoint);
    auto counterClockwiseSorterFunc = CircularSorterComparator<false>(centerPoint);
    std::sort(getCircles->begin() + 1, getCircles->end(), counterClockwiseSorterFunc);
}

Point CircularSorter::CalculateCenterPoint(std::weak_ptr<std::vector<CircularTarget>> circles)
{
    auto getCircles = circles.lock();
    auto compareFuncX = [](const CircularTarget& a, const CircularTarget& b)
    {
        return a.Disk.Center.X < b.Disk.Center.X;
    };

    auto compareFuncY = [](const CircularTarget& a, const CircularTarget& b)
    {
        return a.Disk.Center.Y < b.Disk.Center.Y;
    };

    auto minMaxX = std::minmax_element(getCircles->cbegin(), getCircles->cend(), compareFuncX);
    auto minMaxY = std::minmax_element(getCircles->cbegin(), getCircles->cend(), compareFuncY);

    Point result;
    result.X = (minMaxX.second->Disk.Center.X - minMaxX.first->Disk.Center.X) / 2.0 + minMaxX.first->Disk.Center.X;
    result.Y = (minMaxY.second->Disk.Center.Y - minMaxY.first->Disk.Center.Y) / 2.0 + minMaxY.first->Disk.Center.Y;

    return result;
}

void CircularSorter::MoveCentralCirlceToFirst(std::weak_ptr<std::vector<CircularTarget>> circles, const Point& centerPoint)
{
    auto getCircles = circles.lock();
    auto distanceLess = [&centerPoint](const CircularTarget& a, const CircularTarget& b)
    {
        auto distanceA = std::hypot(a.Disk.Center.X - centerPoint.X, a.Disk.Center.Y - centerPoint.Y);
        auto distanceB = std::hypot(b.Disk.Center.X - centerPoint.X, b.Disk.Center.Y - centerPoint.Y);
        return distanceA < distanceB;
    };
    auto nearestCircle = std::min_element(getCircles->begin(), getCircles->end(), distanceLess);
    std::iter_swap(nearestCircle, getCircles->begin());
}
