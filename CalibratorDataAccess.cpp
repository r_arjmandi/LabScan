#include "Headers/Adapters/CalibratorDataAccess.h"
#include "Headers/Adapters/ConnectionGuard.h"
#include "Headers/Adapters/ICalibratorQueryBuilder.h"
#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Adapters/IDatabaseConnection.h"
#include "Headers/Adapters/IQueryResult.h"

CalibratorDataAccess::CalibratorDataAccess(
        std::shared_ptr<IDatabaseConnection> dbConnection,
        std::shared_ptr<ICalibratorQueryBuilder> calibratorQueryBuilder):
    m_DatabaceConnection(dbConnection),
    m_CalibratorQueryBuilder(calibratorQueryBuilder)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto createTableCommand = m_CalibratorQueryBuilder->GetCreateTableIfNotExistsQuery();
    m_DatabaceConnection->Exec(createTableCommand);
}

void CalibratorDataAccess::InsertOrUpdate(const std::pair<std::string, double> &&calibrator)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto rowCountCommand = m_CalibratorQueryBuilder->GetRowCountQuery();
    rowCountCommand->BindParameter("CalibratorName", calibrator.first);
    auto queryResult = m_DatabaceConnection->Exec(rowCountCommand);

    if (queryResult->GetInt(0) == 0)
    {
        auto insertCommand = m_CalibratorQueryBuilder->GetInsertQuery();
        insertCommand->BindParameter("CalibratorName", calibrator.first);
        insertCommand->BindParameter("Value", calibrator.second);
        m_DatabaceConnection->Exec(insertCommand);
    }
    else
    {
        auto updateCommand = m_CalibratorQueryBuilder->GetUpdateQuery();
        updateCommand->BindParameter("CalibratorName", calibrator.first);
        updateCommand->BindParameter("Value", calibrator.second);
        m_DatabaceConnection->Exec(updateCommand);
    }
}

void CalibratorDataAccess::Delete(const std::string &calibrator)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto deleteCommand = m_CalibratorQueryBuilder->GetDeleteQuery();
    deleteCommand->BindParameter("CalibratorName", calibrator);
    m_DatabaceConnection->Exec(deleteCommand);
}

std::vector<std::pair<std::string, double>> CalibratorDataAccess::GetAllCalibrator()
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto selectAllCommand = m_CalibratorQueryBuilder->GetSelectAllQuery();
    auto queryResult = m_DatabaceConnection->Exec(selectAllCommand);

    auto calibratorList = std::vector<std::pair<std::string, double>>();

    while(queryResult->HasData())
    {
        auto calibratorName = queryResult->GetString(0);
        auto value = queryResult->GetDouble(1);
        auto calibrator = std::make_pair(calibratorName, value);
        calibratorList.push_back(std::move(calibrator));
        queryResult->Next();
    }
    return std::move(calibratorList);
}
