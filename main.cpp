#include "Headers/BusinessEntities/PatientData.h"
#include "Headers/BusinessEntities/DateTimeOperation.h"
#include "Headers/BusinessEntities/FileNameGenerator.h"
#include "Headers/BusinessEntities/CircularSorter.h"
#include "Headers/BusinessEntities/PatientShortInformationRepository.h"

#include "Headers/BusinessRules/BusinessRules.h"
#include "Headers/BusinessRules/PatientListSearchEngine.h"
#include "Headers/BusinessRules/RegionGrowing.h"

#include "Headers/Adapters/FrontEndAdapter.h"
#include "Headers/Adapters/BackEndAdapter.h"
#include "Headers/Adapters/PatientDataAccess.h"
#include "Headers/Adapters/OperatorDataAccess.h"
#include "Headers/Adapters/SampleTypeDataAccess.h"
#include "Headers/Adapters/SampleShapeDataAccess.h"
#include "Headers/Adapters/CommentDataAccess.h"
#include "Headers/Adapters/MediaDataAccess.h"
#include "Headers/Adapters/ImageDataAccess.h"
#include "Headers/Adapters/CalibratorDataAccess.h"

#include "Headers/Framework/SqliteDatabaseConnection.h"
#include "Headers/Framework/DatabaseCommandFactory.h"
#include "Headers/Framework/SqliteQueryResultFactory.h"

#include "Headers/Framework/SqlitePatientQueryBuilder.h"
#include "Headers/Framework/SqliteOperatorQueryBuilder.h"
#include "Headers/Framework/SqliteSampleTypeQueryBuilder.h"
#include "Headers/Framework/SqliteSampleShapeQueryBuilder.h"
#include "Headers/Framework/SqliteCommentQueryBuilder.h"
#include "Headers/Framework/SqliteMediaQueryBuilder.h"
#include "Headers/Framework/SqliteCalibratorQueryBuilder.h"

#include "Headers/Framework/StandardRepositoryDataModel.h"
#include "Headers/Framework/PatientListDataModel.h"
#include "Headers/Framework/BacteriaRepositoryDataModel.h"
#include "Headers/Framework/AntibuticRepositoryDataModel.h"
#include "Headers/Framework/CurrentPatientImageListDataModel.h"
#include "Headers/Framework/CircleRepositoryDataModel.h"
#include "Headers/Framework/CalibratorRepositoryDataModel.h"
#include "Headers/Framework/OperatorRepositoryDataModel.h"
#include "Headers/Framework/SampleTypeRepositoryDataModel.h"
#include "Headers/Framework/SampleShapeRepositoryDataModel.h"
#include "Headers/Framework/GeneralCommentRepositoryDataModel.h"
#include "Headers/Framework/MediaRepositoryDataModel.h"

#include "Headers/Framework/BackEndWrapper.h"
#include "Headers/Framework/CurrentPatientWrapper.h"
#include "Headers/Framework/OpenCvHoughCircleWrapper.h"
#include "Headers/Framework/ScannerDevice.h"
#include "Headers/Framework/PatientListSearchWrapper.h"
#include "Headers/Framework/PatientLogger.h"
#include "Headers/Framework/ExcelFilePrinter.h"

CurrentPatientWrapper *currentPatientWrapper;
PatientListDataModel *patientRepositoryDataModel;
BackEndWrapper *backEndWrapper;
BacteriaRepositoryDataModel* bacteriaRepo;
StandardRepositoryDataModel *standardRepo;
CurrentPatientImageListDataModel *imageListDataModel;
AntibuticRepositoryDataModel *antibuticRepo;
CircleRepositoryDataModel *circleRepo;
CalibratorRepositoryDataModel *calibratorRepo;
OperatorRepositoryDataModel *operatorRepo;
SampleTypeRepositoryDataModel *sampleTypeRepo;
SampleShapeRepositoryDataModel *sampleShapeRepo;
GeneralCommentRepositoryDataModel *commentRepo;
ScannerDevice *scannerDevice;
MediaRepositoryDataModel *mediaRepo;
PatientListSearchWrapper *patientListSearchWrapper;

QObject* BackEndWrapperProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* CurrentPatientWrapperProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* PatientRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* BacteriaRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* StandardRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* ImageRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* AntibuticRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* CircleRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* CalibratorRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* OperatorRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* SampleTypeRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* SampleShapeRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* CommentRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* ScannerDeviceProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* MediaRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine);
QObject* PatientListSearchWrapperProvider(QQmlEngine *engine, QJSEngine *scriptEngine);

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    scannerDevice = new ScannerDevice();

    calibratorRepo = new CalibratorRepositoryDataModel();

    antibuticRepo = new AntibuticRepositoryDataModel();
    bacteriaRepo = new BacteriaRepositoryDataModel();
    standardRepo = new StandardRepositoryDataModel(
                "C:\\LabScanStandards",
                std::shared_ptr<BacteriaRepositoryDataModel>(bacteriaRepo),
                std::shared_ptr<AntibuticRepositoryDataModel>(antibuticRepo));
    mediaRepo = new MediaRepositoryDataModel();

    auto databaseFilePath = std::string("Database.db");
    auto dateTimeOperation = std::make_shared<DateTimeOperation>();
    auto queryResultFactory = std::make_shared<SqliteQueryResultFactory>(dateTimeOperation);
    auto databaseConnection = std::make_shared<SqliteDatabaseConnection>(databaseFilePath, queryResultFactory, dateTimeOperation);
    auto databaseCommandFactory = std::make_shared<DatabaseCommandFactory>();
    auto patientQueryBuilder = std::make_shared<SqlitePatientQueryBuilder>(databaseCommandFactory);
    auto patientDataAccess = std::make_shared<PatientDataAccess>(databaseConnection, patientQueryBuilder);

    auto operatorQueryBuilder = std::make_shared<SqliteOperatorQueryBuilder>(databaseCommandFactory);
    auto sampleTypeQueryBuilder = std::make_shared<SqliteSampleTypeQueryBuilder>(databaseCommandFactory);
    auto sampleShapeQueryBuilder = std::make_shared<SqliteSampleShapeQueryBuilder>(databaseCommandFactory);
    auto commentQueryBuilder = std::make_shared<SqliteCommentQueryBuilder>(databaseCommandFactory);
    auto mediaQueryBuilder = std::make_shared<SqliteMediaQueryBuilder>(databaseCommandFactory);

    auto operatorDataAccess = std::make_shared<OperatorDataAccess>(databaseConnection, operatorQueryBuilder);
    auto sampleTypeDataAccess = std::make_shared<SampleTypeDataAccess>(databaseConnection, sampleTypeQueryBuilder);
    auto sampleShapeDataAccess = std::make_shared<SampleShapeDataAccess>(databaseConnection, sampleShapeQueryBuilder);
    auto commentDataAccess = std::make_shared<CommentDataAccess>(databaseConnection, commentQueryBuilder);
    auto mediaDataAccess = std::make_shared<MediaDataAccess>(databaseConnection, mediaQueryBuilder);

    circleRepo = new CircleRepositoryDataModel(
                std::shared_ptr<IAntibuticRepository>(antibuticRepo),
                std::shared_ptr<ICalibratorRepository>(calibratorRepo));
    imageListDataModel = new CurrentPatientImageListDataModel();
    currentPatientWrapper = new CurrentPatientWrapper(std::shared_ptr<IImageListGui>(imageListDataModel));
    auto fileNameGenerator = std::make_shared<FileNameGenerator>();

    auto openCvCircleDetector = std::make_shared<OpenCvHoughCircleWrapper>();

    operatorRepo = new OperatorRepositoryDataModel();
    sampleTypeRepo = new SampleTypeRepositoryDataModel();
    sampleShapeRepo = new SampleShapeRepositoryDataModel();
    commentRepo = new GeneralCommentRepositoryDataModel(std::shared_ptr<ICurrentPatientGui>(currentPatientWrapper));

    patientRepositoryDataModel = new PatientListDataModel(dateTimeOperation);

    auto imageDataAccess = std::make_shared<ImageDataAccess>(
                std::experimental::filesystem::current_path() / "ImageData",
                fileNameGenerator);

    auto backEndAdapter = std::make_shared<BackEndAdapter>(
                std::shared_ptr<PatientListDataModel>(patientRepositoryDataModel),
                std::shared_ptr<ICurrentPatientGui>(currentPatientWrapper));

    auto calibratorQueryBuilder = std::make_shared<SqliteCalibratorQueryBuilder>(databaseCommandFactory);
    auto calibratorDataAccess = std::make_shared<CalibratorDataAccess>(
                databaseConnection,
                calibratorQueryBuilder);

    auto circularSorter = std::make_shared<CircularSorter>();
    auto patientRepository = std::make_shared<PatientShortInformationRepository>();
    auto patientListSearchEngine = std::make_shared<PatientListSearchEngine>(dateTimeOperation);
    auto patientLogger = std::make_shared<PatientLogger>("PatientLog.txt");
    auto regionGrowing = std::make_shared<RegionGrowing>();
    auto excelFilePrinter = std::make_shared<ExcelFilePrinter>("test.xlsx");
    auto businessRules = std::make_shared<BusinessRules>(
                backEndAdapter,
                openCvCircleDetector,
                circularSorter,
                patientDataAccess,
                imageDataAccess,
                operatorDataAccess,
                sampleTypeDataAccess,
                sampleShapeDataAccess,
                commentDataAccess,
                mediaDataAccess,
                patientRepository,
                std::shared_ptr<ICircleRepository>(circleRepo),
                std::shared_ptr<IOperatorRepository>(operatorRepo),
                std::shared_ptr<ISampleTypeRepository>(sampleTypeRepo),
                std::shared_ptr<ISampleShapeRepository>(sampleShapeRepo),
                std::shared_ptr<IGeneralCommentRepository>(commentRepo),
                std::shared_ptr<IMediaRepository>(mediaRepo),
                patientListSearchEngine,
                dateTimeOperation,
                calibratorDataAccess,
                std::shared_ptr<ICalibratorRepository>(calibratorRepo),
                patientLogger,
                regionGrowing,
                excelFilePrinter);

    currentPatientWrapper->SetCurrenaPatientData(businessRules->GetCurrentPatientData());
    auto adapter = std::make_shared<FrontEndAdapter>(businessRules);
    operatorRepo->SetAdapter(std::shared_ptr<FrontEndAdapter>(adapter));
    sampleTypeRepo->SetAdapter(std::shared_ptr<FrontEndAdapter>(adapter));
    sampleShapeRepo->SetAdapter(std::shared_ptr<FrontEndAdapter>(adapter));
    commentRepo->SetAdapter(std::shared_ptr<FrontEndAdapter>(adapter));
    mediaRepo->SetAdapter(std::shared_ptr<FrontEndAdapter>(adapter));
    calibratorRepo->SetAdapter(std::shared_ptr<FrontEndAdapter>(adapter));

    backEndWrapper = new BackEndWrapper(
                adapter,
                std::shared_ptr<ICurrentPatientGui>(currentPatientWrapper));

    patientListSearchWrapper = new PatientListSearchWrapper(
                std::shared_ptr<IFrontEndAdapter>(adapter),
                dateTimeOperation);

    qmlRegisterSingletonType<BackEndWrapper>("BackEnd.Wrapper", 1, 0, "BackEndWrapper", BackEndWrapperProvider);
    qmlRegisterSingletonType<CurrentPatientWrapper>("CurrentPatient.Wrapper", 1, 0, "CurrentPatientWrapper", CurrentPatientWrapperProvider);
    qmlRegisterSingletonType<PatientListDataModel>("PatientRepository.DataModel", 1, 0, "PatientRepositoryDataModel", PatientRepositoryDataModelProvider);
    qmlRegisterSingletonType<BacteriaRepositoryDataModel>("BacteriaRepository.DataModel", 1, 0, "BacteriaRepositoryDataModel", BacteriaRepositoryDataModelProvider);
    qmlRegisterSingletonType<StandardRepositoryDataModel>("StandardRepository.DataModel", 1, 0, "StandardRepositoryDataModel", StandardRepositoryDataModelProvider);
    qmlRegisterSingletonType<CurrentPatientImageListDataModel>("CurrentPatientImageRepository.DataModel", 1, 0, "CurrentPatientImageRepositoryDataModel", ImageRepositoryDataModelProvider);
    qmlRegisterSingletonType<AntibuticRepositoryDataModel>("AntibuticRepository.DataModel", 1, 0, "AntibuticRepositoryDataModel", AntibuticRepositoryDataModelProvider);
    qmlRegisterSingletonType<CircleRepositoryDataModel>("CircleRepository.DataModel", 1, 0, "CircleRepositoryDataModel", CircleRepositoryDataModelProvider);
    qmlRegisterSingletonType<CalibratorRepositoryDataModel>("CalibratorRepository.DataModel", 1, 0, "CalibratorRepositoryDataModel", CalibratorRepositoryDataModelProvider);
    qmlRegisterSingletonType<OperatorRepositoryDataModel>("OperatorRepository.DataModel", 1, 0, "OperatorRepositoryDataModel", OperatorRepositoryDataModelProvider);
    qmlRegisterSingletonType<SampleTypeRepositoryDataModel>("SampleTypeRepository.DataModel", 1, 0, "SampleTypeRepositoryDataModel", SampleTypeRepositoryDataModelProvider);
    qmlRegisterSingletonType<SampleShapeRepositoryDataModel>("SampleShapeRepository.DataModel", 1, 0, "SampleShapeRepositoryDataModel", SampleShapeRepositoryDataModelProvider);
    qmlRegisterSingletonType<GeneralCommentRepositoryDataModel>("GeneralCommentRepository.DataModel", 1, 0, "GeneralCommentRepositoryDataModel", CommentRepositoryDataModelProvider);
    qmlRegisterSingletonType<ScannerDevice>("Scanner.Device", 1, 0, "ScannerDevice", ScannerDeviceProvider);
    qmlRegisterSingletonType<MediaRepositoryDataModel>("MediaRepository.DataModel", 1, 0, "MediaRepositoryDataModel", MediaRepositoryDataModelProvider);
    qmlRegisterSingletonType<PatientListSearchWrapper>("PatientListSearch.Wrapper", 1, 0, "PatientListSearchWrapper", PatientListSearchWrapperProvider);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

static QObject* BackEndWrapperProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return backEndWrapper;
}

static QObject* CurrentPatientWrapperProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return currentPatientWrapper;
}

static QObject* PatientRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return patientRepositoryDataModel;
}

static QObject* BacteriaRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return bacteriaRepo;
}

static QObject* StandardRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return standardRepo;
}

static QObject* ImageRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return imageListDataModel;
}

static QObject* AntibuticRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return antibuticRepo;
}

static QObject* CircleRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return circleRepo;
}

static QObject* CalibratorRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return calibratorRepo;
}

static QObject* OperatorRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return operatorRepo;
}

static QObject* SampleTypeRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return sampleTypeRepo;
}

static QObject* SampleShapeRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return sampleShapeRepo;
}

static QObject* CommentRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return commentRepo;
}

static QObject* ScannerDeviceProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return scannerDevice;
}

static QObject* MediaRepositoryDataModelProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return mediaRepo;
}

static QObject* PatientListSearchWrapperProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return patientListSearchWrapper;
}
