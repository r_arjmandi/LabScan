#include "Headers/Framework/PatientListSearchWrapper.h"

#include "Headers/Adapters/IFrontEndAdapter.h"
#include "Headers/BusinessEntities/PatientListFilter.h"
#include "Headers/BusinessEntities/IDateTimeOperation.h"

PatientListSearchWrapper::PatientListSearchWrapper(
        std::shared_ptr<IFrontEndAdapter> frontEndAdapter,
        std::shared_ptr<IDateTimeOperation> dateTimeOperation):
    m_FrontEndAdapter(frontEndAdapter),
    m_DateTimeOperation(dateTimeOperation)
{
}

QString PatientListSearchWrapper::id() const
{
    return m_Id;
}

QString PatientListSearchWrapper::name() const
{
    return m_Name;
}

QString PatientListSearchWrapper::familyName() const
{
    return m_FamilyName;
}

QString PatientListSearchWrapper::fromDate() const
{
    return m_FromDate;
}

QString PatientListSearchWrapper::toDate() const
{
    return m_ToDate;
}

void PatientListSearchWrapper::setId(const QString &id)
{
    if(m_Id == id)
    {
        return;
    }

    m_Id = id;
    FilterPatientList();
    emit idChanged();
}

void PatientListSearchWrapper::setName(const QString &name)
{
    if(m_Name == name)
    {
        return;
    }

    m_Name = name;
    FilterPatientList();
    emit nameChanged();
}

void PatientListSearchWrapper::setFamilyName(const QString &familyName)
{
    if(m_FamilyName == familyName)
    {
        return;
    }

    m_FamilyName = familyName;
    FilterPatientList();
    emit familyNameChanged();
}

void PatientListSearchWrapper::setFromDate(const QString &fromDate)
{
    if(m_FromDate == fromDate)
    {
        return;
    }

    m_FromDate = fromDate;
    FilterPatientList();
    emit fromDateChanged();
}

void PatientListSearchWrapper::setToDate(const QString &toDate)
{
    if(m_ToDate == toDate)
    {
        return;
    }

    m_ToDate = toDate;
    FilterPatientList();
    emit toDateChanged();
}

void PatientListSearchWrapper::FilterPatientList() const
{
    auto patientListFilter = std::make_unique<PatientListFilter>();
    patientListFilter->Id = m_Id.toStdString();
    patientListFilter->Name = m_Name.toStdString();
    patientListFilter->FamilyName = m_FamilyName.toStdString();

    std::string fromDateStr = m_FromDate != "" ?
                m_FromDate.toStdString() :
                "1970:01:01";
    auto fromDateTimeStr = fromDateStr + " 00:00:00";
    auto fromDateTime = m_DateTimeOperation->FromString(fromDateTimeStr);
    patientListFilter->FromDateTime = fromDateTime;

    auto nowDate = std::time(0);
    auto nowDateStr = m_DateTimeOperation->ToString(m_DateTimeOperation->FromEpoch(nowDate)).substr(0, 10);
    std::string toDateStr = m_ToDate != "" ?
                m_ToDate.toStdString() :
                nowDateStr;

    auto toDateTimeStr = toDateStr + " 23:59:59";
    auto toDateTime = m_DateTimeOperation->FromString(toDateTimeStr);
    patientListFilter->ToDateTime = toDateTime;

    m_FrontEndAdapter->FilterPatientList(std::move(patientListFilter));
}
