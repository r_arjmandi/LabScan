import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import CurrentPatient.Wrapper 1.0

RowLayout
{
    id: root

    property int gender : CurrentPatientWrapper.gender

    onGenderChanged: {
        if(gender == 1)
        {
            patientPropsMaleRadioButton.checked = true;
        }
        else if(gender == 2)
        {
            patientPropsFemaleRadioButton.checked = true;
        }
    }

    RadioButton{
        id: patientPropsMaleRadioButton
        text: qsTr("Male")
        checked: true

        onCheckedChanged: {
            if(checked === true)
            {
                CurrentPatientWrapper.gender = 1;
            }
        }
    }

    RadioButton{
        id: patientPropsFemaleRadioButton
        text: qsTr("Female")

        onCheckedChanged: {
            if(checked === true)
            {
                CurrentPatientWrapper.gender = 2;
            }
        }
    }
}
