import QtQuick 2.9
import QtQuick.Controls 2.2
import BackEnd.Wrapper 1.0
import QtQuick.Layouts 1.3

import PatientRepository.DataModel 1.0
import BackEnd.Wrapper 1.0
import PatientListSearch.Wrapper 1.0

GroupBox {
    id: root

    DateCalculations{
        id:calc
    }

    signal loadClicked;
    signal patientPageClicked;

    onLoadClicked: {
        BackEndWrapper.LoadPatient(searchResultsListView.currentId);
    }

    signal dateTimePickerPageClicked
    signal updateSelectedDateTime
    property string selectedDateTime
    property var selectedDateComponent

    onUpdateSelectedDateTime: {
        if(selectedDateComponent)
        {
            selectedDateComponent.text = selectedDateTime
        }
    }

    ColumnLayout{
        spacing: 10
        anchors.fill: parent

        RowLayout{
            spacing: 20
            Layout.fillWidth: true

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("ID:")
                font{family: "Arial"; pixelSize: 16}
            }

            TextField{
                id: searchIDField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}

                onTextChanged: {
                    PatientListSearchWrapper.id = text;
                }
            }
        }

        RowLayout{
            spacing: 20
            Layout.fillWidth: true

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("First Name:")
                font{family: "Arial"; pixelSize: 16}
            }

            TextField{
                id: searchFirstNameField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}

                onTextChanged: {
                    PatientListSearchWrapper.name = text;
                }
            }
        }

        RowLayout{
            spacing: 20
            Layout.fillWidth: true

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Last Name:")
                font{family: "Arial"; pixelSize: 16}
            }

            TextField{
                id: searchLastNameField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}

                onTextChanged: {
                    PatientListSearchWrapper.familyName = text;
                }
            }
        }


        RowLayout{
            id: row4
            spacing: 20
            Layout.fillWidth: true

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("From:")
                font{family: "Arial"; pixelSize: 16}
            }

            TextField{
                id: fromDateTimeField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}

                onTextChanged: {
                    var jDateTime = text
                    var jYear = parseInt(jDateTime.substring(0, 4))
                    var jMonth = parseInt(jDateTime.substring(5, 7))
                    var jDay = parseInt(jDateTime.substring(8, 10))
                    var gDate = calc.jalali_to_gregorian(jYear, jMonth, jDay)
                    var gYear = gDate[0]
                    var gMonth = gDate[1] < 10 ? "0" + gDate[1] : gDate[1]
                    var gDay = gDate[2] < 10 ? "0" + gDate[2] : gDate[2]
                    PatientListSearchWrapper.fromDate = gYear + "-" + gMonth + "-" + gDay;
                }
            }

            Button{
                id: selectFromDate
                text: "..."
                Layout.minimumWidth: 50

                onClicked: {
                    root.dateTimePickerPageClicked();
                    root.selectedDateComponent = fromDateTimeField;
                }
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("To:")
                font{family: "Arial"; pixelSize: 16}
            }

            TextField{
                id: toDateTimeField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}

                onTextChanged: {
                    var jDateTime = text
                    var jYear = parseInt(jDateTime.substring(0, 4))
                    var jMonth = parseInt(jDateTime.substring(5, 7))
                    var jDay = parseInt(jDateTime.substring(8, 10))
                    var gDate = calc.jalali_to_gregorian(jYear, jMonth, jDay)
                    var gYear = gDate[0]
                    var gMonth = gDate[1] < 10 ? "0" + gDate[1] : gDate[1]
                    var gDay = gDate[2] < 10 ? "0" + gDate[2] : gDate[2]
                    PatientListSearchWrapper.toDate = gYear + "-" + gMonth + "-" + gDay;
                }
            }

            Button{
                id: selectToDate
                text: "..."
                Layout.minimumWidth: 50

                onClicked: {
                    root.dateTimePickerPageClicked();
                    root.selectedDateComponent = toDateTimeField;
                }
            }
        }

        ListView {
                id: searchResultsListView
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true
                cacheBuffer: 300

                contentWidth: headerItem.width
                flickableDirection: Flickable.VerticalFlick

                property string currentId

                headerPositioning: ListView.OverlayHeader

                header: Row {
                    spacing: 1

                    z : 2

                    function itemAt(index) { return repeater.itemAt(index) }

                    Repeater {
                        id: repeater
                        model: ["ID", "Name", "LastName", "Date"]
                        Label {
                            text: modelData
                            font{family: "Arial"; pixelSize: 16; bold: true}
                            topPadding: 10
                            bottomPadding: 10
                            leftPadding: 5
                            background: Rectangle { color: "silver" }
                            width: searchResultsListView.width / 4
                        }
                    }
                }

                model: PatientRepositoryDataModel
                delegate: Column {

                    id: delegate
                    property int row: index

                    RowLayout {
                        spacing: 1

                        MouseArea{
                            id: itemMouseArea
                            anchors.fill: parent
                            onClicked:
                            {
                                searchResultsListView.currentIndex = index
                                searchResultsListView.currentId = model.ID
                            }

                            onDoubleClicked: {
                                root.loadClicked()
                                root.patientPageClicked()
                            }
                        }

                        Text{
                            text: model.ID
                            font{family: "Arial"; pixelSize: 14}
                            Layout.leftMargin: 16
                            Layout.minimumWidth : searchResultsListView.headerItem.itemAt(0).width
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        Text{
                            text: model.Name
                            font{family: "Arial"; pixelSize: 12}
                            Layout.leftMargin: 16
                            Layout.minimumWidth: searchResultsListView.headerItem.itemAt(1).width
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        Text{
                            text: model.FamilyName
                            font{family: "Arial"; pixelSize: 14}
                            Layout.leftMargin: 16
                            Layout.minimumWidth: searchResultsListView.headerItem.itemAt(2).width
                            anchors.verticalCenter: parent.verticalCenter
                        }

                        Text{

                            text:
                            {
                                var modifyDateTime = model.ModifyDateTime
                                var year = parseInt(modifyDateTime.substring(0, 4))
                                var month = parseInt(modifyDateTime.substring(5, 7))
                                var day = parseInt(modifyDateTime.substring(8, 10))
                                var jalalyDate = calc.gregorian_to_jalali(year, month, day)
                                var jYear = jalalyDate[0]
                                var jMonth = jalalyDate[1] < 10 ? "0" + jalalyDate[1] : jalalyDate[1]
                                var jDay = jalalyDate[2] < 10 ? "0" + jalalyDate[2] : jalalyDate[2]
                                return jYear + "-" + jMonth + "-" + jDay
                            }
                            font{family: "Arial"; pixelSize: 14}
                            Layout.leftMargin: 16
                            Layout.minimumWidth: searchResultsListView.headerItem.itemAt(3).width
                            anchors.verticalCenter: parent.verticalCenter
                        }
                    }

                    Rectangle {
                        color: "silver"
                        width: parent.width
                        height: 1
                    }
                }

                highlight: Rectangle{
                        z : 2
                        color: "#32000000"
                        border.width: 2
                        border.color: "#32000000";
                }

                ScrollIndicator.horizontal: ScrollIndicator { }
                ScrollIndicator.vertical: ScrollIndicator { }
            }
    }
}
