import QtQuick 2.9
import QtQuick.Layouts 1.3

Rectangle{
    id: root

    property url icon
    property url backgroundImage
    property string subject
    property int subjectFontSize
    property string description
    property int descriptionFontSize

    signal clicked;

    Image {
        id: menuItemBackImage
        source: backgroundImage
        anchors.fill: parent
    }

    Text{
        id: menuItemText
        anchors.centerIn: parent

        color: "#FFFFFF"
        antialiasing: true
        smooth: true

        text: subject
        fontSizeMode: Text.Fit; minimumPixelSize: 10
        font{family: "Arial"; pixelSize: subjectFontSize}

        Behavior on scale{
            NumberAnimation{
                duration: 1000
                easing.type: Easing.OutExpo
            }
        }

        Behavior on anchors.horizontalCenterOffset{
            NumberAnimation{
                duration: 1000
                easing.type: Easing.OutExpo
            }
        }

        Behavior on anchors.verticalCenterOffset {
            NumberAnimation{
                duration: 1000
                easing.type: Easing.OutExpo
            }
        }
    }

    Text{
        id: menuItemTextDescription
        anchors.left: menuItemText.left
        anchors.top: menuItemText.bottom

        width: 180
        wrapMode: Text.WordWrap

        opacity: 0

        color: "#FFFFFF"
        antialiasing: true
        smooth: true

        text: description
        fontSizeMode: Text.Fit; minimumPixelSize: 10
        font{family: "Arial"; pixelSize: descriptionFontSize}

        Behavior on opacity{
            NumberAnimation{
                duration: 1000
                easing.type: Easing.OutExpo
            }
        }

        Behavior on anchors.leftMargin{
            NumberAnimation{
                duration: 1000
                easing.type: Easing.OutExpo
            }
        }
    }

    Image {
        id: menuItemIcon
        source: icon

        width: 80
        height: 80

        anchors.right: menuItemText.left
        antialiasing: true
        smooth: true
        anchors.verticalCenter: parent.verticalCenter
        Behavior on anchors.rightMargin {
            NumberAnimation{
                duration: 1000
                easing.type: Easing.OutExpo
            }
        }

        Behavior on scale{
            NumberAnimation{
                duration: 1000
                easing.type: Easing.OutExpo
            }
        }
    }

    MouseArea{
        id: itemMouseArea
        width: parent.width
        height: parent.height

        hoverEnabled: true

        onClicked: root.clicked();

        onEntered: {
            menuItemText.anchors.horizontalCenterOffset -= 30
            menuItemText.anchors.verticalCenterOffset -= 15
            menuItemTextDescription.anchors.leftMargin += 15
            menuItemIcon.anchors.rightMargin -= 30
            menuItemText.scale = 0.75
            menuItemIcon.scale = 0.75
            menuItemTextDescription.opacity = 1
        }

        onExited: {
            menuItemText.anchors.horizontalCenterOffset = 0
            menuItemText.anchors.verticalCenterOffset = 0
            menuItemTextDescription.anchors.leftMargin = 0
            menuItemIcon.anchors.rightMargin = 0
            menuItemText.scale = 1
            menuItemIcon.scale = 1
            menuItemTextDescription.opacity = 0
        }
    }
}
