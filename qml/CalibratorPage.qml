import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtMultimedia 5.9

import CalibratorRepository.DataModel 1.0

Item{
    id: root

    property string source

    FileDialog {
        id: calibRepoFileDialog
        title: "Please choose a image"
        folder: shortcuts.home
        onAccepted: {
            if(plateCanvas.isImageLoaded(calibRepoFileDialog.fileUrl) === false && plateCanvas.isImageLoading(calibRepoFileDialog.fileUrl) === false)
            {
                plateCanvas.loadImage(calibRepoFileDialog.fileUrl)
            }
            plateCanvas.source = calibRepoFileDialog.fileUrl
            plateCanvas.requestPaint()
        }
    }

    onOpacityChanged: {
        if(opacity === 1.0)
        {
            if(root.source == "File")
            {
                calibRepoFileDialog.visible = true
                plateCanvas.visible = true
                videoOutput.visible = false
            }
            else if(source == "Webcam")
            {
                camera.start()
                //liveFreezButton.freez = false
                plateCanvas.visible = false
                videoOutput.visible = true
            }
        }
        if(opacity === 0.0)
        {
            if(root.source == "File")
            {
                calibRepoFileDialog.visible = false
            }
            else if(root.source == "Webcam")
            {
                camera.stop()
            }
        }
    }

    signal platePageClicked

    Rectangle{
        color: "#80000000"
        anchors.fill: parent

        Rectangle{
            width: 800
            height: 480
            radius: 20
            color: "#FFFFFF"

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            ColumnLayout{
                anchors.margins: 30

                anchors.fill: parent
                spacing: 10

                RowLayout{
                    Layout.fillWidth: true

                    spacing: 20

                    Text{
                        text: "Distance(mm): "
                        font{family: "Arial"; pixelSize: 16}
                    }

                    TextField{
                        id: distanceTextField

                        Layout.fillWidth: true

                        validator: DoubleValidator{
                            bottom: 0
                            decimals: 4
                            notation: DoubleValidator.StandardNotation
                        }

                        onFocusChanged: {
                            if(text == "This field is empty")
                            {
                                text = ""
                                color = "#000000"
                            }
                        }
                    }
                }

                RowLayout{
                    spacing: 20

                    Layout.fillWidth: true

                    Text {
                        text: "Calibrator Name: "
                        font{family: "Arial"; pixelSize: 16}
                    }

                    TextField{
                        id: calibratorName
                        Layout.fillWidth: true

                        onFocusChanged: {
                            if(text == "This field is empty")
                            {
                                text = ""
                                color = "#000000"
                            }
                        }
                    }
                }

                RowLayout{
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    spacing: 10

                    Canvas{
                        id: plateCanvas

                        property url source

                        property bool startClicked : false
                        property bool endClicked : false

                        property real point1X
                        property real point1Y

                        property real point2X
                        property real point2Y

                        property real sizeCoefficient: 0.0

                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        Layout.minimumWidth: (width / 4.0) * 3

                        Image{
                            id: plateImage

                            visible: false
                            fillMode: Image.PreserveAspectFit
                            anchors.fill: parent
                        }

                        onPaint: {
                            var ctx = getContext("2d")
                            if(source != "" && isImageLoaded(source))
                            {
                                ctx.clearRect(0, 0, width, height)
                                var offsetX = (width - plateImage.paintedWidth) / 2.0
                                var offsetY = (height - plateImage.paintedHeight) / 2.0
                                ctx.drawImage(source, offsetX, offsetY, plateImage.paintedWidth, plateImage.paintedHeight)

                                if(startClicked === true)
                                {
                                    ctx.lineWidth = 1.5
                                    ctx.strokeStyle = "#ff0000"
                                    ctx.beginPath()
                                    ctx.moveTo(point1X, point1Y)
                                    ctx.lineTo(area.mouseX, area.mouseY)
                                    ctx.stroke()
                                }

                                if(endClicked === true)
                                {
                                    ctx.lineWidth = 1.5
                                    ctx.strokeStyle = "#ff0000"
                                    ctx.beginPath()
                                    ctx.moveTo(point1X, point1Y)
                                    ctx.lineTo(point1X, point1Y)
                                    ctx.stroke()
                                }
                            }
                            else
                            {
                                ctx.clearRect(0, 0, width, height)
                            }
                        }

                        onImageLoaded: {
                            requestPaint()
                            sizeCoefficient = plateImage.sourceSize.width / plateImage.paintedWidth
                        }

                        onSourceChanged: {
                            plateImage.source = source
                        }

                        MouseArea {
                            id: area
                            anchors.fill: parent

                            hoverEnabled: true

                            onPressed: {
                                if(plateCanvas.startClicked === false)
                                {
                                    plateCanvas.point1X = mouseX
                                    plateCanvas.point1Y = mouseY
                                    plateCanvas.startClicked = true
                                }
                                else
                                {
                                    plateCanvas.point2X = mouseX
                                    plateCanvas.point2Y = mouseY
                                    plateCanvas.startClicked = false
                                    plateCanvas.endClicked = true
                                }
                            }

                            onPositionChanged: {
                                if(plateCanvas.startClicked === true)
                                {
                                    plateCanvas.requestPaint()
                                }
                            }
                       }
                    }

                    VideoOutput {
                        id : videoOutput

                        Canvas{
                            id: cameraCanvas

                            property url source

                            //z : 4

                            property bool startClicked : false
                            property bool endClicked : false

                            property real point1X
                            property real point1Y

                            property real point2X
                            property real point2Y

                            property real sizeCoefficient: 0.0

                            anchors.fill: parent

                            onPaint: {
                                    var ctx = getContext("2d")
                                    ctx.clearRect(0, 0, width, height)

                                    if(startClicked === true)
                                    {
                                        ctx.lineWidth = 1.5
                                        ctx.strokeStyle = "#ff0000"
                                        ctx.beginPath()
                                        ctx.moveTo(point1X, point1Y)
                                        ctx.lineTo(cameraArea.mouseX, cameraArea.mouseY)
                                        ctx.stroke()
                                    }

                                    if(endClicked === true)
                                    {
                                        ctx.lineWidth = 1.5
                                        ctx.strokeStyle = "#ff0000"
                                        ctx.beginPath()
                                        ctx.moveTo(point1X, point1Y)
                                        ctx.lineTo(point1X, point1Y)
                                        ctx.stroke()
                                    }
                            }

                            MouseArea {
                                id: cameraArea
                                anchors.fill: parent

                                hoverEnabled: true

                                onPressed: {
                                    if(cameraCanvas.startClicked === false)
                                    {
                                        cameraCanvas.point1X = mouseX
                                        cameraCanvas.point1Y = mouseY
                                        cameraCanvas.startClicked = true
                                    }
                                    else
                                    {
                                        cameraCanvas.point2X = mouseX
                                        cameraCanvas.point2Y = mouseY
                                        cameraCanvas.startClicked = false
                                        cameraCanvas.endClicked = true
                                    }
                                }

                                onPositionChanged: {
                                    if(cameraCanvas.startClicked === true)
                                    {
                                        cameraCanvas.requestPaint()
                                    }
                                }
                           }


                        }

                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        Layout.minimumWidth: (width / 4.0) * 3

                        source: camera

                        Camera {
                            id: camera

                            cameraState: Camera.UnloadedState
                        }
                    }

                    ColumnLayout{
                        spacing: 35

                        Layout.fillHeight: true

                        Button{
                            id: reCaptureButton
                            text: "ReCapture"
                            Layout.fillWidth: true

                            visible: root.source != "Webcam"
                        }

                        Button{
                            id: liveFreezButton
                            text: "Freez"
                            Layout.fillWidth: true

                            visible: root.source == "Webcam"

                            property bool freez: false

                            onClicked: {
                                if(freez === true)
                                {
                                    camera.start()
                                    freez = false
                                    text = "Freez"
                                }
                                else
                                {
                                    camera.stop()
                                    freez = true
                                    text = "Live"
                                }
                            }
                        }

                        Button{
                            id: okButton
                            text: "OK"
                            Layout.fillWidth: true

                            onClicked: {
                                if(distanceTextField.text.length > 0 &&
                                        calibratorName.text.length > 0 &&
                                        (plateCanvas.endClicked === true || cameraCanvas.endClicked === true) &&
                                        distanceTextField.text != "This field is empty" &&
                                        calibratorName.text != "This field is empty")
                                {
                                    var point1X = plateCanvas.point1X * plateCanvas.sizeCoefficient
                                    var point1Y = plateCanvas.point1Y * plateCanvas.sizeCoefficient
                                    var point2X = plateCanvas.point2X * plateCanvas.sizeCoefficient
                                    var point2Y = plateCanvas.point2Y * plateCanvas.sizeCoefficient
                                    var distancePixel = Math.sqrt(Math.pow(point2Y - point1Y, 2) + Math.pow(point2X - point1X, 2))
                                    var distanceMM = parseFloat(distanceTextField.text)
                                    var coefficient = distanceMM / distancePixel

                                    CalibratorRepositoryDataModel.AddCalibrator(calibratorName.text, coefficient)

                                    plateCanvas.startClicked = false
                                    plateCanvas.endClicked = false
                                    distanceTextField.clear()
                                    calibratorName.clear()
                                    plateCanvas.unloadImage(calibRepoFileDialog.fileUrl)
                                    plateCanvas.source = ""
                                    plateCanvas.requestPaint();
                                    root.platePageClicked()
                                }
                                else
                                {
                                    distanceTextField.color = "#FF0000"
                                    distanceTextField.text = "This field is empty"

                                    calibratorName.color = "#FF0000"
                                    calibratorName.text = "This field is empty"
                                }
                            }
                        }

                        Button{
                            id: cancelButton
                            text: "Cancel"
                            Layout.fillWidth: true
                            onClicked: {
                                root.platePageClicked()
                            }
                        }
                    }
                }
            }
        }
    }
}
