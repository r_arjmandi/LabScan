import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

import CircleRepository.DataModel 1.0
import CalibratorRepository.DataModel 1.0
import CurrentPatient.Wrapper 1.0

GroupBox {
    id: root

    signal captureSourcePageClicked(string source)
    signal calibratorPageClicked(string source)
    property bool marksVisibility: true

    FileDialog {
        id: imageRepoFileDialog
        title: "Please choose a image"
        folder: shortcuts.home
        onAccepted: {
            CurrentPatientWrapper.AddNewImage(imageRepoFileDialog.fileUrl);
        }
    }

    ColumnLayout{
        spacing: 10
        anchors.fill: parent

        RowLayout{
            Layout.fillWidth: true
            spacing: 10

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Source:")
                font{family: "Arial"; pixelSize: 16}
            }

            ComboBox{
                id: sourceComboBox
                Layout.fillWidth: true
                model: ["File", "Webcam", "Scanner"]
            }
        }

        RowLayout{
            Layout.fillWidth: true

            spacing: 10

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Calibrator:")
                font{family: "Arial"; pixelSize: 16}
            }

            ComboBox{
                id: calibratorComboBox
                Layout.fillWidth: true
                model: CalibratorRepositoryDataModel
                textRole: "Name"
                onCurrentIndexChanged: {
                    CalibratorRepositoryDataModel.SetCurrentCalibrator(currentIndex)
                    CircleRepositoryDataModel.ChangeCalibrator();
                }
            }

            Button{
                id: newCalbiratorButton
                width: 100
                text: "New Cal."
                onClicked: {
                    root.calibratorPageClicked(sourceComboBox.currentText)
                }
            }

            Button{
                id: deleteCalibratorButton
                width: 50
                text: "Del."
                onClicked: {
                    CalibratorRepositoryDataModel.DeleteCurrentCalibrator()
                }
            }
        }

        ImageListView {
                id: imagesListView
                Layout.fillWidth: true
                height: 64

                onCurrentIndexChanged :
                {
                    if(mainSmapleImageBox.isImageLoaded(imagesListView.currentItem.source) === false && mainSmapleImageBox.isImageLoading(imagesListView.currentItem.source) === false)
                    {
                        mainSmapleImageBox.loadImage(imagesListView.currentItem.source)
                    }
                    mainSmapleImageBox.source = imagesListView.currentItem.source
                    mainSmapleImageBox.requestPaint()
                }

                onAddButtonClicked: {
                    if(sourceComboBox.currentText == "File")
                    {
                        imageRepoFileDialog.visible = true;
                    }
                    else if(sourceComboBox.currentText == "Webcam")
                    {
                        root.captureSourcePageClicked("Webcam")
                    }
                    else if(sourceComboBox.currentText == "Scanner")
                    {
                        root.captureSourcePageClicked("Scanner")
                    }
                }
        }

        PlateImage{
            id: mainSmapleImageBox
            marksVisibility: root.marksVisibility

            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
