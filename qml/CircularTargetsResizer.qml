import QtQuick 2.9
import CircleRepository.DataModel 1.0

MouseArea{
    id: root
    hoverEnabled: true

    property bool resizeMode: false
    property bool translateMode: false
    property int selectedCircle: -1

    property real ratio
    property real offsetX
    property real offsetY
    property url imageSource
    property bool itemsVisibility

    onReleased: {
        resizeMode = false
        translateMode = false
        selectedCircle = -1
        cursorShape = Qt.ArrowCursor;
    }

    onPositionChanged: {
        var circleCount = CircleRepositoryDataModel.Size(imageSource)
        if(resizeMode && selectedCircle != -1)
        {
            var x = CircleRepositoryDataModel.GetData(selectedCircle, 1, 0)
            var y = CircleRepositoryDataModel.GetData(selectedCircle, 1, 1)
            var xTransformed = x * root.ratio + root.offsetX
            var yTransformed = y * root.ratio + root.offsetY

            var distance = Math.sqrt(Math.pow(mouseY - yTransformed, 2) + Math.pow(mouseX - xTransformed, 2))
            CircleRepositoryDataModel.SetRegionRadius(selectedCircle, distance / ratio)
            return
        }
        if(translateMode && selectedCircle != -1)
        {
            var xTransformed = (mouseX - root.offsetX) / root.ratio
            var yTransformed = (mouseY - root.offsetY) / root.ratio
            CircleRepositoryDataModel.TranslateCircularTarget(selectedCircle, xTransformed, yTransformed)
            return
        }
        if(circleCount > 0 && root.itemsVisibility)
        {
            for(var i = 0 ; i < circleCount ; i++)
            {
                var x = CircleRepositoryDataModel.GetData(i, 0, 0)
                var y = CircleRepositoryDataModel.GetData(i, 0, 1)
                var radius = CircleRepositoryDataModel.GetData(i, 1, 2)
                var xTransformed = x * root.ratio + root.offsetX
                var yTransformed = y * root.ratio + root.offsetY
                var radiusTransformed = radius * ratio

                var distance = Math.sqrt(Math.pow(mouseY - yTransformed, 2) + Math.pow(mouseX - xTransformed, 2))
                if(distance < 5)
                {
                    cursorShape = Qt.SizeAllCursor;
                    if(containsPress)
                    {
                        translateMode = true
                        selectedCircle = i
                    }

                    break;
                }
                else if(Math.abs(distance - radiusTransformed) < 3)
                {
                    cursorShape = Qt.SizeBDiagCursor;
                    if(containsPress)
                    {
                        resizeMode = true
                        selectedCircle = i
                    }

                    break;
                }
                else
                {
                    cursorShape = Qt.ArrowCursor;
                }
            }
        }
    }
}
