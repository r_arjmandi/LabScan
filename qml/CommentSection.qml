import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import GeneralCommentRepository.DataModel 1.0
import CurrentPatient.Wrapper 1.0

GroupBox {
    id: root

    signal setCommentSubjectPageClicked(string comment)

    ColumnLayout{
        spacing: 10
        anchors.fill: parent

        RowLayout{
            Layout.fillWidth: true
            spacing: 10

            StyledLabel {
                text: qsTr("General Comment:")
                Layout.alignment: Qt.AlignCenter
            }

            ComboBox{
                id: generalCommentComboBox
                Layout.fillWidth: true
                model: GeneralCommentRepositoryDataModel
                textRole: qsTr("Subject")
                currentIndex: CurrentPatientWrapper.commentSubjectIndex
                onCurrentIndexChanged: {
                    CurrentPatientWrapper.commentSubjectIndex = currentIndex
                }
                onCurrentTextChanged: {
                    CurrentPatientWrapper.commentDescription = GeneralCommentRepositoryDataModel.GetDescription(generalCommentComboBox.currentText)
                }
            }

            Button{
                id: deleteShapeButton
                Layout.maximumHeight: parent.height
                Layout.maximumWidth: Layout.maximumHeight
                Image{
                    source: "qrc:/images/delete.png"
                    anchors.margins: 5
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectFit
                    mipmap: true
                }

                onClicked: {
                    GeneralCommentRepositoryDataModel.Delete(generalCommentComboBox.currentText)
                }
            }
        }

        TextArea{
            id: commentTextArea
            placeholderText: qsTr("Click here for enter description")
            Layout.fillWidth: true
            Layout.fillHeight: true
            selectByMouse: true
            text: CurrentPatientWrapper.commentDescription
            onTextChanged: {
                CurrentPatientWrapper.commentDescription = text;
            }
        }

        Button{
            id: saveAsButton
            Layout.fillWidth: true
            text: qsTr("Save as new Pre-defiend Comment")

            onClicked: {
                if(commentTextArea.text != "")
                {
                    root.setCommentSubjectPageClicked(commentTextArea.text)
                }
            }
        }
    }
}
