import QtQuick 2.9
import QtQuick.Controls 2.2
import CurrentPatient.Wrapper 1.0

import CurrentPatientImageRepository.DataModel 1.0

ListView {
        id: root

        signal addButtonClicked;

        orientation: ListView.Horizontal

        clip: true

        spacing: 10

        flickableDirection: Flickable.HorizontalFlick

        currentIndex : CurrentPatientWrapper.currentImageIndex

        footer: Image{
            source: "qrc:/images/add.png"

            fillMode: Image.PreserveAspectFit

            MouseArea{
                id: footerImageMouseArea
                anchors.fill: parent
                onClicked: {
                    root.addButtonClicked();
                }
            }

            mipmap: true

            width: 64
            height: 64
        }

        model: CurrentPatientImageRepositoryDataModel

        delegate: Image {

            id: delegate

            source: {
                if(modelData)
                {
                    modelData
                }
            }

            width: 64
            height: 64

            mipmap: true

            fillMode: Image.PreserveAspectFit

            MouseArea{
                id: imageMouseArea
                anchors.fill: parent
                onClicked:
                {
                    CurrentPatientWrapper.SetCurrentImage(index)
                }
            }
        }

        highlight: Rectangle{

                width: 64; height: 64
                z : 2

                color: "transparent"

                border.width: 2
                border.color: "yellow";

                y: root.currentItem.y
        }

        ScrollIndicator.horizontal: ScrollIndicator { }
        ScrollIndicator.vertical: ScrollIndicator { }
}
