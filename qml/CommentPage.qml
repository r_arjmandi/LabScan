import QtQuick 2.9
import QtQuick.Controls 2.2

Item{
    id: root

    signal platePageClicked

    Rectangle{
        color: "#80000000"
        anchors.fill: parent

        MouseArea{
            id: mouseArea
            anchors.fill: parent
            enabled: true

            onClicked: root.platePageClicked()
        }

        Rectangle{
            width: 400
            height: 450
            radius: 20
            color: "#FFFFFF"

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            Column{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 30
                anchors.fill: parent

                spacing: 10

                Row{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    spacing: 10

                    ComboBox{
                        id: titleComboBox
                        width: parent.width - deleteTitleButton.width - 10
                        model: ["title", "title1", "title2", "title3"]
                    }

                    Button{
                        id: deleteTitleButton

                        height: titleComboBox.height
                        width: height

                        Image{
                            source: "../images/delete.png"
                            anchors.margins: 5
                            anchors.fill: parent
                            fillMode: Image.PreserveAspectFit
                            mipmap: true
                        }
                    }

                }

                TextArea{
                    id: commentTextArea
                    anchors.left: parent.left
                    anchors.right: parent.right
                    placeholderText: "Please click here for enter comment"
                    height: 220
                }

                Button{
                    id: saveAsNewPredefinedPlateCommentButton
                    text: "Save as new pre-defined plate comment"
                    anchors.left: parent.left
                    anchors.right: parent.right
                }

                Row{
                    spacing: 20

                    anchors.left: parent.left
                    anchors.right: parent.right

                    Button{
                        id: cancelButton
                        text: "Cancel"

                        width: parent.width / 2 - 10
                    }

                    Button{
                        id: okButton
                        text: "OK"

                        width: parent.width / 2 - 10
                    }
                }
            }
        }
    }
}
