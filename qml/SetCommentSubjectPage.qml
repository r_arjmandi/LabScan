import QtQuick 2.9
import QtQuick.Controls 2.2

import GeneralCommentRepository.DataModel 1.0

Item{
    id: root

    property string comment
    signal closeCommentSubjectPage

    Rectangle{
        color: "#80000000"
        anchors.fill: parent

        MouseArea{
            id: mouseArea
            anchors.fill: parent
            enabled: true

            onClicked: root.closeCommentSubjectPage()
        }

        Rectangle{
            width: 400
            height: 200
            radius: 20
            color: "#FFFFFF"

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            Column{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter

                width: 300

                spacing: 10

                TextField{
                    id: subjectField
                    anchors.left: parent.left
                    anchors.right: parent.right
                    placeholderText: "Comment subject"
                    focus: true
                }

                Button{
                    id: saveButton
                    anchors.left: parent.left
                    anchors.right: parent.right
                    text: "Save"

                    onClicked: {
                        if(subjectField.text != "")
                        {
                            GeneralCommentRepositoryDataModel.AddNewComment(subjectField.text, root.comment)
                            subjectField.clear()
                            root.closeCommentSubjectPage()
                        }
                    }
                }
            }
        }
    }
}
