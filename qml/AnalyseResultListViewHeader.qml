import QtQuick 2.9
import QtQuick.Controls 2.2

Row{
    id: root
    spacing: 1

    function itemAt(index) {
        switch(index)
        {
        case(0):
            return diskHeaderLabel;
        case(1):
            return antibuticHeaderLabel;
        case(2):
            return zoneHeaderLabel;
        case(3):
            return minMaxHeaderLabel;
        case(4):
            return sirHeaderLabel;
        case(5):
            return reportHeaderLabel;
        }
    }

    Label {
        id : diskHeaderLabel
        text: "Disk"
        font{family: "Arial"; pixelSize: 16; bold: true}
        topPadding: 10
        bottomPadding: 10
        anchors.verticalCenter: parent.verticalCenter
        background: Rectangle { color: "silver" }
    }

    Label {
        id : antibuticHeaderLabel
        text: "Antibiotic"
        font{family: "Arial"; pixelSize: 16; bold: true}
        topPadding: 10
        bottomPadding: 10
        width: parent.width - diskHeaderLabel.width - zoneHeaderLabel.width - minMaxHeaderLabel.width - sirHeaderLabel.width - reportHeaderLabel.width - 6
        leftPadding: {width/2.0 - 60}
        background: Rectangle { color: "silver" }
    }

    Label {
        id : zoneHeaderLabel
        text: "Zone"
        font{family: "Arial"; pixelSize: 16; bold: true}
        topPadding: 10
        bottomPadding: 10
        background: Rectangle { color: "silver" }
    }

    Label {
        id : minMaxHeaderLabel
        text: "Min/Max"
        font{family: "Arial"; pixelSize: 16; bold: true}
        topPadding: 10
        bottomPadding: 10
        background: Rectangle { color: "silver" }
    }

    Label {
        id : sirHeaderLabel
        text: "S/I/R"
        font{family: "Arial"; pixelSize: 16; bold: true}
        topPadding: 10
        bottomPadding: 10
        background: Rectangle { color: "silver" }
    }

    Label {
        id : reportHeaderLabel
        text: "Report"
        font{family: "Arial"; pixelSize: 16; bold: true}
        topPadding: 10
        bottomPadding: 10
        background: Rectangle { color: "silver" }
    }
}
