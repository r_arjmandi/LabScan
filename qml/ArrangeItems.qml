import QtQuick 2.9
import QtQuick.Controls 2.2
import BackEnd.Wrapper 1.0

Row
{
    id: root

    anchors.left: parent.left
    anchors.right: parent.right

    property int plateArrange
    property int mode

    onModeChanged: {
        if(mode === 1)
        {
            item2Image.source = "../images/circleIcon4.png";
            item3Image.source = "../images/circleIcon3.png";
            item4Image.source = "../images/circleIcon2.png";
            item5Image.source = "../images/circleIcon1.png";
        }
        else if(mode === 2)
        {
            item2Image.source = "../images/rectangleIcon4.png";
            item3Image.source = "../images/rectangleIcon3.png";
            item4Image.source = "../images/rectangleIcon2.png";
            item5Image.source = "../images/rectangleIcon1.png";
        }

    }

    Text{
        text: "Arrange: "
        anchors.top: parent.top
        anchors.topMargin: 15
        font{family: "Arial"; pixelSize: 16}
    }

    RadioButton{
        id: item1
        checked: true

        onCheckedChanged: {
            if(checked === true)
            {
                parent.plateArrange = 1;
                if(root.mode === 1)
                {
                    BackEndWrapper.ShuffleImageCircles();
                }
            }
        }
    }

    Image{
        id: item1Image

        source : "../images/circleIcon5.png"
        anchors.top: parent.top
        anchors.topMargin: 5
        mipmap: true
    }

    RadioButton{
        id: item2

        onCheckedChanged: {
            if(checked === true)
            {
                parent.plateArrange = 2;
                if(root.mode === 1)
                {
                    BackEndWrapper.SortCurrentImageCirclesClockwise();
                }
            }
        }
    }

    Image{
        id: item2Image

        anchors.top: parent.top
        anchors.topMargin: 5
        mipmap: true
    }

    RadioButton{
        id: item3

        onCheckedChanged: {
            if(checked === true)
            {
                parent.plateArrange = 3;
                if(root.mode === 1)
                {
                    BackEndWrapper.SortCurrentImageCirclesCounterClockwise();
                }
            }
        }
    }

    Image{
        id: item3Image

        anchors.top: parent.top
        anchors.topMargin: 5
        mipmap: true
    }

    RadioButton{
        id: item4

        onCheckedChanged: {
            if(checked === true)
            {
                parent.plateArrange = 4;
                if(root.mode === 1)
                {
                    BackEndWrapper.SortCurrentImageCirclesCenterClockwise();
                }
            }
        }
    }

    Image{
        id: item4Image

        anchors.top: parent.top
        anchors.topMargin: 5
        mipmap: true
    }

    RadioButton{
        id: item5

        onCheckedChanged: {
            if(checked === true)
            {
                parent.plateArrange = 5;
                if(root.mode === 1)
                {
                    BackEndWrapper.SortCurrentImageCirclesCenterCounterClockwise();
                }
            }
        }
    }

    Image{
        id: item5Image

        anchors.top: parent.top
        anchors.topMargin: 5
        mipmap: true
    }
}
