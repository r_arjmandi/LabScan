import QtQuick 2.9
import QtQuick.Controls 2.2

Item{
    id: root

    signal settingPageClicked
    signal closePasswordPage

    Rectangle{
        color: "#80000000"
        anchors.fill: parent

        MouseArea{
            id: mouseArea
            anchors.fill: parent
            enabled: true

            onClicked: root.closePasswordPage()
        }

        Rectangle{
            width: 400
            height: 200
            radius: 20
            color: "#FFFFFF"

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            Column{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter

                width: 300

                spacing: 10

                TextField{
                    id: passwordField
                    anchors.left: parent.left
                    anchors.right: parent.right
                    placeholderText: "Password"
                    echoMode: TextInput.Password
                    focus: true
                }

                Button{
                    id: okButton
                    anchors.left: parent.left
                    anchors.right: parent.right
                    text: "OK"

                    onClicked: {
                        if(passwordField.text == "admin")
                        {
                            passwordField.text = ""
                            root.settingPageClicked()
                        }
                        else
                        {
                            passwordField.text = ""
                            passwordField.placeholderText = "wrong password!"
                        }
                    }
                }
            }
        }
    }
}
