
import QtQuick 2.9
import QtQuick.Window 2.3
import BackEnd.Wrapper 1.0

Window {
    id: root
    visible: true

    property real sideBarWidth: root.width / 4.553
    title: qsTr("Analys")

    visibility: "Maximized"

    Sidebar
    {
        id: sidebar
        width: sideBarWidth
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        states: [
                State {
                    name: "patientPage"
                    PropertyChanges { target: patientPage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: archivePage; opacity: 0; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 0; enabled: false}
                    PropertyChanges { target: platePage; opacity: 0; enabled: false}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 0; enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "ArchivePage"
                    PropertyChanges { target: patientPage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: settingPage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: platePage; opacity: 0; enabled: false}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 0; enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "settingPage"
                    PropertyChanges { target: patientPage; opacity: 0; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: platePage; opacity: 0; enabled: false}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 0; enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "platePage"
                    PropertyChanges { target: patientPage; opacity: 0; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 0; enabled: false}
                    PropertyChanges { target: platePage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 0; enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "passwordPage"
                    PropertyChanges { target: patientPage;  opacity: 0; enabled: false}
                    PropertyChanges { target: archivePage ;  opacity: 0; enabled: false}
                    PropertyChanges { target: settingPage;  opacity: 0; enabled: false}
                    PropertyChanges { target: platePage;  opacity: 0; enabled: false}
                    PropertyChanges { target: passwordPage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: commentPage;  opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 0; enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "commentPage"
                    PropertyChanges { target: patientPage; opacity: 0; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 0; enabled: false}
                    PropertyChanges { target: platePage; opacity: 1; z:1; enabled: true}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: captureSourcePage; opacity: 0; enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "captureSourcePage"
                    PropertyChanges { target: patientPage; opacity: 0; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 0; enabled: false}
                    PropertyChanges { target: platePage; opacity: 1; z:1; enabled: true}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "dateTimePickerPage"
                    PropertyChanges { target: patientPage; opacity: 0; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 1 ; z:1; enabled: true}
                    PropertyChanges { target: settingPage; opacity: 0; enabled: false}
                    PropertyChanges { target: platePage; opacity: 0; enabled: false}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 0; enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 1; z:2; enabled: true}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "calibratorPage"
                    PropertyChanges { target: patientPage; opacity: 0; enabled: false}
                    PropertyChanges { target: archivePage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 0; enabled: false}
                    PropertyChanges { target: platePage; opacity: 1; z:1; enabled: true}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 0;  enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 1; z: 2 ; enabled: true}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 0; enabled: false}
                },
                State {
                    name: "setCommentSubjectPage"
                    PropertyChanges { target: patientPage; opacity: 1; z: 1; enabled: true}
                    PropertyChanges { target: archivePage; opacity: 0 ; enabled: false}
                    PropertyChanges { target: settingPage; opacity: 0; enabled: false}
                    PropertyChanges { target: platePage; opacity: 0; enabled: false}
                    PropertyChanges { target: passwordPage; opacity: 0; enabled: false}
                    PropertyChanges { target: commentPage; opacity: 0; enabled: false}
                    PropertyChanges { target: captureSourcePage; opacity: 0;  enabled: false}
                    PropertyChanges { target: dateTimePickerPage; opacity: 0; enabled: false}
                    PropertyChanges { target: calibratorPage; opacity: 0; enabled: false}
                    PropertyChanges { target: setCommentSubjectPage; opacity: 1; z: 2 ; enabled: true}
                }
            ]

        transitions: [
                Transition {
                    from: "*"; to: "*"
                    NumberAnimation { target: platePage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: patientPage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: archivePage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: settingPage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: passwordPage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: commentPage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: captureSourcePage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: dateTimePickerPage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: calibratorPage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                    NumberAnimation { target: setCommentSubjectPage; easing.type: Easing.OutExpo; properties: "opacity"; duration: 2000 }
                }
            ]

        state: "patientPage"

        property string latestState

        onPatientClicked: { sidebar.state = "patientPage" }
        onArchiveClicked: { sidebar.state = "ArchivePage" }
        onSettingClicked: {
            latestState = sidebar.state
            sidebar.state = "passwordPage"
        }
    }

    PatientPage{
        id: patientPage
        visible: true
        enabled: false

        width: parent.width - sideBarWidth
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        onPlateClicked: {sidebar.state = "platePage"}
        onSetCommentSubjectPageClicked: {
            setCommentSubjectPage.comment = comment
            sidebar.state = "setCommentSubjectPage"
        }
    }

    ArchivePage{
        id: archivePage
        visible: true
        enabled: false
        width: parent.width - sideBarWidth
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        selectedDateTime: dateTimePickerPage.selectedDateTime

        onDateTimePickerPageClicked: { sidebar.state = "dateTimePickerPage" }

        onPatientPageClicked: { sidebar.state = "patientPage" }
    }

    SettingPage{
        id: settingPage
        visible: true
        enabled: false

        width: parent.width - sideBarWidth
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
    }

    PlatePage{
        id: platePage
        visible: true
        enabled: false

        width: parent.width - sideBarWidth
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        onPatientPageClicked: { sidebar.state = "patientPage" }
        onCommentPageClicked: { sidebar.state = "commentPage" }
        onCaptureSourcePageClicked: {
            sidebar.state = "captureSourcePage"
            captureSourcePage.source = source
        }
        onCalibratorPageClicked: {
            sidebar.state = "calibratorPage"
            calibratorPage.source = source}
    }

    PasswordPage{
        id: passwordPage
        visible: true
        enabled: false

        width: parent.width
        anchors.fill: parent

        onSettingPageClicked: {sidebar.state = "settingPage"}
        onClosePasswordPage: {sidebar.state = sidebar.latestState}
    }

    CommentPage{
        id: commentPage
        visible: true
        enabled: false

        width: parent.width
        anchors.fill: parent

        onPlatePageClicked: {sidebar.state = "platePage"}
    }

    CaptureSourcePage{
        id: captureSourcePage
        visible: true
        enabled: false

        width: parent.width
        anchors.fill: parent

        onPlatePageClicked: {sidebar.state = "platePage"}
    }

    DateTimePickerPage{
        id: dateTimePickerPage
        visible: true
        enabled: false

        width: parent.width
        anchors.fill: parent

        onCloseDateTimePickerPage: {
            sidebar.state = "ArchivePage";
            archivePage.updateSelectedDateTime();
        }
    }

    CalibratorPage{
        id: calibratorPage
        visible: true
        enabled: false

        width: parent.width
        anchors.fill: parent

        onPlatePageClicked: {sidebar.state = "platePage"}
    }

    SetCommentSubjectPage{
        id: setCommentSubjectPage
        visible: true
        enabled: false

        width: parent.width
        anchors.fill: parent

        onCloseCommentSubjectPage: {sidebar.state = "patientPage"}
    }
}
