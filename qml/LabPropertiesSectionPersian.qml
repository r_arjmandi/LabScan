import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    id: root

    ColumnLayout{
        spacing: 10
        anchors.fill: parent

        RowLayout{
            id: row1
            Layout.fillWidth: true
            anchors.top: parent.top

            spacing: 20

            TextField{
                id: labNameField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("نام:")
                font{family: "Arial"; pixelSize: 16}
            }
        }

        RowLayout{
            id: row2
            Layout.fillWidth: true
            spacing: 20
            anchors.top: row1.bottom

            TextField{
                id: labAddressField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("آدرس:")
                font{family: "Arial"; pixelSize: 16}
            }
        }

        RowLayout{
            id: row3
            Layout.fillWidth: true
            spacing: 20
            anchors.top: row2.bottom

            TextField{
                id: labTelField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("تلفن:")
                font{family: "Arial"; pixelSize: 16}
            }
        }

        RowLayout{
            id: row4
            spacing: 20
            Layout.fillWidth: true
            anchors.top: row3.bottom

            TextField{
                id: labFaxField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("فکس:")
                font{family: "Arial"; pixelSize: 16}
            }
        }

        RowLayout{
            id: row5
            spacing: 20
            Layout.fillWidth: true
            anchors.top: row4.bottom

            TextField{
                id: labEmailField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("رایانامه:")
                font{family: "Arial"; pixelSize: 16}
            }
        }

        RowLayout{
            id: row6
            spacing: 10
            anchors.top: row5.bottom

            Layout.fillWidth: true

            Button{
                id: browseButton
                text: "جستجو..."
            }

            TextField{
                id: labLogoField
                Layout.fillWidth: true
                font{family: "Arial"; pixelSize: 16}
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("نماد:")
                font{family: "Arial"; pixelSize: 16}
            }
        }
    }
}
