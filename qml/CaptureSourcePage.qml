import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtMultimedia 5.9

import CurrentPatient.Wrapper 1.0
import Scanner.Device 1.0

Item{
    id: root

    property string source

    onOpacityChanged: {
        if(opacity === 1 && source == "Webcam")
        {
            cameraOutput.visible = true
            scannerOutput.visible = false
            camera.start()
            liveFreezButton.freez = false
        }
        else if(opacity === 0 && source == "Webcam")
        {
            camera.stop()
            cameraOutput.visible = false
        }
        else if(opacity === 1 && source == "Scanner")
        {
            scannerOutput.visible = true
            cameraOutput.visible = false
        }
        else if(opacity === 0 && source == "Scanner")
        {
            scannerOutput.visible = false
            scannerOutput.source = ""
        }
    }

    signal platePageClicked

    Rectangle{
        color: "#80000000"
        anchors.fill: parent

        MouseArea{
            id: mouseArea
            anchors.fill: parent
            enabled: true

            onClicked: root.platePageClicked()
        }

        Rectangle{
            width: 800
            height: 480
            radius: 20
            color: "#FFFFFF"

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            ColumnLayout{
                anchors.leftMargin: 10
                anchors.rightMargin: 30
                anchors.topMargin: 30
                anchors.bottomMargin: 30

                anchors.fill: parent
                spacing: 10

                RowLayout{
                    spacing: 20
                    Layout.fillWidth: true
                    Layout.leftMargin: 20

                    RadioButton{
                        id: singlePlateRadioButton
                        text: "Single Plate"
                    }

                    RadioButton{
                        id: doublePlateRadioButton
                        text: "Double Plate"
                    }
                }

                RowLayout{
                    spacing: 57
                    Layout.fillWidth: true
                    Layout.leftMargin: 20

                    RadioButton{
                        id: circleRadioButton
                        text: "Circle"
                    }

                    RadioButton{
                        id: rectangleRadioButton
                        text: "Rectangle"
                    }
                }

                RowLayout{
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignRight

                    VideoOutput {
                        id: cameraOutput
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        source: camera

                        Camera {
                            id: camera

                            cameraState: Camera.UnloadedState
                        }
                    }

                    Image{
                        id: scannerOutput

                        fillMode: Image.PreserveAspectFit

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }

                    ColumnLayout{
                        spacing: 35

                        Layout.maximumWidth: 120
                        Layout.minimumWidth: 120
                        Layout.alignment: Qt.AlignRight

                        Button{
                            id: reCaptureButton
                            text: "ReCapture"
                            Layout.fillWidth: true

                            onClicked: {
                                if(root.source == "Webcam")
                                {
                                    camera.imageCapture.capture();
                                }
                                else if(root.source == "Scanner")
                                {
                                    scannerOutput.source = ""
                                    var address = ScannerDevice.StartScan()
                                    scannerOutput.source = address
                                    CurrentPatientImageRepositoryDataModel.AddNewImage(address)
                                }
                            }
                        }

                        Button{
                            id: liveFreezButton
                            text: "Live/Freez"

                            property bool freez: false

                            Layout.fillWidth: true

                            onClicked: {
                                if(freez === true)
                                {
                                    camera.start()
                                    freez = false
                                }
                                else
                                {
                                    camera.stop()
                                    freez = true
                                }
                            }
                        }

                        Button{
                            id: okButton
                            text: "OK"
                            Layout.fillWidth: true

                            onClicked: {

                            }
                        }

                        Connections {
                                target: camera.imageCapture
                                onImageSaved: {
                                    CurrentPatientWrapper.AddNewImage("file:///" + path)
                                }
                        }
                    }
                }
            }
        }
    }
}
