import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQml 2.2

Item{
    id: root

    signal closeDateTimePickerPage
    property string selectedDateTime:
    {
        calendar.todayYear + "-" + (calendar.todayMonth < 10 ? "0" + calendar.todayMonth : calendar.todayMonth) + "-" + (calendar.todayDay < 10 ? "0" + calendar.todayDay : calendar.todayDay);
    }

    Rectangle{
        color: "#80000000"
        anchors.fill: parent

        MouseArea{
            id: mouseArea
            anchors.fill: parent
            enabled: true

            onClicked: root.closeDateTimePickerPage()
        }

        PersianCalendar{
            id: calendar
            width: 400
            height: 300
            radius: 10
            anchors.centerIn: parent
        }

   }
}
