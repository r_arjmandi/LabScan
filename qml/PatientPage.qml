import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import CurrentPatient.Wrapper 1.0
import StandardRepository.DataModel 1.0
import PatientRepository.DataModel 1.0
import BackEnd.Wrapper 1.0

Item{
    id: root

    signal plateClicked
    signal setCommentSubjectPageClicked(string comment);

    Rectangle{

        anchors.fill: parent
        color: "#e9f2ff"

        RowLayout{
            spacing: 20
            anchors.fill: parent

            anchors.leftMargin: 20
            anchors.rightMargin: 20
            anchors.topMargin: 5
            anchors.bottomMargin: 5

            ColumnLayout{
                id: column1

                Layout.fillHeight: true
                Layout.minimumWidth: parent.width * 0.5 - 20

                PatientProperties{
                    id: patientProperties
                    title: qsTr("Patient")

                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }

                OperatorProperties{
                    id: operatorProperties

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }

                SampleProperties{
                    id: sampleProperties
                    title: qsTr("Sample")

                    Layout.fillHeight: true
                    Layout.fillWidth: true
                }

                RowLayout{

                    spacing: 10

                    Layout.fillWidth: true

                    Button{
                        id: clearForm
                        text: qsTr("Clear Form")

                        Layout.fillWidth: true

                        onClicked: {
                            CurrentPatientWrapper.Clear()
                        }
                    }

                    Button{
                        id: save
                        text: qsTr("Save Patient")
                        Layout.minimumWidth: parent.width / 3;
                        onClicked: {
                            BackEndWrapper.SaveCurrentPatient();
                        }
                    }
                }
            }

            ColumnLayout{
                id: column2

                Layout.fillHeight: true
                Layout.minimumWidth: parent.width * 0.5 - 20

                CommentSection{
                    id: commentSection
                    title: qsTr("Comment")

                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    onSetCommentSubjectPageClicked: {
                        root.setCommentSubjectPageClicked(comment)
                    }
                }

                Button{
                    id: goPlatePage
                    text: qsTr("Plate")

                    Layout.fillWidth: true

                    enabled: {
                        CurrentPatientWrapper.id.length
                    }

                    Image{
                        source: "qrc:/images/nextPage.png"
                        anchors.right: parent.right
                        height: parent.height
                        width: height
                    }

                    onClicked:{
                        plateClicked();
                    }
                }
            }
        }
    }
}
