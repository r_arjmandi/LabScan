import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import CurrentPatient.Wrapper 1.0

RowLayout
{
    id: root

    property int isolate : CurrentPatientWrapper.sampleIsolated

    onIsolateChanged: {
        if(isolate === 1)
        {
            yesRadioButton.checked = true
            noRadioButton.checked = false
        }
        if(isolate === -1)
        {
            yesRadioButton.checked = false
            noRadioButton.checked = true
        }
    }

    RadioButton{
        id: yesRadioButton

        text: qsTr("Yes")
        checked: true

        onCheckedChanged: {
            if(checked === true)
            {
                CurrentPatientWrapper.sampleIsolated = 1;
            }
        }
    }

    RadioButton{
        id: noRadioButton
        text: qsTr("No")

        onCheckedChanged: {
            if(checked === true)
            {
                CurrentPatientWrapper.sampleIsolated = -1;
            }
        }
    }
}
