import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    id: root

    signal loadClicked;

    ColumnLayout{
        spacing: 20
        anchors.fill: parent

        Button{
            id: saveABackupButton
            Layout.fillWidth: true
            anchors.top: parent.top
            text: "Save A BackUp"
        }

        Button{
            id: searchButton
            Layout.fillWidth: true
            anchors.top: saveABackupButton.bottom
            text: "Search"
        }

        Button{
            id: deleteRecordButton
            Layout.fillWidth: true
            anchors.top: searchButton.bottom
            text: "Delete Record(s)"
        }

        Button{
            id: loadButton
            Layout.fillWidth: true
            anchors.top: deleteRecordButton.bottom
            text: "Load"

            onClicked: {
                root.loadClicked();
            }
        }

    }
}
