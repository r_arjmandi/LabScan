import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: root

    property real propsHeight

    Rectangle{

        color: "#e9f2ff"
        anchors.fill: parent

        RowLayout{
            spacing: 20
            anchors.fill: parent
            anchors.margins: 20

            LabPropertiesSection{
                id: labPropertiesSection
                Layout.fillHeight: true
                Layout.minimumWidth: parent.width / 2 - 20
                title: "Lab"
            }

            LabPropertiesSectionPersian{
                id: labPropertiesSectionPersian

                Layout.fillHeight: true
                Layout.minimumWidth: parent.width / 2 - 20

                title: "آزمایشگاه"
            }
        }
    }
}
