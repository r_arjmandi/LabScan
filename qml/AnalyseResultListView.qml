import QtQuick 2.9
import QtQuick.Controls 2.2

import CircleRepository.DataModel 1.0
import AntibuticRepository.DataModel 1.0

ListView {
        id: root

        clip: true

        cacheBuffer: 500

        contentWidth: width

        flickableDirection: Flickable.VerticalFlick

        headerPositioning: ListView.OverlayHeader

        header: AnalyseResultListViewHeader{
            anchors.left: parent.left
            anchors.right: parent.right
            z : 2
        }

        model: CircleRepositoryDataModel
        delegate: Column{
            id: delegate

            property int row: index
            anchors.left: parent.left
            anchors.right: parent.right

            Row{
                spacing: 1

                anchors.left: parent.left
                anchors.right: parent.right

                Text{
                    leftPadding: 16
                    text: (delegate.row + 1).toString()
                    font{family: "Arial"; pixelSize: 14}
                    anchors.verticalCenter: parent.verticalCenter
                    width: root.headerItem.itemAt(0).width
                }

                ComboBox{
                    model: AntibuticRepositoryDataModel
                    textRole: "Name"
                    anchors.verticalCenter: parent.verticalCenter
                    width: resultsListView.headerItem.itemAt(1).width

                    currentIndex: {
                        var lastAntibutic = CircleRepositoryDataModel.GetAntibutic(delegate.row);
                        if(lastAntibutic === "")
                        {
                            currentIndex = -1;
                            return;
                        }

                        for(var i = 0; i<AntibuticRepositoryDataModel.rowCount(); i++)
                        {
                            if(AntibuticRepositoryDataModel.GetData(i) === lastAntibutic)
                            {
                                currentIndex = i;
                                return;
                            }
                        }
                        currentIndex = -1;
                    }

                    onCurrentIndexChanged: {
                        if(currentIndex != -1)
                        {
                            CircleRepositoryDataModel.SetAntibutic(delegate.row, currentIndex)
                        }
                    }
                }

                Text{
                    leftPadding: 6
                    text: Radius
                    font{family: "Arial"; pixelSize: 12}
                    anchors.verticalCenter: parent.verticalCenter
                    width: resultsListView.headerItem.itemAt(2).width
                }

                Text{
                    leftPadding: 12
                    text: MinMax
                    font{family: "Arial"; pixelSize: 14}
                    anchors.verticalCenter: parent.verticalCenter
                    width: resultsListView.headerItem.itemAt(3).width
                }

                Text{
                    leftPadding: 12
                    text: SIR
                    font{family: "Arial"; pixelSize: 14}
                    anchors.verticalCenter: parent.verticalCenter
                    width: resultsListView.headerItem.itemAt(4).width
                }

                CheckBox{
                    anchors.verticalCenter: parent.verticalCenter
                    width: resultsListView.headerItem.itemAt(5).width
                    checked: Report

                    onCheckedChanged: {
                        CircleRepositoryDataModel.SetReport(delegate.row, checked)
                    }
                }
            }

            Rectangle {
                color: "silver"
                width: parent.width
                height: 1
            }
        }


        ScrollIndicator.horizontal: ScrollIndicator { }
        ScrollIndicator.vertical: ScrollIndicator { }
}
