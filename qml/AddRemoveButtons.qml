import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

RowLayout{
    id: root
    spacing: 10

    signal addClicked;
    signal deleteClicked;

    Button{
        id: addShapeButton
        Layout.maximumHeight: parent.height
        Layout.maximumWidth: Layout.maximumHeight

        Image{
            source: "qrc:/images/add.png"
            anchors.margins: 5
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            mipmap: true
        }

        onClicked: {root.addClicked()}
    }

    Button{
        id: deleteShapeButton
        Layout.maximumHeight: parent.height
        Layout.maximumWidth: Layout.maximumHeight
        Image{
            source: "qrc:/images/delete.png"
            anchors.margins: 5
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            mipmap: true
        }
        onClicked: {root.deleteClicked()}
    }
}
