import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0

Rectangle {
    id: root

    color: "#FFFFFFFF"

    DateCalculations{
        id:calc
    }


    property var todayDate: calc.today()
    property int todayYear: todayDate[0]
    property int todayMonth: todayDate[1]
    property int todayDay: todayDate[2]
    property var thisMonth: calc.monthName(todayDate[1])
    property int daysInMonth:calc.dayInMonth(todayYear,todayMonth)
    property var firstDayOfMonth: calc.dayNumber(todayYear,todayMonth,1)
    property var todayNumber: calc.dayNumber(todayYear,todayMonth,todayDay)
    property var todayName: calc.dayOfWeek(todayYear,todayMonth,todayDay)

    function nextMonth(){
        if(todayMonth==12)
            todayDate=[todayYear+1,1,1];
        else
            todayDate=[todayYear,todayMonth+1,1];
        fillCells();
        selectedCellChanged(todayDay)
    }

    function previousMonth(){
        if(todayMonth==1)
            todayDate=[todayYear-1,12,1];
        else
            todayDate=[todayYear,todayMonth-1,1];
        fillCells();
        selectedCellChanged(todayDay)
    }

    function fillCells()
    {
        var i ;
        var j ;
        var counter=1;

        for(i=0;i<5;i++)
        {
            if(counter<=daysInMonth)
            {
                for (j=0;j<7;j++)
                {
                    repeater.itemAt(i*7+j).children[0].text= " ";
                    if((j===firstDayOfMonth&&counter==1) || (counter>1&&counter<=daysInMonth))
                    {
                        repeater.itemAt(i*7+j).children[0].text=counter;
                        counter++;
                    }
                }
            }
        }
    }

    function selectedCellChanged(x){
            var j;
            var i;

            for(i=0;i<5;i++)
            {
                for(j=0;j<7;j++)
                {
                    if(repeater.itemAt(i*7+j).children[0].text == " ")
                    {
                        repeater.itemAt(i*7+j).border.color = "#FFFFFFFF"
                        repeater.itemAt(i*7+j).border.width = 1
                    }
                    else if(repeater.itemAt(i*7+j).children[0].text == x)
                    {
                        repeater.itemAt(i*7+j).border.color = "#FF0000FF"
                        repeater.itemAt(i*7+j).border.width = 1
                    }
                    else
                    {
                        repeater.itemAt(i*7+j).border.color = "#FFFFFFFF"
                        repeater.itemAt(i*7+j).border.width = 1
                    }
                    todayDate=[todayYear,todayMonth,x]
                }
            }
    }

    ColumnLayout{
        id : calendarLayout
        spacing: 10
        anchors.fill: parent

        Text {
            id: yearTextId
            Layout.alignment: Qt.AlignHCenter
            text: todayName + " " + todayDay + " " + thisMonth + " " + todayYear
            font{bold: true; pixelSize: 14}
        }

        RowLayout{
            id:monthSection
            Layout.alignment: Qt.AlignHCenter
            spacing: 10

            Image {
                id: leftNextId
                rotation: 180
                source: "qrc:/images/next.png"
                Layout.maximumWidth: 30
                Layout.maximumHeight: 30
                mipmap: true
                smooth: true
                MouseArea{
                    anchors.fill: parent
                    onClicked: nextMonth()
                }
            }

            Text {
                id: monthId
                text: thisMonth
                font{bold: true; pixelSize: 14}
            }

            Image {
                id: rightNextId
                source: "qrc:/images/next.png"
                Layout.maximumWidth: 30
                Layout.maximumHeight: 30
                mipmap: true
                smooth: true
                MouseArea{
                    anchors.fill: parent
                    onClicked: previousMonth()
                }
            }
        }

        RowLayout{
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            spacing : 27

            Repeater{
                model: 7
                Rectangle{
                    width: 30
                    color: "#00FFFFFF"
                    Text{
                        anchors.centerIn: parent
                        text : calc.dayName(index)
                        z : 2
                        font{bold: true; pixelSize: 14}
                    }
                }
            }
        }

        GridLayout{
            id:gridId
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.fillHeight: true
            columnSpacing: 2
            rowSpacing: 2
            columns: 7

            Repeater{
                id: repeater
                model: 35
                Rectangle{
                    id: dayElement
                    width: 55
                    height: 42
                    radius: 100

                    Text{
                        id: dayElementText
                        font{pixelSize: 14}
                        anchors.centerIn: parent
                        z : 2
                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            selectedCellChanged(dayElementText.text)
                        }
                    }
                }
            }
        }

        Component.onCompleted:
        {
            fillCells()
            selectedCellChanged(todayDay)
        }
    }
}
