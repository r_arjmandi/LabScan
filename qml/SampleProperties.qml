import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import BacteriaRepository.DataModel 1.0
import StandardRepository.DataModel 1.0
import SampleTypeRepository.DataModel 1.0
import SampleShapeRepository.DataModel 1.0
import CurrentPatient.Wrapper 1.0

GroupBox {
    id: root

    ColumnLayout{
        anchors.fill: parent

        RowLayout{
            spacing: 10
            Layout.fillWidth: true

            StyledLabel {
                text: qsTr("Type:")
                Layout.alignment: Qt.AlignCenter
            }

            ComboBox{
                id: typeComboBox
                Layout.fillWidth: true
                currentIndex: CurrentPatientWrapper.sampleType

                model: SampleTypeRepositoryDataModel
                textRole: qsTr("Name")

                onAccepted: {
                    SampleTypeRepositoryDataModel.AddNewType(editText)
                    editable = false
                }

                onCurrentIndexChanged: {
                    CurrentPatientWrapper.sampleType = currentIndex;
                }
            }

            AddRemoveButtons{
                id: typeAddRemoveButtons

                onAddClicked: {
                    typeComboBox.editable = true;
                    typeComboBox.editText = ""
                    typeComboBox.focus = true
                }

                onDeleteClicked: {
                    if(typeComboBox.editable === true)
                    {
                        typeComboBox.editable = false;
                        return;
                    }
                    SampleTypeRepositoryDataModel.Delete(typeComboBox.currentText)
                }
            }
        }

        RowLayout{
            spacing: 10
            Layout.fillWidth: true

            StyledLabel {
                text: qsTr("Standard:")
                Layout.alignment: Qt.AlignCenter
            }

            ComboBox{
                id: standardComboBox
                Layout.fillWidth: true

                model: StandardRepositoryDataModel
                textRole: qsTr("FileName")
                currentIndex: CurrentPatientWrapper.sampleStandard

                onCurrentIndexChanged: {
                    if(currentIndex != -1)
                    {
                        StandardRepositoryDataModel.SetCurrentStandard(currentIndex);
                    }
                    CurrentPatientWrapper.sampleStandard = currentIndex;
                }
            }
        }

        RowLayout{
            spacing: 10
            Layout.fillWidth: true

            StyledLabel{
                text: qsTr("Bacteria:")
                Layout.alignment: Qt.AlignCenter
            }

            ComboBox{
                id: bacteriaComboBox
                Layout.fillWidth: true
                currentIndex: CurrentPatientWrapper.sampleBacteria
                model:BacteriaRepositoryDataModel
                textRole: qsTr("Name")

                onCurrentTextChanged: {
                    if(currentText != "")
                    {
                        StandardRepositoryDataModel.SetCurrentBacteria(currentText);
                    }
                }

                onCurrentIndexChanged: {
                    CurrentPatientWrapper.sampleBacteria = currentIndex;
                }
            }
        }

        RowLayout{
            spacing: 10
            Layout.fillWidth: true

            StyledLabel {
                id: shapeLabel
                text: qsTr("Shape:")
                Layout.alignment: Qt.AlignCenter
            }

            ComboBox{
                id: shapeComboBox
                Layout.fillWidth: true
                currentIndex: CurrentPatientWrapper.sampleShape

                model: SampleShapeRepositoryDataModel
                textRole: qsTr("Name")

                onAccepted: {
                    SampleShapeRepositoryDataModel.AddNewShape(editText)
                    editable = false
                }

                onCurrentIndexChanged: {
                    CurrentPatientWrapper.sampleShape = currentIndex;
                }
            }

            AddRemoveButtons{
                id: shapeAddRemoveButtons

                onAddClicked: {
                    shapeComboBox.editable = true;
                    shapeComboBox.editText = ""
                    shapeComboBox.focus = true
                }

                onDeleteClicked: {
                    if(shapeComboBox.editable === true)
                    {
                        shapeComboBox.editable = false;
                        return;
                    }
                    SampleShapeRepositoryDataModel.Delete(shapeComboBox.currentText)
                }
            }
        }

        RowLayout{
            Layout.fillWidth: true

            StyledLabel {
                text: qsTr("Gram:")
            }

            GramItem{
                id: gramItem
            }

            StyledLabel {
                Layout.leftMargin: 50
                text: qsTr("Isolated:")
            }

            IsolatedItem{
                id: isolatedItem
            }
        }

        RowLayout{
            spacing: 10

            Layout.fillWidth: true

            StyledLabel {
                text: qsTr("Count:")
                Layout.alignment: Qt.AlignCenter
            }

            StyledTextBox{
                id: countTextField
                text : CurrentPatientWrapper.sampleCount
                Layout.fillWidth: true
                onTextChanged: {
                    CurrentPatientWrapper.sampleCount = text;
                }
            }
        }
    }
}
