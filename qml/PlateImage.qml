import QtQuick 2.9
import QtQuick.Controls 2.2
import CircleRepository.DataModel 1.0

Canvas{
    id: root

    property bool marksVisibility
    property url source
    property real imagePaintedWidthToSourceWidthRatio : fitImage.paintedWidth / fitImage.sourceSize.width
    property real paintOffsetX : (width - fitImage.paintedWidth) / 2.0
    property real paintOffsetY : (height - fitImage.paintedHeight) / 2.0

    CircularTargetsResizer{
        id: circularTargetsResizer
        anchors.fill: parent
        ratio: imagePaintedWidthToSourceWidthRatio
        offsetX: paintOffsetX
        offsetY: paintOffsetY
        imageSource: root.source
        itemsVisibility: root.marksVisibility
    }

    Image {
        id: fitImage
        visible: false
        fillMode: Image.PreserveAspectFit
        anchors.fill: parent
    }

    onPaint: {
        if(source != "" && isImageLoaded(source)){
            var ctx = getContext("2d")
            ctx.clearRect(0, 0, width, height)
            ctx.drawImage(source, paintOffsetX, paintOffsetY, fitImage.paintedWidth, fitImage.paintedHeight)

            var circleCount = CircleRepositoryDataModel.Size(source)
            if(circleCount > 0 && root.marksVisibility)
            {
                ctx.lineWidth = 2
                ctx.font = 'bold 18px Arial';
                ctx.fillStyle = "#24d12e";

                for(var i = 0 ; i < circleCount ; i++)
                {
                    var xDisk = CircleRepositoryDataModel.GetData(i, 0, 0)
                    var yDisk = CircleRepositoryDataModel.GetData(i, 0, 1)
                    var radiusDisk = CircleRepositoryDataModel.GetData(i, 0, 2)
                    var xDiskTransformed = xDisk * imagePaintedWidthToSourceWidthRatio + paintOffsetX
                    var yDiskTransformed = yDisk * imagePaintedWidthToSourceWidthRatio + paintOffsetY
                    var radiusDiskTransformed = radiusDisk * imagePaintedWidthToSourceWidthRatio
                    ctx.fillText((i + 1 ).toString(), xDiskTransformed, yDiskTransformed)
                    ctx.beginPath()
                    ctx.arc(xDiskTransformed, yDiskTransformed, radiusDiskTransformed, 0, 2 * Math.PI, false)
                    ctx.closePath()
                    ctx.strokeStyle = '#ffffff'
                    ctx.stroke()

                    var xRegion = CircleRepositoryDataModel.GetData(i, 1, 0)
                    var yRegion = CircleRepositoryDataModel.GetData(i, 1, 1)
                    var radiusRegion = CircleRepositoryDataModel.GetData(i, 1, 2)
                    var xRegionTransformed = xRegion * imagePaintedWidthToSourceWidthRatio + paintOffsetX
                    var yRegionTransformed = yRegion * imagePaintedWidthToSourceWidthRatio + paintOffsetY
                    var radiusRegionTransformed = radiusRegion * imagePaintedWidthToSourceWidthRatio
                    var status = CircleRepositoryDataModel.GetData(i, 0, 3)
                    ctx.beginPath()
                    ctx.arc(xRegionTransformed, yRegionTransformed, radiusRegionTransformed, 0, 2 * Math.PI, false)
                    ctx.closePath()
                    if(status == 1)
                        ctx.strokeStyle = '#00ff00'
                    else if(status == 2)
                        ctx.strokeStyle = '#ffff00'
                    else if(status == 3)
                        ctx.strokeStyle = '#ff0000'
                    else
                        ctx.strokeStyle = '#ffffff'
                    ctx.stroke()
                }
            }
        }
    }

    onImageLoaded: {
        requestPaint()
    }

    onSourceChanged: {
        fitImage.source = source
        CircleRepositoryDataModel.SetCurrentImage(source)
    }

    Connections {
        target: CircleRepositoryDataModel
        onUpdateRepository: {
            mainSmapleImageBox.requestPaint()
        }
    }

    onMarksVisibilityChanged: {
        mainSmapleImageBox.requestPaint()
    }
}

