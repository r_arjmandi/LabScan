import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import CurrentPatient.Wrapper 1.0
import BackEnd.Wrapper 1.0
import PatientRepository.DataModel 1.0

Item {
    id: root

    signal commentPageClicked
    signal patientPageClicked
    signal captureSourcePageClicked(string source)
    signal calibratorPageClicked(string source)

    Rectangle{

        color: "#e9f2ff"
        anchors.fill: parent

        RowLayout{
            spacing: 20
            anchors.fill: parent
            anchors.margins: 20

            ColumnLayout{
                id: column1
                spacing: 10

                Layout.fillHeight: true
                Layout.minimumWidth: parent.width / 2 - 20

                PlateProperties{
                    id: plateProperties
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    title: "Plate"

                    onCommentPageClicked: root.commentPageClicked()
                }

                RowLayout{
                    spacing: 10

                    Layout.fillWidth: true

                    Button{
                        id: goPatientPageButton
                        Layout.fillWidth: true
                        text: "Patient"

                        Image{
                            anchors.left: parent.left
                            source: "qrc:/images/prevPage.png"
                            mipmap: true
                            fillMode: Image.PreserveAspectFit
                            height: parent.height
                        }

                        onClicked: patientPageClicked()
                    }

                    Button{
                        id: printButton
                        Layout.fillWidth: true
                        text: "Print"
                        onClicked: BackEndWrapper.PrintCurrentPatient()
                    }

                    Button{
                        id: saveButton
                        Layout.minimumWidth: parent.width / 3.
                        text: "Save Patient"

                        onClicked: {
                            BackEndWrapper.SaveCurrentPatient()
                        }
                    }
                }
            }

            ColumnLayout{
                id: column2
                spacing: 10

                Layout.fillHeight: true
                Layout.minimumWidth: parent.width / 2 - 20

                PlateConfiguration{
                    id: plateConfiguration

                    topPadding: 35

                    Layout.fillWidth: true
                    Layout.fillHeight: true


                    onCaptureSourcePageClicked: root.captureSourcePageClicked(source)

                    onCalibratorPageClicked: root.calibratorPageClicked(source)
                }

                RowLayout{
                    spacing: 10

                    Layout.fillWidth: true

                    Button{
                        id: analyseButton
                        width: parent.width / 6 - 15
                        text: "Analyse"

                        onClicked:{

                            CurrentPatientWrapper.plateTemplate = plateProperties.plateTemplate
                            CurrentPatientWrapper.plateDiskNo = plateProperties.plateDiskNo
                            CurrentPatientWrapper.plateDiskDiameter = plateProperties.plateDiskDiameter
                            CurrentPatientWrapper.plateDiskType = plateProperties.plateType
                            CurrentPatientWrapper.plateDiskArrange = plateProperties.plateArrange
                            BackEndWrapper.AnalyseImage(plateProperties.plateDiskDiameter, plateProperties.plateDiskNo)
                        }
                    }

                    Button{
                        id: hideMarksButton
                        width: parent.width / 6 - 15
                        text: "Hide Marks"
                        font.pixelSize: 9

                        onClicked: {
                            if(text == "Hide Marks")
                            {
                                plateConfiguration.marksVisibility = false
                                text = "Show Marks"
                            }
                            else
                            {
                                plateConfiguration.marksVisibility = true
                                text = "Hide Marks"
                            }
                        }
                    }

                    Button{
                        id: enumDisksButton
                        width: parent.width / 6 - 15
                        text: "Enum Disks"
                        font.pixelSize: 9
                    }

                    RowLayout{

                        RadioButton{
                            id: numbersRadioButton
                            text: "Numbers"
                            checked: true
                        }

                        RadioButton{
                            id: namesRadioButton
                            text: "Names"
                        }

                    }

                    Button{
                        id: adjustDisksButton
                        width: parent.width / 6 - 15
                        text: "Adjust"
                    }
                }
            }
        }
    }
}
