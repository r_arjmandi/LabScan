import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3

import OperatorRepository.DataModel 1.0
import CurrentPatient.Wrapper 1.0

GroupBox{
    id: root

        RowLayout{
            spacing: 10

            anchors.fill: parent

            StyledLabel {
                id: operatorLabel
                text: qsTr("Operator:")
                Layout.alignment: Qt.AlignCenter
            }

            ComboBox{
                id: operatorComboBox
                Layout.fillWidth: true
                currentIndex: CurrentPatientWrapper.labOperator

                model: OperatorRepositoryDataModel
                textRole: qsTr("Name")

                onAccepted: {
                    OperatorRepositoryDataModel.AddNewOperator(editText)
                    editable = false
                }

                onCurrentIndexChanged: {
                    CurrentPatientWrapper.labOperator = currentIndex
                }
            }

            AddRemoveButtons{
                id: operatorAddRemoveButtons

                onAddClicked: {
                    operatorComboBox.editable = true;
                    operatorComboBox.editText = ""
                    operatorComboBox.focus = true
                }

                onDeleteClicked: {
                    if(operatorComboBox.editable === true)
                    {
                        operatorComboBox.editable = false;
                        return;
                    }
                    OperatorRepositoryDataModel.Delete(operatorComboBox.currentText)
                }
            }
        }
}
