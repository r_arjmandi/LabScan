import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQml.Models 2.3

import MediaRepository.DataModel 1.0
import CurrentPatient.Wrapper 1.0

GroupBox {
    id: root

    signal commentPageClicked

    property string plateTemplate: templateComboBox.currentText
    property int plateDiskNo: diskNoSpinBox.value
    property real plateDiskDiameter: parseFloat(diameterTextBox.text)
    property int plateType: row4.plateType
    property int plateArrange: arrange.plateArrange

    ColumnLayout{
        spacing: 10
        anchors.fill: parent

        RowLayout{
            Layout.fillWidth: true

            spacing: 10

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Media:")
                font{family: "Arial"; pixelSize: 16}
            }

            ComboBox{
                id: mediaComboBox

                Layout.fillWidth: true

                model: MediaRepositoryDataModel
                textRole: qsTr("Name")

                onAccepted: {
                    MediaRepositoryDataModel.AddNewMedia(editText)
                    editable = false
                }

                currentIndex: CurrentPatientWrapper.plateMediaIndex

                onCurrentIndexChanged: {
                    CurrentPatientWrapper.plateMediaIndex = currentIndex;
                }
            }

            AddRemoveButtons{
                id: mediaAddRemoveButtons

                onAddClicked: {
                    mediaComboBox.editable = true;
                    mediaComboBox.editText = ""
                    mediaComboBox.focus = true
                }

                onDeleteClicked: {
                    if(mediaComboBox.editable === true)
                    {
                        mediaComboBox.editable = false;
                        return;
                    }
                    MediaRepositoryDataModel.Delete(mediaComboBox.currentText)
                }
            }
        }

        RowLayout{
            Layout.fillWidth: true

            spacing: 10

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Template:")
                font{family: "Arial"; pixelSize: 16}
            }

            ComboBox{
                id: templateComboBox
                Layout.fillWidth: true
                model: ["Template", "template1", "template2", "template3", "template4"]
            }

            Button{
                id: deleteTemplateButton

                Layout.maximumHeight: parent.height
                Layout.maximumWidth: Layout.maximumHeight

                Image{
                    source: "qrc:/images/delete.png"
                    anchors.margins: 5
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectFit
                    mipmap: true
                }
            }
        }

        RowLayout{
            id: row3
            spacing: 20

            Layout.fillWidth: true

            Text {
                id: diskNoText
                text: "Disk NO:"
                anchors.verticalCenter: parent.verticalCenter
                font{family: "Arial"; pixelSize: 16}
            }

            SpinBox{
                id: diskNoSpinBox
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Diameter:")
                font{family: "Arial"; pixelSize: 16}
            }

            //ComboBox{
            //    id: diameterComboBox
            //    Layout.fillWidth: true
            //    model: ["Select Diameter", "1 mm", "2 mm", "3 mm", "4 mm"]
            //}

            StyledTextBox{
                id: diameterTextBox
                Layout.fillWidth: true
                }
        }

        RowLayout{
            id: row4
            spacing: 20

            Layout.fillWidth: true

            property int plateType

            Text {
                text: "Type:"
                anchors.verticalCenter: parent.verticalCenter
                font{family: "Arial"; pixelSize: 16}
            }

            RadioButton{
                text: "Circle"

                checked: true

                onCheckedChanged: {
                    if(checked === true)
                    {
                        parent.plateType = 1
                        arrange.mode = 1
                    }
                }
            }

            RadioButton{
                text: "Rectangle"

                onCheckedChanged: {
                    if(checked === true)
                    {
                        parent.plateType = 2
                        arrange.mode = 2
                    }
                }
            }
        }

        ArrangeItems{
            id: arrange
            Layout.fillWidth: true
            mode : 1
        }


        AnalyseResultListView{
            id: resultsListView
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Button{
            id: saveAsNewTemplateButton
            Layout.fillWidth: true
            text: "Save As New Template"
        }

        Button{
            id: commentForThisPlateButton
            Layout.fillWidth: true
            text: "Comment For This Plate"

            onClicked: root.commentPageClicked()
        }
    }
}
