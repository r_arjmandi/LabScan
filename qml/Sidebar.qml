import QtQuick 2.9
import QtQuick.Layouts 1.3

Item {
    id: root

    property real itemHeight: root.height / 6.4

    signal patientClicked
    signal archiveClicked
    signal settingClicked

    property real iconWidth: root.height / 9.6
    property real iconHeight: root.height / 9.6

    Rectangle{
        anchors.fill: parent
        color: "#000000"
        //border.color: "#000000"

        ColumnLayout{
            anchors.fill: parent
            spacing: 0

            //--------------------menu header--------------------//
            Rectangle{
                id: menuHeaderItem

                height: 180
                Layout.fillWidth: true

                Image {
                    id: menuHeaderItemBackImage
                    source: "../images/menuBackground.jpg"
                    anchors.fill: parent
                }

                Text{
                    id: menuHeaderItemText
                    anchors.centerIn: parent

                    color: "#FFFFFF"
                    antialiasing: true
                    smooth: true

                    text: "Menu"
                    fontSizeMode: Text.Fit; minimumPixelSize: 10;
                    font{family: "Arial"; pixelSize: 32}
                }

                Image {
                    id: menuHeaderIcon
                    source: "../images/menu.png"

                    width: iconWidth
                    height: iconHeight

                    anchors.right: menuHeaderItemText.left
                    antialiasing: true
                    smooth: true
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            //--------------------Item 1--------------------//
            SidebarItem{
                id: item1

                Layout.minimumHeight: root.itemHeight
                Layout.fillWidth: true

                icon : "../images/user.png"
                backgroundImage : "../images/item1Background.jpg"
                subject : "Patient"
                subjectFontSize : 32
                description : "Create new patient, load image data and analys the data"
                descriptionFontSize : 10

                onClicked: root.patientClicked();
            }

            SidebarItem{
                id: item2

                Layout.minimumHeight: itemHeight
                Layout.fillWidth: true

                icon : "../images/analys.png"
                backgroundImage : "../images/item2Background.jpg"
                subject : "Archive"
                subjectFontSize : 32
                description : "Search the history of patient data"
                descriptionFontSize : 10

                onClicked: root.archiveClicked();
            }

            SidebarItem{
                id: item3

                Layout.minimumHeight: itemHeight
                Layout.fillWidth: true

                icon : "../images/setting.png"
                backgroundImage : "../images/item3Background.jpg"
                subject : "Setting"
                subjectFontSize : 32
                description : "Changing app settings include: company name, address, logo and ..."
                descriptionFontSize : 10

                onClicked: root.settingClicked()
            }
            //--------------------footer--------------------//
            Rectangle{
                id: sidebarFooter
                Layout.fillWidth: true
                Layout.fillHeight: true

                Image {
                    id: footerBackImage
                    source: "../images/menuBackground.jpg"
                    anchors.fill: parent
                }
            }
        }
    }
}
