import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import CurrentPatient.Wrapper 1.0

RowLayout
{
    id: root

    property int gram : CurrentPatientWrapper.sampleGram;

    onGramChanged: {
        if(gram === 1)
        {
            positiveRadioButton.checked = true;
            negativeRadioButton.checked = false;
        }
        else if(gram === -1)
        {
            positiveRadioButton.checked = false;
            negativeRadioButton.checked = true;
        }
    }

    RadioButton{
        id: positiveRadioButton
        text: qsTr("Positive")
        checked: true

        onCheckedChanged: {
            if(checked === true)
            {
                CurrentPatientWrapper.sampleGram = 1;
            }
        }
    }

    RadioButton{
        id: negativeRadioButton
        text: qsTr("Negative")

        onCheckedChanged: {
            if(checked === true)
            {
                CurrentPatientWrapper.sampleGram = -1;
            }
        }
    }
}
