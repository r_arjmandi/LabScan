import QtQuick 2.9
import QtQuick.Controls 2.2
import BackEnd.Wrapper 1.0
import QtQuick.Layouts 1.3

Item {
    id: root

    signal dateTimePickerPageClicked
    signal updateSelectedDateTime
    signal patientPageClicked;

    onUpdateSelectedDateTime: searchSection.updateSelectedDateTime()

    property string selectedDateTime

    Rectangle{

        color: "#e9f2ff"
        anchors.fill: parent

        RowLayout{
            spacing: 20
            anchors.fill: parent
            anchors.margins: 20

            ColumnLayout{
                id: column1
                spacing: 20
                Layout.fillHeight: true
                Layout.minimumWidth: (2 * parent.width / 3) - 20

                SearchSection{
                    id: searchSection
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    title: "Search"
                    selectedDateTime: root.selectedDateTime
                    onDateTimePickerPageClicked: root.dateTimePickerPageClicked()
                    onPatientPageClicked: {
                        root.patientPageClicked()
                    }
                }
            }

            ArchivePageButtonsSection{
                id: plateConfiguration
                topPadding: 35
                Layout.fillHeight: true
                Layout.minimumWidth: parent.width / 3 - 20

                onLoadClicked: {
                    searchSection.loadClicked()
                    root.patientPageClicked()
                }
            }
        }
    }
}
