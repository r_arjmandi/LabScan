import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3

import CurrentPatient.Wrapper 1.0

GroupBox{
    id: root

    ColumnLayout{
        anchors.fill: parent

        RowLayout{
            spacing: 20
            Layout.fillWidth: true

            StyledLabel {
                text: qsTr("ID:")
                Layout.alignment: Qt.AlignCenter
            }

            StyledTextBox{
                id: patientPropsIDField
                Layout.fillWidth: true
                text: CurrentPatientWrapper.id
                onTextChanged: {
                    CurrentPatientWrapper.id = text;
                }
            }
        }

        RowLayout{
            spacing: 20
            Layout.fillWidth: true

            StyledLabel {
                text: qsTr("Name:")
                Layout.alignment: Qt.AlignCenter
            }

            StyledTextBox{
                id: patientPropsNameField
                Layout.fillWidth: true
                text: CurrentPatientWrapper.name
                onTextChanged: {
                    CurrentPatientWrapper.name = text;
                }
            }
        }

        RowLayout{
            spacing: 20
            Layout.fillWidth: true

            StyledLabel {
                text: qsTr("Family Name:")
                Layout.alignment: Qt.AlignCenter
            }

            StyledTextBox{
                id: patientPropsFamilyNameField
                Layout.fillWidth: true
                text: CurrentPatientWrapper.family
                onTextChanged: {
                    CurrentPatientWrapper.family = text;
                }
            }
        }

        RowLayout{
            Layout.fillWidth: true
            spacing: 20

            StyledLabel {
                Layout.alignment: Qt.AlignCenter
                text: qsTr("Age:")
            }

            StyledTextBox{
                id: patientPropsAgeField
                Layout.fillWidth: true
                validator: IntValidator {bottom: 1; top: 200;}
                text: CurrentPatientWrapper.age
                onTextChanged: {
                    CurrentPatientWrapper.age = text;
                }
            }

            GenderItem{
                id: genderSelect
            }

            CheckBox{
                id: qcCheckBox
                anchors.right: parent.right
                text: qsTr("QC")
            }
        }
    }
}
