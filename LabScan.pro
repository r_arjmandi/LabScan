TEMPLATE = app

QT += qml quick widgets
CONFIG += c++17

SOURCES += main.cpp \
    BackEndWrapper.cpp \
    FrontEndAdapter.cpp \
    BackEndAdapter.cpp \
    BusinessRules.cpp \
    PatientDataAccess.cpp \
    SqliteDatabaseConnection.cpp \
    SqlitePatientQueryBuilder.cpp \
    SqliteQueryResultFactory.cpp \
    DatabaseCommandFactory.cpp \
    DateTimeOperation.cpp \
    DateTime.cpp \
    DatabaseCommand.cpp \
    DataAccessException.cpp \
    SqliteQueryResult.cpp \
    SqliteDatabaseException.cpp \
    CurrentPatientWrapper.cpp \
    BacteriaRepositoryDataModel.cpp \
    StandardRepositoryDataModel.cpp \
    AntibuticRepositoryDataModel.cpp \
    OpenCvHoughCircleWrapper.cpp \
    CircleRepositoryDataModel.cpp \
    CalibratorRepositoryDataModel.cpp \
    OperatorRepositoryDataModel.cpp \
    OperatorDataAccess.cpp \
    SqliteOperatorQueryBuilder.cpp \
    SampleTypeRepositoryDataModel.cpp \
    SampleTypeDataAccess.cpp \
    SqliteSampleTypeQueryBuilder.cpp \
    SampleShapeRepositoryDataModel.cpp \
    SampleShapeDataAccess.cpp \
    SqliteSampleShapeQueryBuilder.cpp \
    GeneralCommentRepositoryDataModel.cpp \
    CommentDataAccess.cpp \
    SqliteCommentQueryBuilder.cpp \
    ScannerDevice.cpp \
    FileNameGenerator.cpp \
    MediaRepositoryDataModel.cpp \
    MediaDataAccess.cpp \
    SqliteMediaQueryBuilder.cpp \
    CircularSorter.cpp \
    ImageDataAccess.cpp \
    PatientListDataModel.cpp \
    PatientData.cpp \
    CurrentPatientImageListDataModel.cpp \
    PatientListSearchWrapper.cpp \
    PatientListSearchEngine.cpp \
    PatientShortInformationRepository.cpp \
    CalibratorDataAccess.cpp \
    SqliteCalibratorQueryBuilder.cpp \
    PatientLogger.cpp \
    RegionGrowing.cpp \
    ExcelFilePrinter.cpp

win32:CONFIG(release, debug|release): LIBS += -L"C:\Program Files\Boost\lib"
else:win32:CONFIG(debug, debug|release): LIBS += -L"C:\Program Files\Boost\lib"

win32:CONFIG(release, debug|release): LIBS += -L"C:\Program Files\Sqlite3\lib.Release" -lSqlite
else:win32:CONFIG(debug, debug|release): LIBS += -L"C:\Program Files\Sqlite3\lib.Debug" -lSqlite

win32:CONFIG(release, debug|release): LIBS += -L"C:\Program Files\opencv3.3.1\lib.Release" -lopencv_world331
else:win32:CONFIG(debug, debug|release): LIBS += -L"C:\Program Files\opencv3.3.1\lib.Debug" -lopencv_world331d

INCLUDEPATH += "C:\Program Files\Boost\include\boost-1_62" "C:\Program Files\Boost\include" \
    "C:\Program Files\Sqlite3\include"  "C:\Program Files\opencv3.3.1\include"

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    Headers/Framework/BackEndWrapper.h \
    Headers/Adapters/FrontEndAdapter.h \
    Headers/Adapters/IFrontEndAdapter.h \
    Headers/Adapters/BackEndAdapter.h \
    Headers/BusinessRules/IBusinessRules.h \
    Headers/BusinessRules/BusinessRules.h \
    Headers/Adapters/PatientDataAccess.h \
    Headers/Adapters/IPatientQueryBuilder.h \
    Headers/Adapters/IDatabaseConnection.h \
    Headers/Adapters/IDatabaseCommand.h \
    Headers/Adapters/IQueryResult.h \
    Headers/Adapters/ConnectionGuard.h \
    Headers/Framework/SqliteDatabaseConnection.h \
    Headers/Framework/SqliteDatabaseException.h \
    Headers/Framework/CurrentPatientWrapper.h \
    Headers/BusinessEntities/IBacteriaRepository.h \
    Headers/Framework/BacteriaRepositoryDataModel.h \
    Headers/BusinessEntities/IStandardRepository.h \
    Headers/BusinessEntities/IAntibuticRepository.h \
    Headers/Framework/AntibuticRepositoryDataModel.h \
    Headers/BusinessEntities/ICircleDetector.h \
    Headers/BusinessEntities/Point.h \
    Headers/Framework/OpenCvHoughCircleWrapper.h \
    Headers/BusinessEntities/ICircleRepository.h \
    Headers/Framework/CircleRepositoryDataModel.h \
    Headers/BusinessEntities/ICalibratorRepository.h \
    Headers/Framework/CalibratorRepositoryDataModel.h \
    Headers/BusinessEntities/Range.h \
    Headers/BusinessEntities/IOperatorRepository.h \
    Headers/Framework/OperatorRepositoryDataModel.h \
    Headers/Adapters/OperatorDataAccess.h \
    Headers/Adapters/IOperatorQueryBuilder.h \
    Headers/Framework/SqliteOperatorQueryBuilder.h \
    Headers/Framework/SqlitePatientQueryBuilder.h \
    Headers/Framework/SampleTypeRepositoryDataModel.h \
    Headers/BusinessEntities/ISampleTypeRepository.h \
    Headers/Adapters/SampleTypeDataAccess.h \
    Headers/Adapters/ISampleTypeQueryBuilder.h \
    Headers/Framework/SqliteSampleTypeQueryBuilder.h \
    Headers/BusinessEntities/ISampleShapeRepository.h \
    Headers/Framework/SampleShapeRepositoryDataModel.h \
    Headers/Adapters/SampleShapeDataAccess.h \
    Headers/Adapters/ISampleShapeQueryBuilder.h \
    Headers/Framework/SqliteSampleShapeQueryBuilder.h \
    Headers/Framework/StandardRepositoryDataModel.h \
    Headers/BusinessEntities/IGeneralCommentRepository.h \
    Headers/Framework/GeneralCommentRepositoryDataModel.h \
    Headers/BusinessEntities/CommentData.h \
    Headers/Adapters/CommentDataAccess.h \
    Headers/Adapters/ICommentQueryBuilder.h \
    Headers/Framework/SqliteCommentQueryBuilder.h \
    Headers/Framework/ScannerDevice.h \
    Headers/BusinessEntities/IFileNameGenerator.h \
    Headers/BusinessEntities/FileNameGenerator.h \
    Headers/BusinessEntities/IMediaRepository.h \
    Headers/Framework/MediaRepositoryDataModel.h \
    Headers/Adapters/MediaDataAccess.h \
    Headers/Adapters/IMediaQueryBuilder.h \
    Headers/Framework/SqliteMediaQueryBuilder.h \
    Headers/BusinessEntities/ICircularSorter.h \
    Headers/BusinessEntities/CircularSorter.h \
    Headers/Adapters/ImageDataAccess.h \
    Headers/Framework/PatientListDataModel.h \
    Headers/BusinessRules/IOperatorDataAccess.h \
    Headers/BusinessRules/IImageDataAccess.h \
    Headers/BusinessRules/IPatientDataAccess.h \
    Headers/BusinessRules/ISampleShapeDataAccess.h \
    Headers/Adapters/IPatientListGui.h \
    Headers/BusinessRules/ICommentDataAccess.h \
    Headers/BusinessRules/IBackEndAdapter.h \
    Headers/BusinessRules/IMediaDataAccess.h \
    Headers/BusinessEntities/PatientData.h \
    Headers/BusinessEntities/IPatientData.h \
    Headers/BusinessEntities/SampleData.h \
    Headers/Framework/IImageListGui.h \
    Headers/Framework/CurrentPatientImageListDataModel.h \
    Headers/Adapters/ICurrentPatientGui.h \
    Headers/Framework/PatientListSearchWrapper.h \
    Headers/BusinessEntities/PatientListFilter.h \
    Headers/BusinessRules/IPatientListSearchEngine.h \
    Headers/BusinessRules/PatientListSearchEngine.h \
    Headers/BusinessEntities/IPatientShortInformationRepository.h \
    Headers/BusinessEntities/PatientShortInformationRepository.h \
    Headers/BusinessEntities/PatientShortInformation.h \
    Headers/BusinessEntities/DateTimeOperation.h \
    Headers/BusinessRules/ICalibratorDataAccess.h \
    Headers/Adapters/CalibratorDataAccess.h \
    Headers/Adapters/ICalibratorQueryBuilder.h \
    Headers/Framework/SqliteCalibratorQueryBuilder.h \
    Headers/BusinessRules/IPatientLogger.h \
    Headers/Framework/PatientLogger.h \
    Headers/BusinessEntities/CircularTarget.h \
    Headers/BusinessRules/IRegionGrowing.h \
    Headers/BusinessRules/RegionGrowing.h \
    Headers/BusinessRules/IExcelFilePrinter.h \
    Headers/Framework/ExcelFilePrinter.h \
    Headers/BusinessEntities/Circle.h \
    Headers/BusinessEntities/Antibiotic.h

PRECOMPILED_HEADER = "stable.h"
