#include "Headers/BusinessEntities/FileNameGenerator.h"

std::string FileNameGenerator::Generate() const
{
    auto uuid = boost::uuids::random_generator()();
    return boost::uuids::to_string(uuid);
}
