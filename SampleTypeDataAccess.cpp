#include "Headers/Adapters/SampleTypeDataAccess.h"

#include "Headers/Adapters/ConnectionGuard.h"
#include "Headers/Adapters/ISampleTypeQueryBuilder.h"
#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Adapters/IDatabaseConnection.h"
#include "Headers/Adapters/IQueryResult.h"

SampleTypeDataAccess::SampleTypeDataAccess(
        std::shared_ptr<IDatabaseConnection> dbConnection,
        std::shared_ptr<ISampleTypeQueryBuilder> sampleTypeQueryBuilder):
    m_DatabaceConnection(dbConnection),
    m_SampleTypeQueryBuilder(sampleTypeQueryBuilder)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto createTableCommand = m_SampleTypeQueryBuilder->GetCreateTableIfNotExistsQuery();
    m_DatabaceConnection->Exec(createTableCommand);
}

void SampleTypeDataAccess::InsertOrUpdate(const std::string &typeName)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto rowCountCommand = m_SampleTypeQueryBuilder->GetRowCountQuery();
    rowCountCommand->BindParameter("TypeName", typeName);
    auto queryResult = m_DatabaceConnection->Exec(rowCountCommand);

    if (queryResult->GetInt(0) == 0)
    {
        auto insertCommand = m_SampleTypeQueryBuilder->GetInsertQuery();
        insertCommand->BindParameter("TypeName", typeName);
        m_DatabaceConnection->Exec(insertCommand);
    }
    else
    {
        auto updateCommand = m_SampleTypeQueryBuilder->GetUpdateQuery();
        updateCommand->BindParameter("TypeName", typeName);
        m_DatabaceConnection->Exec(updateCommand);
    }
}

void SampleTypeDataAccess::Delete(const std::string &typeName)
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto deleteCommand = m_SampleTypeQueryBuilder->GetDeleteQuery();
    deleteCommand->BindParameter("TypeName", typeName);
    m_DatabaceConnection->Exec(deleteCommand);
}

std::set<std::string> SampleTypeDataAccess::GetAllTypes()
{
    ConnectionGuard<IDatabaseConnection> connectionGuard(m_DatabaceConnection);
    auto selectAllCommand = m_SampleTypeQueryBuilder->GetSelectAllQuery();
    auto queryResult = m_DatabaceConnection->Exec(selectAllCommand);

    auto typeList = std::set<std::string>();

    while(queryResult->HasData())
    {
        auto typeName = queryResult->GetString(0);
        typeList.insert(typeName);
        queryResult->Next();
    }
    return std::move(typeList);
}
