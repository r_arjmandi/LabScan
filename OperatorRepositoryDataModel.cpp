#include "Headers/Framework/OperatorRepositoryDataModel.h"
#include "Headers/Adapters/IFrontEndAdapter.h"

OperatorRepositoryDataModel::OperatorRepositoryDataModel()
{
    m_OperatorRoleNames[Name] = "Name";
}

void OperatorRepositoryDataModel::SetAdapter(std::shared_ptr<IFrontEndAdapter> adapter)
{
    m_adapter = adapter;
}

int OperatorRepositoryDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return static_cast<int>(m_OperatorList.size());
}

QVariant OperatorRepositoryDataModel::data(const QModelIndex &index, int role) const
{
    if(m_OperatorList.empty())
    {
        return QVariant();
    }

    int row = index.row();
    if(row < 0 || row >= m_OperatorList.size())
    {
        return QVariant();
    }

    switch(role) {
        case Name:
            auto operatorName = m_OperatorList.begin();
            for(auto i = 0; i < row; i++)
            {
                operatorName++;
            }
            return QString(operatorName->c_str());
    }

    return QVariant();
}

void OperatorRepositoryDataModel::SetOperatorList(std::set<std::string>&& operatorList)
{
    m_OperatorList = std::move(operatorList);
    emit endResetModel();
}

void OperatorRepositoryDataModel::AddNewOperator(const QString &operatorName)
{
    if(operatorName != "")
    {
        m_OperatorList.insert(operatorName.toStdString());
        m_adapter->InsertOrUpdateOperator(operatorName.toStdString());
        emit endResetModel();
    }
}

void OperatorRepositoryDataModel::Delete(const QString &operatorName)
{
    if(operatorName != "")
    {
        auto findValue = m_OperatorList.find(operatorName.toStdString());
        if(findValue == m_OperatorList.end())
        {
            return;
        }

        m_OperatorList.erase(findValue);
        m_adapter->DeleteOperator(operatorName.toStdString());
        emit endResetModel();
    }
}

QHash<int, QByteArray> OperatorRepositoryDataModel::roleNames() const
{
    return m_OperatorRoleNames;
}


