#include "Headers/Framework/SqliteCalibratorQueryBuilder.h"

#include "Headers/Adapters/IDatabaseCommand.h"
#include "Headers/Framework/IDatabaseCommandFactory.h"

SqliteCalibratorQueryBuilder::SqliteCalibratorQueryBuilder(
        std::shared_ptr<IDatabaseCommandFactory> commandFactory):
    m_CommandFactory(commandFactory)
{
}

std::shared_ptr<IDatabaseCommand> SqliteCalibratorQueryBuilder::GetCreateTableIfNotExistsQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "CREATE TABLE IF NOT EXISTS Table_CalibratorData "
        "(CalibratorName TEXT PRIMARY KEY NOT NULL, "
        "Value REAL)");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCalibratorQueryBuilder::GetSelectAllQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("SELECT CalibratorName, Value FROM Table_CalibratorData");
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCalibratorQueryBuilder::GetInsertQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "INSERT INTO "
        "Table_CalibratorData (CalibratorName, Value) "
        "VALUES(:CalibratorName, :Value)");
    dbCommand->AddParameter("CalibratorName", ":CalibratorName", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("Value", ":Value", IDatabaseCommand::ParameterType::Real);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCalibratorQueryBuilder::GetRowCountQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString(
        "SELECT COUNT(Table_CalibratorData.CalibratorName) "
        "FROM Table_CalibratorData "
        "WHERE :CalibratorName = CalibratorName");
    dbCommand->AddParameter("CalibratorName", ":CalibratorName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCalibratorQueryBuilder::GetUpdateQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("UPDATE Table_CalibratorData SET "
        "Value = :Value "
        "WHERE CalibratorName = :CalibratorName ");
    dbCommand->AddParameter("CalibratorName", ":CalibratorName", IDatabaseCommand::ParameterType::String);
    dbCommand->AddParameter("Value", ":Value", IDatabaseCommand::ParameterType::Real);
    return dbCommand;
}

std::shared_ptr<IDatabaseCommand> SqliteCalibratorQueryBuilder::GetDeleteQuery() const
{
    auto dbCommand = m_CommandFactory->CreateCommand();
    dbCommand->SetCommandString("DELETE FROM Table_CalibratorData WHERE CalibratorName = :CalibratorName");
    dbCommand->AddParameter("CalibratorName", ":CalibratorName", IDatabaseCommand::ParameterType::String);
    return dbCommand;
}

